// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class RechargeActivity$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.RechargeActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131362066, "field 'mHeadframeIb'");
    target.mHeadframeIb = finder.castView(view, 2131362066, "field 'mHeadframeIb'");
    view = finder.findRequiredView(source, 2131362067, "field 'mHeadframeTitle'");
    target.mHeadframeTitle = finder.castView(view, 2131362067, "field 'mHeadframeTitle'");
    view = finder.findRequiredView(source, 2131361948, "field 'mRechargeBtnTenYuan'");
    target.mRechargeBtnTenYuan = finder.castView(view, 2131361948, "field 'mRechargeBtnTenYuan'");
    view = finder.findRequiredView(source, 2131361949, "field 'mRechargeBtnTwotyYuan'");
    target.mRechargeBtnTwotyYuan = finder.castView(view, 2131361949, "field 'mRechargeBtnTwotyYuan'");
    view = finder.findRequiredView(source, 2131361950, "field 'mRechargeBtnFiftyYuan'");
    target.mRechargeBtnFiftyYuan = finder.castView(view, 2131361950, "field 'mRechargeBtnFiftyYuan'");
    view = finder.findRequiredView(source, 2131361951, "field 'mRechargeBtnOnehundredYuan'");
    target.mRechargeBtnOnehundredYuan = finder.castView(view, 2131361951, "field 'mRechargeBtnOnehundredYuan'");
    view = finder.findRequiredView(source, 2131361952, "field 'mRechargeBtnFivehundredYuan'");
    target.mRechargeBtnFivehundredYuan = finder.castView(view, 2131361952, "field 'mRechargeBtnFivehundredYuan'");
    view = finder.findRequiredView(source, 2131361953, "field 'mRechargeBtnOtherYuan'");
    target.mRechargeBtnOtherYuan = finder.castView(view, 2131361953, "field 'mRechargeBtnOtherYuan'");
    view = finder.findRequiredView(source, 2131361955, "field 'mRechargeBtnRecharge'");
    target.mRechargeBtnRecharge = finder.castView(view, 2131361955, "field 'mRechargeBtnRecharge'");
    view = finder.findRequiredView(source, 2131361954, "field 'etOtherMoney'");
    target.etOtherMoney = finder.castView(view, 2131361954, "field 'etOtherMoney'");
  }

  @Override public void reset(T target) {
    target.mHeadframeIb = null;
    target.mHeadframeTitle = null;
    target.mRechargeBtnTenYuan = null;
    target.mRechargeBtnTwotyYuan = null;
    target.mRechargeBtnFiftyYuan = null;
    target.mRechargeBtnOnehundredYuan = null;
    target.mRechargeBtnFivehundredYuan = null;
    target.mRechargeBtnOtherYuan = null;
    target.mRechargeBtnRecharge = null;
    target.etOtherMoney = null;
  }
}
