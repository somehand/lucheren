// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class PaySuccessActivity$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.PaySuccessActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131362066, "field 'mHeadframeIb'");
    target.mHeadframeIb = finder.castView(view, 2131362066, "field 'mHeadframeIb'");
    view = finder.findRequiredView(source, 2131362067, "field 'mHeadframeTitle'");
    target.mHeadframeTitle = finder.castView(view, 2131362067, "field 'mHeadframeTitle'");
    view = finder.findRequiredView(source, 2131362068, "field 'mHeadframeBtn'");
    target.mHeadframeBtn = finder.castView(view, 2131362068, "field 'mHeadframeBtn'");
    view = finder.findRequiredView(source, 2131362069, "field 'mHeadframeLayout'");
    target.mHeadframeLayout = finder.castView(view, 2131362069, "field 'mHeadframeLayout'");
    view = finder.findRequiredView(source, 2131362071, "field 'mHeadframeGrayLine'");
    target.mHeadframeGrayLine = finder.castView(view, 2131362071, "field 'mHeadframeGrayLine'");
    view = finder.findRequiredView(source, 2131361940, "field 'mPayOkTvorderNumber'");
    target.mPayOkTvorderNumber = finder.castView(view, 2131361940, "field 'mPayOkTvorderNumber'");
    view = finder.findRequiredView(source, 2131361941, "field 'mPayOkTvpayWay'");
    target.mPayOkTvpayWay = finder.castView(view, 2131361941, "field 'mPayOkTvpayWay'");
    view = finder.findRequiredView(source, 2131361942, "field 'mPayOkTvexamRoom'");
    target.mPayOkTvexamRoom = finder.castView(view, 2131361942, "field 'mPayOkTvexamRoom'");
    view = finder.findRequiredView(source, 2131361943, "field 'mPayOkTvorderCarType'");
    target.mPayOkTvorderCarType = finder.castView(view, 2131361943, "field 'mPayOkTvorderCarType'");
    view = finder.findRequiredView(source, 2131361944, "field 'mPayOkTvorderYeardate'");
    target.mPayOkTvorderYeardate = finder.castView(view, 2131361944, "field 'mPayOkTvorderYeardate'");
    view = finder.findRequiredView(source, 2131361945, "field 'mPayOkTvorderTime'");
    target.mPayOkTvorderTime = finder.castView(view, 2131361945, "field 'mPayOkTvorderTime'");
    view = finder.findRequiredView(source, 2131361946, "field 'mPayOkTvorderTelephoneNumber'");
    target.mPayOkTvorderTelephoneNumber = finder.castView(view, 2131361946, "field 'mPayOkTvorderTelephoneNumber'");
    view = finder.findRequiredView(source, 2131361947, "field 'mPayOkTvexamRoomAddress'");
    target.mPayOkTvexamRoomAddress = finder.castView(view, 2131361947, "field 'mPayOkTvexamRoomAddress'");
  }

  @Override public void reset(T target) {
    target.mHeadframeIb = null;
    target.mHeadframeTitle = null;
    target.mHeadframeBtn = null;
    target.mHeadframeLayout = null;
    target.mHeadframeGrayLine = null;
    target.mPayOkTvorderNumber = null;
    target.mPayOkTvpayWay = null;
    target.mPayOkTvexamRoom = null;
    target.mPayOkTvorderCarType = null;
    target.mPayOkTvorderYeardate = null;
    target.mPayOkTvorderTime = null;
    target.mPayOkTvorderTelephoneNumber = null;
    target.mPayOkTvexamRoomAddress = null;
  }
}
