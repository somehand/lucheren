// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class ExamDetailsActivity$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.ExamDetailsActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131362281, "field 'mExamActivityCurrentNum'");
    target.mExamActivityCurrentNum = finder.castView(view, 2131362281, "field 'mExamActivityCurrentNum'");
    view = finder.findRequiredView(source, 2131362282, "field 'mExamActivityTotalNum'");
    target.mExamActivityTotalNum = finder.castView(view, 2131362282, "field 'mExamActivityTotalNum'");
    view = finder.findRequiredView(source, 2131362284, "field 'mExamActivityUndo'");
    target.mExamActivityUndo = finder.castView(view, 2131362284, "field 'mExamActivityUndo'");
    view = finder.findRequiredView(source, 2131362285, "field 'mExamActivityCommit'");
    target.mExamActivityCommit = finder.castView(view, 2131362285, "field 'mExamActivityCommit'");
    view = finder.findRequiredView(source, 2131362286, "field 'mExamActivityTime'");
    target.mExamActivityTime = finder.castView(view, 2131362286, "field 'mExamActivityTime'");
    view = finder.findRequiredView(source, 2131362287, "field 'mExamActivityPrevious'");
    target.mExamActivityPrevious = finder.castView(view, 2131362287, "field 'mExamActivityPrevious'");
    view = finder.findRequiredView(source, 2131362288, "field 'mExamActivityNext'");
    target.mExamActivityNext = finder.castView(view, 2131362288, "field 'mExamActivityNext'");
  }

  @Override public void reset(T target) {
    target.mExamActivityCurrentNum = null;
    target.mExamActivityTotalNum = null;
    target.mExamActivityUndo = null;
    target.mExamActivityCommit = null;
    target.mExamActivityTime = null;
    target.mExamActivityPrevious = null;
    target.mExamActivityNext = null;
  }
}
