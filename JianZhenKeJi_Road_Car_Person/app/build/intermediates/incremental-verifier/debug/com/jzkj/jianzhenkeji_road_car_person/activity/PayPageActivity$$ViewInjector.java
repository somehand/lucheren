// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class PayPageActivity$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.PayPageActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131362217, "field 'mPayName'");
    target.mPayName = finder.castView(view, 2131362217, "field 'mPayName'");
    view = finder.findRequiredView(source, 2131362219, "field 'mPayPrice'");
    target.mPayPrice = finder.castView(view, 2131362219, "field 'mPayPrice'");
    view = finder.findRequiredView(source, 2131362221, "field 'mPayBox1'");
    target.mPayBox1 = finder.castView(view, 2131362221, "field 'mPayBox1'");
    view = finder.findRequiredView(source, 2131362220, "field 'mPayLayoutAli'");
    target.mPayLayoutAli = finder.castView(view, 2131362220, "field 'mPayLayoutAli'");
    view = finder.findRequiredView(source, 2131362223, "field 'mPayBox2'");
    target.mPayBox2 = finder.castView(view, 2131362223, "field 'mPayBox2'");
    view = finder.findRequiredView(source, 2131362222, "field 'mPayLayoutCommon'");
    target.mPayLayoutCommon = finder.castView(view, 2131362222, "field 'mPayLayoutCommon'");
    view = finder.findRequiredView(source, 2131362225, "field 'mPayBox3'");
    target.mPayBox3 = finder.castView(view, 2131362225, "field 'mPayBox3'");
    view = finder.findRequiredView(source, 2131362224, "field 'mPayLayoutWeichat'");
    target.mPayLayoutWeichat = finder.castView(view, 2131362224, "field 'mPayLayoutWeichat'");
    view = finder.findRequiredView(source, 2131362226, "field 'mPayBtn'");
    target.mPayBtn = finder.castView(view, 2131362226, "field 'mPayBtn'");
  }

  @Override public void reset(T target) {
    target.mPayName = null;
    target.mPayPrice = null;
    target.mPayBox1 = null;
    target.mPayLayoutAli = null;
    target.mPayBox2 = null;
    target.mPayLayoutCommon = null;
    target.mPayBox3 = null;
    target.mPayLayoutWeichat = null;
    target.mPayBtn = null;
  }
}
