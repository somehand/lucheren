// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class OrderRoomInfoActivity$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.OrderRoomInfoActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131362066, "field 'mHeadframeIb'");
    target.mHeadframeIb = finder.castView(view, 2131362066, "field 'mHeadframeIb'");
    view = finder.findRequiredView(source, 2131362067, "field 'mHeadframeTitle'");
    target.mHeadframeTitle = finder.castView(view, 2131362067, "field 'mHeadframeTitle'");
    view = finder.findRequiredView(source, 2131361923, "field 'mOrderRoomInfoName'");
    target.mOrderRoomInfoName = finder.castView(view, 2131361923, "field 'mOrderRoomInfoName'");
    view = finder.findRequiredView(source, 2131361925, "field 'mOrderRoomInfoTime'");
    target.mOrderRoomInfoTime = finder.castView(view, 2131361925, "field 'mOrderRoomInfoTime'");
    view = finder.findRequiredView(source, 2131361927, "field 'mOrderRoomInfoCar'");
    target.mOrderRoomInfoCar = finder.castView(view, 2131361927, "field 'mOrderRoomInfoCar'");
    view = finder.findRequiredView(source, 2131361933, "field 'mOrderRoomInfoSafety'");
    target.mOrderRoomInfoSafety = finder.castView(view, 2131361933, "field 'mOrderRoomInfoSafety'");
    view = finder.findRequiredView(source, 2131361937, "field 'mOrderRoomInfoMoney'");
    target.mOrderRoomInfoMoney = finder.castView(view, 2131361937, "field 'mOrderRoomInfoMoney'");
    view = finder.findRequiredView(source, 2131361938, "field 'mOrderRoomInfoCommit'");
    target.mOrderRoomInfoCommit = finder.castView(view, 2131361938, "field 'mOrderRoomInfoCommit'");
    view = finder.findRequiredView(source, 2131361924, "field 'mOrderRoomInfoBtn1'");
    target.mOrderRoomInfoBtn1 = finder.castView(view, 2131361924, "field 'mOrderRoomInfoBtn1'");
    view = finder.findRequiredView(source, 2131361926, "field 'mOrderRoomInfoBtn2'");
    target.mOrderRoomInfoBtn2 = finder.castView(view, 2131361926, "field 'mOrderRoomInfoBtn2'");
    view = finder.findRequiredView(source, 2131361932, "field 'mOrderRoomInfoBtn3'");
    target.mOrderRoomInfoBtn3 = finder.castView(view, 2131361932, "field 'mOrderRoomInfoBtn3'");
    view = finder.findRequiredView(source, 2131361928, "field 'mOrderRoomInfoRadio1'");
    target.mOrderRoomInfoRadio1 = finder.castView(view, 2131361928, "field 'mOrderRoomInfoRadio1'");
    view = finder.findRequiredView(source, 2131361929, "field 'mOrderRoomInfoRadio2'");
    target.mOrderRoomInfoRadio2 = finder.castView(view, 2131361929, "field 'mOrderRoomInfoRadio2'");
    view = finder.findRequiredView(source, 2131361930, "field 'mOrderRoomInfoRadio3'");
    target.mOrderRoomInfoRadio3 = finder.castView(view, 2131361930, "field 'mOrderRoomInfoRadio3'");
    view = finder.findRequiredView(source, 2131361931, "field 'mOrderRoomInfoRadio4'");
    target.mOrderRoomInfoRadio4 = finder.castView(view, 2131361931, "field 'mOrderRoomInfoRadio4'");
    view = finder.findRequiredView(source, 2131361936, "field 'mOrderRoomInfoClock'");
    target.mOrderRoomInfoClock = finder.castView(view, 2131361936, "field 'mOrderRoomInfoClock'");
    view = finder.findRequiredView(source, 2131361935, "field 'mOrderRoomInfoBtn4'");
    target.mOrderRoomInfoBtn4 = finder.castView(view, 2131361935, "field 'mOrderRoomInfoBtn4'");
    view = finder.findRequiredView(source, 2131361934, "field 'mOrderRoomInfoDivider'");
    target.mOrderRoomInfoDivider = finder.castView(view, 2131361934, "field 'mOrderRoomInfoDivider'");
    view = finder.findRequiredView(source, 2131361920, "field 'civLogo'");
    target.civLogo = finder.castView(view, 2131361920, "field 'civLogo'");
    view = finder.findRequiredView(source, 2131361921, "field 'tvUserNickName'");
    target.tvUserNickName = finder.castView(view, 2131361921, "field 'tvUserNickName'");
    view = finder.findRequiredView(source, 2131361922, "field 'tvSchoolName'");
    target.tvSchoolName = finder.castView(view, 2131361922, "field 'tvSchoolName'");
  }

  @Override public void reset(T target) {
    target.mHeadframeIb = null;
    target.mHeadframeTitle = null;
    target.mOrderRoomInfoName = null;
    target.mOrderRoomInfoTime = null;
    target.mOrderRoomInfoCar = null;
    target.mOrderRoomInfoSafety = null;
    target.mOrderRoomInfoMoney = null;
    target.mOrderRoomInfoCommit = null;
    target.mOrderRoomInfoBtn1 = null;
    target.mOrderRoomInfoBtn2 = null;
    target.mOrderRoomInfoBtn3 = null;
    target.mOrderRoomInfoRadio1 = null;
    target.mOrderRoomInfoRadio2 = null;
    target.mOrderRoomInfoRadio3 = null;
    target.mOrderRoomInfoRadio4 = null;
    target.mOrderRoomInfoClock = null;
    target.mOrderRoomInfoBtn4 = null;
    target.mOrderRoomInfoDivider = null;
    target.civLogo = null;
    target.tvUserNickName = null;
    target.tvSchoolName = null;
  }
}
