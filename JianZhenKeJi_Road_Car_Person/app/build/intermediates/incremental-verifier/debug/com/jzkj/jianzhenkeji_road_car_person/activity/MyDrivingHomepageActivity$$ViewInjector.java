// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class MyDrivingHomepageActivity$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.MyDrivingHomepageActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131362066, "field 'mHeadframeIb'");
    target.mHeadframeIb = finder.castView(view, 2131362066, "field 'mHeadframeIb'");
    view = finder.findRequiredView(source, 2131362067, "field 'mHeadframeTitle'");
    target.mHeadframeTitle = finder.castView(view, 2131362067, "field 'mHeadframeTitle'");
    view = finder.findRequiredView(source, 2131361887, "field 'mDriveImgLogo'");
    target.mDriveImgLogo = finder.castView(view, 2131361887, "field 'mDriveImgLogo'");
    view = finder.findRequiredView(source, 2131361888, "field 'mDriveTvabout'");
    target.mDriveTvabout = finder.castView(view, 2131361888, "field 'mDriveTvabout'");
    view = finder.findRequiredView(source, 2131361894, "field 'mTvAddress'");
    target.mTvAddress = finder.castView(view, 2131361894, "field 'mTvAddress'");
    view = finder.findRequiredView(source, 2131361890, "field 'mTvTelephone'");
    target.mTvTelephone = finder.castView(view, 2131361890, "field 'mTvTelephone'");
    view = finder.findRequiredView(source, 2131361892, "field 'mTvUrl'");
    target.mTvUrl = finder.castView(view, 2131361892, "field 'mTvUrl'");
  }

  @Override public void reset(T target) {
    target.mHeadframeIb = null;
    target.mHeadframeTitle = null;
    target.mDriveImgLogo = null;
    target.mDriveTvabout = null;
    target.mTvAddress = null;
    target.mTvTelephone = null;
    target.mTvUrl = null;
  }
}
