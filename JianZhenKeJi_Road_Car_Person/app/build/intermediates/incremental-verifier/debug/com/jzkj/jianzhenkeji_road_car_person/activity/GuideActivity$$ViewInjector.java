// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class GuideActivity$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.GuideActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131361861, "field 'mGuidePager'");
    target.mGuidePager = finder.castView(view, 2131361861, "field 'mGuidePager'");
    view = finder.findRequiredView(source, 2131361862, "field 'mGuideIndicator'");
    target.mGuideIndicator = finder.castView(view, 2131361862, "field 'mGuideIndicator'");
    view = finder.findRequiredView(source, 2131361863, "field 'mGuideBtn'");
    target.mGuideBtn = finder.castView(view, 2131361863, "field 'mGuideBtn'");
  }

  @Override public void reset(T target) {
    target.mGuidePager = null;
    target.mGuideIndicator = null;
    target.mGuideBtn = null;
  }
}
