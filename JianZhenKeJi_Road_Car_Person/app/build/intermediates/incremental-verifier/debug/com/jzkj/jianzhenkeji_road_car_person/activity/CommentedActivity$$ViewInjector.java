// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class CommentedActivity$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.CommentedActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131361824, "field 'commentGroup'");
    target.commentGroup = finder.castView(view, 2131361824, "field 'commentGroup'");
    view = finder.findRequiredView(source, 2131361825, "field 'good_but'");
    target.good_but = finder.castView(view, 2131361825, "field 'good_but'");
    view = finder.findRequiredView(source, 2131361827, "field 'normal_but'");
    target.normal_but = finder.castView(view, 2131361827, "field 'normal_but'");
    view = finder.findRequiredView(source, 2131361829, "field 'bad_but'");
    target.bad_but = finder.castView(view, 2131361829, "field 'bad_but'");
    view = finder.findRequiredView(source, 2131361830, "field 'view1'");
    target.view1 = view;
    view = finder.findRequiredView(source, 2131361831, "field 'view2'");
    target.view2 = view;
    view = finder.findRequiredView(source, 2131361832, "field 'view3'");
    target.view3 = view;
    view = finder.findRequiredView(source, 2131361826, "field 'view4'");
    target.view4 = view;
    view = finder.findRequiredView(source, 2131361828, "field 'view5'");
    target.view5 = view;
    view = finder.findRequiredView(source, 2131362067, "field 'tvTitle'");
    target.tvTitle = finder.castView(view, 2131362067, "field 'tvTitle'");
    view = finder.findRequiredView(source, 2131361833, "field 'mServiceRatingbar'");
    target.mServiceRatingbar = finder.castView(view, 2131361833, "field 'mServiceRatingbar'");
    view = finder.findRequiredView(source, 2131361834, "field 'mMajorRatingbar'");
    target.mMajorRatingbar = finder.castView(view, 2131361834, "field 'mMajorRatingbar'");
    view = finder.findRequiredView(source, 2131361823, "field 'etContent'");
    target.etContent = finder.castView(view, 2131361823, "field 'etContent'");
    view = finder.findRequiredView(source, 2131361838, "field 'etgiveScore'");
    target.etgiveScore = finder.castView(view, 2131361838, "field 'etgiveScore'");
    view = finder.findRequiredView(source, 2131361837, "field 'btnsubmit'");
    target.btnsubmit = finder.castView(view, 2131361837, "field 'btnsubmit'");
    view = finder.findRequiredView(source, 2131361822, "field 'civHead'");
    target.civHead = finder.castView(view, 2131361822, "field 'civHead'");
    view = finder.findRequiredView(source, 2131362066, "field 'mHeadframeIb'");
    target.mHeadframeIb = finder.castView(view, 2131362066, "field 'mHeadframeIb'");
    view = finder.findRequiredView(source, 2131361836, "field 'mCheckBox'");
    target.mCheckBox = finder.castView(view, 2131361836, "field 'mCheckBox'");
  }

  @Override public void reset(T target) {
    target.commentGroup = null;
    target.good_but = null;
    target.normal_but = null;
    target.bad_but = null;
    target.view1 = null;
    target.view2 = null;
    target.view3 = null;
    target.view4 = null;
    target.view5 = null;
    target.tvTitle = null;
    target.mServiceRatingbar = null;
    target.mMajorRatingbar = null;
    target.etContent = null;
    target.etgiveScore = null;
    target.btnsubmit = null;
    target.civHead = null;
    target.mHeadframeIb = null;
    target.mCheckBox = null;
  }
}
