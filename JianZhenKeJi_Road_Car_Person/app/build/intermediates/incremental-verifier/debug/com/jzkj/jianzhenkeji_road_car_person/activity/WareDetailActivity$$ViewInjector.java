// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class WareDetailActivity$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.WareDetailActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131362066, "field 'mHeadframeIb'");
    target.mHeadframeIb = finder.castView(view, 2131362066, "field 'mHeadframeIb'");
    view = finder.findRequiredView(source, 2131362067, "field 'mHeadframeTitle'");
    target.mHeadframeTitle = finder.castView(view, 2131362067, "field 'mHeadframeTitle'");
    view = finder.findRequiredView(source, 2131361968, "field 'mWareDetailHeadimg'");
    target.mWareDetailHeadimg = finder.castView(view, 2131361968, "field 'mWareDetailHeadimg'");
    view = finder.findRequiredView(source, 2131361969, "field 'mTextView6'");
    target.mTextView6 = finder.castView(view, 2131361969, "field 'mTextView6'");
    view = finder.findRequiredView(source, 2131361971, "field 'goodsname'");
    target.goodsname = finder.castView(view, 2131361971, "field 'goodsname'");
    view = finder.findRequiredView(source, 2131361970, "field 'mWareDetailBtnexchange'");
    target.mWareDetailBtnexchange = finder.castView(view, 2131361970, "field 'mWareDetailBtnexchange'");
  }

  @Override public void reset(T target) {
    target.mHeadframeIb = null;
    target.mHeadframeTitle = null;
    target.mWareDetailHeadimg = null;
    target.mTextView6 = null;
    target.goodsname = null;
    target.mWareDetailBtnexchange = null;
  }
}
