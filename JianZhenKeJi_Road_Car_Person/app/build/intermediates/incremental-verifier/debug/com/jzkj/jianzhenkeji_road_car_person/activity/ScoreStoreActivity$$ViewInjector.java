// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class ScoreStoreActivity$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.ScoreStoreActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131362066, "field 'mHeadframeIb'");
    target.mHeadframeIb = finder.castView(view, 2131362066, "field 'mHeadframeIb'");
    view = finder.findRequiredView(source, 2131362067, "field 'mHeadframeTitle'");
    target.mHeadframeTitle = finder.castView(view, 2131362067, "field 'mHeadframeTitle'");
    view = finder.findRequiredView(source, 2131361960, "field 'tvCurrentScore'");
    target.tvCurrentScore = finder.castView(view, 2131361960, "field 'tvCurrentScore'");
    view = finder.findRequiredView(source, 2131361957, "field 'mScoreStoreHeadimg'");
    target.mScoreStoreHeadimg = finder.castView(view, 2131361957, "field 'mScoreStoreHeadimg'");
    view = finder.findRequiredView(source, 2131361963, "field 'mScoreStoreGridview'");
    target.mScoreStoreGridview = finder.castView(view, 2131361963, "field 'mScoreStoreGridview'");
    view = finder.findRequiredView(source, 2131361958, "field 'mScoreStoreTvheadName'");
    target.mScoreStoreTvheadName = finder.castView(view, 2131361958, "field 'mScoreStoreTvheadName'");
    view = finder.findRequiredView(source, 2131361959, "field 'mScoreStoreTvcurrentScore'");
    target.mScoreStoreTvcurrentScore = finder.castView(view, 2131361959, "field 'mScoreStoreTvcurrentScore'");
    view = finder.findRequiredView(source, 2131361961, "field 'mScoreStoreTvscoreRecord'");
    target.mScoreStoreTvscoreRecord = finder.castView(view, 2131361961, "field 'mScoreStoreTvscoreRecord'");
    view = finder.findRequiredView(source, 2131361962, "field 'mScoreStoreTvrecordExchange'");
    target.mScoreStoreTvrecordExchange = finder.castView(view, 2131361962, "field 'mScoreStoreTvrecordExchange'");
    view = finder.findRequiredView(source, 2131361956, "field 'mPullToRefreshScrollView'");
    target.mPullToRefreshScrollView = finder.castView(view, 2131361956, "field 'mPullToRefreshScrollView'");
  }

  @Override public void reset(T target) {
    target.mHeadframeIb = null;
    target.mHeadframeTitle = null;
    target.tvCurrentScore = null;
    target.mScoreStoreHeadimg = null;
    target.mScoreStoreGridview = null;
    target.mScoreStoreTvheadName = null;
    target.mScoreStoreTvcurrentScore = null;
    target.mScoreStoreTvscoreRecord = null;
    target.mScoreStoreTvrecordExchange = null;
    target.mPullToRefreshScrollView = null;
  }
}
