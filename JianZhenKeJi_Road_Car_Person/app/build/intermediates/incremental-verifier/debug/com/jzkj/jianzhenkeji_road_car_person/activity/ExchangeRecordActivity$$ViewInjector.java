// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class ExchangeRecordActivity$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.ExchangeRecordActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131361839, "field 'ptrfLv'");
    target.ptrfLv = finder.castView(view, 2131361839, "field 'ptrfLv'");
    view = finder.findRequiredView(source, 2131362066, "field 'mHeadframeIb'");
    target.mHeadframeIb = finder.castView(view, 2131362066, "field 'mHeadframeIb'");
    view = finder.findRequiredView(source, 2131362067, "field 'mHeadframeTitle'");
    target.mHeadframeTitle = finder.castView(view, 2131362067, "field 'mHeadframeTitle'");
  }

  @Override public void reset(T target) {
    target.ptrfLv = null;
    target.mHeadframeIb = null;
    target.mHeadframeTitle = null;
  }
}
