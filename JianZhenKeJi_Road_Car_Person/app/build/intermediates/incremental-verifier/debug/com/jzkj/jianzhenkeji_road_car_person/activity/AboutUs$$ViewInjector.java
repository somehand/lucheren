// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class AboutUs$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.AboutUs> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131362066, "field 'mHeadframeIb'");
    target.mHeadframeIb = finder.castView(view, 2131362066, "field 'mHeadframeIb'");
    view = finder.findRequiredView(source, 2131362067, "field 'mHeadframeTitle'");
    target.mHeadframeTitle = finder.castView(view, 2131362067, "field 'mHeadframeTitle'");
    view = finder.findRequiredView(source, 2131361813, "field 'mAboutPhone'");
    target.mAboutPhone = finder.castView(view, 2131361813, "field 'mAboutPhone'");
    view = finder.findRequiredView(source, 2131361812, "field 'mAboutWeb'");
    target.mAboutWeb = finder.castView(view, 2131361812, "field 'mAboutWeb'");
  }

  @Override public void reset(T target) {
    target.mHeadframeIb = null;
    target.mHeadframeTitle = null;
    target.mAboutPhone = null;
    target.mAboutWeb = null;
  }
}
