// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class ExamResultActivity$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.ExamResultActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131362000, "field 'mExamResultBack'");
    target.mExamResultBack = finder.castView(view, 2131362000, "field 'mExamResultBack'");
    view = finder.findRequiredView(source, 2131362002, "field 'mExamResultScore'");
    target.mExamResultScore = finder.castView(view, 2131362002, "field 'mExamResultScore'");
    view = finder.findRequiredView(source, 2131362004, "field 'mExamResultTag'");
    target.mExamResultTag = finder.castView(view, 2131362004, "field 'mExamResultTag'");
    view = finder.findRequiredView(source, 2131362005, "field 'mExamResultDate'");
    target.mExamResultDate = finder.castView(view, 2131362005, "field 'mExamResultDate'");
    view = finder.findRequiredView(source, 2131362006, "field 'mExamResultTime'");
    target.mExamResultTime = finder.castView(view, 2131362006, "field 'mExamResultTime'");
    view = finder.findRequiredView(source, 2131362007, "field 'mExamResultTotalNum'");
    target.mExamResultTotalNum = finder.castView(view, 2131362007, "field 'mExamResultTotalNum'");
    view = finder.findRequiredView(source, 2131362008, "field 'mExamResultCorrectNum'");
    target.mExamResultCorrectNum = finder.castView(view, 2131362008, "field 'mExamResultCorrectNum'");
    view = finder.findRequiredView(source, 2131362009, "field 'mExamResultWrongNum'");
    target.mExamResultWrongNum = finder.castView(view, 2131362009, "field 'mExamResultWrongNum'");
    view = finder.findRequiredView(source, 2131362010, "field 'mExamResultPercent'");
    target.mExamResultPercent = finder.castView(view, 2131362010, "field 'mExamResultPercent'");
    view = finder.findRequiredView(source, 2131362011, "field 'mExamResultBtn'");
    target.mExamResultBtn = finder.castView(view, 2131362011, "field 'mExamResultBtn'");
  }

  @Override public void reset(T target) {
    target.mExamResultBack = null;
    target.mExamResultScore = null;
    target.mExamResultTag = null;
    target.mExamResultDate = null;
    target.mExamResultTime = null;
    target.mExamResultTotalNum = null;
    target.mExamResultCorrectNum = null;
    target.mExamResultWrongNum = null;
    target.mExamResultPercent = null;
    target.mExamResultBtn = null;
  }
}
