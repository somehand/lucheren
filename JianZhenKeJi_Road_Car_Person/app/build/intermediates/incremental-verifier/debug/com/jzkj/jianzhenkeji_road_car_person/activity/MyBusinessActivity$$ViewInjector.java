// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class MyBusinessActivity$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.MyBusinessActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131362066, "field 'mHeadframeIb'");
    target.mHeadframeIb = finder.castView(view, 2131362066, "field 'mHeadframeIb'");
    view = finder.findRequiredView(source, 2131362067, "field 'mHeadframeTitle'");
    target.mHeadframeTitle = finder.castView(view, 2131362067, "field 'mHeadframeTitle'");
    view = finder.findRequiredView(source, 2131361868, "field 'mMyBusinessHeadimg'");
    target.mMyBusinessHeadimg = finder.castView(view, 2131361868, "field 'mMyBusinessHeadimg'");
    view = finder.findRequiredView(source, 2131361869, "field 'mMyBusinessName'");
    target.mMyBusinessName = finder.castView(view, 2131361869, "field 'mMyBusinessName'");
    view = finder.findRequiredView(source, 2131361870, "field 'mMyBusinessDriveschool'");
    target.mMyBusinessDriveschool = finder.castView(view, 2131361870, "field 'mMyBusinessDriveschool'");
    view = finder.findRequiredView(source, 2131361871, "field 'mMyBusinessRequestcode'");
    target.mMyBusinessRequestcode = finder.castView(view, 2131361871, "field 'mMyBusinessRequestcode'");
    view = finder.findRequiredView(source, 2131361872, "field 'mMyBusinessChoosetemplate'");
    target.mMyBusinessChoosetemplate = finder.castView(view, 2131361872, "field 'mMyBusinessChoosetemplate'");
    view = finder.findRequiredView(source, 2131361873, "field 'mMyBusinessEtexplain'");
    target.mMyBusinessEtexplain = finder.castView(view, 2131361873, "field 'mMyBusinessEtexplain'");
    view = finder.findRequiredView(source, 2131361874, "field 'mMyBusinessBtnshare'");
    target.mMyBusinessBtnshare = finder.castView(view, 2131361874, "field 'mMyBusinessBtnshare'");
  }

  @Override public void reset(T target) {
    target.mHeadframeIb = null;
    target.mHeadframeTitle = null;
    target.mMyBusinessHeadimg = null;
    target.mMyBusinessName = null;
    target.mMyBusinessDriveschool = null;
    target.mMyBusinessRequestcode = null;
    target.mMyBusinessChoosetemplate = null;
    target.mMyBusinessEtexplain = null;
    target.mMyBusinessBtnshare = null;
  }
}
