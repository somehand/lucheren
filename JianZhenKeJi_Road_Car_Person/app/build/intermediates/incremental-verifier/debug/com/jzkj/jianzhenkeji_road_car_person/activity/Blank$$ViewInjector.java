// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class Blank$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.Blank> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131362066, "field 'mback'");
    target.mback = finder.castView(view, 2131362066, "field 'mback'");
    view = finder.findRequiredView(source, 2131362067, "field 'mHeadframeTitle'");
    target.mHeadframeTitle = finder.castView(view, 2131362067, "field 'mHeadframeTitle'");
    view = finder.findRequiredView(source, 2131361973, "field 'mBlankWeb'");
    target.mBlankWeb = finder.castView(view, 2131361973, "field 'mBlankWeb'");
    view = finder.findRequiredView(source, 2131362069, "field 'mHeadframeLayout'");
    target.mHeadframeLayout = finder.castView(view, 2131362069, "field 'mHeadframeLayout'");
  }

  @Override public void reset(T target) {
    target.mback = null;
    target.mHeadframeTitle = null;
    target.mBlankWeb = null;
    target.mHeadframeLayout = null;
  }
}
