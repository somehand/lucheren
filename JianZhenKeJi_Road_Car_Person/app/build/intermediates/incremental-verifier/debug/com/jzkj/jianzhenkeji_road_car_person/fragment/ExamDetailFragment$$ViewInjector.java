// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.fragment;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class ExamDetailFragment$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.fragment.ExamDetailFragment> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131362263, "field 'mExamQuestion'");
    target.mExamQuestion = finder.castView(view, 2131362263, "field 'mExamQuestion'");
    view = finder.findRequiredView(source, 2131362264, "field 'mExamImg'");
    target.mExamImg = finder.castView(view, 2131362264, "field 'mExamImg'");
    view = finder.findRequiredView(source, 2131362266, "field 'mExamBox1'");
    target.mExamBox1 = finder.castView(view, 2131362266, "field 'mExamBox1'");
    view = finder.findRequiredView(source, 2131362267, "field 'mExamTxt1'");
    target.mExamTxt1 = finder.castView(view, 2131362267, "field 'mExamTxt1'");
    view = finder.findRequiredView(source, 2131362269, "field 'mExamBox2'");
    target.mExamBox2 = finder.castView(view, 2131362269, "field 'mExamBox2'");
    view = finder.findRequiredView(source, 2131362270, "field 'mExamTxt2'");
    target.mExamTxt2 = finder.castView(view, 2131362270, "field 'mExamTxt2'");
    view = finder.findRequiredView(source, 2131362272, "field 'mExamBox3'");
    target.mExamBox3 = finder.castView(view, 2131362272, "field 'mExamBox3'");
    view = finder.findRequiredView(source, 2131362273, "field 'mExamTxt3'");
    target.mExamTxt3 = finder.castView(view, 2131362273, "field 'mExamTxt3'");
    view = finder.findRequiredView(source, 2131362276, "field 'mExamBox4'");
    target.mExamBox4 = finder.castView(view, 2131362276, "field 'mExamBox4'");
    view = finder.findRequiredView(source, 2131362277, "field 'mExamTxt4'");
    target.mExamTxt4 = finder.castView(view, 2131362277, "field 'mExamTxt4'");
    view = finder.findRequiredView(source, 2131362274, "field 'mExamDivider3'");
    target.mExamDivider3 = view;
    view = finder.findRequiredView(source, 2131362278, "field 'mExamlDivider4'");
    target.mExamlDivider4 = view;
    view = finder.findRequiredView(source, 2131362265, "field 'mExamDetailLayout1'");
    target.mExamDetailLayout1 = finder.castView(view, 2131362265, "field 'mExamDetailLayout1'");
    view = finder.findRequiredView(source, 2131362268, "field 'mExamDetailLayout2'");
    target.mExamDetailLayout2 = finder.castView(view, 2131362268, "field 'mExamDetailLayout2'");
    view = finder.findRequiredView(source, 2131362271, "field 'mExamDetailLayout3'");
    target.mExamDetailLayout3 = finder.castView(view, 2131362271, "field 'mExamDetailLayout3'");
    view = finder.findRequiredView(source, 2131362279, "field 'mExamDetailBtn'");
    target.mExamDetailBtn = finder.castView(view, 2131362279, "field 'mExamDetailBtn'");
    view = finder.findRequiredView(source, 2131362275, "field 'mExamDetailLayout4'");
    target.mExamDetailLayout4 = finder.castView(view, 2131362275, "field 'mExamDetailLayout4'");
  }

  @Override public void reset(T target) {
    target.mExamQuestion = null;
    target.mExamImg = null;
    target.mExamBox1 = null;
    target.mExamTxt1 = null;
    target.mExamBox2 = null;
    target.mExamTxt2 = null;
    target.mExamBox3 = null;
    target.mExamTxt3 = null;
    target.mExamBox4 = null;
    target.mExamTxt4 = null;
    target.mExamDivider3 = null;
    target.mExamlDivider4 = null;
    target.mExamDetailLayout1 = null;
    target.mExamDetailLayout2 = null;
    target.mExamDetailLayout3 = null;
    target.mExamDetailBtn = null;
    target.mExamDetailLayout4 = null;
  }
}
