// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class MyOrderNewsDetailsActivity$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.MyOrderNewsDetailsActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131362066, "field 'mHeadframeIb'");
    target.mHeadframeIb = finder.castView(view, 2131362066, "field 'mHeadframeIb'");
    view = finder.findRequiredView(source, 2131362067, "field 'mHeadframeTitle'");
    target.mHeadframeTitle = finder.castView(view, 2131362067, "field 'mHeadframeTitle'");
    view = finder.findRequiredView(source, 2131361903, "field 'mOrderDetailCoachName'");
    target.mOrderDetailCoachName = finder.castView(view, 2131361903, "field 'mOrderDetailCoachName'");
    view = finder.findRequiredView(source, 2131361902, "field 'mMyOrderNewsDetails2Caochname'");
    target.mMyOrderNewsDetails2Caochname = finder.castView(view, 2131361902, "field 'mMyOrderNewsDetails2Caochname'");
    view = finder.findRequiredView(source, 2131361904, "field 'mOrderDetailSubject'");
    target.mOrderDetailSubject = finder.castView(view, 2131361904, "field 'mOrderDetailSubject'");
    view = finder.findRequiredView(source, 2131361905, "field 'mOrderDetailOrderTime'");
    target.mOrderDetailOrderTime = finder.castView(view, 2131361905, "field 'mOrderDetailOrderTime'");
    view = finder.findRequiredView(source, 2131361907, "field 'mOrderDetailOrdermoney'");
    target.mOrderDetailOrdermoney = finder.castView(view, 2131361907, "field 'mOrderDetailOrdermoney'");
    view = finder.findRequiredView(source, 2131361909, "field 'mMyOrderNewsDetails2Btnpay'");
    target.mMyOrderNewsDetails2Btnpay = finder.castView(view, 2131361909, "field 'mMyOrderNewsDetails2Btnpay'");
    view = finder.findRequiredView(source, 2131361906, "field 'mOrderDetailExamroom'");
    target.mOrderDetailExamroom = finder.castView(view, 2131361906, "field 'mOrderDetailExamroom'");
    view = finder.findRequiredView(source, 2131361908, "field 'mOrderDetailStudentgv'");
    target.mOrderDetailStudentgv = finder.castView(view, 2131361908, "field 'mOrderDetailStudentgv'");
  }

  @Override public void reset(T target) {
    target.mHeadframeIb = null;
    target.mHeadframeTitle = null;
    target.mOrderDetailCoachName = null;
    target.mMyOrderNewsDetails2Caochname = null;
    target.mOrderDetailSubject = null;
    target.mOrderDetailOrderTime = null;
    target.mOrderDetailOrdermoney = null;
    target.mMyOrderNewsDetails2Btnpay = null;
    target.mOrderDetailExamroom = null;
    target.mOrderDetailStudentgv = null;
  }
}
