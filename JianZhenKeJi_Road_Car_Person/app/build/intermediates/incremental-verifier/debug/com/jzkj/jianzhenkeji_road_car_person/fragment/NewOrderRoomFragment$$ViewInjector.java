// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.fragment;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class NewOrderRoomFragment$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.fragment.NewOrderRoomFragment> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131362181, "field 'longtime'");
    target.longtime = finder.castView(view, 2131362181, "field 'longtime'");
    view = finder.findRequiredView(source, 2131362184, "field 'cartype'");
    target.cartype = finder.castView(view, 2131362184, "field 'cartype'");
    view = finder.findRequiredView(source, 2131362187, "field 'roomtime'");
    target.roomtime = finder.castView(view, 2131362187, "field 'roomtime'");
    view = finder.findRequiredView(source, 2131362190, "field 'roomMoney'");
    target.roomMoney = finder.castView(view, 2131362190, "field 'roomMoney'");
    view = finder.findRequiredView(source, 2131362192, "field 'paymoney'");
    target.paymoney = finder.castView(view, 2131362192, "field 'paymoney'");
    view = finder.findRequiredView(source, 2131362193, "field 'btnpay'");
    target.btnpay = finder.castView(view, 2131362193, "field 'btnpay'");
    view = finder.findRequiredView(source, 2131362179, "field 'scrollView'");
    target.scrollView = finder.castView(view, 2131362179, "field 'scrollView'");
  }

  @Override public void reset(T target) {
    target.longtime = null;
    target.cartype = null;
    target.roomtime = null;
    target.roomMoney = null;
    target.paymoney = null;
    target.btnpay = null;
    target.scrollView = null;
  }
}
