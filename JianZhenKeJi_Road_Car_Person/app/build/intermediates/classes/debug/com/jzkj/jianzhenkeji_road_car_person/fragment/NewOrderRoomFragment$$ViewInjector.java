// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.fragment;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class NewOrderRoomFragment$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.fragment.NewOrderRoomFragment> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131362192, "field 'longtime'");
    target.longtime = finder.castView(view, 2131362192, "field 'longtime'");
    view = finder.findRequiredView(source, 2131362195, "field 'cartype'");
    target.cartype = finder.castView(view, 2131362195, "field 'cartype'");
    view = finder.findRequiredView(source, 2131362198, "field 'roomtime'");
    target.roomtime = finder.castView(view, 2131362198, "field 'roomtime'");
    view = finder.findRequiredView(source, 2131362201, "field 'roomMoney'");
    target.roomMoney = finder.castView(view, 2131362201, "field 'roomMoney'");
    view = finder.findRequiredView(source, 2131362203, "field 'paymoney'");
    target.paymoney = finder.castView(view, 2131362203, "field 'paymoney'");
    view = finder.findRequiredView(source, 2131362204, "field 'btnpay'");
    target.btnpay = finder.castView(view, 2131362204, "field 'btnpay'");
    view = finder.findRequiredView(source, 2131362190, "field 'scrollView'");
    target.scrollView = finder.castView(view, 2131362190, "field 'scrollView'");
  }

  @Override public void reset(T target) {
    target.longtime = null;
    target.cartype = null;
    target.roomtime = null;
    target.roomMoney = null;
    target.paymoney = null;
    target.btnpay = null;
    target.scrollView = null;
  }
}
