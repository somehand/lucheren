// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class ClassActivity$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.ClassActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131362072, "field 'mHeadframeIb'");
    target.mHeadframeIb = finder.castView(view, 2131362072, "field 'mHeadframeIb'");
    view = finder.findRequiredView(source, 2131362073, "field 'mHeadframeTitle'");
    target.mHeadframeTitle = finder.castView(view, 2131362073, "field 'mHeadframeTitle'");
    view = finder.findRequiredView(source, 2131362077, "field 'mHeadframeBtn'");
    target.mHeadframeBtn = finder.castView(view, 2131362077, "field 'mHeadframeBtn'");
    view = finder.findRequiredView(source, 2131361821, "field 'mPullToRefreshListView'");
    target.mPullToRefreshListView = finder.castView(view, 2131361821, "field 'mPullToRefreshListView'");
  }

  @Override public void reset(T target) {
    target.mHeadframeIb = null;
    target.mHeadframeTitle = null;
    target.mHeadframeBtn = null;
    target.mPullToRefreshListView = null;
  }
}
