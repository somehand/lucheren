// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class Blank$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.Blank> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131362072, "field 'mback'");
    target.mback = finder.castView(view, 2131362072, "field 'mback'");
    view = finder.findRequiredView(source, 2131362073, "field 'mHeadframeTitle'");
    target.mHeadframeTitle = finder.castView(view, 2131362073, "field 'mHeadframeTitle'");
    view = finder.findRequiredView(source, 2131361978, "field 'mBlankWeb'");
    target.mBlankWeb = finder.castView(view, 2131361978, "field 'mBlankWeb'");
    view = finder.findRequiredView(source, 2131362078, "field 'mHeadframeLayout'");
    target.mHeadframeLayout = finder.castView(view, 2131362078, "field 'mHeadframeLayout'");
  }

  @Override public void reset(T target) {
    target.mback = null;
    target.mHeadframeTitle = null;
    target.mBlankWeb = null;
    target.mHeadframeLayout = null;
  }
}
