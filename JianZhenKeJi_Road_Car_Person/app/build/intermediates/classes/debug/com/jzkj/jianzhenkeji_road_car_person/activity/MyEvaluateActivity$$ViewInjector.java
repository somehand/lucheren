// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class MyEvaluateActivity$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.MyEvaluateActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131361986, "field 'mSafetyHeader'");
    target.mSafetyHeader = finder.castView(view, 2131361986, "field 'mSafetyHeader'");
    view = finder.findRequiredView(source, 2131361987, "field 'mSafetyName'");
    target.mSafetyName = finder.castView(view, 2131361987, "field 'mSafetyName'");
    view = finder.findRequiredView(source, 2131361988, "field 'mSafetySign'");
    target.mSafetySign = finder.castView(view, 2131361988, "field 'mSafetySign'");
    view = finder.findRequiredView(source, 2131362145, "field 'mSafetyRadio1'");
    target.mSafetyRadio1 = finder.castView(view, 2131362145, "field 'mSafetyRadio1'");
    view = finder.findRequiredView(source, 2131362146, "field 'mSafetyRadio2'");
    target.mSafetyRadio2 = finder.castView(view, 2131362146, "field 'mSafetyRadio2'");
    view = finder.findRequiredView(source, 2131362147, "field 'mSafetyRadio3'");
    target.mSafetyRadio3 = finder.castView(view, 2131362147, "field 'mSafetyRadio3'");
    view = finder.findRequiredView(source, 2131362148, "field 'mChooseSafetyBtn' and method 'comit'");
    target.mChooseSafetyBtn = finder.castView(view, 2131362148, "field 'mChooseSafetyBtn'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.comit();
        }
      });
    view = finder.findRequiredView(source, 2131362072, "method 'back'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.back();
        }
      });
  }

  @Override public void reset(T target) {
    target.mSafetyHeader = null;
    target.mSafetyName = null;
    target.mSafetySign = null;
    target.mSafetyRadio1 = null;
    target.mSafetyRadio2 = null;
    target.mSafetyRadio3 = null;
    target.mChooseSafetyBtn = null;
  }
}
