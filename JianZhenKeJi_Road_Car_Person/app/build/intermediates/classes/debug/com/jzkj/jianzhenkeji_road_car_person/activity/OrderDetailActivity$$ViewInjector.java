// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class OrderDetailActivity$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.OrderDetailActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131362072, "field 'mHeadframeIb'");
    target.mHeadframeIb = finder.castView(view, 2131362072, "field 'mHeadframeIb'");
    view = finder.findRequiredView(source, 2131362073, "field 'mHeadframeTitle'");
    target.mHeadframeTitle = finder.castView(view, 2131362073, "field 'mHeadframeTitle'");
    view = finder.findRequiredView(source, 2131361913, "field 'mOrderDetailCoachname'");
    target.mOrderDetailCoachname = finder.castView(view, 2131361913, "field 'mOrderDetailCoachname'");
    view = finder.findRequiredView(source, 2131361916, "field 'mOrderDetailSubnum'");
    target.mOrderDetailSubnum = finder.castView(view, 2131361916, "field 'mOrderDetailSubnum'");
    view = finder.findRequiredView(source, 2131361917, "field 'mOrderDetailOrdertime'");
    target.mOrderDetailOrdertime = finder.castView(view, 2131361917, "field 'mOrderDetailOrdertime'");
    view = finder.findRequiredView(source, 2131361918, "field 'mOrderDetailOrderstate'");
    target.mOrderDetailOrderstate = finder.castView(view, 2131361918, "field 'mOrderDetailOrderstate'");
    view = finder.findRequiredView(source, 2131361919, "field 'mOrderDetailApplytime'");
    target.mOrderDetailApplytime = finder.castView(view, 2131361919, "field 'mOrderDetailApplytime'");
    view = finder.findRequiredView(source, 2131361920, "field 'mOrderDetailCancelorder' and method 'resetAppointment'");
    target.mOrderDetailCancelorder = finder.castView(view, 2131361920, "field 'mOrderDetailCancelorder'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.resetAppointment();
        }
      });
    view = finder.findRequiredView(source, 2131361921, "field 'mOrderDetailCheckother'");
    target.mOrderDetailCheckother = finder.castView(view, 2131361921, "field 'mOrderDetailCheckother'");
    view = finder.findRequiredView(source, 2131361914, "field 'RealName'");
    target.RealName = finder.castView(view, 2131361914, "field 'RealName'");
    view = finder.findRequiredView(source, 2131361915, "field 'call'");
    target.call = finder.castView(view, 2131361915, "field 'call'");
  }

  @Override public void reset(T target) {
    target.mHeadframeIb = null;
    target.mHeadframeTitle = null;
    target.mOrderDetailCoachname = null;
    target.mOrderDetailSubnum = null;
    target.mOrderDetailOrdertime = null;
    target.mOrderDetailOrderstate = null;
    target.mOrderDetailApplytime = null;
    target.mOrderDetailCancelorder = null;
    target.mOrderDetailCheckother = null;
    target.RealName = null;
    target.call = null;
  }
}
