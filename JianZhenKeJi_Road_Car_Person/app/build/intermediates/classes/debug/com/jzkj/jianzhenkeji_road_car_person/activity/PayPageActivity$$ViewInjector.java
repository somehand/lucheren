// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class PayPageActivity$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.PayPageActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131362228, "field 'mPayName'");
    target.mPayName = finder.castView(view, 2131362228, "field 'mPayName'");
    view = finder.findRequiredView(source, 2131362230, "field 'mPayPrice'");
    target.mPayPrice = finder.castView(view, 2131362230, "field 'mPayPrice'");
    view = finder.findRequiredView(source, 2131362232, "field 'mPayBox1'");
    target.mPayBox1 = finder.castView(view, 2131362232, "field 'mPayBox1'");
    view = finder.findRequiredView(source, 2131362231, "field 'mPayLayoutAli'");
    target.mPayLayoutAli = finder.castView(view, 2131362231, "field 'mPayLayoutAli'");
    view = finder.findRequiredView(source, 2131362234, "field 'mPayBox2'");
    target.mPayBox2 = finder.castView(view, 2131362234, "field 'mPayBox2'");
    view = finder.findRequiredView(source, 2131362233, "field 'mPayLayoutCommon'");
    target.mPayLayoutCommon = finder.castView(view, 2131362233, "field 'mPayLayoutCommon'");
    view = finder.findRequiredView(source, 2131362236, "field 'mPayBox3'");
    target.mPayBox3 = finder.castView(view, 2131362236, "field 'mPayBox3'");
    view = finder.findRequiredView(source, 2131362235, "field 'mPayLayoutWeichat'");
    target.mPayLayoutWeichat = finder.castView(view, 2131362235, "field 'mPayLayoutWeichat'");
    view = finder.findRequiredView(source, 2131362237, "field 'mPayBtn'");
    target.mPayBtn = finder.castView(view, 2131362237, "field 'mPayBtn'");
  }

  @Override public void reset(T target) {
    target.mPayName = null;
    target.mPayPrice = null;
    target.mPayBox1 = null;
    target.mPayLayoutAli = null;
    target.mPayBox2 = null;
    target.mPayLayoutCommon = null;
    target.mPayBox3 = null;
    target.mPayLayoutWeichat = null;
    target.mPayBtn = null;
  }
}
