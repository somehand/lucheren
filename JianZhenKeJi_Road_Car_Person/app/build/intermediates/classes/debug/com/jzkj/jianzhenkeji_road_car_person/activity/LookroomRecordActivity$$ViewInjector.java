// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class LookroomRecordActivity$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.LookroomRecordActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131361863, "field 'mLookroomRecordLv'");
    target.mLookroomRecordLv = finder.castView(view, 2131361863, "field 'mLookroomRecordLv'");
  }

  @Override public void reset(T target) {
    target.mLookroomRecordLv = null;
  }
}
