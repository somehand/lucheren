// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class LookRoomDetailsActivity$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.LookRoomDetailsActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131362072, "field 'mHeadframeIb'");
    target.mHeadframeIb = finder.castView(view, 2131362072, "field 'mHeadframeIb'");
    view = finder.findRequiredView(source, 2131362073, "field 'mHeadframeTitle'");
    target.mHeadframeTitle = finder.castView(view, 2131362073, "field 'mHeadframeTitle'");
    view = finder.findRequiredView(source, 2131361856, "field 'mLookRoomDetailsExamname'");
    target.mLookRoomDetailsExamname = finder.castView(view, 2131361856, "field 'mLookRoomDetailsExamname'");
    view = finder.findRequiredView(source, 2131361857, "field 'mLookRoomDetailsOrdertime'");
    target.mLookRoomDetailsOrdertime = finder.castView(view, 2131361857, "field 'mLookRoomDetailsOrdertime'");
    view = finder.findRequiredView(source, 2131361858, "field 'mLookRoomDetailsCartype'");
    target.mLookRoomDetailsCartype = finder.castView(view, 2131361858, "field 'mLookRoomDetailsCartype'");
    view = finder.findRequiredView(source, 2131361859, "field 'mLookRoomDetailsAutotype'");
    target.mLookRoomDetailsAutotype = finder.castView(view, 2131361859, "field 'mLookRoomDetailsAutotype'");
    view = finder.findRequiredView(source, 2131361860, "field 'mLookRoomDetailsSafetyname'");
    target.mLookRoomDetailsSafetyname = finder.castView(view, 2131361860, "field 'mLookRoomDetailsSafetyname'");
    view = finder.findRequiredView(source, 2131361861, "field 'mLookRoomDetailsPaymoney'");
    target.mLookRoomDetailsPaymoney = finder.castView(view, 2131361861, "field 'mLookRoomDetailsPaymoney'");
    view = finder.findRequiredView(source, 2131361862, "field 'mLookRoomDetailsBtnState'");
    target.mLookRoomDetailsBtnState = finder.castView(view, 2131361862, "field 'mLookRoomDetailsBtnState'");
  }

  @Override public void reset(T target) {
    target.mHeadframeIb = null;
    target.mHeadframeTitle = null;
    target.mLookRoomDetailsExamname = null;
    target.mLookRoomDetailsOrdertime = null;
    target.mLookRoomDetailsCartype = null;
    target.mLookRoomDetailsAutotype = null;
    target.mLookRoomDetailsSafetyname = null;
    target.mLookRoomDetailsPaymoney = null;
    target.mLookRoomDetailsBtnState = null;
  }
}
