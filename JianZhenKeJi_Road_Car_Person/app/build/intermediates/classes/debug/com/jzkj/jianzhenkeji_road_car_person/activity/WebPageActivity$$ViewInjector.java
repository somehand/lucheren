// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class WebPageActivity$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.WebPageActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131361977, "field 'mWebpageWb'");
    target.mWebpageWb = finder.castView(view, 2131361977, "field 'mWebpageWb'");
    view = finder.findRequiredView(source, 2131362072, "field 'mHeadframeIb'");
    target.mHeadframeIb = finder.castView(view, 2131362072, "field 'mHeadframeIb'");
    view = finder.findRequiredView(source, 2131362073, "field 'mHeadframeTitle'");
    target.mHeadframeTitle = finder.castView(view, 2131362073, "field 'mHeadframeTitle'");
    view = finder.findRequiredView(source, 2131362075, "field 'mHeaframeRb1'");
    target.mHeaframeRb1 = finder.castView(view, 2131362075, "field 'mHeaframeRb1'");
    view = finder.findRequiredView(source, 2131362076, "field 'mHeadframeRb2'");
    target.mHeadframeRb2 = finder.castView(view, 2131362076, "field 'mHeadframeRb2'");
    view = finder.findRequiredView(source, 2131362074, "field 'mHeaframeRg'");
    target.mHeaframeRg = finder.castView(view, 2131362074, "field 'mHeaframeRg'");
  }

  @Override public void reset(T target) {
    target.mWebpageWb = null;
    target.mHeadframeIb = null;
    target.mHeadframeTitle = null;
    target.mHeaframeRb1 = null;
    target.mHeadframeRb2 = null;
    target.mHeaframeRg = null;
  }
}
