// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class MyOrderNewsActivity$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.MyOrderNewsActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131362072, "field 'mHeadframeIb'");
    target.mHeadframeIb = finder.castView(view, 2131362072, "field 'mHeadframeIb'");
    view = finder.findRequiredView(source, 2131362073, "field 'mHeadframeTitle'");
    target.mHeadframeTitle = finder.castView(view, 2131362073, "field 'mHeadframeTitle'");
    view = finder.findRequiredView(source, 2131362077, "field 'mHeadframeBtn'");
    target.mHeadframeBtn = finder.castView(view, 2131362077, "field 'mHeadframeBtn'");
    view = finder.findRequiredView(source, 2131362078, "field 'mHeadframeLayout'");
    target.mHeadframeLayout = finder.castView(view, 2131362078, "field 'mHeadframeLayout'");
    view = finder.findRequiredView(source, 2131362080, "field 'mHeadframeGrayLine'");
    target.mHeadframeGrayLine = finder.castView(view, 2131362080, "field 'mHeadframeGrayLine'");
    view = finder.findRequiredView(source, 2131361922, "field 'mCheckOrderLv'");
    target.mCheckOrderLv = finder.castView(view, 2131361922, "field 'mCheckOrderLv'");
  }

  @Override public void reset(T target) {
    target.mHeadframeIb = null;
    target.mHeadframeTitle = null;
    target.mHeadframeBtn = null;
    target.mHeadframeLayout = null;
    target.mHeadframeGrayLine = null;
    target.mCheckOrderLv = null;
  }
}
