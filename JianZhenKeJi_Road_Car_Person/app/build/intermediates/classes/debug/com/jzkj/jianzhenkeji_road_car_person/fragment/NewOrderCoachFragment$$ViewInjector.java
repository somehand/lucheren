// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.fragment;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class NewOrderCoachFragment$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.fragment.NewOrderCoachFragment> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131361792, "field 'gridView'");
    target.gridView = finder.castView(view, 2131361792, "field 'gridView'");
    view = finder.findRequiredView(source, 2131362185, "field 'realName'");
    target.realName = finder.castView(view, 2131362185, "field 'realName'");
    view = finder.findRequiredView(source, 2131362187, "field 'coachtotal'");
    target.coachtotal = finder.castView(view, 2131362187, "field 'coachtotal'");
    view = finder.findRequiredView(source, 2131361852, "field 'head_img'");
    target.head_img = finder.castView(view, 2131361852, "field 'head_img'");
    view = finder.findRequiredView(source, 2131362188, "field 'scrollview'");
    target.scrollview = finder.castView(view, 2131362188, "field 'scrollview'");
    view = finder.findRequiredView(source, 2131362189, "field 'comitBt' and method 'commit'");
    target.comitBt = finder.castView(view, 2131362189, "field 'comitBt'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.commit();
        }
      });
    view = finder.findRequiredView(source, 2131362186, "field 'year'");
    target.year = finder.castView(view, 2131362186, "field 'year'");
  }

  @Override public void reset(T target) {
    target.gridView = null;
    target.realName = null;
    target.coachtotal = null;
    target.head_img = null;
    target.scrollview = null;
    target.comitBt = null;
    target.year = null;
  }
}
