// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class RechargeActivity$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.RechargeActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131362072, "field 'mHeadframeIb'");
    target.mHeadframeIb = finder.castView(view, 2131362072, "field 'mHeadframeIb'");
    view = finder.findRequiredView(source, 2131362073, "field 'mHeadframeTitle'");
    target.mHeadframeTitle = finder.castView(view, 2131362073, "field 'mHeadframeTitle'");
    view = finder.findRequiredView(source, 2131361953, "field 'mRechargeBtnTenYuan'");
    target.mRechargeBtnTenYuan = finder.castView(view, 2131361953, "field 'mRechargeBtnTenYuan'");
    view = finder.findRequiredView(source, 2131361954, "field 'mRechargeBtnTwotyYuan'");
    target.mRechargeBtnTwotyYuan = finder.castView(view, 2131361954, "field 'mRechargeBtnTwotyYuan'");
    view = finder.findRequiredView(source, 2131361955, "field 'mRechargeBtnFiftyYuan'");
    target.mRechargeBtnFiftyYuan = finder.castView(view, 2131361955, "field 'mRechargeBtnFiftyYuan'");
    view = finder.findRequiredView(source, 2131361956, "field 'mRechargeBtnOnehundredYuan'");
    target.mRechargeBtnOnehundredYuan = finder.castView(view, 2131361956, "field 'mRechargeBtnOnehundredYuan'");
    view = finder.findRequiredView(source, 2131361957, "field 'mRechargeBtnFivehundredYuan'");
    target.mRechargeBtnFivehundredYuan = finder.castView(view, 2131361957, "field 'mRechargeBtnFivehundredYuan'");
    view = finder.findRequiredView(source, 2131361958, "field 'mRechargeBtnOtherYuan'");
    target.mRechargeBtnOtherYuan = finder.castView(view, 2131361958, "field 'mRechargeBtnOtherYuan'");
    view = finder.findRequiredView(source, 2131361960, "field 'mRechargeBtnRecharge'");
    target.mRechargeBtnRecharge = finder.castView(view, 2131361960, "field 'mRechargeBtnRecharge'");
    view = finder.findRequiredView(source, 2131361959, "field 'etOtherMoney'");
    target.etOtherMoney = finder.castView(view, 2131361959, "field 'etOtherMoney'");
    view = finder.findRequiredView(source, 2131361951, "field 'title_tx'");
    target.title_tx = finder.castView(view, 2131361951, "field 'title_tx'");
    view = finder.findRequiredView(source, 2131361952, "field 'linearLayout'");
    target.linearLayout = finder.castView(view, 2131361952, "field 'linearLayout'");
  }

  @Override public void reset(T target) {
    target.mHeadframeIb = null;
    target.mHeadframeTitle = null;
    target.mRechargeBtnTenYuan = null;
    target.mRechargeBtnTwotyYuan = null;
    target.mRechargeBtnFiftyYuan = null;
    target.mRechargeBtnOnehundredYuan = null;
    target.mRechargeBtnFivehundredYuan = null;
    target.mRechargeBtnOtherYuan = null;
    target.mRechargeBtnRecharge = null;
    target.etOtherMoney = null;
    target.title_tx = null;
    target.linearLayout = null;
  }
}
