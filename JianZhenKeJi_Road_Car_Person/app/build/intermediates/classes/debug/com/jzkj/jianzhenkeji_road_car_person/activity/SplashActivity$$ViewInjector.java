// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class SplashActivity$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.SplashActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131361969, "field 'mSplashImageview'");
    target.mSplashImageview = finder.castView(view, 2131361969, "field 'mSplashImageview'");
    view = finder.findRequiredView(source, 2131361970, "field 'mTextView'");
    target.mTextView = finder.castView(view, 2131361970, "field 'mTextView'");
  }

  @Override public void reset(T target) {
    target.mSplashImageview = null;
    target.mTextView = null;
  }
}
