// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class MyCoachActivity$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.MyCoachActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131362072, "field 'mHeadframeIb'");
    target.mHeadframeIb = finder.castView(view, 2131362072, "field 'mHeadframeIb'");
    view = finder.findRequiredView(source, 2131362073, "field 'mHeadframeTitle'");
    target.mHeadframeTitle = finder.castView(view, 2131362073, "field 'mHeadframeTitle'");
    view = finder.findRequiredView(source, 2131361879, "field 'mMyCoachHeadimg'");
    target.mMyCoachHeadimg = finder.castView(view, 2131361879, "field 'mMyCoachHeadimg'");
    view = finder.findRequiredView(source, 2131361881, "field 'mMyCoachRatingbar'");
    target.mMyCoachRatingbar = finder.castView(view, 2131361881, "field 'mMyCoachRatingbar'");
    view = finder.findRequiredView(source, 2131361884, "field 'mMyCoachLv'");
    target.mMyCoachLv = finder.castView(view, 2131361884, "field 'mMyCoachLv'");
    view = finder.findRequiredView(source, 2131361880, "field 'mMyCoachCoachname'");
    target.mMyCoachCoachname = finder.castView(view, 2131361880, "field 'mMyCoachCoachname'");
    view = finder.findRequiredView(source, 2131361882, "field 'mMyCoachAcceptTimes'");
    target.mMyCoachAcceptTimes = finder.castView(view, 2131361882, "field 'mMyCoachAcceptTimes'");
    view = finder.findRequiredView(source, 2131361883, "field 'mMyCoachTotleSudents'");
    target.mMyCoachTotleSudents = finder.castView(view, 2131361883, "field 'mMyCoachTotleSudents'");
    view = finder.findRequiredView(source, 2131361878, "field 'mPullToRefreshScrollView'");
    target.mPullToRefreshScrollView = finder.castView(view, 2131361878, "field 'mPullToRefreshScrollView'");
    view = finder.findRequiredView(source, 2131362079, "field 'mHeadframeLayoutMycoachShare'");
    target.mHeadframeLayoutMycoachShare = finder.castView(view, 2131362079, "field 'mHeadframeLayoutMycoachShare'");
  }

  @Override public void reset(T target) {
    target.mHeadframeIb = null;
    target.mHeadframeTitle = null;
    target.mMyCoachHeadimg = null;
    target.mMyCoachRatingbar = null;
    target.mMyCoachLv = null;
    target.mMyCoachCoachname = null;
    target.mMyCoachAcceptTimes = null;
    target.mMyCoachTotleSudents = null;
    target.mPullToRefreshScrollView = null;
    target.mHeadframeLayoutMycoachShare = null;
  }
}
