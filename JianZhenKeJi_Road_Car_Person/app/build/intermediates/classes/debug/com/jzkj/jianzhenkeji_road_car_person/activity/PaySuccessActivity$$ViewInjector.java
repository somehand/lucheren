// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class PaySuccessActivity$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.PaySuccessActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131362072, "field 'mHeadframeIb'");
    target.mHeadframeIb = finder.castView(view, 2131362072, "field 'mHeadframeIb'");
    view = finder.findRequiredView(source, 2131362073, "field 'mHeadframeTitle'");
    target.mHeadframeTitle = finder.castView(view, 2131362073, "field 'mHeadframeTitle'");
    view = finder.findRequiredView(source, 2131362077, "field 'mHeadframeBtn'");
    target.mHeadframeBtn = finder.castView(view, 2131362077, "field 'mHeadframeBtn'");
    view = finder.findRequiredView(source, 2131362078, "field 'mHeadframeLayout'");
    target.mHeadframeLayout = finder.castView(view, 2131362078, "field 'mHeadframeLayout'");
    view = finder.findRequiredView(source, 2131362080, "field 'mHeadframeGrayLine'");
    target.mHeadframeGrayLine = finder.castView(view, 2131362080, "field 'mHeadframeGrayLine'");
    view = finder.findRequiredView(source, 2131361943, "field 'mPayOkTvorderNumber'");
    target.mPayOkTvorderNumber = finder.castView(view, 2131361943, "field 'mPayOkTvorderNumber'");
    view = finder.findRequiredView(source, 2131361944, "field 'mPayOkTvpayWay'");
    target.mPayOkTvpayWay = finder.castView(view, 2131361944, "field 'mPayOkTvpayWay'");
    view = finder.findRequiredView(source, 2131361945, "field 'mPayOkTvexamRoom'");
    target.mPayOkTvexamRoom = finder.castView(view, 2131361945, "field 'mPayOkTvexamRoom'");
    view = finder.findRequiredView(source, 2131361946, "field 'mPayOkTvorderCarType'");
    target.mPayOkTvorderCarType = finder.castView(view, 2131361946, "field 'mPayOkTvorderCarType'");
    view = finder.findRequiredView(source, 2131361947, "field 'mPayOkTvorderYeardate'");
    target.mPayOkTvorderYeardate = finder.castView(view, 2131361947, "field 'mPayOkTvorderYeardate'");
    view = finder.findRequiredView(source, 2131361948, "field 'mPayOkTvorderTime'");
    target.mPayOkTvorderTime = finder.castView(view, 2131361948, "field 'mPayOkTvorderTime'");
    view = finder.findRequiredView(source, 2131361949, "field 'mPayOkTvorderTelephoneNumber'");
    target.mPayOkTvorderTelephoneNumber = finder.castView(view, 2131361949, "field 'mPayOkTvorderTelephoneNumber'");
    view = finder.findRequiredView(source, 2131361950, "field 'mPayOkTvexamRoomAddress'");
    target.mPayOkTvexamRoomAddress = finder.castView(view, 2131361950, "field 'mPayOkTvexamRoomAddress'");
  }

  @Override public void reset(T target) {
    target.mHeadframeIb = null;
    target.mHeadframeTitle = null;
    target.mHeadframeBtn = null;
    target.mHeadframeLayout = null;
    target.mHeadframeGrayLine = null;
    target.mPayOkTvorderNumber = null;
    target.mPayOkTvpayWay = null;
    target.mPayOkTvexamRoom = null;
    target.mPayOkTvorderCarType = null;
    target.mPayOkTvorderYeardate = null;
    target.mPayOkTvorderTime = null;
    target.mPayOkTvorderTelephoneNumber = null;
    target.mPayOkTvexamRoomAddress = null;
  }
}
