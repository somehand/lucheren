// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.fragment;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class Mine$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.fragment.Mine> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131362062, "field 'mMineHeader'");
    target.mMineHeader = finder.castView(view, 2131362062, "field 'mMineHeader'");
    view = finder.findRequiredView(source, 2131362063, "field 'mMineName'");
    target.mMineName = finder.castView(view, 2131362063, "field 'mMineName'");
    view = finder.findRequiredView(source, 2131362065, "field 'mScorerecord'");
    target.mScorerecord = finder.castView(view, 2131362065, "field 'mScorerecord'");
    view = finder.findRequiredView(source, 2131362066, "field 'mRecharge'");
    target.mRecharge = finder.castView(view, 2131362066, "field 'mRecharge'");
    view = finder.findRequiredView(source, 2131362064, "field 'tvCurrentScore'");
    target.tvCurrentScore = finder.castView(view, 2131362064, "field 'tvCurrentScore'");
    view = finder.findRequiredView(source, 2131362067, "field 'mMyData'");
    target.mMyData = finder.castView(view, 2131362067, "field 'mMyData'");
    view = finder.findRequiredView(source, 2131362069, "field 'btnExit'");
    target.btnExit = finder.castView(view, 2131362069, "field 'btnExit'");
  }

  @Override public void reset(T target) {
    target.mMineHeader = null;
    target.mMineName = null;
    target.mScorerecord = null;
    target.mRecharge = null;
    target.tvCurrentScore = null;
    target.mMyData = null;
    target.btnExit = null;
  }
}
