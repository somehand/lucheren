// Generated code from Butter Knife. Do not modify!
package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class MyBusinessActivity$$ViewInjector<T extends com.jzkj.jianzhenkeji_road_car_person.activity.MyBusinessActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131362072, "field 'mHeadframeIb'");
    target.mHeadframeIb = finder.castView(view, 2131362072, "field 'mHeadframeIb'");
    view = finder.findRequiredView(source, 2131362073, "field 'mHeadframeTitle'");
    target.mHeadframeTitle = finder.castView(view, 2131362073, "field 'mHeadframeTitle'");
    view = finder.findRequiredView(source, 2131361871, "field 'mMyBusinessHeadimg'");
    target.mMyBusinessHeadimg = finder.castView(view, 2131361871, "field 'mMyBusinessHeadimg'");
    view = finder.findRequiredView(source, 2131361872, "field 'mMyBusinessName'");
    target.mMyBusinessName = finder.castView(view, 2131361872, "field 'mMyBusinessName'");
    view = finder.findRequiredView(source, 2131361873, "field 'mMyBusinessDriveschool'");
    target.mMyBusinessDriveschool = finder.castView(view, 2131361873, "field 'mMyBusinessDriveschool'");
    view = finder.findRequiredView(source, 2131361874, "field 'mMyBusinessRequestcode'");
    target.mMyBusinessRequestcode = finder.castView(view, 2131361874, "field 'mMyBusinessRequestcode'");
    view = finder.findRequiredView(source, 2131361875, "field 'mMyBusinessChoosetemplate'");
    target.mMyBusinessChoosetemplate = finder.castView(view, 2131361875, "field 'mMyBusinessChoosetemplate'");
    view = finder.findRequiredView(source, 2131361876, "field 'mMyBusinessEtexplain'");
    target.mMyBusinessEtexplain = finder.castView(view, 2131361876, "field 'mMyBusinessEtexplain'");
    view = finder.findRequiredView(source, 2131361877, "field 'mMyBusinessBtnshare'");
    target.mMyBusinessBtnshare = finder.castView(view, 2131361877, "field 'mMyBusinessBtnshare'");
  }

  @Override public void reset(T target) {
    target.mHeadframeIb = null;
    target.mHeadframeTitle = null;
    target.mMyBusinessHeadimg = null;
    target.mMyBusinessName = null;
    target.mMyBusinessDriveschool = null;
    target.mMyBusinessRequestcode = null;
    target.mMyBusinessChoosetemplate = null;
    target.mMyBusinessEtexplain = null;
    target.mMyBusinessBtnshare = null;
  }
}
