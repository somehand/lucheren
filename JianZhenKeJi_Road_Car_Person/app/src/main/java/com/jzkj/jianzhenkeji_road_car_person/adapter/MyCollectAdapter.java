package com.jzkj.jianzhenkeji_road_car_person.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.MyApplication;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.activity.Blank;
import com.jzkj.jianzhenkeji_road_car_person.activity.MyCollectActivity;
import com.jzkj.jianzhenkeji_road_car_person.bean.MyCollectBean;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Administrator on 2016/3/2 0002.
 */
public class MyCollectAdapter extends QuickAdapter<MyCollectBean> {

	private MyCollectActivity context;

	public MyCollectAdapter(MyCollectActivity context, int layoutResId) {
		super(context, layoutResId);
		this.context = context;
	}

	@Override
	protected void convert(BaseAdapterHelper helper, final MyCollectBean item) {
		ImageView header = helper.getView(R.id.mycollect_item_header);
		TextView name = helper.getView(R.id.mycollect_item_name);
		TextView time = helper.getView(R.id.mycollect_item_time);
		TextView content = helper.getView(R.id.mycollect_item_content);
		ImageView img = helper.getView(R.id.mycollect_item_img);

		ImageLoader.getInstance().displayImage(item.getImgurl1(),img , MyApplication.options_other);
		ImageLoader.getInstance().displayImage(item.getHeaderUrl(), header, MyApplication.options_user);
		name.setText(item.getName());
		name.setTag(item.getAppIndexArticleId());
		time.setText(item.getTime());
		content.setText(item.getTitle());
		helper.getView().setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(context, Blank.class);
				intent.putExtra("url",item.getUrl());
				intent.putExtra("articelId", item.getAppIndexArticleId());
//				intent.putExtra("title", mArrayList.get(i).getTitle());
				context.startActivity(intent);
			}
		});
		helper.getView().setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View view) {
				new AlertDialog.Builder(context).setMessage("确定删除?").setPositiveButton("确定", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						context.CollectCancel(item.getAppIndexArticleId());
					}
				}).setNeutralButton("取消", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						dialogInterface.dismiss();
					}
				}).show();
				return true;
			}
		});
	}
}
