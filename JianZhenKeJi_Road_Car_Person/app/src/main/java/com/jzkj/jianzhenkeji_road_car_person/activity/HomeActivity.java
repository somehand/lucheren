package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.UpdateService;
import com.jzkj.jianzhenkeji_road_car_person.bean.ADBean;
import com.jzkj.jianzhenkeji_road_car_person.bean.SchoolName;
import com.jzkj.jianzhenkeji_road_car_person.bean.UpdateVersionInfoBean;
import com.jzkj.jianzhenkeji_road_car_person.fragment.HomePage;
import com.jzkj.jianzhenkeji_road_car_person.fragment.Mine;
import com.jzkj.jianzhenkeji_road_car_person.fragment.NewOrderCoachFragment;
import com.jzkj.jianzhenkeji_road_car_person.fragment.NewOrderRoomFragment;
import com.jzkj.jianzhenkeji_road_car_person.network.HttpUtils;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;
import com.umeng.socialize.Config;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.utils.Log;

import org.apache.http.Header;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends FragmentActivity implements OnClickListener {

	private ImageButton ibBack;
	private TextView tvTitle, tvLine;
	private ViewGroup mDots;
//	private RadioGroup rg;
//	private RadioButton rbExam,rbExamRoom,rbCoach,rbMine;

	private HomePage homeFragment = new HomePage();
	private Mine mine = new Mine();
	private NewOrderRoomFragment room = new NewOrderRoomFragment();
	private NewOrderCoachFragment coach = new NewOrderCoachFragment();

	private Fragment[] fragments = {
			homeFragment, coach, room, mine
	};
	private ImageButton tab1, tab3, tab2, tab4;
	private int[] images = {
			R.drawable.exam_drive_rb_img,
			R.drawable.exam_drive_rbgray_img,
			R.drawable.order_coach_rb_img,
			R.drawable.order_coach_rbgray_img,
			R.drawable.exam_room_rb_img,
			R.drawable.exam_room_rbgray_img,
			R.drawable.mine_rb_img,
			R.drawable.mine_rbgray_img};
	private ImageButton[] tabs = new ImageButton[4];
	private FragmentTransaction fragmentTransaction;
	private Button selectBtn;
	private ArrayList<String> examRoomNames;
	private ArrayList<Integer> examRoomIds;
	private ArrayList<String> curriculums;
	PopupWindow popupWindow;

	private final String TAG = this.getClass().getName();
	private final int UPDATA_NONEED = 0;
	private final int UPDATA_CLIENT = 1;
	private final int GET_UNDATAINFO_ERROR = 2;
	private final int SDCARD_NOMOUNTED = 3;
	private final int DOWN_ERROR = 4;
	private UpdateVersionInfoBean info;
	private String localVersion;
	private String subjectNum = "2";
	private int item = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		ButterKnife.inject(this);
		init();
		initData();
//		tvTitle.setText("路车人");
		Intent intent = getIntent();
		if (intent.hasExtra("fromPayPage")) {
			showDialog();
		}

		if (intent.hasExtra("item")) {
			item = intent.getIntExtra("item", 0);
			getLogin();
		}
		switchTab(item);
		EventBus.getDefault().register(this);
		getVersionInfo();
		if (HttpUtils.checkNetworkAvailable(HomeActivity.this) == false) {
			AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
			builder.setMessage("当前无可用网络，请检查网络...");
			AlertDialog dialog = builder.create();
			dialog.show();
		}
		getAD();
		getAD123();
	}
	private void getAD123() {
		RequestParams params = new RequestParams();
		MyHttp.getInstance(this).post("http://183.230.133.205:18887/WebServiceGetInfo.asmx", params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e("test111111111111111");
				KLog.e(Utils.TAG_DEBUG, response.toString());
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e("test111111111111111++++++");
				KLog.json(Utils.TAG_DEBUG, response.toString());

			}


			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				KLog.e("errorcode　", statusCode + " " + responseString);

			}
		});
	}

	public int getItem() {
		return item;
	}

	private void getAD() {
		RequestParams params = new RequestParams();
		MyHttp.getInstance(this).post(MyHttp.RC_Advertisement, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, response.toString());
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(Utils.TAG_DEBUG, response.toString());
				try {
					JSONArray array = response.getJSONArray("Result");
					for (int i = 0; i < array.length(); i++) {
						JSONObject object = (JSONObject) array.get(i);
						ADBean bean = new ADBean();
						bean.setId(object.getInt("Id"));
						bean.setAdvUrl(object.getString("AdvUrl"));
						bean.setIconImgUrl(object.getString("IconImgUrl"));
						bean.setIconUrl(object.getString("IconUrl"));
						bean.setNumber(object.getInt("Number"));
						Utils.ADMap.put("ad" + bean.getNumber(), bean);
					}
					//KLog.e(Utils.TAG_DEBUG, Utils.ADMap.size() + "");
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}


			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				KLog.e("errorcode　", statusCode + " " + responseString);

			}
		});
	}

	private void initData() {
		examRoomNames = new ArrayList<String>();
		examRoomIds = new ArrayList<Integer>();
		curriculums = new ArrayList<String>();
		curriculums.add("科目二");
		curriculums.add("科目三");
		getLogin();

	}

	public String getSubjectnum() {
		return subjectNum;
	}


	/**
	 * 接管intent
	 *
	 * @param /*
	 *//*
	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		if (intent.hasExtra("fromPayPage")) {
			showDialog();
		} else if (intent.hasExtra("item")) {
			switchTab(intent.getIntExtra("item", 0));
		}
	}
*/
	@OnClick(R.id.headframe_title)
	public void click() {
		if (Utils.SPGetString(this, Utils.userName, "").equals("18623097475")) {
			startActivity(new Intent(this, ChooseSchoolActivity.class));
		}

	}


	/**
	 * 初始化界面
	 */
	private void init() {
		ibBack = (ImageButton) findViewById(R.id.headframe_ib);
		tvTitle = (TextView) findViewById(R.id.headframe_title);
		tvLine = (TextView) findViewById(R.id.headframe_gray_line);
		selectBtn = (Button) findViewById(R.id.headframe_btn);
		mDots = (ViewGroup) findViewById(R.id.headframe_layout);
		Drawable drawable = selectBtn.getCompoundDrawables()[2];
		drawable.setBounds(0, 0, getResources().getDimensionPixelSize(R.dimen.x30), getResources()
				.getDimensionPixelSize(R.dimen.y24));
		selectBtn.setCompoundDrawables(null, null, drawable, null);
		ibBack.setVisibility(View.GONE);

		fragmentTransaction = getSupportFragmentManager().beginTransaction();
		for (int i = 0; i < fragments.length; i++) {
			fragmentTransaction.add(R.id.global_frame_vp, fragments[i]);
			fragmentTransaction.hide(fragments[i]);
		}
		fragmentTransaction.commit();

		tab1 = (ImageButton) findViewById(R.id.main_tab_1);
		tab2 = (ImageButton) findViewById(R.id.main_tab_2);
		tab3 = (ImageButton) findViewById(R.id.main_tab_3);
		tab4 = (ImageButton) findViewById(R.id.main_tab_4);
		tabs[0] = tab1;
		tabs[1] = tab2;
		tabs[2] = tab3;
		tabs[3] = tab4;

		String currentExamRoomName = Utils.SPGetString(this, Utils.currentExamRoomName, "");
//		if( currentExamRoomName.length() != 0){
		selectBtn.setText("科目二");
//		}
		tab1.setOnClickListener(this);
		tab2.setOnClickListener(this);
		tab3.setOnClickListener(this);
		tab4.setOnClickListener(this);

	}


	/**
	 * 设置RadioButton图片大小
	 *
	 * @param btn
	 * @param id
	 */
	private void setDawableSize(RadioButton btn, int id) {
		Drawable drawable = getResources().getDrawable(id);
		drawable.setBounds(0, 0, getResources().getDimensionPixelSize(R.dimen.x77), getResources().getDimensionPixelSize(R.dimen.x77));
		btn.setCompoundDrawables(null, drawable, null, null);
	}

	/**
	 * 切换tab
	 *
	 * @param i
	 */
	private void switchTab(int i) {
		tabs[i].setImageResource(images[2 * i]);
		currentTabID = i;
		if (i == 0) {
			mDots.setVisibility(View.GONE);
//			mDots.setOnClickListener(this);
			selectBtn.setVisibility(View.GONE);
			if (TextUtils.isEmpty(Utils.SPGetString(HomeActivity.this, Utils.schoolName, ""))) {
				tvTitle.setText("首页");
			} else {
				tvTitle.setText(Utils.SPGetString(HomeActivity.this, Utils.schoolName, ""));
			}
//			tvTitle.setText(Utils.SPGetString(HomeActivity.this, Utils.schoolName, "首页"));
		} else if (i == 1) {
			mDots.setVisibility(View.GONE);
			selectBtn.setVisibility(View.VISIBLE);
			selectBtn.setOnClickListener(this);
			item = 1;
			tvTitle.setText("预约练车");
		} else if (i == 2) {
			mDots.setVisibility(View.GONE);
			selectBtn.setVisibility(View.VISIBLE);
			selectBtn.setOnClickListener(this);
			tvTitle.setText("预约合场");
		} else {
			selectBtn.setVisibility(View.GONE);
			mDots.setVisibility(View.GONE);
			tvTitle.setText("个人中心");
		}
		for (int j = 0; j < tabs.length; j++) {
			if (j != i) {
				tabs[j].setImageResource(images[2 * j + 1]);
			}

		}
		switchFragment(i);
	}

	/**
	 * 切换fragment
	 *
	 * @param i
	 */
	private void switchFragment(int i) {
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.show(fragments[i]);
		for (int j = 0; j < fragments.length; j++) {
			if (j != i) {
				transaction.hide(fragments[j]);
			}
		}
		transaction.commit();
	}


	/**
	 * 点击监听
	 *
	 * @param v
	 */
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.main_tab_1:
				switchTab(0);
				break;
			case R.id.main_tab_2:
				switchTab(1);
				coach.getMyCoach_1();
				break;
			case R.id.main_tab_3:
				switchTab(2);
				break;
			case R.id.main_tab_4:
				switchTab(3);
				break;
			case R.id.headframe_btn:
				initPopup();
				if (popupWindow.isShowing()) {
					popupWindow.dismiss();
				} else {
					popupWindow.showAsDropDown(selectBtn);
				}

				break;

			case R.id.headframe_layout:
				initShare();
				break;
		}

	}

	private int currentTabID = 0;

	public int getcurrentTabID() {
		return currentTabID;
	}

	private void initPopup() {
		View view = LayoutInflater.from(this).inflate(R.layout.popup_order_info_layout, null);
		popupWindow = new PopupWindow(view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup
				.LayoutParams.WRAP_CONTENT);
		popupWindow.setFocusable(true);
		popupWindow.setOutsideTouchable(true);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		ListView listView = (ListView) view.findViewById(R.id.popup_order_info_list);
		ArrayAdapter adapter = new ArrayAdapter(this, R.layout.select_driving_school_lv_item, R.id
				.select_driving_school_text);
		listView.setAdapter(adapter);
		adapter.addAll(curriculums);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				selectBtn.setText(curriculums.get(i));
				popupWindow.dismiss();
				if (currentTabID == 2)
					room.getRoomList(Integer.parseInt(convertsubjectNum(selectBtn.getText().toString())), true);
				else
					coach.CoachDate(convertsubjectNum(selectBtn.getText().toString()), "");
//				Utils.SPPutInt(HomeActivity.this, Utils.currentExamRoomId , examRoomIds.get(0));
//				Utils.SPutString(HomeActivity.this, Utils.currentExamRoomName , "西彭考场");
//				KLog.e(Utils.TAG_DEBUG , "当前选择考场的id : " + examRoomIds.get(i));
//				if (i != 0) {
//					Utils.ToastShort(HomeActivity.this,"该考场尚未开放");
//				}
			}
		});
	}

	public String convertsubjectNum(String str) {

		if (str.equals("科目二")) {
			subjectNum = "2";
		} else {
			subjectNum = "3";
		}
		return subjectNum;
	}

	long time;

	/**
	 * 接管返回键
	 */
	@Override
	public void onBackPressed() {
		if (System.currentTimeMillis() - time > 2000) {
			Utils.ToastShort(this, "再按一次退出应用");
			time = System.currentTimeMillis();
		} else {
			System.exit(0);
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		EventBus.getDefault().unregister(this);
	}

	/**
	 * onEventBus recive
	 *
	 * @param schoolName
	 */
	@Subscribe(threadMode = ThreadMode.MAIN)
	public void setSchoolName(SchoolName schoolName) {
		if (item == 0)
			tvTitle.setText(schoolName.getName());
	}


	private void showDialog() {
		AlertDialog dialog = new AlertDialog.Builder(this).create();
		dialog.setMessage("未完成支付的订单可在\"合场记录\"中查看");
		dialog.show();
	}

	/**
	 * 获取考场列表
	 */
//	public void getExamRoomList() {
//		MyHttp.getInstance(this).post(MyHttp.EXAM_ROOM_LIST, new JsonHttpResponseHandler() {
//			@Override
//			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
//				super.onSuccess(statusCode, headers, response);
//				KLog.e(Utils.TAG_DEBUG, "jsonObject :");
//				KLog.json(Utils.TAG_DEBUG, response.toString());
//			}
//
//			@Override
//			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
//				super.onSuccess(statusCode, headers, response);
//				KLog.e(Utils.TAG_DEBUG, "jsonArray :");
//				KLog.json(Utils.TAG_DEBUG, response.toString());
//				try {
//					for (int i = 0; i < response.length(); i++) {
//						JSONObject object = response.getJSONObject(i);
//						/**
//						 * 这里写死了 只显示西彭考场
//						 */
//						if(i == 0)
//						examRoomNames.add(object.getString("ExaminationName"));
//						examRoomIds.add(object.getInt("ExaminationId"));
//					}
//					Utils.SPPutInt(HomeActivity.this, Utils.currentExamRoomId , examRoomIds.get(0));
//					Utils.SPutString(HomeActivity.this, Utils.currentExamRoomName , "西彭考场");
//					initPopup();// 需要先获取popup中list的item 才能实例化 popup
//
//				} catch (JSONException e) {
//					e.printStackTrace();
//				}
//
//
//			}
//
//		});
//	}


	/**
	 * 弹出分享界面
	 */
	public void initShare() {
		final SHARE_MEDIA[] displaylist = new SHARE_MEDIA[]{
				SHARE_MEDIA.WEIXIN, SHARE_MEDIA.WEIXIN_CIRCLE, SHARE_MEDIA.QQ, SHARE_MEDIA.QZONE, SHARE_MEDIA.SINA
		};
		/*注意新浪分享的title是不显示的，URL链接只能加在分享文字后显示，并且需要确保withText()不为空
		当同时传递URL参数和图片时，注意确保图片不能超过32K，否则无法分享，不传递URL参数时图片不受32K限制*/
		Log.LOG = true;
		Config.OpenEditor = false;
		Config.IsToastTip = true;
		ProgressDialog dialog = new ProgressDialog(this);
		dialog.setMessage("正在分享中");
		Config.dialog = dialog;
		new ShareAction(this).setDisplayList(displaylist)
				.withText("路车人分享")
				.withTitle("路车人")

				.withTargetUrl("http://zgjzkj.net/Share/index.aspx")
				.withMedia(new UMImage(this, R.drawable.ic_launcher))
				.setListenerList(new UMShareListener() {
					@Override
					public void onResult(SHARE_MEDIA share_media) {
						Utils.ToastShort(HomeActivity.this, "分享成功");
					}

					@Override
					public void onError(SHARE_MEDIA share_media, Throwable throwable) {
						Utils.ToastShort(HomeActivity.this, "分享失败");
					}

					@Override
					public void onCancel(SHARE_MEDIA share_media) {
						Utils.ToastShort(HomeActivity.this, "分享已取消");

					}
				})
				.open();
	}


	/**
	 * 获取登录数据
	 */
	private void getLogin() {
		KLog.e("getLogin()");


		RequestParams params = new RequestParams();
		params.put("UserName", Utils.SPGetString(this, Utils.userName, ""));
		params.put("PassWord", Utils.SPGetString(this, Utils.passWord, ""));
		params.put("Type", 1);
		KLog.e("params", params.toString());
		MyHttp.getInstance(HomeActivity.this).post(MyHttp.USER_LOGIN, params, new JsonHttpResponseHandler(
		) {

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());

			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				try {
					JSONObject jsonObject = response.getJSONObject(0);
					KLog.e(Utils.TAG_DEBUG, "jsonArray :");//TeachType  string  //基础训练学员or 已预约考试学员
					KLog.json(Utils.TAG_DEBUG, response.toString());

					//Utils.SPPutBoolean(HomeActivity.this, "isLogin", true);
					//Toast.makeText(HomeActivity.this, "登录成功(^_^)", Toast.LENGTH_LONG).show();
					Utils.SPutString(HomeActivity.this, Utils.userId, jsonObject.getString("UserId"));
					Utils.SPutString(HomeActivity.this, Utils.userName, jsonObject.getString("UserName"));
//					Utils.SPutString(HomeActivity.this, Utils.integral, jsonObject.getString("Integral"));

					Utils.SPutString(HomeActivity.this, Utils.realName, jsonObject.getString("RealName"));
					Utils.SPutString(HomeActivity.this, Utils.nickName, jsonObject.getString("NickName"));
					Utils.SPutString(HomeActivity.this, Utils.idCarNumber, jsonObject.getString("IDCardNumber"));
					Utils.SPutString(HomeActivity.this, Utils.userSex, jsonObject.getString("UserSex"));
					Utils.SPutString(HomeActivity.this, Utils.userAddress, jsonObject.getString("UserAddress"));
					Utils.SPutString(HomeActivity.this, Utils.userLogo, jsonObject.getString("UserLogo"));
					Utils.SPutString(HomeActivity.this, Utils.schoolName, jsonObject.getString("SchoolName"));

					Utils.SPPutInt(HomeActivity.this, Utils.IsVip, jsonObject.getInt("IsVip"));
					Utils.SPPutBoolean(HomeActivity.this, Utils.isLogin, true);
					Utils.SPutString(HomeActivity.this, Utils.schoolId, jsonObject.getString("DrivingSchoolId"));
					Utils.SPutString(HomeActivity.this, Utils.coachId, jsonObject.getString("CoachId"));
					Utils.SPutString(HomeActivity.this, Utils.CoachName, jsonObject.getString("CoachName"));
					Utils.SPutString(HomeActivity.this, Utils.SchoolUrl, jsonObject.getString("SchoolProfiles"));
					Utils.SPutString(HomeActivity.this, Utils.TeachType, jsonObject.getString("TeachType"));
					if (jsonObject.getInt("DrivingType") == 1) {
						Utils.SPutString(HomeActivity.this, Utils.DrivingType, "C1");
					} else {
						Utils.SPutString(HomeActivity.this, Utils.DrivingType, "C2");
					}
					/*if (item == 0) {
						tvTitle.setText(Utils.SPGetString(HomeActivity.this, Utils.schoolName, ""));
					}*/
				} catch (JSONException e) {
					e.printStackTrace();
				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);

			}
		});
//		}
	}


	/**
	 * 获取版本信息
	 */
	private void getVersionInfo() {
		try {
			localVersion = getVersionName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		RequestParams params = new RequestParams();
		params.put("EdiOrStu", 2); //2 对应学员版
		MyHttp.getInstance(HomeActivity.this).post(MyHttp.GET_UPDATE, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
				info = new UpdateVersionInfoBean();

				try {
					if (response.getInt("Code") == 1) {
						JSONArray array = response.getJSONArray("Result");
						JSONObject object = array.getJSONObject(0);
						String url = object.getString("Url");
						int versionCode = object.getInt("Version");
						info.setUrl(url);
						downloadUrl = url;
//					info.setUrl("http://download.taobaocdn.com/wireless/dingtalk/2.7.9-26218-4/10002068@Rimet-dingtalk-2.7.9.apk");
						int currentVersionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
						info.setDescription("亲，有新版本发布了！");
						if (versionCode <= currentVersionCode) {
							Log.i(TAG, "版本号相同");
							Message msg = new Message();
							msg.what = UPDATA_NONEED;
							handler.sendMessage(msg);
							Utils.SPPutBoolean(HomeActivity.this, Utils.noUpdate, true);
						} else {
							Log.i(TAG, "版本号不相同 ");
							Message msg = new Message();
							msg.what = UPDATA_CLIENT;
							handler.sendMessage(msg);
						}
					}

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (PackageManager.NameNotFoundException e) {
					e.printStackTrace();
				}
			}


		});

	}

	private String getVersionName() throws Exception {
		//getPackageName()是你当前类的包名，0代表是获取版本信息
		PackageManager packageManager = getPackageManager();
		PackageInfo packInfo = packageManager.getPackageInfo(getPackageName(),
				0);
		return packInfo.versionName;
	}

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			super.handleMessage(msg);
			switch (msg.what) {
				case UPDATA_NONEED:
					break;
				case UPDATA_CLIENT:
					//对话框通知用户升级程序
					showUpdataDialog();
					break;
				case GET_UNDATAINFO_ERROR:
					//服务器超时
					Toast.makeText(getApplicationContext(), "获取服务器更新信息失败", Toast.LENGTH_LONG).show();
					break;
				case DOWN_ERROR:
					//下载apk失败
					Toast.makeText(getApplicationContext(), "下载新版本失败", Toast.LENGTH_LONG).show();
					break;
			}
		}
	};

	/**
	 * 弹出对话框通知用户更新程序
	 * <p>
	 * 弹出对话框的步骤：
	 * 1.创建alertDialog的builder.
	 * 2.要给builder设置属性, 对话框的内容,样式,按钮
	 * 3.通过builder 创建一个对话框
	 * 4.对话框show()出来
	 */
	protected void showUpdataDialog() {
		AlertDialog.Builder builer = new AlertDialog.Builder(this);
		builer.setTitle("版本升级");
		builer.setMessage(info.getDescription());
		//当点确定按钮时从服务器上下载 新的apk 然后安装   װ
		builer.setPositiveButton("更新", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				Log.i(TAG, "下载apk,更新");
				downLoadApk();
			}
		});
		builer.setNegativeButton("取消", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		AlertDialog dialog = builer.create();
		dialog.show();
	}

	/**
	 * 从服务器中下载APK
	 */
	String downloadUrl;

	protected void downLoadApk() {
		Intent intent = new Intent(HomeActivity.this, UpdateService.class);
		intent.putExtra("url", downloadUrl);
		intent.putExtra("name", "路车人");
		startService(intent);
		/*final ProgressDialog pd;    //进度条对话框
		pd = new ProgressDialog(this);
		pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		pd.setMessage("正在下载更新");
		pd.show();
		new Thread() {
			@Override
			public void run() {
				try {
					File file = DownLoadManager.getFileFromServer(info.getUrl(), pd);
					sleep(3000);
					installApk(file);
					pd.dismiss(); //结束掉进度条对话框
				} catch (Exception e) {
					Message msg = new Message();
					msg.what = DOWN_ERROR;
					handler.sendMessage(msg);
					e.printStackTrace();
				}
			}
		}.start();*/
	}

	//安装apk
	protected void installApk(File file) {
		Intent intent = new Intent();
		//执行动作
		intent.setAction(Intent.ACTION_VIEW);
		//执行的数据类型
		intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
		startActivity(intent);
	}
}
