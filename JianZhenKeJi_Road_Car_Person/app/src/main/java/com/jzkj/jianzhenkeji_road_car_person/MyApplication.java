package com.jzkj.jianzhenkeji_road_car_person;

import android.app.Application;
import android.graphics.Bitmap;

import com.jzkj.jianzhenkeji_road_car_person.util.CrashHandler;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.utils.StorageUtils;
import com.socks.library.KLog;
import com.umeng.socialize.PlatformConfig;

import java.io.File;

import cn.jpush.android.api.JPushInterface;

/**
 * Created by Administrator on 2016/2/29 0029.
 */
public class MyApplication extends Application {

	public static final String cacheDirUtils =/* Environment.getExternalStorageDirectory() +*/ "/crop/images/"; // ----该图片sd卡缓存地址.
	public static DisplayImageOptions options_user, options_other, options_video, options_welcome;


	@Override
	public void onCreate() {
		super.onCreate();
		initImageLoader();
		initYouMengShare();
		JPushInterface.setDebugMode(false);
		JPushInterface.init(this);
//		JPushInterface.setDefaultPushNotificationBuilder(new BasicPushNotificationBuilder(this).statusBarDrawable);
		CrashHandler crashHandler = CrashHandler.getInstance();
		crashHandler.init(getApplicationContext());
		KLog.init(true); // 正式版false
//		KLog.init(BuildConfig.DEBUG);

	}

	/**
	 * 初始化图片加载类配置信息
	 **/
	public void initImageLoader() {
		File cacheDir = StorageUtils.getOwnCacheDirectory(getApplicationContext(), cacheDirUtils);
		//System.out.println("缓存地址-----" + cacheDir);
		ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(getApplicationContext());
		config.threadPriority(Thread.NORM_PRIORITY - 2);
		config.denyCacheImageMultipleSizesInMemory();
		config.diskCache(new UnlimitedDiscCache(cacheDir));
		config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
		config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
		config.tasksProcessingOrder(QueueProcessingType.LIFO);
		if (BuildConfig.DEBUG) {
			config.writeDebugLogs(); // Remove for release app
		}

		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config.build());

		options_user = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.ic_launcher_big) //在ImageView加载过程中显示图片
				.showImageForEmptyUri(R.drawable.ic_launcher_big) //image连接地址为空时
				.showImageOnFail(R.drawable.ic_launcher_big) //image加载失败
				.cacheInMemory(true) //加载图片时会在内存中加载缓存
				.cacheOnDisc(true) //加载图片时会在磁盘中加载缓存
				.imageScaleType(ImageScaleType.EXACTLY).bitmapConfig(Bitmap.Config.RGB_565) //图片色值配置
				.build();

		options_other = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(R.drawable.ic_img) //image连接地址为空时
				.showImageOnFail(R.drawable.ic_img) //image加载失败
				.cacheInMemory(true) //加载图片时会在内存中加载缓存
				.cacheOnDisc(true) //加载图片时会在磁盘中加载缓存
				.imageScaleType(ImageScaleType.EXACTLY).bitmapConfig(Bitmap.Config.RGB_565) //图片色值配置
				.build();

		options_video = new DisplayImageOptions.Builder().showImageOnLoading(R.color.transparent) //在ImageView加载过程中显示图片
				.showImageForEmptyUri(R.color.transparent) //image连接地址为空时
				.showImageOnFail(R.color.transparent) //image加载失败
				.cacheInMemory(true) //加载图片时会在内存中加载缓存
				.cacheOnDisc(true) //加载图片时会在磁盘中加载缓存
				.imageScaleType(ImageScaleType.EXACTLY).bitmapConfig(Bitmap.Config.RGB_565) //图片色值配置
				.build();
		options_welcome = new DisplayImageOptions.Builder().showImageOnLoading(R.color.transparent) //在ImageView加载过程中显示图片
				.showImageForEmptyUri(R.color.transparent) //image连接地址为空时
				.showImageOnFail(R.color.transparent) //image加载失败
				.cacheInMemory(true) //加载图片时会在内存中加载缓存
				.cacheOnDisc(true) //加载图片时会在磁盘中加载缓存
				.imageScaleType(ImageScaleType.EXACTLY).bitmapConfig(Bitmap.Config.RGB_565) //图片色值配置
				.build();

	}

	/**
	 * 初始化友盟分享
	 */
	public void initYouMengShare() {
		PlatformConfig.setWeixin("wx170300f1254850b4", "482faf7847c8781d7e064a2b9950df8d");
		//微信 appid appsecret
		PlatformConfig.setSinaWeibo("3921700954", "04b48b094faeb16683c32669824ebdad");
		//新浪微博 appkey appsecret
		PlatformConfig.setQQZone("1105160925", "51aIcJvjvqF7Ym96");
		// QQ和Qzone appid appkey
	}
}
