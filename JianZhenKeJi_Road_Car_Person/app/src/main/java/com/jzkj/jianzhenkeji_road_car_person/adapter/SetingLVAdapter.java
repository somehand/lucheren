package com.jzkj.jianzhenkeji_road_car_person.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.sax.StartElementListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.activity.MyDataActivity;
import com.jzkj.jianzhenkeji_road_car_person.bean.ItIsUser;
import com.jzkj.jianzhenkeji_road_car_person.bean.MySeting;
import com.jzkj.jianzhenkeji_road_car_person.util.Util;

public class SetingLVAdapter extends BaseAdapter{

	private Context context;
	private ArrayList<MySeting> list;
	private LayoutInflater inflater;
	
	public SetingLVAdapter(Context context, ArrayList<MySeting> list) {
		super();
		this.context = context;
		this.list = list;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list == null? 0:list.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		viewHolder holder = null;
		if (arg1 == null) {
			arg1 = inflater.inflate(R.layout.seting_lv_item, null);
		}
		holder = (viewHolder) arg1.getTag();
		if (holder == null) {
			holder = new viewHolder();
			holder.tvItemName = (TextView) arg1.findViewById(R.id.seting_item_name);
		}
		holder.clear();
		MySeting mySeting = list.get(arg0);
		holder.tvItemName.setText(mySeting.getItemName());
		return arg1;
	}
	class viewHolder{
		private TextView tvItemName;
		void clear(){
			tvItemName.setText("");
		}
	}
}
