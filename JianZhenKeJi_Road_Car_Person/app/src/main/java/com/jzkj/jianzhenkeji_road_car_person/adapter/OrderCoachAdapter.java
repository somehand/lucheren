package com.jzkj.jianzhenkeji_road_car_person.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.CircleImageView;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.bean.CoachInformation;

import java.util.List;

public class OrderCoachAdapter extends BaseAdapter{

	private Context context;
	private List<CoachInformation> list;
	private LayoutInflater inflater;
	public OrderCoachAdapter(Context context, List<CoachInformation> list) {
		super();
		this.context = context;
		this.list = list;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list == null ? 0:list.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		viewHolder holder = null;
		if (arg1 == null) {
			arg1 = inflater.inflate(R.layout.order_coach_item, null);
		}
		holder = (viewHolder) arg1.getTag();
		if (holder == null) {
			holder = new viewHolder();
			holder.civHead = (CircleImageView) arg1.findViewById(R.id.ordercoach_civ);
			holder.imgStarLevel = (ImageView) arg1.findViewById(R.id.ordercoach_item_starlevel);
			holder.imgGrade = (ImageView) arg1.findViewById(R.id.ordercoach_item_imggrade);
			holder.evaluateNum = (TextView) arg1.findViewById(R.id.ordercoach_item_evaluatenum);
			holder.name = (TextView) arg1.findViewById(R.id.ordercoach_item_name);
			holder.orderNum = (TextView) arg1.findViewById(R.id.ordercoach_item_ordernum);
		}
		holder.clear();
		CoachInformation cInformation = list.get(arg0);
		holder.civHead.setImageResource(cInformation.getImgHead());
		holder.imgGrade.setBackgroundResource(cInformation.getImgGrad());
		holder.imgStarLevel.setBackgroundResource(cInformation.getStarLevel());
		holder.evaluateNum.setText(cInformation.getEvaluateNum());
		holder.name.setText(cInformation.getCoachName());
		holder.orderNum.setText(cInformation.getOrderNum());
		return arg1;
	}

	class viewHolder{
		private CircleImageView civHead;
		private ImageView imgStarLevel;
		private ImageView imgGrade;
		private TextView name;
		private TextView orderNum;
		private TextView evaluateNum;
		public void clear() {
			civHead.setBackgroundDrawable(null);
			imgStarLevel.setBackgroundDrawable(null);
			imgGrade.setBackgroundDrawable(null);
			name.setText("");
			orderNum.setText("");
			evaluateNum.setText("");
			
		}
	}
}
