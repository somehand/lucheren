package com.jzkj.jianzhenkeji_road_car_person.util;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;

/**
 * 管理所有Activity的工具类
 * @author Administrator
 *
 */
public class ActivityCollect {
	public static List<Activity> mactivitylist = new ArrayList<Activity>();
	/**
	 * 添加activity
	 * @param activity
	 */
	public static void addActivity(Activity activity){
		mactivitylist.add(activity);
	}
	/**
	 * 移除activity
	 * @param activity
	 */
	public static void removeActivity(Activity activity){
		mactivitylist.remove(activity);
	}
	/**
	 * finish所有的activity
	 */
	public static void finishAll(){
		for (Activity activity : mactivitylist) {
			if (!activity.isFinishing()) {
				activity.finish();
			}
		}
	}
}
