package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.util.KeyBoardUtils;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Administrator on 2016/4/6.
 */
public class RechargeActivity extends Activity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
	@InjectView(R.id.headframe_ib)
	ImageButton mHeadframeIb;
	@InjectView(R.id.headframe_title)
	TextView mHeadframeTitle;
	@InjectView(R.id.recharge_btn_ten_yuan)
	CheckBox mRechargeBtnTenYuan;
	@InjectView(R.id.recharge_btn_twoty_yuan)
	CheckBox mRechargeBtnTwotyYuan;
	@InjectView(R.id.recharge_btn_fifty_yuan)
	CheckBox mRechargeBtnFiftyYuan;
	@InjectView(R.id.recharge_btn_twohundred_yuan)
	CheckBox mRechargeBtnOnehundredYuan;
	@InjectView(R.id.recharge_btn_fivehundred_yuan)
	CheckBox mRechargeBtnFivehundredYuan;
	@InjectView(R.id.recharge_btn_other_yuan)
	LinearLayout mRechargeBtnOtherYuan;
	@InjectView(R.id.recharge_btn_recharge)
	Button mRechargeBtnRecharge;
	@InjectView(R.id.recharge_btn_et_other_yuan)
	EditText etOtherMoney;
	@InjectView(R.id.title)
	TextView title_tx;
	@InjectView(R.id.linearLayout)
	LinearLayout linearLayout;

	private int Paymoney;

	private boolean flag =false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_recharge);
		ButterKnife.inject(this);
		init();
		initData();
		initListener();
	}

	private void initListener() {
		mHeadframeIb.setOnClickListener(this);
		mRechargeBtnRecharge.setOnClickListener(this);

		mRechargeBtnTenYuan.setOnCheckedChangeListener(this);
		mRechargeBtnTwotyYuan.setOnCheckedChangeListener(this);
		mRechargeBtnFiftyYuan.setOnCheckedChangeListener(this);
		mRechargeBtnOnehundredYuan.setOnCheckedChangeListener(this);
		mRechargeBtnFivehundredYuan.setOnCheckedChangeListener(this);
		etOtherMoney.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(etOtherMoney.getText().length()==0) {
					Paymoney = 0;
					settitle(1);
				}
				mRechargeBtnTenYuan.setChecked(false);
				mRechargeBtnTwotyYuan.setChecked(false);
				mRechargeBtnFiftyYuan.setChecked(false);
				mRechargeBtnOnehundredYuan.setChecked(false);
				mRechargeBtnFivehundredYuan.setChecked(false);
			}
		});
		linearLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				KeyBoardUtils.closeKeybord(etOtherMoney,RechargeActivity.this);
			}
		});
//		mRechargeBtnOtherYuan.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View view) {
//				if(etOtherMoney.getText().length()!=0) {
//					Paymoney = 0;
//					settitle(1);
//				}
//				mRechargeBtnTenYuan.setChecked(false);
//				mRechargeBtnTwotyYuan.setChecked(false);
//				mRechargeBtnFiftyYuan.setChecked(false);
//				mRechargeBtnOnehundredYuan.setChecked(false);
//				mRechargeBtnFivehundredYuan.setChecked(false);
//			}
//		});
		etOtherMoney.addTextChangedListener(textWatcher);
	}
	private TextWatcher textWatcher = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
								  int count) {
			if(Paymoney!=10&&Paymoney!=20&&Paymoney!=50&&Paymoney!=200&&Paymoney!=500) {
				if (s.length() > 0) {
					int money = Integer.parseInt(etOtherMoney.getText().toString());
					if (money > 1000) {
						etOtherMoney.setText(1000 + "");
					}
					settitle(Integer.parseInt(etOtherMoney.getText().toString()));
				} else {
						settitle(1);
				}
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
									  int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
		}
	};

	private void initData() {

	}

	private void getRechargeInfo() {
		if (Paymoney != 0) {
			/*RequestParams params = new RequestParams();
			params.put("Ordername", "积分充值");
			params.put("Userid", Utils.SPGetString(RechargeActivity.this, Utils.userId, ""));
			params.put("Paymoney", Paymoney);
			params.put("Type", 1);
			params.put("PracticeId", 0);
			KLog.e("params", params.toString());
			MyHttp.getInstance(RechargeActivity.this).post(MyHttp.GET_RECHARGE_INFO, params, new JsonHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
					super.onSuccess(statusCode, headers, response);
					KLog.e("Debug", "JSONObject:");
					KLog.json(response.toString());
					try {
						if (response.getInt("Code") == 1) {
							Utils.ToastShort(RechargeActivity.this, response.getString("Reason"));*/
							Intent intent = new Intent(RechargeActivity.this, PayPageActivity.class);
							intent.putExtra("price", Paymoney+"");
							intent.putExtra("orderName",Utils.SPGetString(this,Utils.realName,"")+"-积分充值");
							intent.putExtra("PracticeId",0+"");
							intent.putExtra("type",1);
							startActivity(intent);
					/*	}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			});*/
		} else {
			Utils.ToastShort(RechargeActivity.this, "充值金额不能为空");
		}
	}

	private void init() {
		mHeadframeTitle.setText("充值");
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.headframe_ib:
				finish();
				break;
			case R.id.recharge_btn_recharge:
				if (!TextUtils.isEmpty(etOtherMoney.getText().toString().trim())) {
					Paymoney = Integer.parseInt(etOtherMoney.getText().toString().trim());
				}
				getRechargeInfo();
				break;
			default:
				break;
		}
	}

	public void settitle(int money){
		title_tx.setText(money+"元="+money*10+"积分");
	}

	@Override
	public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
		switch (compoundButton.getId()) {
			case R.id.recharge_btn_ten_yuan:
				flag =true;
				if (mRechargeBtnTenYuan.isChecked()) {
					Paymoney = 10;
					settitle(10);
					title_tx.setText(Paymoney+"元="+Paymoney*10+"积分");
					mRechargeBtnTwotyYuan.setChecked(false);
					mRechargeBtnFiftyYuan.setChecked(false);
					mRechargeBtnOnehundredYuan.setChecked(false);
					mRechargeBtnFivehundredYuan.setChecked(false);
					KeyBoardUtils.closeKeybord(etOtherMoney,this);

					etOtherMoney.setText("");
				}


				break;
			case R.id.recharge_btn_twoty_yuan:
				flag =true;
				if (mRechargeBtnTwotyYuan.isChecked()) {
					Paymoney = 20;
					settitle(20);
					title_tx.setText(Paymoney+"元="+Paymoney*10+"积分");

					mRechargeBtnTenYuan.setChecked(false);
					mRechargeBtnFiftyYuan.setChecked(false);
					mRechargeBtnOnehundredYuan.setChecked(false);
					mRechargeBtnFivehundredYuan.setChecked(false);
					KeyBoardUtils.closeKeybord(etOtherMoney,this);
					etOtherMoney.setText("");
				}


				break;
			case R.id.recharge_btn_fifty_yuan:
				flag =true;
				if (mRechargeBtnFiftyYuan.isChecked()) {
					Paymoney = 50;
					settitle(50);
					title_tx.setText(Paymoney+"元="+Paymoney*10+"积分");

					mRechargeBtnTenYuan.setChecked(false);
					mRechargeBtnTwotyYuan.setChecked(false);
					mRechargeBtnOnehundredYuan.setChecked(false);
					mRechargeBtnFivehundredYuan.setChecked(false);
					KeyBoardUtils.closeKeybord(etOtherMoney,this);
					etOtherMoney.setText("");
				}


				break;
			case R.id.recharge_btn_twohundred_yuan:
				flag =true;
				if (mRechargeBtnOnehundredYuan.isChecked()) {
					Paymoney = 200;
					settitle(200);
					title_tx.setText(Paymoney+"元="+Paymoney*10+"积分");

					mRechargeBtnTenYuan.setChecked(false);
					mRechargeBtnTwotyYuan.setChecked(false);
					mRechargeBtnFiftyYuan.setChecked(false);
					mRechargeBtnFivehundredYuan.setChecked(false);
					KeyBoardUtils.closeKeybord(etOtherMoney,this);
					etOtherMoney.setText("");
				}


				break;
			case R.id.recharge_btn_fivehundred_yuan:
				flag =true;
				if (mRechargeBtnFivehundredYuan.isChecked()) {
					Paymoney = 500;
					settitle(500);
					title_tx.setText(Paymoney+"元="+Paymoney*10+"积分");

					mRechargeBtnTenYuan.setChecked(false);
					mRechargeBtnTwotyYuan.setChecked(false);
					mRechargeBtnFiftyYuan.setChecked(false);
					mRechargeBtnOnehundredYuan.setChecked(false);
					KeyBoardUtils.closeKeybord(etOtherMoney,this);
					etOtherMoney.setText("");
				}


				break;
			default:
				break;
		}
	}
}
