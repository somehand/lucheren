package com.jzkj.jianzhenkeji_road_car_person.bean;

public class ChooseSafetyDetails {
	private String name;
	private int imgHead;
	private String sign;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getImgHead() {
		return imgHead;
	}
	public void setImgHead(int imgHead) {
		this.imgHead = imgHead;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public ChooseSafetyDetails(String name, int imgHead, String sign) {
		super();
		this.name = name;
		this.imgHead = imgHead;
		this.sign = sign;
	}
	
}
