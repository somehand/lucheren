package com.jzkj.jianzhenkeji_road_car_person.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.MyApplication;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.bean.ApplyDrivingSchoolBean;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Administrator on 2016/5/12 0012.
 */
public class ApplyDrivingSchoolAdapter extends QuickAdapter<ApplyDrivingSchoolBean> {
	public ApplyDrivingSchoolAdapter(Context context, int layoutResId) {
		super(context, layoutResId);
	}

	@Override
	protected void convert(BaseAdapterHelper helper, ApplyDrivingSchoolBean item) {
		ImageView logo = helper.getView(R.id.img_apply_school_logo);
		TextView schoolName = helper.getView(R.id.text_apply_school_name);
		TextView isRecommend = helper.getView(R.id.text_apply_school_recommend);
		TextView schoolAddress = helper.getView(R.id.text_apply_school_address);
		TextView price = helper.getView(R.id.text_apply_school_money);
		RatingBar ratingBar = helper.getView(R.id.rating_apply_school_star);

		ImageLoader.getInstance().displayImage(item.getSchoolLogo(), logo, MyApplication.options_other);
		schoolName.setText(item.getSchoolName());
		schoolAddress.setText(item.getSchoolAddress());
		ratingBar.setRating(Float.parseFloat(item.getStar() + ""));
		price.setText(item.getPrice());
		if (item.getIsRecommend() == 1) {//（1.不是  2.推荐）
			isRecommend.setVisibility(View.GONE);
		} else {
			isRecommend.setVisibility(View.VISIBLE);
		}
	}
}
