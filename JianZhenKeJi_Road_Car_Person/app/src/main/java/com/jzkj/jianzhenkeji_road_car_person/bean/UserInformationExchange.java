package com.jzkj.jianzhenkeji_road_car_person.bean;

public class UserInformationExchange {
	private String title;
	private String id;

	private String imgOne;
	private String imgtwo;
	private String imgthree;

	private String readnum;
	private String url;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImgOne() {
		return imgOne;
	}

	public void setImgOne(String imgOne) {
		this.imgOne = imgOne;
	}

	public String getImgtwo() {
		return imgtwo;
	}

	public void setImgtwo(String imgtwo) {
		this.imgtwo = imgtwo;
	}

	public String getImgthree() {
		return imgthree;
	}

	public void setImgthree(String imgthree) {
		this.imgthree = imgthree;
	}

	public String getReadnum() {
		return readnum;
	}

	public void setReadnum(String readnum) {
		this.readnum = readnum;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public UserInformationExchange(String id, String title, String imgone, String imgtwo,
	                               String imgthree, String readnum , String url) {
		super();
		this.id = id;
		this.title = title;
		this.imgOne = imgone;
		this.imgtwo = imgtwo;
		this.imgthree = imgthree;
		this.readnum = readnum;
		this.url = url;
	}

	@Override
	public String toString() {
		return "UserInformationExchange{" +
				"title='" + title + '\'' +
				", id='" + id + '\'' +
				", imgOne='" + imgOne + '\'' +
				", imgtwo='" + imgtwo + '\'' +
				", imgthree='" + imgthree + '\'' +
				", readnum='" + readnum + '\'' +
				", url='" + url + '\'' +
				'}';
	}
}
