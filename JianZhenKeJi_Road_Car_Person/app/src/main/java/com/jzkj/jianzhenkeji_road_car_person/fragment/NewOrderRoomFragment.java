package com.jzkj.jianzhenkeji_road_car_person.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.MyApplication;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.activity.ChooseSafetyActivity;
import com.jzkj.jianzhenkeji_road_car_person.activity.HomeActivity;
import com.jzkj.jianzhenkeji_road_car_person.activity.LookRoomDetails1Activity;
import com.jzkj.jianzhenkeji_road_car_person.bean.ExaminationCarBean;
import com.jzkj.jianzhenkeji_road_car_person.bean.RoomBean;
import com.jzkj.jianzhenkeji_road_car_person.bean.SecurityBean;
import com.jzkj.jianzhenkeji_road_car_person.util.CenterShowHorizontalScrollView;
import com.jzkj.jianzhenkeji_road_car_person.util.DateTimePickDialogUtil;
import com.jzkj.jianzhenkeji_road_car_person.util.DialogUtils;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class NewOrderRoomFragment extends Fragment implements View.OnClickListener, View.OnTouchListener {

	@InjectView(R.id.longtime)
	TextView longtime;
	@InjectView(R.id.cartype)
	TextView cartype;
	@InjectView(R.id.roomtime)
	TextView roomtime;
	@InjectView(R.id.room_money)
	TextView roomMoney;
	@InjectView(R.id.pay_money)
	TextView paymoney;
	@InjectView(R.id.OfficerName)
	TextView OfficerName;
	@InjectView(R.id.OfficerName_line)
	RelativeLayout OfficerName_line;
	@InjectView(R.id.new_order_room_btn_orderandpay)
	Button btnpay;
	@InjectView(R.id.horizontallistview1)
	CenterShowHorizontalScrollView scrollView;
	@InjectView(R.id.order_room_sv)
	ScrollView scrollViewlayout;
	//	@InjectView(R.id.mycar)
//	CheckBox mycar; //是否自备车
	private ArrayList<RoomBean> roomDatas = new ArrayList<RoomBean>();
	private Map<String, ArrayList<ExaminationCarBean>> carMap = new HashMap<String, ArrayList<ExaminationCarBean>>();
	private Map<String, ArrayList<SecurityBean>> securityMap = new HashMap<String, ArrayList<SecurityBean>>();
	private ArrayList<ExaminationCarBean> carDatas = null;

	ExaminationCarBean tempcar;
	SecurityBean securityBean;
	View view = null;
	int longtimeTag = 0;
	String examinationId = null;
	AlertDialog dialog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.new_fragment_order_room, null);
		dialog = new AlertDialog.Builder(getActivity()).create();
		initlistener();
		initData();
		getRoomList(2, false);
		getcurrentDate();
		return view;
	}

	public void initlistener() {
		ButterKnife.inject(this, view);
		view.setOnTouchListener(this);
		longtime.setOnClickListener(this);
		roomtime.setOnClickListener(this);
		cartype.setOnClickListener(this);
		OfficerName.setOnClickListener(this);
		btnpay.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				pay();
			}
		});
	}


	public void initData() {
		DateFormat df = new SimpleDateFormat("yyyy-M-d");
		for (int i = 0; i < 15; i++) {
			String time = df.format(new Date().getTime() + i * 24 * 60 * 60 * 1000);
			roomtimes[i] = time.substring(5, time.length()).replaceAll("-", "月") + "日";
		}
	}


	//预约考场
	public void pay() {
		if (tempcar == null) {
			new AlertDialog.Builder(getActivity())
					.setMessage("请选择车辆")
					.show();
			return;
		}
		if (roomtime.getText().toString().equals("点击选择时间")) {
			new AlertDialog.Builder(getActivity())
					.setMessage("请选择合场时间")
					.show();
			return;
		}
		if (longtimeTag == 0) {
			new AlertDialog.Builder(getActivity())
					.setMessage("请选择合场时间长")
					.show();
			return;
		}
		RequestParams params = new RequestParams();
		params.put("ExaminationId", examinationId);
		params.put("UserId", Utils.SPGetString(getActivity(), Utils.userId, ""));
		params.put("PracticeHour", longtimeTag + "");
	/*	if(mycar.isChecked())
			params.put("SelfoOrExam",1);
		else
			params.put("SelfoOrExam",2);*/
		if (securityBean == null)
			params.put("SecurityOfficerId", "0");
		else
			params.put("SecurityOfficerId", securityBean.getSecurityOfficerId());

		params.put("StartDate", roomtime.getText().toString());
		params.put("Subject", Integer.parseInt(((HomeActivity) getActivity()).getSubjectnum()));
		int i = tempcar.getCarType().indexOf("（");
		params.put("CarType", tempcar.getCarType().substring(0, i));
		params.put("Gear", tempcar.getGear());
		params.put("ExaminationPriceId", tempcar.getExaminationPriceId());
		KLog.e(params.toString());
		MyHttp.getInstance(getActivity()).post(MyHttp.ORDER_ROOM_PAY, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				try {
					String code = response.getString("Code");
					KLog.json(response.toString());
					if ("1".equals(code)) {
						String str = response.getString("Result");
						Utils.SPutString(getActivity(), Utils.currentExamRoomName, currentName);
						Utils.SPutString(getActivity(), Utils.carType, cartype.getText().toString());
						Utils.SPutString(getActivity(), Utils.yearAndDate, roomtime.getText().toString());
						Utils.SPutString(getActivity(), Utils.orderTime, longtime.getText().toString());
						Intent intent = new Intent(getActivity(), LookRoomDetails1Activity.class);
						intent.putExtra("price", paymoney.getTag().toString().trim());
						intent.putExtra("PracticeId", str);
						intent.putExtra("currentName", currentName);
						intent.putExtra("cartype", cartype.getText().toString());
						intent.putExtra("longtime", longtime.getText().toString());
						intent.putExtra("roomtime", roomtime.getText().toString());
						intent.putExtra("sec", securityBean);
						startActivity(intent);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				//Log.d(TAG,response.toString());
			}

		});
	}

	//得到考场的车辆信息
	public void getExaminationCars(final String eId) {
		RequestParams params = new RequestParams();
		params.put("ExaminationId", eId);
		MyHttp.getInstance(getActivity()).post(MyHttp.GET_EXAMINATION_CARS, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				try {
					String code = response.getString("Code");
					Log.d(TAG, response.toString());
					if ("1".equals(code)) {
						carDatas = new ArrayList<ExaminationCarBean>();
						JSONArray array = response.getJSONArray("Result"); // 集合转为json
						for (int i = 0; i < array.length(); i++) {
							JSONObject jsonObject = (JSONObject) array.get(i);
							ExaminationCarBean carBean = new ExaminationCarBean();
							carBean.setExaminationId(jsonObject.getString("ExaminationId"));
							carBean.setPrice(jsonObject.getString("Price"));
							carBean.setCarType(jsonObject.getString("CarType"));
							carBean.setExaminationPriceId(jsonObject.getString("ExaminationPriceId"));
							carBean.setGear(jsonObject.getString("Gear"));
							carDatas.add(carBean);
						}
						carMap.put(eId, carDatas);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}

	//得到考场列表
	public void getRoomList(int sub, final boolean isdel) {
		if (sub == 3)
			OfficerName_line.setVisibility(View.VISIBLE);
		else
			OfficerName_line.setVisibility(View.GONE);
		RequestParams params = new RequestParams();
		params.put("Subject", sub);
		setText();
		examinationId = null;

		MyHttp.getInstance(getActivity()).post(MyHttp.GET_ROOM_LIST, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				roomDatas.clear();
				if (isdel)
					scrollView.removeAll();
				try {
					String code = response.getString("Code");
					if ("1".equals(code)) {
						FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
								LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
						JSONArray array = response.getJSONArray("Result"); // 集合转为json

						for (int i = 0; i < array.length(); i++) {
							JSONObject jsonObject = (JSONObject) array.get(i);
							RoomBean roomBean = new RoomBean();
							roomBean.setExaminationAddress(jsonObject.getString("ExaminationAddress"));
							roomBean.setExaminationId(jsonObject.getString("ExaminationId"));
							roomBean.setExaminationMobile(jsonObject.getString("ExaminationMobile"));
							roomBean.setExaminationName(jsonObject.getString("ExaminationName"));
							roomBean.setExaminationURL(jsonObject.getString("ExaminationLogo"));
							roomDatas.add(roomBean);
							LinearLayout mShowLinear = new LinearLayout(getActivity());
							mShowLinear.setOrientation(LinearLayout.VERTICAL);
							ImageView imageView = new ImageView(getActivity());
							imageView.setScaleType(ImageView.ScaleType.FIT_XY);
							ImageLoader.getInstance().displayImage(roomBean.getExaminationURL(), imageView, MyApplication.options_other);
//							imageView.setImageResource(R.drawable.room1);
							imageView.setPadding(5, 5, 5, 5);
							mShowLinear.addView(imageView, getResources().getDimensionPixelOffset(R.dimen.x400), getResources().getDimensionPixelOffset(R.dimen.y330));
							TextView textView = new TextView(getActivity());
							textView.setText(roomBean.getExaminationName());
							textView.setGravity(Gravity.CENTER);
							textView.setTag(roomBean.getExaminationId());
							mShowLinear.addView(textView, params);
							mShowLinear.setPadding(15, 0, 15, 0);
							mShowLinear.setOnClickListener(new roomClick());
							if (i == 0) {
								imageView.setBackgroundColor(getResources().getColor(R.color.text_blue));
								textView.setTextColor(getResources().getColor(R.color.text_blue));
								currentView = mShowLinear;
								currentName = roomBean.getExaminationName();
								examinationId = roomBean.getExaminationId();
								getExaminationCars(roomBean.getExaminationId());
//								get_Securitys(roomBean.getExaminationId());
							}
							scrollView.addItemView(mShowLinear, i);
						}
						Log.d(TAG, array.toString());
					} else {
						if (((HomeActivity) getActivity()).getcurrentTabID() == 2) {
							new AlertDialog.Builder(getActivity())
									.setMessage("没有可用考场")
									.setNeutralButton("确定", new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialogInterface, int i) {
											dialogInterface.dismiss();
										}
									})
									.show();
						}
						return;
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
			}
		});
	}

	//考场列表点击事件
	class roomClick implements View.OnClickListener {
		@Override
		public void onClick(View view) {
			scrollView.onClicked(view);
			ImageView imageView1 = (ImageView) currentView.getChildAt(0);
			imageView1.setBackgroundColor(getResources().getColor(R.color.white));
			TextView textView1 = (TextView) currentView.getChildAt(1);
			textView1.setTextColor(getResources().getColor(R.color.black));
			LinearLayout linearLayout = (LinearLayout) view;
			ImageView imageView2 = (ImageView) linearLayout.getChildAt(0);
			imageView2.setBackgroundColor(getResources().getColor(R.color.text_blue));
			TextView textView2 = (TextView) linearLayout.getChildAt(1);
			textView2.setTextColor(getResources().getColor(R.color.text_blue));
			currentView = linearLayout;
			currentName = textView2.getText().toString();
			examinationId = textView2.getTag().toString();
			setText();
			if (!carMap.containsKey(examinationId)) {
				getExaminationCars(examinationId);
//				get_Securitys(examinationId);
			}
		}
	}

	public void setText() {
		cartype.setText("点击选择车型");
		OfficerName.setText("点击选择安全员");
		roomtime.setText("点击选择时间");
		paymoney.setText("");
		paymoney.setTag(0);
		roomMoney.setText("");
		tempcar = null;
		securityBean = null;
	}

	public LinearLayout currentView;
	String currentName;

	public static String TAG = "test";

	String[] longtimes = new String[]{"1小时", "2小时", "3小时"};
	String[] roomtimes = new String[15];

	DateTimePickDialogUtil dateTimePicKDialog;

	public void getcurrentDate() {
		dateTimePicKDialog = new DateTimePickDialogUtil(getActivity(), null);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.longtime:
				new AlertDialog.Builder(getActivity())
						.setTitle("请选择时长：")
						.setItems(longtimes, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialogInterface, int i) {
								longtimeTag = i + 1;
								longtime.setText(longtimes[i]);
								double d = Double.parseDouble(roomMoney.getTag().toString());
								paymoney.setText(longtimeTag * (int) d + "元");
								paymoney.setTag(longtimeTag * (int) d);
								dialogInterface.dismiss();
							}
						})
						.show();
				break;
			case R.id.roomtime:
				dateTimePicKDialog.dateTimePicKDialog(roomtime);
				break;
			case R.id.cartype:
				if (carMap.containsKey(examinationId)) {
					final ArrayList<ExaminationCarBean> arrayList = carMap.get(examinationId);
					final String[] cars = new String[arrayList.size()];
					for (int i = 0; i < arrayList.size(); i++) {
						cars[i] = arrayList.get(i).getCarType();
					}
					new AlertDialog.Builder(getActivity())
							.setTitle("请选择车型：")
							.setItems(cars, new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialogInterface, int i) {
									tempcar = arrayList.get(i);
									cartype.setText(cars[i]);
									String price = tempcar.getPrice();
									price = price.substring(0, price.lastIndexOf("."));
									roomMoney.setText(price + "元/小时");
									roomMoney.setTag(tempcar.getPrice());
									dialogInterface.dismiss();
									double d = Double.parseDouble(price);
									paymoney.setText(longtimeTag * (int) d + "元");
									paymoney.setTag(longtimeTag * (int) d);
								}
							})
							.show();
				} else {
					DialogUtils.getInstans().Showdiag(getActivity(), "请选择车辆类型", 2000);
				}
				break;
			case R.id.OfficerName:
				Intent intent = new Intent(getActivity(), ChooseSafetyActivity.class);
				intent.putExtra("examinationId", examinationId);
				startActivityForResult(intent, 0x111);
				break;
		}
	}

	String securityId;
	String securityName;

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (data != null && requestCode == 0x111 && resultCode == 6888) {
			securityId = data.getStringExtra("securityId");
			securityName = data.getStringExtra("securityName");
			if (securityBean == null) {
				securityBean = new SecurityBean();
				OfficerName.setText(securityName);
				securityBean.setSecurityOfficerId(securityId);
				securityBean.setOfficerName(securityName);
			}
		}
	}

	@Override
	public boolean onTouch(View view, MotionEvent motionEvent) {
		return true;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		ButterKnife.reset(this);
	}

}
