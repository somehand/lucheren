package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.jzkj.jianzhenkeji_road_car_person.CircleImageView;
import com.jzkj.jianzhenkeji_road_car_person.MyApplication;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.bean.CarInfoBean;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class
OrderRoomInfoActivity extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener, CompoundButton.OnCheckedChangeListener {

	@InjectView(R.id.headframe_ib)
	ImageButton mHeadframeIb;
	@InjectView(R.id.headframe_title)
	TextView mHeadframeTitle;
	@InjectView(R.id.order_room_info_name)
	TextView mOrderRoomInfoName;
	@InjectView(R.id.order_room_info_time)
	TextView mOrderRoomInfoTime;
	@InjectView(R.id.order_room_info_car)
	TextView mOrderRoomInfoCar;
	@InjectView(R.id.order_room_info_safety)
	TextView mOrderRoomInfoSafety;
	@InjectView(R.id.order_room_info_money)
	TextView mOrderRoomInfoMoney;
	@InjectView(R.id.order_room_info_commit)
	Button mOrderRoomInfoCommit;
	@InjectView(R.id.order_room_info_btn_1)
	LinearLayout mOrderRoomInfoBtn1;
	@InjectView(R.id.order_room_info_btn_2)
	LinearLayout mOrderRoomInfoBtn2;
	@InjectView(R.id.order_room_info_btn_3)
	RelativeLayout mOrderRoomInfoBtn3;
	@InjectView(R.id.order_room_info_radio_1)
	RadioButton mOrderRoomInfoRadio1;
	@InjectView(R.id.order_room_info_radio_2)
	RadioButton mOrderRoomInfoRadio2;
	@InjectView(R.id.order_room_info_radio_3)
	RadioButton mOrderRoomInfoRadio3;
	@InjectView(R.id.order_room_info_radio_4)
	RadioButton mOrderRoomInfoRadio4;
	@InjectView(R.id.order_room_info_clock)
	TextView mOrderRoomInfoClock;
	@InjectView(R.id.order_room_info_btn_4)
	RelativeLayout mOrderRoomInfoBtn4;
	@InjectView(R.id.order_room_info_divider)
	TextView mOrderRoomInfoDivider;
	@InjectView(R.id.order_room_info_civ_logo)
	CircleImageView civLogo;
	@InjectView(R.id.order_room_info_user_nickname)
	TextView tvUserNickName;
	@InjectView(R.id.order_room_info_school_name)
	TextView tvSchoolName;

	PopupWindow timePopupWindow, carPopupWindow, namePopupWindow;
	ArrayAdapter adapter;
	String[] s = {"1小时", "2小时", "3小时", "4小时"};
	ArrayList<CarInfoBean> carList;
	ArrayList<String> carNameList;
	int hourSelect = 1;
	double carPrice = 0;
	String price;

	private Context context;
	private int carType = 1;//排挡 1.自动2.手动
	boolean isCarSelect;
	boolean isSecuritySelect;
	String securityId;
	String securityName;
	String date;

	ArrayList<String> times = new ArrayList();


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_order_room_info);
		ButterKnife.inject(this);
		context = OrderRoomInfoActivity.this;
		//getIntent();
		initView();
		initData();
		initListener();

	}

	private void initView() {
		mHeadframeTitle.setText("预约详情");
		ImageLoader.getInstance().displayImage(Utils.SPGetString(OrderRoomInfoActivity.this, Utils.userLogo, "userlogo"), civLogo, MyApplication.options_user);
		tvUserNickName.setText(Utils.SPGetString(OrderRoomInfoActivity.this, Utils.realName, "realName"));
		tvSchoolName.setText(Utils.SPGetString(OrderRoomInfoActivity.this, Utils.schoolName, "schoolName"));
		mOrderRoomInfoName.setText(/*Utils.SPGetString(this, Utils.currentExamRoomName, "西彭考场")*/"西彭考场");
		initPopupWindow();
	}


	private void initData() {
		carList = new ArrayList<CarInfoBean>();
		carNameList = new ArrayList<String>();
		for (int i = 0; i < s.length; i++) {
			times.add(s[i]);
		}
		getCarList();
		getHeaderName();

	}


	private void initListener() {
		mHeadframeIb.setOnClickListener(this);
		mOrderRoomInfoBtn1.setOnClickListener(this);
		mOrderRoomInfoBtn2.setOnClickListener(this);
		mOrderRoomInfoBtn3.setOnClickListener(this);
		mOrderRoomInfoBtn4.setOnClickListener(this);
		mOrderRoomInfoCommit.setOnClickListener(this);
		mOrderRoomInfoRadio1.setOnCheckedChangeListener(this);
		mOrderRoomInfoRadio2.setOnCheckedChangeListener(this);
		mOrderRoomInfoRadio3.setOnCheckedChangeListener(this);
		mOrderRoomInfoRadio4.setOnCheckedChangeListener(this);

	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.headframe_ib:
				finish();
				break;
			case R.id.order_room_info_btn_1:
				adapter.clear();
				adapter.addAll(times);
				if (timePopupWindow.isShowing()) {
					timePopupWindow.dismiss();
				} else {
					timePopupWindow.showAsDropDown(mOrderRoomInfoTime);
				}
				break;
			case R.id.order_room_info_btn_2:
				adapter.clear();
				adapter.addAll(carNameList);
				if (carPopupWindow.isShowing()) {

					carPopupWindow.dismiss();
				} else {
					carPopupWindow.showAsDropDown(mOrderRoomInfoCar);
				}
				break;
			case R.id.order_room_info_btn_3:
				Intent intent1 = new Intent(this, ChooseSafetyActivity.class);
				startActivityForResult(intent1, 0x111);
				break;
			case R.id.order_room_info_btn_4:
				final Calendar calendar = new GregorianCalendar();
				TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
					@Override
					public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
						if (timePicker.isShown()) {
							calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
							calendar.set(Calendar.MINUTE, minute);
							Date date = calendar.getTime();  // 待设定的时间
							SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
							String s = dateFormat.format(date);
							mOrderRoomInfoClock.setText(s);
							System.out.println("------------" + s);
						}
					}
				}, 9, 0, true);
				timePickerDialog.setCanceledOnTouchOutside(true);
				timePickerDialog.show();
				break;
			case R.id.order_room_info_commit:
				if (mOrderRoomInfoRadio3.isChecked()) {
					if (mOrderRoomInfoSafety.getText().toString().length() != 0) {
						if (isCarSelect) {
							if (mOrderRoomInfoClock.getText().toString().length() == 0 || mOrderRoomInfoClock.getText().toString().equals("")) {
								Utils.ToastShort(context, "请选择预约时间");
							} else {
								getOrderExamInfo(securityId);
							}
						} else {
							Utils.ToastShort(this, "请选择车型");
						}
					} else {
						Utils.ToastShort(this, "请选择安全员");
					}
				} else {
					if (isCarSelect) {
						if (mOrderRoomInfoClock.getText().toString().length() == 0 || mOrderRoomInfoClock.getText().toString().equals("")) {
							Utils.ToastShort(context, "请选择预约时间");
						} else {
							getOrderExamInfo(null);
						}
					} else {
						Utils.ToastShort(this, "请选择车型");
					}
				}


				break;
		}
	}

	void initPopupWindow() {
		View view = LayoutInflater.from(this).inflate(R.layout.popup_order_info_layout, null);
		timePopupWindow = new PopupWindow(view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		carPopupWindow = new PopupWindow(view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		//namePopupWindow = new PopupWindow(view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		timePopupWindow.setFocusable(true);
		timePopupWindow.setOutsideTouchable(true);
		carPopupWindow.setFocusable(true);
		carPopupWindow.setOutsideTouchable(true);
		//namePopupWindow.setFocusable(true);
		//namePopupWindow.setOutsideTouchable(true);
		timePopupWindow.setBackgroundDrawable(new BitmapDrawable());
		carPopupWindow.setBackgroundDrawable(new BitmapDrawable());
		//namePopupWindow.setBackgroundDrawable(new BitmapDrawable());
		ListView listView = (ListView) view.findViewById(R.id.popup_order_info_list);
		adapter = new ArrayAdapter<String>(this, R.layout.select_driving_school_lv_item, R.id.select_driving_school_text);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
		if (timePopupWindow.isShowing()) {
			mOrderRoomInfoTime.setText(times.get(i));
			hourSelect = i + 1;
			timePopupWindow.dismiss();

		} else if (carPopupWindow.isShowing()) {

			mOrderRoomInfoCar.setText(carNameList.get(i));
			carPrice = carList.get(i).getPrice();
			System.out.println("价格 :" + carPrice);
			double priceCount = hourSelect * carPrice;
			DecimalFormat format = new DecimalFormat("#.00");
			price = format.format(priceCount);

			int j = carList.get(i).getGearType();
			if (j == 2) {
				mOrderRoomInfoRadio1.toggle();
			} else {
				mOrderRoomInfoRadio2.toggle();
			}
			carPopupWindow.dismiss();
			isCarSelect = true;
		}
		double priceCount = hourSelect * carPrice;
		DecimalFormat format = new DecimalFormat("#.00");
		price = format.format(priceCount);
		mOrderRoomInfoMoney.setText( price);
	}

	/**
	 * 接受返回数据
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (data != null && requestCode == 0x111 && resultCode == RESULT_OK) {
			securityId = data.getStringExtra("securityId");
			securityName = data.getStringExtra("securityName");
			mOrderRoomInfoSafety.setText(securityName);
		}
	}

	/**
	 * 获取车型列表
	 */
	public void getCarList() {
		RequestParams params = new RequestParams();
		params.put("ExaminationId", Utils.SPGetInt(this, Utils.currentExamRoomId, 1));
		MyHttp.getInstance(this).post(MyHttp.EXAM_CAR_LIST, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonObject :");
				KLog.json(Utils.TAG_DEBUG, response.toString());
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonArray :");
				KLog.json(Utils.TAG_DEBUG, response.toString());
				try {
					for (int i = 0; i < response.length(); i++) {
						JSONObject object = response.getJSONObject(i);
						CarInfoBean bean = new CarInfoBean();
						bean.setCarId(object.getInt("ExaminationPriceId"));
						bean.setExamRoomId(object.getInt("ExaminationId"));
						bean.setCarType(object.getString("CarType"));
						bean.setGearType(object.getInt("Gear"));
						bean.setPrice(object.getDouble("Price"));
						carNameList.add(bean.getCarType());
						carList.add(bean);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * 提交预约
	 * @param securityId
	 */
	public void getOrderExamInfo(String securityId) {
		if (mOrderRoomInfoRadio1.isChecked()) {
			carType = 2;
		} else {
			carType = 1;
		}
		/*if (mOrderRoomInfoRadio3.isChecked()) {
			if (mOrderRoomInfoSafety.getText().toString().trim() == null) {
				Utils.ToastShort(context,"请选择安全员");
			}

		}
		if (mOrderRoomInfoClock.getText().toString() == null || mOrderRoomInfoClock.getText().toString().equals("")) {
			Utils.ToastShort(context,"请选择预约时间");
		}*/
		date = getIntent().getStringExtra("date");
		/*Log.e("date",date);
		Log.e("PracticeHour",mOrderRoomInfoTime.getText().toString());
		Log.e("securityId",securityId + "");
		Log.e("Gear",carType+"");
		Log.e("CarType",mOrderRoomInfoCar.getText().toString());
		Log.e("currentExamRoomId",Utils.SPGetInt(context, Utils.currentExamRoomId, 0)+"");
		Log.e("userid",Utils.SPGetString(context, Utils.userId, "userId"));*/
		RequestParams params = new RequestParams();
		params.put("ExaminationId", Utils.SPGetInt(context, Utils.currentExamRoomId, 0));
		params.put("UserId", Utils.SPGetString(context, Utils.userId, "userId"));
		params.put("CarType", mOrderRoomInfoCar.getText().toString());
		params.put("Gear", carType);
		params.put("PracticeDate", date);
		params.put("PracticeHour", hourSelect);
		params.put("StartDate", date + " " + mOrderRoomInfoClock.getText().toString());
		params.put("SecurityOfficerId", securityId);

		Log.e("securityid", securityId + "+++++");
		KLog.e(Utils.TAG_DEBUG, params.toString());
		MyHttp.getInstance(context).post(MyHttp.LOOK_EXAM_ROOM, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
				try {
					if (response.getInt("Code") == 1) {
						String time = "0";
						for (int i = 0; i < s.length; i++) {
							if (hourSelect == (i + 1)) {
								SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
								try {
									Date nowDate = dateFormat.parse(mOrderRoomInfoClock.getText().toString());
									Date newDate = new Date(nowDate.getTime() + hourSelect * 60 * 60 * 1000);
									time = mOrderRoomInfoClock.getText().toString() + "-" + (new SimpleDateFormat("HH:mm").format(newDate));
								} catch (ParseException e) {
									e.printStackTrace();
								}
							}
						}
						Utils.SPutString(context, Utils.carType, mOrderRoomInfoCar.getText().toString());
						Utils.SPutString(context, Utils.yearAndDate, date);
						Utils.SPutString(context, Utils.orderTime, time);
						Log.e("zhangshuhua", mOrderRoomInfoCar.getText().toString() + date + time);
						Intent intent = new Intent(context, PayPageActivity.class);
						intent.putExtra("price", mOrderRoomInfoMoney.getText().toString());
						intent.putExtra("PracticeId", response.getInt("Result"));
						startActivity(intent);
					} else {
						//Utils.ToastShort(context, response.getString("Reason"));
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

		});
	}

	@Override
	public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
		if (b) {
			switch (compoundButton.getId()) {
				case R.id.order_room_info_radio_1:

					break;
				case R.id.order_room_info_radio_2:

					break;
				case R.id.order_room_info_radio_3:
					mOrderRoomInfoBtn3.setVisibility(View.VISIBLE);
					mOrderRoomInfoDivider.setVisibility(View.VISIBLE);
					break;
				case R.id.order_room_info_radio_4:
					mOrderRoomInfoBtn3.setVisibility(View.GONE);
					mOrderRoomInfoDivider.setVisibility(View.GONE);
					break;
			}
		}

	}

	/**
	 * 获取用户信息
	 */
	private void getHeaderName(){
		RequestParams params = new RequestParams();
		params.put("UserId" , Utils.SPGetString(this , Utils.userId , ""));
		KLog.e("userid",Utils.SPGetString(this , Utils.userId , ""));
		MyHttp.getInstance(this).post(MyHttp.GET_USER_HEADER, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG , response.toString());

				try {
					JSONObject object = response.getJSONObject(0);
					final String name = object.getString("NickName");
					final String logoUrl = object.getString("UserLogo");
					Utils.SPutString(OrderRoomInfoActivity.this , Utils.realName , name);
					Utils.SPutString(OrderRoomInfoActivity.this , Utils.userLogo , logoUrl);
					civLogo.post(new Runnable() {
						@Override
						public void run() {
							tvUserNickName.setText(name);
							ImageLoader.getInstance().displayImage(logoUrl ,civLogo , MyApplication.options_user);
						}
					});
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				KLog.e("errorcode　",statusCode + " " + responseString);

			}
		});


	}

}
