package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.jzkj.jianzhenkeji_road_car_person.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Administrator on 2016/3/27.
 */
public class ToCompanyActivity extends Activity {
	@InjectView(R.id.company_wv)
	WebView mCompanyWv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_to_company);
		ButterKnife.inject(this);
		WebSettings webSettings = mCompanyWv.getSettings();

		//设置加载显示的URL
		mCompanyWv.loadUrl("http://www.zgjzkj.net/");
		//支持JavaScript
		webSettings.setJavaScriptEnabled(true);
		webSettings.setSupportZoom(true);
		webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
		webSettings.setBuiltInZoomControls(true);//support zoom
		webSettings.setUseWideViewPort(true);// 这个很关键
		webSettings.setLoadWithOverviewMode(true);
		mCompanyWv.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
		mCompanyWv.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}
		});
	}

}
