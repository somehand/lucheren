package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.MyApplication;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Administrator on 2016/3/9 0009.
 */
public class SplashActivity extends Activity {

	@InjectView(R.id.splash_imageview)
	ImageView mSplashImageview;
	@InjectView(R.id.splash_textview)
	TextView mTextView;
	boolean showing = true;
	int currentSecond = 3;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		ButterKnife.inject(this);
		mHandler.sendEmptyMessageDelayed(1, 1000);
		mTextView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				/*Intent intent = new Intent(SplashActivity.this, GuideActivity.class);
				showing = false;
				startActivity(intent);
				finish();*/
				showing = false;
				mHandler.sendEmptyMessage(5);
			}
		});

	}


	Thread mThread = new Thread(new Runnable() {
		@Override
		public void run() {
			showing = true;
			try {
				while (showing) {

					mHandler.post(new Runnable() {
						@Override
						public void run() {
							mTextView.setText("跳过 " + currentSecond + " s");
						}
					});
					Thread.sleep(1000);
					currentSecond--;
					if (currentSecond <= 0) {

						showing = false;
						mHandler.sendEmptyMessage(5);
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	});

	Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
				case 1:
					ImageLoader.getInstance().displayImage(MyHttp.SPLASH_INFO, mSplashImageview, MyApplication.options_welcome,new ImageLoadingListener() {
						@Override
						public void onLoadingStarted(String s, View view) {

						}
						@Override
						public void onLoadingFailed(String s, View view, FailReason failReason) {
							sendEmptyMessageDelayed(5, 0);

						}

						@Override
						public void onLoadingComplete(String s, View view, Bitmap bitmap) {
							mTextView.setVisibility(View.VISIBLE);
							mThread.start();
						}

						@Override
						public void onLoadingCancelled(String s, View view) {

						}
					});
					break;
				case 5:
					mTextView.setText("跳过 0 s");
					Intent intent = new Intent(SplashActivity.this, GuideActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
					startActivity(intent);
					finish();
					break;
			}
		}
	};

	@Override
	protected void onRestart() {
		super.onRestart();
		if (showing == false) {
			Intent intent = new Intent(SplashActivity.this, GuideActivity.class);
			startActivity(intent);
			finish();
		}
	}


	@Override
	protected void onPause() {
		super.onPause();
		showing = false;

	}
}
