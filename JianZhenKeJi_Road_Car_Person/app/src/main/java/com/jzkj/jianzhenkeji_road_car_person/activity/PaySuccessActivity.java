package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.bean.RefreshEvent;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.ScreenUtils;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class PaySuccessActivity extends Activity {


	@InjectView(R.id.headframe_ib)
	ImageButton mHeadframeIb;
	@InjectView(R.id.headframe_title)
	TextView mHeadframeTitle;
	@InjectView(R.id.headframe_btn)
	Button mHeadframeBtn;
	@InjectView(R.id.headframe_layout)
	LinearLayout mHeadframeLayout;
	@InjectView(R.id.headframe_gray_line)
	TextView mHeadframeGrayLine;
	@InjectView(R.id.pay_ok_tvorder_number)
	TextView mPayOkTvorderNumber;
	@InjectView(R.id.pay_ok_tvpay_way)
	TextView mPayOkTvpayWay;
	@InjectView(R.id.pay_ok_tvexam_room)
	TextView mPayOkTvexamRoom;
	@InjectView(R.id.pay_ok_tvorder_car_type)
	TextView mPayOkTvorderCarType;
	@InjectView(R.id.pay_ok_tvorder_yeardate)
	TextView mPayOkTvorderYeardate;
	@InjectView(R.id.pay_ok_tvorder_time)
	TextView mPayOkTvorderTime;
	@InjectView(R.id.pay_ok_tvorder_telephone_number)
	TextView mPayOkTvorderTelephoneNumber;
	@InjectView(R.id.pay_ok_tvexam_room_address)
	TextView mPayOkTvexamRoomAddress;

	@InjectView(R.id.ll_end_show)
	LinearLayout llEndShow;
	@InjectView(R.id.ll_examinationname)
	LinearLayout llExaminationName;
	@InjectView(R.id.ll_cartype)
	LinearLayout llCarType;
	@InjectView(R.id.empty_view2)
	View mView2;
	@InjectView(R.id.empty_view1)
	View mView1;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pay_success);
		ButterKnife.inject(this);
		mHeadframeTitle.setText("支付成功");
		mPayOkTvorderNumber.setText(Utils.SPGetString(this, Utils.orderCode, ""));
		if (getIntent().getIntExtra("PAY_TYPE", 0) == 0) {
			mPayOkTvpayWay.setText("支付宝支付");
		}
		if (getIntent().getIntExtra("type", 3) == 1) {//充值
			llEndShow.setVisibility(View.GONE);
			llCarType.setVisibility(View.GONE);
			llExaminationName.setVisibility(View.GONE);
			mView1.setVisibility(View.VISIBLE);
			mView2.setVisibility(View.VISIBLE);
		} else if (getIntent().getIntExtra("type", 3) == 2) {//报名驾校
			llEndShow.setVisibility(View.GONE);
			llCarType.setVisibility(View.GONE);
			llExaminationName.setVisibility(View.GONE);
			mView1.setVisibility(View.VISIBLE);
			mView2.setVisibility(View.VISIBLE);
		} else {
			llEndShow.setVisibility(View.VISIBLE);
			llCarType.setVisibility(View.VISIBLE);
			llExaminationName.setVisibility(View.VISIBLE);
			mView1.setVisibility(View.GONE);
			mView2.setVisibility(View.GONE);
			getPaySuccess();
//			mPayOkTvexamRoom.setText(Utils.SPGetString(this, Utils.currentExamRoomName, ""));
//			mPayOkTvorderCarType.setText(Utils.SPGetString(this, Utils.carType, ""));
//			mPayOkTvorderYeardate.setText(Utils.SPGetString(this, Utils.yearAndDate, ""));
//			mPayOkTvorderTime.setText(Utils.SPGetString(this, Utils.orderTime, ""));
//			mPayOkTvorderTelephoneNumber.setText(Utils.SPGetString(this, Utils.userName, ""));
		}

		if (getIntent().getIntExtra("type", 3) == 2) {
			item = 0;
		} else if (getIntent().getIntExtra("type", 3) == 3) {
			item = 2;
		}
		mHeadframeIb.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				new AlertDialog.Builder(PaySuccessActivity.this).setMessage("是否保存截图?截图可作为凭证，直接合场.").setPositiveButton("确定", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						String bitmap = ScreenUtils.snapShotWithStatusBar(PaySuccessActivity.this);
						if (bitmap != null)
							Toast.makeText(PaySuccessActivity.this, "截图成功.存放在SD卡根目录,文件名:" + bitmap, Toast.LENGTH_SHORT).show();
						in();
					}
				}).setNegativeButton("取消", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						in();
					}
				}).show();

			}
		});
	}

	int item = 3;

	public void in() {
		Intent intent = new Intent(PaySuccessActivity.this, HomeActivity.class);
		EventBus.getDefault().post(new RefreshEvent());
		intent.putExtra("item", item);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
	}

	/**
	 * 获取支付成功后的数据
	 */
	private void getPaySuccess() {
		RequestParams params = new RequestParams();
		params.put("OrderId", Utils.SPGetString(this, Utils.orderCode, ""));
		KLog.e(params.toString());
		MyHttp.getInstance(PaySuccessActivity.this).post(MyHttp.PAY_SUCCESS_INFO, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
				try {
					JSONArray array = response.getJSONArray("Result");
					JSONObject object = array.getJSONObject(0);
					mPayOkTvorderNumber.setText(object.getString("OrderCode"));
					mPayOkTvexamRoom.setText(object.getString("kaochang"));
					if (object.getInt("Gear") == 1) {//0什么都不是  1手动  2自动
						mPayOkTvorderCarType.setText(object.getString("CarName") + "C1");
					} else if (object.getInt("Gear") == 2) {
						mPayOkTvorderCarType.setText(object.getString("CarName") + "C2");
					} else {
						mPayOkTvorderCarType.setText(object.getString("CarName") + "");
					}
					mPayOkTvorderYeardate.setText(object.getString("StartDate").split("\\ ")[0]);
					mPayOkTvorderTelephoneNumber.setText(object.getString("phone"));
					mPayOkTvexamRoomAddress.setText(object.getString("dizhi"));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}

}
