package com.jzkj.jianzhenkeji_road_car_person.util;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONObject;


/**
 * Created by Administrator on 2016/5/19.
 */
public class ErrorService extends Service {
	String appname;
	String appversion;
	String errorLog;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (intent.hasExtra("appname")) {
			appname = intent.getStringExtra("appname");
			appversion = intent.getStringExtra("appversion");
			errorLog = intent.getStringExtra("errorLog");
			RequestParams params = new RequestParams();
			params.put("AppName", appname);
			params.put("Appversion", appversion);
			params.put("Log", errorLog);
			Log.e("params", params.toString());
			MyHttp.getInstance(this).post(MyHttp.UPLOADLOG, params, new JsonHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
					super.onSuccess(statusCode, headers, response);
				}

			});
		}
		return super.onStartCommand(intent, flags, startId);
	}
}
