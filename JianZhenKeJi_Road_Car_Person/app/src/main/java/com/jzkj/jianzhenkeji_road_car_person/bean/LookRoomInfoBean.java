package com.jzkj.jianzhenkeji_road_car_person.bean;

/**
 * Created by Administrator on 2016/3/11.
 */
public class LookRoomInfoBean {
	private String examRoom;
	private String longTime;
	private String yearAndDate;
	private String timeClock;
	private String payMoney;
	private String payState;

	public String getExamRoom() {
		return examRoom;
	}

	public void setExamRoom(String examRoom) {
		this.examRoom = examRoom;
	}

	public String getLongTime() {
		return longTime;
	}

	public void setLongTime(String longTime) {
		this.longTime = longTime;
	}

	public String getYearAndDate() {
		return yearAndDate;
	}

	public void setYearAndDate(String yearAndDate) {
		this.yearAndDate = yearAndDate;
	}

	public String getTimeClock() {
		return timeClock;
	}

	public void setTimeClock(String timeClock) {
		this.timeClock = timeClock;
	}

	public String getPayMoney() {
		return payMoney;
	}

	public void setPayMoney(String payMoney) {
		this.payMoney = payMoney;
	}

	public String getPayState() {
		return payState;
	}

	public void setPayState(String payState) {
		this.payState = payState;
	}

	public LookRoomInfoBean(String examRoom, String longTime, String yearAndDate, String timeClock, String payMoney, String payState) {
		this.examRoom = examRoom;
		this.longTime = longTime;
		this.yearAndDate = yearAndDate;
		this.timeClock = timeClock;
		this.payMoney = payMoney;
		this.payState = payState;
	}
}
