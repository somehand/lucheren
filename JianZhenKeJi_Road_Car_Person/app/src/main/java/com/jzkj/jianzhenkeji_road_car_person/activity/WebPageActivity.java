package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.kanak.emptylayout.EmptyLayout;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class WebPageActivity extends Activity implements View.OnClickListener {

	@InjectView(R.id.webpage_wb)
	WebView mWebpageWb;
	@InjectView(R.id.headframe_ib)
	ImageButton mHeadframeIb;
	@InjectView(R.id.headframe_title)
	TextView mHeadframeTitle;
	@InjectView(R.id.heaframe_rb1)
	RadioButton mHeaframeRb1;
	@InjectView(R.id.headframe_rb2)
	RadioButton mHeadframeRb2;
	@InjectView(R.id.heaframe_rg)
	RadioGroup mHeaframeRg;

	private ProgressDialog dialog;
	private int type = 1;
	private EmptyLayout mEmptyLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_webpage);
		ButterKnife.inject(this);
		dialog = new ProgressDialog(WebPageActivity.this);
		mHeadframeIb.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});
		WebSettings webSettings = mWebpageWb.getSettings();

		type = getIntent().getIntExtra("type", 1);
		if (type == 3) {//banner跳转
			mWebpageWb.loadUrl(getIntent().getStringExtra("url"));
			mHeaframeRg.setVisibility(View.GONE);
			mHeadframeTitle.setVisibility(View.VISIBLE);
			mHeadframeTitle.setText(getIntent().getStringExtra("titleName"));
		} else if (type == 4) {//首页我的驾校跳转
			mWebpageWb.loadUrl(getIntent().getStringExtra("url"));
			mHeaframeRg.setVisibility(View.GONE);
			mHeadframeTitle.setVisibility(View.VISIBLE);
			mHeadframeTitle.setText(getIntent().getStringExtra("titleName"));
		} else if (type == 5) {//关于我们跳转
			mWebpageWb.loadUrl(getIntent().getStringExtra("url"));
			mHeaframeRg.setVisibility(View.GONE);
			mHeadframeTitle.setVisibility(View.VISIBLE);
			mHeadframeTitle.setText(getIntent().getStringExtra("titleName"));
		} else {
			mHeaframeRg.setVisibility(View.VISIBLE);
			mHeadframeTitle.setVisibility(View.GONE);
			showInfo();
		}
		mHeaframeRb1.setOnClickListener(this);
		mHeadframeRb2.setOnClickListener(this);

		//支持JavaScript
		webSettings.setJavaScriptEnabled(true);
		webSettings.setSupportZoom(true);
		webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
		webSettings.setBuiltInZoomControls(true);//support zoom
		webSettings.setUseWideViewPort(true);// 这个很关键
		webSettings.setLoadWithOverviewMode(true);
		mWebpageWb.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
		//设置WebView的切换方式
		mWebpageWb.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				super.onPageStarted(view, url, favicon);

				dialog.setMessage("正在加载中...");
				System.out.println("WebPageActivity.onPageStarted");
				dialog.setCanceledOnTouchOutside(true);
				if (!dialog.isShowing()) {
					dialog.show();
				}
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				System.out.println("WebPageActivity.onPageFinished");
				dialog.cancel();

			}

			@Override
			public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {

				// *** NEVER DO THIS!!! ***
				// super.onReceivedSslError(view, handler, error);

				// let's ignore ssl error
				 handler.proceed();
			}
		});
	}

	@Override
	public void onClick(View view) {
		mHeaframeRg.setVisibility(View.VISIBLE);
		mHeadframeTitle.setVisibility(View.GONE);
		switch (view.getId()) {
			case R.id.heaframe_rb1:
				if (mHeaframeRb1.isChecked()) {
					type = 1;
					showInfo();
				}
				break;
			case R.id.headframe_rb2:
				if (mHeadframeRb2.isChecked()) {
					type = 2;
					showInfo();
				}
				break;
		}
	}

	private void showInfo() {
		/*if (typ == 3) {
			mHeadframeTitle.setText(getIntent().getStringExtra("titleName"));
			mWebpageWb.loadUrl(getIntent().getStringExtra("url"));
			mHeaframeRg.setVisibility(View.GONE);
		} else if (typ == 2) {
//			mHeadframeTitle.setText(intent.getStringExtra("titleName"));
			//设置加载显示的URL
			mWebpageWb.loadUrl("cqjj.ggjfw.com:9688/Veh/drive/baseInfo.jsp");
			mHeaframeRg.setVisibility(View.VISIBLE);
			mHeadframeTitle.setVisibility(View.GONE);
		} else {
			mWebpageWb.loadUrl("https://cq.122.gov.cn/m/login/");
			mHeaframeRg.setVisibility(View.VISIBLE);
			mHeadframeTitle.setVisibility(View.GONE);
		}*/
		MyHttp.getInstance(WebPageActivity.this).post(MyHttp.GET_NET_INFO, null, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e("Debug", "JSONArray:");
				KLog.json(response.toString());
				try {
					JSONObject object1 = response.getJSONObject(0);
					JSONObject object2 = response.getJSONObject(1);
					mHeaframeRb1.setText(object1.getString("webname"));
					mHeadframeRb2.setText(object2.getString("webname"));
					if (type == 1) {
						mWebpageWb.loadUrl(object1.getString("weburl"));
					} else if (type == 2) {
						mWebpageWb.loadUrl(object2.getString("weburl"));
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}

			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e("Debug", "JSONObject:");
				KLog.json(response.toString());
//				mEmptyLayout = new EmptyLayout(WebPageActivity.this,mWebpageWb);
//				mEmptyLayout.setEmptyMessage("宝宝好伤心，没有数据了~");
//				mEmptyLayout.showEmpty();
			}
		});
	}
}
