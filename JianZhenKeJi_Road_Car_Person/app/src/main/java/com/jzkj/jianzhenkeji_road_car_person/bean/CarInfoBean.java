package com.jzkj.jianzhenkeji_road_car_person.bean;

/**
 * Created by Administrator on 2016/3/8 0008.
 */
public class CarInfoBean {
	int carId;
	int examRoomId;
	String carType;
	int gearType;
	double price;

	public int getCarId() {
		return carId;
	}

	public void setCarId(int carId) {
		this.carId = carId;
	}

	public int getExamRoomId() {
		return examRoomId;
	}

	public void setExamRoomId(int examRoomId) {
		this.examRoomId = examRoomId;
	}

	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public int getGearType() {
		return gearType;
	}

	public void setGearType(int gearType) {
		this.gearType = gearType;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
}
