package com.jzkj.jianzhenkeji_road_car_person.bean;

/**
 * Created by Administrator on 2016/5/13 0013.
 */
public class ChooseCoachBean {
	private String coachName;
	private String LicensePlate;
	private String coachAddress;
	private String coachLogo;
	private String coachId;

	public String getCoachName() {
		return coachName;
	}

	public void setCoachName(String coachName) {
		this.coachName = coachName;
	}

	public String getLicensePlate() {
		return LicensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		LicensePlate = licensePlate;
	}

	public String getCoachAddress() {
		return coachAddress;
	}

	public void setCoachAddress(String coachAddress) {
		this.coachAddress = coachAddress;
	}

	public String getCoachLogo() {
		return coachLogo;
	}

	public void setCoachLogo(String coachLogo) {
		this.coachLogo = coachLogo;
	}

	public String getCoachId() {
		return coachId;
	}

	public void setCoachId(String coachId) {
		this.coachId = coachId;
	}

	public ChooseCoachBean() {
	}
}
