package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.adapter.OrderDetailGvstudentAdapter;
import com.jzkj.jianzhenkeji_road_car_person.bean.OrderDetailGvstudentBean;
import com.jzkj.jianzhenkeji_road_car_person.bean.OrderNewsBean;
import com.jzkj.jianzhenkeji_road_car_person.util.MyGridView;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Administrator on 2016/3/21.
 */
public class MyOrderNewsDetailsActivity extends Activity implements View.OnClickListener {
	@InjectView(R.id.headframe_ib)
	ImageButton mHeadframeIb;
	@InjectView(R.id.headframe_title)
	TextView mHeadframeTitle;
	OrderNewsBean bean;

	String ExaminationId;
	@InjectView(R.id.order_detail_coach_name)
	TextView mOrderDetailCoachName;
	@InjectView(R.id.my_order_news_details2_caochname)
	LinearLayout mMyOrderNewsDetails2Caochname;
	@InjectView(R.id.order_detail_subject)
	TextView mOrderDetailSubject;
	@InjectView(R.id.order_detail_order_time)
	TextView mOrderDetailOrderTime;
	@InjectView(R.id.order_detail_ordermoney)
	TextView mOrderDetailOrdermoney;
	@InjectView(R.id.my_order_news_details2_btnpay)
	Button mMyOrderNewsDetails2Btnpay;

	String coachPhoneNum;
	@InjectView(R.id.order_detail_examroom)
	TextView mOrderDetailExamroom;
	@InjectView(R.id.order_detail_studentgv)
	MyGridView mOrderDetailStudentgv;
	/*@InjectView(R.id.mine_civ)
	CircleImageView mMineCiv;
	@InjectView(R.id.order_info_student_name)
	TextView mOrderInfoStudentName;
	@InjectView(R.id.order_info_date)
	TextView mOrderInfoDate;
	@InjectView(R.id.order_info_which_date)
	TextView mOrderInfoWhichDate;
	@InjectView(R.id.order_info_time)
	TextView mOrderInfoTime;
	@InjectView(R.id.order_info_subject)
	TextView mOrderInfoSubject;
	@InjectView(R.id.order_info_agree)
	Button mOrderInfoAgree;*/
	OrderDetailGvstudentAdapter mAdapter;
	ArrayList<OrderDetailGvstudentBean> list;
	String practiceId;
	String examRoomName;
	String price;
	String telephone;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_order_news_details2);
		ButterKnife.inject(this);
		mHeadframeTitle.setText("消息详情");
		list = new ArrayList<OrderDetailGvstudentBean>();
		bean = (OrderNewsBean) getIntent().getSerializableExtra("bean");
		mAdapter = new OrderDetailGvstudentAdapter(this, R.layout.order_detail_gvstudent_item);

//		getDetailid();
		initData();
		initListener();
		/*ImageLoader.getInstance().displayImage(bean.getHeaderUrl(), mMineCiv, MyApplication.options_user);
		mOrderInfoStudentName.setText(bean.getName());
		mOrderInfoTime.setText(bean.getTime());*/
		/*switch (bean.getIsAgree()){
			case 0:
				mOrderInfoAgree.setText("未处理");
				break;
			case 1:
				mOrderInfoAgree.setText("已同意");
				mOrderInfoAgree.setBackgroundColor(getResources().getColor(R.color.common_blue));

				break;
			case 2:
				mOrderInfoAgree.setText("已拒绝");
				mOrderInfoAgree.setBackgroundColor(getResources().getColor(R.color.red));

				break;
		}*/
		/*switch (bean.getSubject()) {
			case 1:
				mOrderInfoSubject.setText("科目一");
				break;
			case 2:
				mOrderInfoSubject.setText("科目二");
				break;
			case 3:
				mOrderInfoSubject.setText("科目三");
				break;
			case 4:
				mOrderInfoSubject.setText("科目四");
				break;
		}*/
	}

	private void initData() {
//		getDetail();
//		getOrderedStudent();
		getAllLookRoomInfo();

	}
	/**
	 * 标记已读
	 *//*
	private void makeRead() {
		RequestParams params = new RequestParams();
		params.put("messagetype", 2);
		params.put("type", 2);
		params.put("pageindex", 1);
		params.put("pagesize", 20);
		params.put("userid", Utils.SPGetString(MyOrderNewsDetailsActivity.this, Utils.userId, ""));
		KLog.e("params", params.toString());
		MyHttp.getInstance(this).post(MyHttp.GET_MYNEWS_INFO, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());

			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				KLog.e(responseString);
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
				super.onFailure(statusCode, headers, throwable, errorResponse);
				KLog.e(errorResponse.toString());
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
				super.onFailure(statusCode, headers, throwable, errorResponse);
				KLog.e(errorResponse.toString());
			}


		});
	}*/

	/**
	 * 获取消息详情
	 */
	private void getAllLookRoomInfo() {
		RequestParams params = new RequestParams();
		params.put("ExaminationId", bean.getNewsId());
		KLog.e("debug", params.toString());
		MyHttp.getInstance(MyOrderNewsDetailsActivity.this).post(MyHttp.GET_ALL_INFO, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json("debug", response.toString());
				try {
					if (response.getInt("Code") == 0) {
						Toast.makeText(MyOrderNewsDetailsActivity.this, response.getString("Reason"), Toast.LENGTH_SHORT).show();
						return;
					}
					JSONArray array = response.getJSONArray("Result");
					JSONObject objectTop = array.getJSONObject(0);
					examRoomName = objectTop.getString("ExaminationName");
					mOrderDetailOrderTime.setText(objectTop.getString("PracticeDate").split("\\ ")[0]);
					price = objectTop.getString("PracticePrice");
					if ("2".equals(objectTop.getString("subject"))) {
						mOrderDetailSubject.setText("科目二");
					} else {
						mOrderDetailSubject.setText("科目三");
					}
					mOrderDetailExamroom.setText(examRoomName);
					mOrderDetailOrdermoney.setText(price + "元");
					mOrderDetailCoachName.setText(objectTop.getString("RealName"));
					JSONArray arrayStudents = response.getJSONArray("Result2");
					for (int i = 0; i < arrayStudents.length(); i++) {
						JSONObject objectStudent = arrayStudents.getJSONObject(i);
						OrderDetailGvstudentBean bean = new OrderDetailGvstudentBean();
						bean.setImg(objectStudent.getString("UserLogo"));
						bean.setStudentName(objectStudent.getString("RealName"));
						bean.setPracticeId(objectStudent.getString("PracticeId"));
						bean.setIsPay(objectStudent.getInt("IsPay"));
						if (objectStudent.getString("UserName").equals(Utils.SPGetString(MyOrderNewsDetailsActivity.this, Utils.userName, ""))) {
							practiceId = objectStudent.getString("PracticeId");
							KLog.e("id+++++++", practiceId + "------------------------");
						}
						if (objectStudent.getInt("IsPay") == 2) {
							mMyOrderNewsDetails2Btnpay.setText("已支付");
							mMyOrderNewsDetails2Btnpay.setEnabled(false);
							mMyOrderNewsDetails2Btnpay.setBackgroundResource(R.drawable.look_room_details_gray_btnbg2);
						}
						list.add(bean);
					}
					mOrderDetailStudentgv.setAdapter(mAdapter);
					mAdapter.addAll(list);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				KLog.e("onfail");
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
				super.onFailure(statusCode, headers, throwable, errorResponse);
				KLog.json("errorResponse");
			}


		});
	}

	private void initListener() {
		mMyOrderNewsDetails2Caochname.setOnClickListener(this);
		mMyOrderNewsDetails2Btnpay.setOnClickListener(this);
		mHeadframeIb.setOnClickListener(this);
	}

	/**
	 * 获取消息详情需要的ID
	 */
	public void getDetailid() {
		RequestParams params = new RequestParams();
		params.put("messagetype", 1);
		params.put("type", 2);
		params.put("messageid", bean.getNewsId());
		params.put("Userid", Utils.SPGetString(MyOrderNewsDetailsActivity.this, Utils.userId, ""));
		KLog.e("params", params.toString());
		MyHttp.getInstance(this).post(MyHttp.GET_MYNEWS_INFO, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonObject :");
				KLog.json(Utils.TAG_DEBUG, response.toString());
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonArray :");
				KLog.json(Utils.TAG_DEBUG, response.toString());
				try {
					JSONObject object = response.getJSONObject(0);
					ExaminationId = object.getString("ExaminationId");

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

		});
	}

	/**
	 * 获取消息详情  pass
	 */
	public void getDetail() {
		RequestParams params = new RequestParams();
		params.put("examinationid", /*bean.getNewsId()*/3);
//		params.put("type",2);
//		params.put("messageid",bean.getNewsId());
//		params.put("Userid", Utils.SPGetString(MyOrderNewsDetailsActivity.this,Utils.userId,""));
		KLog.e("params", params.toString());
		MyHttp.getInstance(this).post(MyHttp.GET_MSGDETAIL_INFO, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonObject :");
				KLog.json(Utils.TAG_DEBUG, response.toString());
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonArray :");
				KLog.json(Utils.TAG_DEBUG, response.toString());

				JSONObject object = null;
				try {
					object = response.getJSONObject(0);
					price = object.getString("PracticePrice");
					examRoomName = object.getString("ExaminationName");
					mOrderDetailCoachName.setText(object.getString("RealName"));
					telephone = object.getString("UserName");
					if ("2".equals(object.getString("subject"))) {
						mOrderDetailSubject.setText("科目二");
					} else {
						mOrderDetailSubject.setText("科目三");
					}
					mOrderDetailOrderTime.setText(object.getString("PracticeDate"));
					mOrderDetailExamroom.setText(examRoomName);
					mOrderDetailOrdermoney.setText(price + "元");
					KLog.e("****************", practiceId + price + examRoomName);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

		});
	}

	/**
	 * 获取已有学员   pass
	 */
	private void getOrderedStudent() {
		RequestParams params = new RequestParams();
		params.put("ExaminationId", /*bean.getNewsId()*/3);
		KLog.e("params", params.toString());
		MyHttp.getInstance(MyOrderNewsDetailsActivity.this).post(MyHttp.GET_ORDEREDSTUDENT_INFO, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonObject :");
				KLog.json(Utils.TAG_DEBUG, response.toString());
				try {
					JSONArray array = response.getJSONArray("Result");
					for (int i = 0; i < array.length(); i++) {
						JSONObject object = array.getJSONObject(i);
						OrderDetailGvstudentBean bean = new OrderDetailGvstudentBean();
						bean.setImg(object.getString("UserLogo"));
						bean.setStudentName(object.getString("RealName"));
						bean.setPracticeId(object.getString("PracticeId"));
						list.add(bean);
					}
					mOrderDetailStudentgv.setAdapter(mAdapter);
					mAdapter.addAll(list);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonArray :");
				KLog.json(Utils.TAG_DEBUG, response.toString());
			}
		});
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.headframe_ib:
				finish();
				break;
			case R.id.my_order_news_details2_caochname:
				Intent intentcall = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + telephone));
				intentcall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intentcall);
				break;
			case R.id.my_order_news_details2_btnpay:
				Intent intent = new Intent(MyOrderNewsDetailsActivity.this, PayPageActivity.class);
				intent.putExtra("price", price + "");
				intent.putExtra("orderName", examRoomName);
				intent.putExtra("PracticeId", practiceId);
				intent.putExtra("type", 3);
				startActivity(intent);
				break;
		}
	}
}
