package com.jzkj.jianzhenkeji_road_car_person.adapter;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.bean.OrderCoachBean;
import com.jzkj.jianzhenkeji_road_car_person.fragment.NewOrderCoachFragment;

import java.util.Date;


public class MyGridAdapter extends QuickAdapter<OrderCoachBean> {
	private OrderCoachBean item;
	private NewOrderCoachFragment orderCoachFragment;

	public MyGridAdapter(Context context, int layoutResId, NewOrderCoachFragment orderCoachFragment) {
		super(context, layoutResId);
		this.orderCoachFragment = orderCoachFragment;
	}

	View view = null;
	LinearLayout liner_item;

	@Override
	protected void convert(final BaseAdapterHelper helper, OrderCoachBean item) {
		liner_item = helper.getView(R.id.liner_item);
		TextView title = helper.getView(R.id.title);
		TextView yiyuenum = helper.getView(R.id.yiyuenum);
		TextView keyuenum = helper.getView(R.id.keyuenum);
		TextView TeachType = helper.getView(R.id.TeachType);
		TextView carPlate = helper.getView(R.id.carplate);
		TextView date = helper.getView(R.id.date);
		title.setText(item.getSubjectNum());
		carPlate.setText(item.getCarNum());
		yiyuenum.setText("已约" + item.getYiyuenum() + "人");
		keyuenum.setText("可约" + item.getKeyuenum() + "人");
		TeachType.setText(item.getTeachType());
		date.setText(item.getStartStopDate());
		if (!compareDate(item.getStartStopDate()) && orderCoachFragment.getButpostion() == 1) {
			liner_item.setBackgroundResource(R.drawable.grid_bg);
			title.setText(item.getSubjectNum() + "(已过期)");
			liner_item.setOnClickListener(null);
		} else {
			if (item.getIsBooking() != 0) {
				liner_item.setBackgroundResource(R.drawable.grid_bg2);
				title.setText(item.getSubjectNum() + "(已预约)");
				liner_item.setOnClickListener(new Click(item.getCoachBookingsDateId()));
			} else if (item.getKeyuenum() == 0) {
				liner_item.setBackgroundResource(R.drawable.grid_bg1);
				title.setText(item.getSubjectNum() + "(已满)");
				liner_item.setOnClickListener(null);
			} else if (item.getIsAppointemt() == 2) {
				liner_item.setBackgroundResource(R.drawable.grid_bg1);
				title.setText(item.getSubjectNum() + "(不可预约)");
				liner_item.setOnClickListener(null);
			} else if ("2".equals(item.getIsyue())) {//只可以看，灰色显示
				liner_item.setBackgroundResource(R.drawable.grid_bg);
				title.setText(item.getSubjectNum());
				liner_item.setOnClickListener(null);
			} else {
				liner_item.setBackgroundResource(R.drawable.grid_bg3);
			}
		}
//		liner_item.setOnClickListener(new girdItemclick());
//		final LinearLayout linearLayout = helper.getView(R.id.liner_item);
//		title.setText(item.getSubjectNum());
//		date.setText(item.getStartStopDate());
//		detail.setText(item.getDetail());
//		if(item.getType()==4||item.getType()==3){
//		}else if(item.getType()==1){
//			linearLayout.setBackgroundResource(R.drawable.grid_bg1);
//			helper.getView().setOnClickListener(null);
//		}else if(item.getType()==2){
//			linearLayout.setBackgroundResource(R.drawable.grid_bg);
//			helper.getView().setOnClickListener(null);
//		}else{
//			linearLayout.setBackgroundResource(R.drawable.grid_bg2);
//			helper.getView().setOnClickListener(null);
//		}
	}

	//比较时间计算是否过期
	public boolean compareDate(String resultDate) {
		int hour = Integer.parseInt(resultDate.substring(0, 2));
		int min = Integer.parseInt(resultDate.substring(3, 5));
//		Log.d("test", "hour:" + hour + ",min:" + min);
		int syshour = new Date().getHours();
		int sysmin = new Date().getMinutes();
//		Log.d("test", "hour:" + hour + ",min:" + min + "syshour:" + syshour + ",sysmin:" + sysmin);

//		if(hour<syshour)
//			return false;
//		else if(min<sysmin)
//			return false;
		Date mydate = new Date();
		mydate.setHours(hour);
		mydate.setMinutes(min);

		if (mydate.getTime() < new Date().getTime()) {
			return false;
		}
		return true;
	}

	class Click implements View.OnClickListener {
		private String mCoachBookingsDateId;

		public Click(String coachBookingsDateId) {
			mCoachBookingsDateId = coachBookingsDateId;
		}

		@Override
		public void onClick(View view) {
			orderCoachFragment.clickBooking(mCoachBookingsDateId);

		}
	}
}
