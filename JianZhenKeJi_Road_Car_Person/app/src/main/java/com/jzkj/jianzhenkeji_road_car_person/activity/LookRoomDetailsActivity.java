package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class LookRoomDetailsActivity extends Activity implements View.OnClickListener {

	@InjectView(R.id.headframe_ib)
	ImageButton mHeadframeIb;
	@InjectView(R.id.headframe_title)
	TextView mHeadframeTitle;
	@InjectView(R.id.look_room_details_examname)
	TextView mLookRoomDetailsExamname;
	@InjectView(R.id.look_room_details_ordertime)
	TextView mLookRoomDetailsOrdertime;
	@InjectView(R.id.look_room_details_cartype)
	TextView mLookRoomDetailsCartype;
	@InjectView(R.id.look_room_details_autotype)
	TextView mLookRoomDetailsAutotype;
	@InjectView(R.id.look_room_details_safetyname)
	TextView mLookRoomDetailsSafetyname;
	@InjectView(R.id.look_room_details_paymoney)
	TextView mLookRoomDetailsPaymoney;
	@InjectView(R.id.look_room_details_btn_state)
	Button mLookRoomDetailsBtnState;

	private String practiceId;

	private String payMoney;
	private String orderName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_look_room_details);
		ButterKnife.inject(this);
		mHeadframeTitle.setText("合场详情");
		practiceId = getIntent().getStringExtra("practiceid");
		getLookRoomDetailsInfo();
		bindListener();
	}


	private void bindListener() {
		mHeadframeIb.setOnClickListener(this);
		mLookRoomDetailsBtnState.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.headframe_ib:
				finish();
				break;
			case R.id.look_room_details_btn_state:
				if (TextUtils.isEmpty(payMoney)) {
					Utils.ToastShort(LookRoomDetailsActivity.this, "金额不能为空");
				} else {
					Intent intent = new Intent(this, PayPageActivity.class);
//				intent.putExtra("userId",userId);
					intent.putExtra("price", payMoney);
					intent.putExtra("orderName", orderName);
					intent.putExtra("PracticeId", practiceId);
					intent.putExtra("type", 3);
					intent.putExtra(Utils.toPayPageid_3, 3);
					startActivity(intent);
					this.finish();
				}
				break;
			default:
				break;
		}
	}

	private void getLookRoomDetailsInfo() {
		RequestParams params = new RequestParams();
		params.put("PracticeId", practiceId);
		KLog.e(params.toString());
		MyHttp.getInstance(this).post(MyHttp.LOOK_ROOM_DETAILS, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
				try {
					Utils.ToastShort(LookRoomDetailsActivity.this, response.getString("Reason"));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
				try {
					JSONObject object = response.getJSONObject(0);
					orderName = object.getString("ExaminationName");
					mLookRoomDetailsExamname.setText(orderName);
					mLookRoomDetailsOrdertime.setText(object.getInt("PracticeHour") + "小时");
					mLookRoomDetailsCartype.setText(object.getString("CarType"));
					if (object.getInt("Gear") == 1) {//1.自动 2.手动
						mLookRoomDetailsAutotype.setText("自动");
					} else {
						mLookRoomDetailsAutotype.setText("手动");
					}
					if (object.getString("OfficerName").equals("") || object.getString("OfficerName") == "") {
						mLookRoomDetailsSafetyname.setText("无");
					} else {
						mLookRoomDetailsSafetyname.setText(object.getString("OfficerName"));
					}
					Utils.SPutString(LookRoomDetailsActivity.this, Utils.currentExamRoomName, orderName);
					Utils.SPutString(LookRoomDetailsActivity.this, Utils.carType, object.getString("CarType"));
					Utils.SPutString(LookRoomDetailsActivity.this, Utils.yearAndDate, object.getString("StartDate"));
					Utils.SPutString(LookRoomDetailsActivity.this, Utils.orderTime, object.getString("PracticeHour"));
					String moneyStr = object.getString("PayMoney");
					if (moneyStr.equals("") || moneyStr == "") {
						mLookRoomDetailsPaymoney.setText(moneyStr);
					} else {
						String[] money = moneyStr.split("\\.");
						for (int i = 0; i < money.length; i++) {
							if (i == 0) {
								mLookRoomDetailsPaymoney.setText(money[0] + ".00元");
								payMoney = money[0] + ".00";
							}
						}
					}
					if (/*object.getInt("IsPay") == 1*/"未支付".equals(String.valueOf(object.getString("IsPay")))) {//1.未支付 2.已支付 3.已退款
						mLookRoomDetailsBtnState.setText(object.getString("IsPay"));
						mLookRoomDetailsBtnState.setBackgroundResource(R.drawable.order_detail_btn_img);
						mLookRoomDetailsBtnState.setEnabled(true);
					} else if (/*object.getInt("IsPay") == 2*/"已支付".equals(String.valueOf(object.getString("IsPay")))) {
						mLookRoomDetailsBtnState.setText(object.getString("IsPay"));
						mLookRoomDetailsBtnState.setBackgroundResource(R.drawable.look_room_details_gray_btnbg2);
						mLookRoomDetailsBtnState.setEnabled(false);
					} else {
						mLookRoomDetailsBtnState.setText("已退款");
						mLookRoomDetailsBtnState.setBackgroundResource(R.drawable.look_room_details_gray_btnbg2);
						mLookRoomDetailsBtnState.setEnabled(false);
					}
					payMoney = moneyStr;

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}
}
