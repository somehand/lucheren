package com.jzkj.jianzhenkeji_road_car_person.bean;

/**
 * Created by Administrator on 2016/5/12.
 * IconImgUrl		类型  string		图标点击之后的广告图片地址
 IconUrl			类型  string		图标点击之后的跳转地址
 AdvUrl			类型  string		广告图片点击之后跳转的地址
 Number			类型  int			图标顺序的编号（先左边从上往下分别是,2,3,4  再右边从上往下分别是,6,7,8）
 */
public class ADBean {
    private int number;
    private String AdvUrl;
    private String IconUrl;
    private int Id;
    private String IconImgUrl;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getAdvUrl() {
        return AdvUrl;
    }

    public void setAdvUrl(String advUrl) {
        AdvUrl = advUrl;
    }

    public String getIconUrl() {
        return IconUrl;
    }

    public void setIconUrl(String iconUrl) {
        IconUrl = iconUrl;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getIconImgUrl() {
        return IconImgUrl;
    }

    public void setIconImgUrl(String iconImgUrl) {
        IconImgUrl = iconImgUrl;
    }

    @Override
    public String toString() {
        return "number:"+number+"\nAdvUrl:"+AdvUrl+"\nIconImgUrl:"+IconImgUrl+"\nIconUrl:"+IconUrl;
    }
}
