package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.BaseActivity;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import pl.droidsonroids.gif.GifImageView;

public class FeedBackActivity extends BaseActivity implements View.OnClickListener {
	private ImageButton ibBack;
	private TextView tvFinish;
	private EditText etTextInfo;
	GifImageView mGifImageView;

	private String textInfo;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.feedback);

		ibBack = (ImageButton) findViewById(R.id.feedback_back);
		etTextInfo = (EditText) findViewById(R.id.feed_back_info);
		tvFinish = (TextView) findViewById(R.id.feedback_finish);

		mGifImageView = (GifImageView) findViewById(R.id.feedback_gif);

		bindListener();

		/*MyHttp.getInstance(this).get("http://p1.so.qhimg.com/t01bdd6de93fc06e631.gif", new BinaryHttpResponseHandler() {
			@Override
			public void onSuccess(int i, Header[] headers, byte[] bytes) {
				ByteBuffer buffer = ByteBuffer.wrap(bytes);
				try {
					GifDrawable gifDrawable = new GifDrawable(bytes);
					mGifImageView.setImageDrawable(gifDrawable);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {

			}
		});*/
		/*MediaController controller = new MediaController(FeedBackActivity.this);
		controller.setMediaPlayer((GifDrawable) mGifImageView.getDrawable());
		controller.setAnchorView(mGifImageView);
		controller.show();*/

		//mGifImageView.setImageResource(R.drawable.gif);
	}

	private void bindListener() {
		ibBack.setOnClickListener(this);
		tvFinish.setOnClickListener(this);
	}

	private void submitFeedBacInfo() {
		RequestParams params = new RequestParams();
		params.put("UserId", Utils.SPGetString(this,Utils.userId,"userid"));
		params.put("Content",textInfo);
		params.put("Terminaltype",1);
		MyHttp.getInstance(FeedBackActivity.this).post(MyHttp.FEED_BACK_INFO,params,new JsonHttpResponseHandler(){
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
				try {
					Utils.ToastShort(FeedBackActivity.this,response.getString("Reason"));
					FeedBackActivity.this.finish();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.feedback_back:
				finish();
				break;
			case R.id.feedback_finish:
				textInfo = etTextInfo.getText().toString().trim();
				char[] a = textInfo.toCharArray();
				if (textInfo.equals("") || textInfo == "") {
					Utils.ToastShort(this,"意见反馈不能为空");
				}else if (a.length < 10) {
					Utils.ToastShort(this,"反馈内容不少于10个字");
				}else{
					submitFeedBacInfo();
				}
				break;
		}
	}
}
