package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.adapter.ApplyDrivingSchoolAdapter;
import com.jzkj.jianzhenkeji_road_car_person.bean.ApplyDrivingSchoolBean;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Administrator on 2016/5/10 0010.
 */
public class ApplyDrivingSchoolActivity extends Activity {

	@InjectView(R.id.headframe_ib)
	ImageButton mHeadframeIb;
	@InjectView(R.id.headframe_title)
	TextView tvTitle;
	@InjectView(R.id.apply_driving_school_lv)
	PullToRefreshListView mApplyDrivingSchoolLv;

	ListView lv;
	ArrayList<ApplyDrivingSchoolBean> list;
	ApplyDrivingSchoolAdapter adapter;
	int nowPage = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_apply_driving_school);
		ButterKnife.inject(this);
		initView();
		initListener();
	}

	private void initListener() {
		mHeadframeIb.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});
		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				ApplyDrivingSchoolBean bean = (ApplyDrivingSchoolBean) adapterView.getAdapter().getItem(i);
				Intent intent = new Intent(ApplyDrivingSchoolActivity.this, MyDrivingHomepageActivity.class);
				intent.putExtra("bean", bean);
				startActivity(intent);
			}
		});
	}

	private void initView() {
		tvTitle.setText("报名驾校");
		list = new ArrayList<ApplyDrivingSchoolBean>();
		getDrivingSchoolInfo(this);
		mApplyDrivingSchoolLv.setMode(PullToRefreshBase.Mode.BOTH);
		lv = mApplyDrivingSchoolLv.getRefreshableView();
		mApplyDrivingSchoolLv.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
				String label = DateUtils.formatDateTime(ApplyDrivingSchoolActivity.this, System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_ALL);
				refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);
				nowPage = 1;
				getDrivingSchoolInfo(ApplyDrivingSchoolActivity.this);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
				String label = DateUtils.formatDateTime(ApplyDrivingSchoolActivity.this, System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_ALL);
				refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);
				nowPage++;
				getDrivingSchoolInfo(ApplyDrivingSchoolActivity.this);
			}
		});
	}

	ProgressDialog mDialog;
	private void getDrivingSchoolInfo(final Context context) {
		mDialog = new ProgressDialog(this);
		mDialog.setMessage("正在加载中...");
		mDialog.setIndeterminate(true);
		mDialog.setCanceledOnTouchOutside(false);
		mDialog.show();
		RequestParams params = new RequestParams();
		params.put("nowpage", nowPage);
		params.put("Pagesize", 10);
		KLog.e("params", params.toString());
		MyHttp.getInstance(context).post(MyHttp.APPLY_DRIVING_SCHOOL_INFO, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
				if (nowPage == 1) {
					list.clear();
				}
				try {
					if (nowPage != 1 && TextUtils.isEmpty(response.getString("Result"))) {
						Utils.ToastShort(ApplyDrivingSchoolActivity.this, "当前已是最后一页!");
					} else {
						JSONArray array = response.getJSONArray("Result");
						for (int i = 0; i < array.length(); i++) {
							JSONObject object = array.getJSONObject(i);
							ApplyDrivingSchoolBean bean = new ApplyDrivingSchoolBean();
							bean.setSchoolUrl(object.getString("SchoolUrl"));
							bean.setPrice(object.getString("Price"));
							bean.setSchoolAddress(object.getString("SchoolAddress"));
							bean.setSchoolLogo(object.getString("SchoolLogo"));
							bean.setSchoolName(object.getString("SchoolName"));
							bean.setStar(object.getInt("star"));
							bean.setIsRecommend(object.getInt("IsRecommend"));
							bean.setSchoolProfiles(object.getString("SchoolProfiles"));
							bean.setSchoolPhone(object.getString("SchoolPhone"));
							bean.setDrivingSchoolId(object.getString("DrivingSchoolId"));
							list.add(bean);
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				adapter = new ApplyDrivingSchoolAdapter(context, R.layout.activity_apply_driving_school_item);
				lv.setAdapter(adapter);
				adapter.addAll(list);
				mApplyDrivingSchoolLv.postDelayed(new Runnable() {
					@Override
					public void run() {
						if (mApplyDrivingSchoolLv != null) {
							mApplyDrivingSchoolLv.onRefreshComplete();
							mDialog.dismiss();
						}
					}
				}, 1000);
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				mApplyDrivingSchoolLv.postDelayed(new Runnable() {
					@Override
					public void run() {
						mApplyDrivingSchoolLv.onRefreshComplete();
					}
				}, 1000);
				mDialog.dismiss();

			}
		});
	}
}
