package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.MyApplication;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.bean.RefreshEvent;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Administrator on 2016/4/6.
 */
public class WareDetailActivity extends Activity implements View.OnClickListener {
	@InjectView(R.id.headframe_ib)
	ImageButton mHeadframeIb;
	@InjectView(R.id.headframe_title)
	TextView mHeadframeTitle;
	@InjectView(R.id.ware_detail_headimg)
	ImageView mWareDetailHeadimg;
	@InjectView(R.id.textView6)
	TextView mTextView6;
	@InjectView(R.id.ware_detail_goodsname)
	TextView goodsname;
	@InjectView(R.id.ware_detail_btnexchange)
	Button mWareDetailBtnexchange;

	String goodsid;
	String goodsName;
	String price;
	String pic;
	String integ;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ware_detail);
		ButterKnife.inject(this);
		init();
		initData();
		initListener();
	}

	private void initListener() {
		mHeadframeIb.setOnClickListener(this);
		mWareDetailBtnexchange.setOnClickListener(this);
	}

	private void initData() {
		getStoreInfo();
	}

	private void init() {
		mHeadframeTitle.setText("商品详情");
		goodsid = getIntent().getStringExtra("id");
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.headframe_ib:
				finish();
				break;
			case R.id.ware_detail_btnexchange:
				isBuy();
				break;
			default:
				break;
		}
	}

	private void isBuy() {
		AlertDialog.Builder builder = new AlertDialog.Builder(WareDetailActivity.this);
		builder.setMessage("是否换购商品？");
		builder.setNegativeButton("否", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {
				dialogInterface.dismiss();
			}
		});
		builder.setPositiveButton("是", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {
				toExchangeGoods();
//				WareDetailActivity.this.finish();
			}
		});
		AlertDialog alertDialog = builder.create();
		alertDialog.show();
	}

	private void toExchangeGoods() {
		RequestParams params = new RequestParams();
		params.put("userid", Utils.SPGetString(WareDetailActivity.this, Utils.userId, ""));
		params.put("goodsid", goodsid);
		params.put("goodsname", goodsName);
		params.put("price", price);
		params.put("pic", pic);
		params.put("integral", integ);
		KLog.e("params", params.toString());
		MyHttp.getInstance(WareDetailActivity.this).post(MyHttp.EXCHANGEGOODS_INFO, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
				try {
					if (response.getInt("Code") == 1) {
						Utils.ToastShort(WareDetailActivity.this, response.getString("Reason"));
						EventBus.getDefault().post(new RefreshEvent());
						WareDetailActivity.this.finish();
					} else {
						Utils.ToastShort(WareDetailActivity.this, response.getString("Reason"));
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}


	private void getStoreInfo() {
		RequestParams params = new RequestParams();
		params.put("type", 2);
		params.put("goodsid", goodsid);
		KLog.e("Debug", params.toString());
		MyHttp.getInstance(this).post(MyHttp.GET_STORE_INFO, params, new JsonHttpResponseHandler() {

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e("Debug", "JSONArray:");
				KLog.json(response.toString());
				try {
					JSONObject object = response.getJSONObject(0);
					goodsName = object.getString("GoodsName");
					integ = object.getString("Integral");
					goodsname.setText(goodsName);
					ImageLoader.getInstance().displayImage(object.getString("Picdetail"), mWareDetailHeadimg, MyApplication.options_other);
					mTextView6.setText(integ);
					price = object.getString("Price");
					pic = object.getString("pic");
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e("Debug","JSONObject:");
				KLog.json("debug" , response.toString());
				try {
					if(response.getInt("Code") == 0){
						AlertDialog dialog = new AlertDialog.Builder(WareDetailActivity.this).
								setMessage("对不起 , 您;当前浏览的商品已下架!").setTitle("提示").create();
						dialog.show();
						dialog.setCanceledOnTouchOutside(false);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}
}
