package com.jzkj.jianzhenkeji_road_car_person.bean;

/**
 * Created by Administrator on 2016/4/6.
 * 预约教练列表实体
 *   CoachBookingsDateId	String	可约时间id
     StartStopDate	String	时间段
     SubjectNum	Int	科目
     yiyuenum	int	已经预约
     keyuenum	Int	可预约
        isBooking 是否预约
 isAppointemt 是否可预约
 carNum车牌号
 isyue  string  //1.可以预约，2.不可以预约显示成灰色
 */
public class OrderCoachBean {
    String SubjectNum;
    String StartStopDate;
    String CoachBookingsDateId;
    String TeachType;
    String isyue;
    String carNum;
    int yiyuenum;
    int keyuenum;
    int isBooking;
    int isAppointemt;

    public String getCarNum() {
        return carNum;
    }

    public void setCarNum(String carNum) {
        this.carNum = carNum;
    }

    public String getIsyue() {
        return isyue;
    }

    public void setIsyue(String isyue) {
        this.isyue = isyue;
    }

    public String getTeachType() {
        return TeachType;
    }

    public void setTeachType(String teachType) {
        TeachType = teachType;
    }

    public int getIsAppointemt() {
        return isAppointemt;
    }

    public void setIsAppointemt(int isAppointemt) {
        this.isAppointemt = isAppointemt;
    }

    public int getIsBooking() {
        return isBooking;
    }

    public void setIsBooking(int isBooking) {
        this.isBooking = isBooking;
    }

    public String getSubjectNum() {
        return SubjectNum;
    }

    public void setSubjectNum(String subjectNum) {
        SubjectNum = subjectNum;
    }

    public String getStartStopDate() {
        return StartStopDate;
    }

    public void setStartStopDate(String startStopDate) {
        StartStopDate = startStopDate;
    }

    public String getCoachBookingsDateId() {
        return CoachBookingsDateId;
    }

    public void setCoachBookingsDateId(String coachBookingsDateId) {
        CoachBookingsDateId = coachBookingsDateId;
    }

    public int getYiyuenum() {
        return yiyuenum;
    }

    public void setYiyuenum(int yiyuenum) {
        this.yiyuenum = yiyuenum;
    }

    public int getKeyuenum() {
        return keyuenum;
    }

    public void setKeyuenum(int keyuenum) {
        this.keyuenum = keyuenum;
    }
}