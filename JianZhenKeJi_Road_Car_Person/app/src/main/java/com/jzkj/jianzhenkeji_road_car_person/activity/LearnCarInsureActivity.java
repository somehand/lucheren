package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Administrator on 2016/5/11 0011.
 */
public class LearnCarInsureActivity extends Activity {
	@InjectView(R.id.headframe_ib)
	ImageButton mHeadframeIb;
	@InjectView(R.id.headframe_title)
	TextView mHeadframeTitle;
	@InjectView(R.id.btn_next_commit)
	Button mBtnNextCommit;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_learn_car_insure);
		ButterKnife.inject(this);
		initView();
		initListener();
	}

	private void initListener() {
		mHeadframeIb.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});
		mBtnNextCommit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(LearnCarInsureActivity.this,InsureDetailsActivity.class);
				startActivity(intent);
			}
		});
	}

	private void initView() {
		mHeadframeTitle.setText("学车保险");
	}
}
