package com.jzkj.jianzhenkeji_road_car_person.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.CircleImageView;
import com.jzkj.jianzhenkeji_road_car_person.MyApplication;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.bean.MyCoachBean;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Administrator on 2016/4/6.
 */
public class MyCoachAdapter extends QuickAdapter<MyCoachBean> {
	public MyCoachAdapter(Context context, int layoutResId) {
		super(context, layoutResId);
	}

	@Override
	protected void convert(BaseAdapterHelper helper, MyCoachBean item) {
		CircleImageView img = helper.getView(R.id.my_coach_lv_item_headimg);
		TextView studentName = helper.getView(R.id.my_coach_lv_item_name);
		TextView evaluate = helper.getView(R.id.my_coach_lv_item_evaluate);
		TextView time = helper.getView(R.id.my_coach_lv_item_time);
		TextView explain = helper.getView(R.id.my_coach_lv_item_explains);
		ImageLoader.getInstance().displayImage(item.getStudentLogo(), img, MyApplication.options_user);
		studentName.setText(item.getStudentName());
		evaluate.setText(item.getEvaluated());
		time.setText(item.getCommentDate());
		explain.setText(item.getContents());
		evaluate.setVisibility(View.GONE);
		if ("好评".equals(String.valueOf(item.getEvaluated()))) {
			evaluate.setTextColor(Color.parseColor("#ec7206"));
		} else if ("中评".equals(String.valueOf(item.getEvaluated()))) {
			evaluate.setTextColor(Color.parseColor("#0d9b02"));
		} else {
			evaluate.setTextColor(Color.parseColor("#999999"));
		}
		/*img.setImageResource(item.getImg());
		studentName.setText(item.getName());
		evaluate.setText(item.getEvaluate());
		time.setText(item.getTime());
		explain.setText(item.getExplain());*/
	}
}
