package com.jzkj.jianzhenkeji_road_car_person.util;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.PersistentCookieStore;

public class MyHttp {

	public static AsyncHttpClient getInstance(Context context) {
		AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
		asyncHttpClient.setTimeout(10 * 1000);
		PersistentCookieStore cookieStore = new PersistentCookieStore(context);
		asyncHttpClient.setCookieStore(cookieStore);
		return asyncHttpClient;
	}

	//	public static  String SERVER_ADD = "http://wx.chanhoudaojia.com/RecoveryService";
	public static final String SERVER_TEST = "http://183.230.133.205:17997/";
	public static final String SERVER_OFFICIAL = "http://www.lucheren.net:8081/";

	public static String getServer() {
//		if (BuildConfig.DEBUG) {
//			return SERVER_TEST;
//		}
		return SERVER_OFFICIAL;
	}

	/**
	 * 首页驾校名称和banner图
	 */
	public static String DRIVINGSCHOOL_BANNER = getServer() + "User/IndexInfo.ashx";

	/**
	 * 获取随机模拟题 科目一
	 */
	public static String QUESTION_CLASS_1 = "http://api2.juheapi" +
			".com/jztk/query?subject=1&model=c1&key=8f5c64eb001a5b104c13173c382fa6a0&testType=rand";

	/**
	 * 获取随机模拟题 科目四
	 */
	public static String QUESTION_CLASS_4 = "http://api2.juheapi" +
			".com/jztk/query?subject=4&model=c1&key=8f5c64eb001a5b104c13173c382fa6a0&testType=rand";


	//############################  注册与登录  ##############################
	/**
	 * 获取注册验证码
	 */
	public static String GET_CODE = getServer() + "User/InsertSms.ashx";

	/**
	 * 获取修改、找回密码验证码
	 */
	public static String GET_CODE_MODIDY = getServer() + "User/BackPassWordSms.ashx";

	/**
	 * 找回密码
	 */
	public static String RTV_PASSWORD = getServer() + "User/BackPassWord.ashx";

	/**
	 * 修改资料
	 */
	public static String MODIFY_DATA = getServer() + "User/UpdateData.ashx";

	/**
	 * 修改密码
	 */
	public static String MODIFY_PASSWORD = getServer() + "User/UpdatePassWord.ashx";

	/**
	 * 注册
	 */
	public static String REGIST_USER = getServer() + "User/InsertUser.ashx";

	/**
	 * 用户登录
	 */
	public static String USER_LOGIN = getServer() + "User/UserLogin.ashx";

	//############################  首页  ##############################

	/**
	 * 获取轮播图 和 驾校名
	 */
	public static String GET_BANNER = getServer() + "Index/IndexInfo.ashx";

	/**
	 * 获取首页推荐文章
	 */
	public static String GET_HOME_ARTICLE = getServer() + "Article/IndexArticle.ashx";

	/**
	 * 获取单个科目对应的文章列表
	 */
	public static String GET_CLASS_ARTICLE = getServer() + "Article/ArticleForSubject.ashx";


	//############################  个人页  ##############################
	/**
	 * 上传头像
	 */
	public static String UPLOAD_HEADER = getServer() + "User/UpdateLogo.ashx";

	/**
	 * 我的预约
	 */
	public static String GET_MY_ORDER = getServer() + "User/MyAppointment.ashx";

	/**
	 * 收藏列表
	 */
	public static String GET_MY_COLLECT = getServer() + "Article/CollectionArticle.ashx";

	/**
	 * 判断文章是否收藏
	 */
	public static String COLLECT_JUDGE = getServer() + "Article/IsCollection.ashx";

	/**
	 * 收藏文章
	 */
	public static String COLLECT = getServer() + "Article/InsertCollection.ashx";
	/**
	 * 取消收藏
	 */
	public static String COLLECT_CANCEL = getServer() + "Article/DeleteConllection.ashx";

	/**
	 * 安全员信息
	 */
	public static String GET_SAFETY_INFO = getServer() + "Examination/SecurityOfficer.ashx";
	/**
	 * 查询已预约时段id
	 */
	public static String ORDER_COACH_LIST_TIME = getServer() + "Reservation/IsAppointment.ashx";


	/**
	 * 查询所有可预约时段
	 */
	public static String ORDER_COACH_LIST = getServer() + "Reservation/CoachDate.ashx";

	/**
	 * 预约教练
	 */
	public static String ORDER_COACH = getServer() + "Reservation/Appointment.ashx";

	/**
	 * 点击量增加
	 */
	public static String ARTICLE_READNUM_ADD = getServer() + "Article/ClickNum.ashx";

	/**
	 * 获取考场列表
	 */
	public static String EXAM_ROOM_LIST = getServer() + "Examination/ExaminationName.ashx";

	/**
	 * 获取获取车辆列表
	 */
	public static String EXAM_CAR_LIST = getServer() + "Examination/AppointmentInfo.ashx";

	/**
	 * 支付接口
	 */
	public static String PAY_AFTER = getServer() + "Pay/PayLog.ashx";

	/**
	 * 意见反馈
	 */
	public static String FEED_BACK_INFO = getServer() + "User/Feedback.ashx";


	/**
	 * 预约合场
	 */
	public static String LOOK_EXAM_ROOM = getServer() + "Examination/Practice.ashx";

	/**
	 * 获取用户信息
	 */
	public static String GET_USER_HEADER = getServer() + "User/GetUserInfo.ashx";

	/**
	 * 我的预约合场
	 */
	public static String LOOK_ROOM = getServer() + "User/MyExamination.ashx";

	/**
	 * 我的预约合场详情
	 */
	public static String LOOK_ROOM_DETAILS = getServer() + "User/ExaminationInfo.ashx";

	/**
	 * 评价安全员
	 */
	public static String EVALUATE_SECURITY = getServer() + "User/Comment.ashx";

	/**
	 * 我的统计
	 */
	public static String MY_EXAM_COUNT = getServer() + "User/Statistics.ashx";
	/**
	 * 我的预约消息详情
	 */
	public static String MY_ORDER_NEWS_DETAIL = getServer() + "Examination/PushExamination.ashx";
	/**
	 * 我的预约消息列表
	 */
	public static String MY_ORDER_NEWS_LIST = getServer() + "User/MyExaminationList.ashx";

	/**
	 * 系统消息
	 */
	public static String GET_SYSTEM_NEWS = getServer() + "Msg/MyMsg.ashx";

	/**
	 * 学员预约教练成功后发推送消息
	 */
	public static String ORDER_SEND_JPUSH = getServer() + "JPush/JPushSend.ashx";

	/**
	 * 预约教练评论
	 */
	public static String EVALUATE_COACH = getServer() + "Examination/StudentComment.ashx";

	/**
	 * 更新版本信息
	 */
	public static String VERSION_INFO = getServer() + "";

	/**
	 * 获取驾校列表
	 */
	public static String GET_SCHOOL = getServer() + "User/Choose.ashx";
	/**
	 * 搜索驾校
	 */
	public static String SEARCH_SCHOOL = getServer() + "User/QueryName.ashx";
	/**
	 * 搜索驾校
	 */
	public static String BIND_SCHOOL = getServer() + "User/UpdateSchool.ashx";


	/**
	 * 我的教练
	 */
	public static String GET_COACH_INFO = getServer() + "User/MyCoach.ashx";

	/**
	 * 积分商城
	 */
	public static String GET_STORE_INFO = getServer() + "Goods/GoodsLists.ashx";

	/**
	 * 积分记录
	 */
	public static String GET_SCORERECORD_INFO = getServer() + "User/IntegralRecord.ashx";

	/**
	 * 兑换记录
	 */
	public static String GET_EXCHANGERECORD_INFO = getServer() + "User/ExchangeRecord.ashx";

	/**
	 * 充值
	 */
	public static String GET_RECHARGE_INFO = getServer() + "Pay/PayLog.ashx";

	/**
	 * 预约列表
	 */
	public static String GET_ORDER_INFO = getServer() + "User/MyAppointment.ashx";

	/**
	 * 我的消息
	 */
	public static String GET_MYNEWS_INFO = getServer() + "Msg/MyMsg.ashx";

	/**
	 * 评价
	 */
	public static String GET_EVALUATE_INFO = getServer() + "Comment/AddComment.ashx";

	/**
	 * 我的驾校
	 */
	public static String GET_DRIVING_INFO = getServer() + "User/MyDrivingSchool.ashx";

	/**
	 * 我的名片（当前用户积分）
	 */
	public static String GET_CURRENT_INFO = getServer() + "User/GetUserInfo.ashx";

	/**
	 * 兑换商品
	 */
	public static String EXCHANGEGOODS_INFO = getServer() + "Goods/ExchangeGoods.ashx";

	/**
	 * 团购消息详情
	 */
	public static String GET_MSGDETAIL_INFO = getServer() + "Msg/MsgDetail.ashx";

	/**
	 * 获取已有学员
	 */
	public static String GET_ORDEREDSTUDENT_INFO = getServer() + "ReleaseTrial_CC/Rc_PracticeUserByExaminationId.ashx";

	/**
	 * 获取团购消息列表
	 */
	public static String GET_GROUPBUY_INFO = getServer() + "Msg/MyTuanGouMsg.ashx";
	/**
	 * 每日任务
	 */
	public static String GET_DailyTask = getServer() + "User/DailyTask.ashx";

	/**
	 * 获取团购详情信息
	 */
	public static String GET_ALL_INFO = getServer() + "NewReleaseTrial_CC/SelectReleaseTrial_CC.ashx";

	/**
	 * 预约考试，网站信息
	 */
	public static String GET_NET_INFO = getServer() + "Examination/AppointmentKS.ashx";

	/**
	 * 得到考场的安全员信息
	 */
	public static String GET_SECURITY_INFO = getServer() + "TryField_CC/RC_SecurityOfficerByExaminationId.ashx";
	/**
	 * 分享连接
	 */
	public static String SHARE_URL = getServer() + "SharePage/CoachShare/index.html?coachid=";

	/**
	 * 取得预约详情
	 */
	public static String GET_ORDER_DETAIL = getServer() + "V114_ZC/NewAppointment.ashx";

	/**
	 * 获取splash图片
	 */
	public static String SPLASH_INFO = getServer() + "images/start/1.png";

	/**
	 * 取教练的名字 头像
	 */
	public static String GET_MY_COACH = getServer() + "User/MyCoach.ashx";


	/**
	 * //取得教练的预约列表
	 */
	public static String GET_MY_COACH_DATE = getServer() + "V114_ZC/NewCoachDate.ashx";

	/**
	 * //提交预约
	 */
	public static String COMMIT_ORDER = getServer() + "V114_ZC/NewAppointment.ashx";

	/**
	 * //得到考场的车辆信息
	 */
	public static String GET_EXAMINATION_CARS = getServer() + "TryField_CC/RC_ExaminationById.ashx";

	/**
	 * //得到考场列表
	 */
	public static String GET_ROOM_LIST = getServer() + "RC_ExaminationBySubject_CC/RC_ExaminationBySubject_CC.ashx";

	/**
	 * //预约考场
	 */
	public static String ORDER_ROOM_PAY = getServer() + "TryField_CC/AddRC_Practice.ashx";

	/**
	 * //支付成功
	 */
	public static String PAY_SUCCESS_INFO = getServer() + "V1.1.0_CC/PaymentSuccess.ashx";
	/**
	 * 获取新版code 和下载地址
	 */
	public static String GET_UPDATE = getServer() + "V1.1.0_CC/RC_AndroidV.ashx";

	/**
	 * //报名驾校列表
	 */
	public static String APPLY_DRIVING_SCHOOL_INFO = getServer() + "V1.1.0_CC/DrivingSchoolList.ashx";


	/**
	 * 广告
	 */
	public static String RC_Advertisement = getServer() + "V1.1.0_CC/RC_Advertisement.ashx";

	/**
	 * 填写报名信息
	 */
	public static String WRITE_APPLY_INFO = getServer() + "V1.1.0_CC/StuSignUp_CC.ashx";

	/**
	 * 保险
	 */
	public static String BaoXian = getServer() + "V1.1.0_CC/BaoXian.ashx";

	/**
	 * 获取教练列表
	 */
	public static String GET_COACHINFO = getServer() + "V1.1.0_CC/CoachList.ashx";

	/**
	 * 绑定教练
	 */
	public static String BIND_COACH = getServer() + "V1.1.0_CC/StuBindingCoach.ashx";
	/**
	 * 指定没有轮播时显示的图片
	 */
	public static String STABLE_BANNER = getServer() + "images/banner/fac8454f-ffbe-4d18-be9e-b433c22a91d0.png";

	/**
	 * 车联网介绍
	 */
	public static String USER_AGREEMENT = getServer() + "ZhouDi/UserAgr/index.html";

	/**
	 * 上传错误日志
	 */
	public static String UPLOADLOG = "http://183.230.133.205:17998/ashx/addlog.ashx";

	/**
	 * 签到
	 */
	public static String SIGN_IN = getServer() + "V111_CC/RC_Sign_CC.ashx";

}
