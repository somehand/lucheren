package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.bean.SecurityBean;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class LookRoomDetails1Activity extends Activity implements View.OnClickListener {

	@InjectView(R.id.headframe_ib)
	ImageButton mHeadframeIb;
	@InjectView(R.id.headframe_title)
	TextView mHeadframeTitle;
	@InjectView(R.id.look_room_details_examname)
	TextView mLookRoomDetailsExamname;
	@InjectView(R.id.look_room_details_ordertime)
	TextView mLookRoomDetailsOrdertime;
	@InjectView(R.id.look_room_details_cartype)
	TextView mLookRoomDetailsCartype;
	@InjectView(R.id.look_room_details_paymoney)
	TextView mLookRoomDetailsPaymoney;
	@InjectView(R.id.look_room_details_btn_state)
	Button mLookRoomDetailsBtnState;
	@InjectView(R.id.per)
	TextView sec_tv;
	@InjectView(R.id.safety_rl)
	RelativeLayout safety;
	private String practiceId;

	private String payMoney;
	private String orderName;
	private String type;
	private String currentName;
	private String cartype;
	private String longtime;
	private String roomtime;
	private SecurityBean securityBean;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_look_room_details1);
		ButterKnife.inject(this);
		mHeadframeTitle.setText("合场详情");
		practiceId = getIntent().getStringExtra("PracticeId");
		type = getIntent().getStringExtra("type");
		currentName = getIntent().getStringExtra("currentName");
		cartype = getIntent().getStringExtra("cartype");
		longtime = getIntent().getStringExtra("longtime");
		roomtime = getIntent().getStringExtra("roomtime");
		payMoney = getIntent().getStringExtra("price");
		securityBean = (SecurityBean) getIntent().getSerializableExtra("sec");
		mLookRoomDetailsExamname.setText(currentName);
		mLookRoomDetailsOrdertime.setText(roomtime + "|" + longtime);
		mLookRoomDetailsCartype.setText(cartype);
		mLookRoomDetailsPaymoney.setText(payMoney + "元");
		if (securityBean == null)
			safety.setVisibility(View.GONE);
		else {
			safety.setVisibility(View.VISIBLE);
			sec_tv.setText(securityBean.getOfficerName());
		}
		bindListener();
	}


	private void bindListener() {
		mHeadframeIb.setOnClickListener(this);
		mLookRoomDetailsBtnState.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.headframe_ib:
				finish();
				break;
			case R.id.look_room_details_btn_state:
				if (TextUtils.isEmpty(payMoney)) {
					Utils.ToastShort(LookRoomDetails1Activity.this, "金额不能为空");
				} else {
					Intent intent = new Intent(this, PayPageActivity.class);
//				intent.putExtra("userId",userId);
					intent.putExtra("price", payMoney);
					intent.putExtra("orderName", currentName + " " + longtime);
					intent.putExtra("PracticeId", practiceId);
					intent.putExtra("type", 3);
					intent.putExtra(Utils.toPayPageid_3, 1);
					startActivity(intent);
					this.finish();
				}
				break;
			default:
				break;
		}
	}
}
