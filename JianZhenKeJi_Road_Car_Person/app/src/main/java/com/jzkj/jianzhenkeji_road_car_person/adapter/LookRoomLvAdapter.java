package com.jzkj.jianzhenkeji_road_car_person.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.activity.LookRoomDetailsActivity;
import com.jzkj.jianzhenkeji_road_car_person.activity.MyEvaluateActivity;
import com.jzkj.jianzhenkeji_road_car_person.bean.LookRoomBean;

/**
 * Created by Administrator on 2016/3/11.
 */
public class LookRoomLvAdapter extends QuickAdapter<LookRoomBean>{
	public LookRoomLvAdapter(Context context, int layoutResId) {
		super(context, layoutResId);
	}

	@Override
	protected void convert(BaseAdapterHelper helper, LookRoomBean item) {
		TextView tvExamName = helper.getView(R.id.look_room_examname);
		TextView tvLongTime = helper.getView(R.id.look_room_longtime);
		TextView tvYearAndDate = helper.getView(R.id.look_room_year_ordate);
		TextView tvTimeClock = helper.getView(R.id.look_room_timeclock);
		TextView tvPayMoney = helper.getView(R.id.look_room_paymoney);
		TextView tvPayState = helper.getView(R.id.look_room_paystate);
		Button btnLookDetails = helper.getView(R.id.look_room_lookdetails);
		TextView evaluate = helper.getView(R.id.look_room_evaluate);

		tvExamName.setText(item.getExaminationName());
		String[] str = item.getStartDate().split("\\ ");
		tvLongTime.setText(str[0]+")");
		tvPayState.setText(item.getIsPay());
//		tvPayMoney.setText(item.getPayMoney());
//		tvYearAndDate.setText(item.get());
//		tvTimeClock.setText(item.getTimeClock());
		if (item.getPayMoney() == null || item.getPayMoney().equals("")) {
			tvPayMoney.setText(item.getPayMoney());
		}else {
			String[] money = item.getPayMoney().split("\\.");
			for (int i = 0; i < money.length; i++) {
				if (i == 0) {
					tvPayMoney.setText(money[0]/*+".00"*/+"元");
				}
			}
		}

		if (item.getIsPay().equals("已支付") || item.getIsPay() == "已支付") {
			tvPayState.setTextColor(context.getResources().getColor(R.color.globalframe_rb_gray));
		}else{
			tvPayState.setTextColor(context.getResources().getColor(R.color.globalframe_rb_blue));
		}
		/*if (item.getIsPay() == 1) {

			evaluate.setVisibility(View.GONE);

		} else if (item.getIsPay() == 2) {
			tvPayState.setText("已支付");
		}else{
			evaluate.setVisibility(View.GONE);

			tvPayState.setText("已退款");
		}*/
		btnLookDetails.setOnClickListener(new MyListener(item));
		evaluate.setOnClickListener(new MyListener(item));

		evaluate.setVisibility(View.GONE);
		/*if(item.getOfficerCommentId() != 0 || item.getSecurityName().length() == 0){
			evaluate.setVisibility(View.GONE);
		}*/

	}

	class MyListener implements View.OnClickListener {
		LookRoomBean item;
		public MyListener(LookRoomBean item) {
			this.item = item;
		}

		@Override
		public void onClick(View view) {
			switch (view.getId()){
				case R.id.look_room_lookdetails:
					Intent intent = new Intent(context, LookRoomDetailsActivity.class);
					intent.putExtra("practiceid",item.getPracticeId());
					intent.putExtra("Ordername",item.getExaminationName());
					intent.putExtra("price",item.getPayMoney());
					intent.putExtra("type",3);
					context.startActivity(intent);
					break;
				case R.id.look_room_evaluate:
					Intent intent1 = new Intent(context , MyEvaluateActivity.class);
					intent1.putExtra("PracticeId",item.getPracticeId());
					intent1.putExtra("SecurityId" , item.getSecurityId());
					intent1.putExtra("SecurityName" , item.getSecurityName());
					intent1.putExtra("SecuritySign", item.getSecuritySign());
					intent1.putExtra("SecurityLogo" , item.getSecurityLogo());
					context.startActivity(intent1);
					break;
			}


		}
	}
}
