package com.jzkj.jianzhenkeji_road_car_person.bean;

/**
 * Created by Administrator on 2016/3/14.
 */
public class LookRoomBean {
	private String PracticeId;
	private String ExaminationName;
	private String StartDate;
	private String PayMoney;
	private String IsPay;

	private String SecurityName;
	private int SecurityId;
	private String SecuritySign;
	private String SecurityLogo;
	private int OfficerCommentId;

	public int getOfficerCommentId() {
		return OfficerCommentId;
	}

	public void setOfficerCommentId(int officerCommentId) {
		OfficerCommentId = officerCommentId;
	}

	public String getSecurityLogo() {
		return SecurityLogo;
	}

	public void setSecurityLogo(String securityLogo) {
		SecurityLogo = securityLogo;
	}

	public String getSecurityName() {
		return SecurityName;
	}

	public void setSecurityName(String securityName) {
		SecurityName = securityName;
	}

	public int getSecurityId() {
		return SecurityId;
	}

	public void setSecurityId(int securityId) {
		SecurityId = securityId;
	}

	public String getSecuritySign() {
		return SecuritySign;
	}

	public void setSecuritySign(String securitySign) {
		SecuritySign = securitySign;
	}

	public String getPracticeId() {
		return PracticeId;
	}

	public void setPracticeId(String practiceId) {
		PracticeId = practiceId;
	}

	public String getExaminationName() {
		return ExaminationName;
	}

	public void setExaminationName(String examinationName) {
		ExaminationName = examinationName;
	}

	public String getStartDate() {
		return StartDate;
	}

	public void setStartDate(String startDate) {
		StartDate = startDate;
	}

	public String getPayMoney() {
		return PayMoney;
	}

	public void setPayMoney(String payMoney) {
		PayMoney = payMoney;
	}

	public String getIsPay() {
		return IsPay;
	}

	public void setIsPay(String isPay) {
		IsPay = isPay;
	}

	public LookRoomBean() {
	}
}
