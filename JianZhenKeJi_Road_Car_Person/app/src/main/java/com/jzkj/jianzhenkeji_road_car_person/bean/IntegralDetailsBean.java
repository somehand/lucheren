package com.jzkj.jianzhenkeji_road_car_person.bean;

/**
 * Created by Administrator on 2016/4/8.
 */
public class IntegralDetailsBean {
	private String id;
	private String typeDetail;
	private String score;
	private String time;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTypeDetail() {
		return typeDetail;
	}

	public void setTypeDetail(String typeDetail) {
		this.typeDetail = typeDetail;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public IntegralDetailsBean() {
	}
}
