package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.BaseActivity;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.MyMd5;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Administrator on 2016/2/26.
 */
public class ForgetPasswordActivity extends BaseActivity implements View.OnClickListener {
	private ImageButton ibBack;
	private TextView tvTitle, tvGetCode;
	private EditText etMobile, etNewPw, etSurePw, etCode;
	private Button btnSubmit;
	private int code;
	private String mobile;
	private String pw;
	private Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.forget_password);

		init();
		initData();
		bindListener();

	}

	/**
	 * 设置监听
	 */
	private void bindListener() {
		ibBack.setOnClickListener(this);
		tvGetCode.setOnClickListener(this);
		btnSubmit.setOnClickListener(this);
	}

	/**
	 * 获取数据
	 */
	private void initData() {

	}

	/**
	 * 初始化绑定ID
	 */
	private void init() {
		ibBack = (ImageButton) findViewById(R.id.headframe_ib);
		tvTitle = (TextView) findViewById(R.id.headframe_title);
		tvGetCode = (TextView) findViewById(R.id.modify_pw_tv_getcode);
		etCode = (EditText) findViewById(R.id.modify_pw_etcode);
		etMobile = (EditText) findViewById(R.id.modify_pw_etmobile);
		etNewPw = (EditText) findViewById(R.id.modify_pw_et_newpw);
		etSurePw = (EditText) findViewById(R.id.modify_pw_et_surepw);
		btnSubmit = (Button) findViewById(R.id.modify_pw_btn_submit);
		tvTitle.setText("忘记密码");
		context = ForgetPasswordActivity.this;
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.headframe_ib:
				finish();
				break;
			case R.id.modify_pw_tv_getcode:
				mobile = etMobile.getText().toString().trim();
				if (TextUtils.isEmpty(mobile)) {
					Utils.ToastShort(context, "请输入手机号");
				} else {
					getCode();
					MyCount count = new MyCount(60000, 1000);
					count.start();
				}
				break;
			case R.id.modify_pw_btn_submit:
				pw = etNewPw.getText().toString().trim();
				String surePw = etSurePw.getText().toString().trim();
				String userCode = etCode.getText().toString().trim();
				if (TextUtils.isEmpty(userCode)) {
					Utils.ToastShort(context, "请获取验证码");
				} else if (TextUtils.isEmpty(pw) || TextUtils.isEmpty(surePw)) {
					Utils.ToastShort(context, "密码或确认密码不能为空");
				} else if (pw.length() < 6 || surePw.length() < 6) {
					Utils.ToastShort(context, "密码至少为6位");
				} else if (!pw.equals(surePw)) {
					Utils.ToastShort(context, "两次输入的密码不一致");
				} else if (!TextUtils.isEmpty(userCode) && userCode.equals(String.valueOf(code))) {
					getRtvPassword();
					Intent intent = new Intent(context, LoginActivity.class);
					startActivity(intent);
					finish();
				} else {
					Utils.ToastShort(context, "验证码错误");
				}
				break;
			default:
				break;
		}
	}

	/**
	 * 找回密码
	 */
	private void getRtvPassword() {
		String md5Pw = MyMd5.getMd5(pw);
		RequestParams params = new RequestParams();
		params.put("Mobile", mobile);
		params.put("PassWord", md5Pw);
		Log.e("params", params.toString());
		MyHttp.getInstance(context).post(MyHttp.RTV_PASSWORD, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
				try {
					if (response.getInt("Code") == 1) {//0失败   1成功
						Utils.ToastShort(context, response.getString("Reason"));
					} else {
						Utils.ToastShort(context, response.getString("Reason"));
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * 获取验证码
	 */
	private void getCode() {
		RequestParams params = new RequestParams();
		Log.e("zhangshuhua", mobile);
		params.put("Mobile", mobile);
		MyHttp.getInstance(context).post(MyHttp.GET_CODE_MODIDY, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
				try {
					if (response.getInt("Code") == 1) {
						Utils.ToastShort(context, response.getString("Reason"));
						code = response.getInt("Result");
					} else {
						Utils.ToastShort(context, response.getString("Reason"));
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * 倒计时（内部类）
	 */
	class MyCount extends CountDownTimer {

		public MyCount(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}

		@Override
		public void onTick(long l) {
			tvGetCode.setText((l / 1000) + "s");
			tvGetCode.setTextColor(getResources().getColor(R.color.regist_hintcolor));
			tvGetCode.setEnabled(false);
		}

		@Override
		public void onFinish() {
			tvGetCode.setText("获取验证码");
			tvGetCode.setEnabled(true);
			tvGetCode.setTextColor(getResources().getColor(R.color.regist_gaincodecolor));
		}
	}

}
