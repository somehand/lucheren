package com.jzkj.jianzhenkeji_road_car_person.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/3/7 0007.
 */
public class MonthDayBean implements Serializable{
	int month;
	int day;

	public MonthDayBean(int month, int day) {
		this.month = month;
		this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}
}
