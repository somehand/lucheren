package com.jzkj.jianzhenkeji_road_car_person.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.CircleImageView;
import com.jzkj.jianzhenkeji_road_car_person.MyApplication;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.bean.OrderNewsBean;
import com.nostra13.universalimageloader.core.ImageLoader;


/**
 * Created by Administrator on 2016/3/16.
 */
public class OrderNewsAdapter extends QuickAdapter<OrderNewsBean> {
	public OrderNewsAdapter(Context context, int layoutResId) {
		super(context, layoutResId);
	}

	@Override
	protected void convert(BaseAdapterHelper helper, OrderNewsBean item) {
//		TextView tvTime = helper.getView(R.id.order_details_item_times);
		TextView name = helper.getView(R.id.order_details_item_studentname);
		TextView tvSubject = helper.getView(R.id.order_details_item_subjectnum);
		CircleImageView header = helper.getView(R.id.mine_civ);
		TextView tag = helper.getView(R.id.text_tag);
		tag.setVisibility(View.GONE);
		if(item.getTypes() == 3){
			if(item.getStatus() == 0)
				tag.setVisibility(View.VISIBLE);

		}
		/*switch (item.getStatus()){
			case 0:
				tag.setVisibility(View.VISIBLE);
				break;
			case 1:
				tag.setVisibility(View.GONE);
				break;
		}*/

//		if (item.getTypes() == 1) {
//			name.setText(item.getTime());
//		}else{
		name.setText(item.getTime());
//		}
		tvSubject.setText(item.getContent());
//		tvTime.setText(item.getTime());


		/*switch (item.getSubject()){
			case 1:
				tvSubject.setText("科目一");
				break;
			case 2:
				tvSubject.setText("科目二");

				break;
			case 3:
				tvSubject.setText("科目三");

				break;
			case 4:
				tvSubject.setText("科目四");

				break;
		}*/
		ImageLoader.getInstance().displayImage(item.getHeaderUrl(), header, MyApplication.options_user);


	}
}
