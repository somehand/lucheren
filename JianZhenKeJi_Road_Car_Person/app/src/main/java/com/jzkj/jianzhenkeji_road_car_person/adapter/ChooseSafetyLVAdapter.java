package com.jzkj.jianzhenkeji_road_car_person.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.CircleImageView;
import com.jzkj.jianzhenkeji_road_car_person.MyApplication;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.activity.OrderRoomInfoActivity;
import com.jzkj.jianzhenkeji_road_car_person.bean.SecurityBean;
import com.nostra13.universalimageloader.core.ImageLoader;

public class ChooseSafetyLVAdapter extends QuickAdapter<SecurityBean> {

	public ChooseSafetyLVAdapter(Context context, int layoutResId) {
		super(context, layoutResId);
	}

	@Override
	protected void convert(BaseAdapterHelper helper, SecurityBean item) {
		TextView tvName = helper.getView(R.id.choose_safety_item_name);
		TextView tvSign = helper.getView(R.id.choose_safety_item_sign);
		CircleImageView civHead = helper.getView(R.id.choose_safety_civ);
		Button btnSelect = helper.getView(R.id.choose_safety_item_btn);

		tvName.setText(item.getOfficerName());
		tvSign.setText(item.getSignature());
		ImageLoader.getInstance().displayImage(item.getLogoURL(), civHead, MyApplication.options_user);

		btnSelect.setOnClickListener(new MyClickListener(item));
	}


	class MyClickListener implements View.OnClickListener {
		SecurityBean item;

		public MyClickListener(SecurityBean item) {
			this.item = item;
		}

		@Override
		public void onClick(View view) {
			Log.e("securityId", item.getSecurityOfficerId());
			Intent intent = new Intent(context, OrderRoomInfoActivity.class);
			intent.putExtra("securityName", item.getOfficerName());
			intent.putExtra("securityId", item.getSecurityOfficerId());
			((Activity) context).setResult(6888, intent);
			((Activity) context).finish();
		}
	}
}
