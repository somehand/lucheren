package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.MyMd5;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class BaseInformationActivity extends Activity {

	@InjectView(R.id.headframe_title)
	TextView headframe_title;
	@InjectView(R.id.realName_tv)
	EditText realName;
	@InjectView(R.id.iDcardNum_tv)
	EditText IDCard;
	@InjectView(R.id.textView8)
	TextView licenseType;
	@InjectView(R.id.textView10)
	TextView schoolName;
	@InjectView(R.id.headframe_ib)
	ImageButton mHeadframeIb;

	private String userName;
	private String passWord;
	private String code;
	private String DrivingSchoolId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_base_information);
		ButterKnife.inject(this);
		initViews();
	}

	public void initViews() {
		headframe_title.setText("基本信息");
		userName = getIntent().getStringExtra("username");
		passWord = getIntent().getStringExtra("password");
		code = getIntent().getStringExtra("code");
		mHeadframeIb.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});
	}

	/**
	 * 验证身份证
	 */
	public boolean isCard(String idNum) {
		//定义判别用户身份证号的正则表达式（要么是15位，要么是18位，最后一位可以为字母）
		Pattern idNumPattern = Pattern.compile("(\\d{14}[0-9a-zA-Z])|(\\d{17}[0-9a-zA-Z])");
		//通过Pattern获得Matcher
		Matcher idNumMatcher = idNumPattern.matcher(idNum);
		//判断用户输入是否为身份证号
		if (idNumMatcher.matches()) {
			return true;
		}
		return false;
	}

	public void regist() {
		String type = licenseType.getText().toString();
		if ("".equals(realName.getText().toString())) {
			Toast.makeText(BaseInformationActivity.this, "请输入真实姓名", Toast.LENGTH_SHORT).show();
			return;
		}
		if (!isCard(IDCard.getText().toString())) {
			Toast.makeText(BaseInformationActivity.this, "请输入正确格式的身份证号码", Toast.LENGTH_SHORT).show();
			return;
		}

//		if ("".equals(schoolName.getText().toString())) {
//			Toast.makeText(BaseInformationActivity.this, "请选择驾校", Toast.LENGTH_SHORT).show();
//			return;
//		}
		final String md5pw = MyMd5.getMd5(passWord);
		RequestParams params = new RequestParams();
		params.put("UserName", userName);
		params.put("PassWord", md5pw);
		params.put("RegChannel", 2);
		params.put("UserType", 1);
		params.put("RealName", realName.getText().toString());
		params.put("IDCardNumber", IDCard.getText().toString());
		if ("C1".equals(type))
			params.put("CarType", 1);
		else
			params.put("CarType", 2);

//		params.put("DrivingSchoolId", DrivingSchoolId);
		params.put("InvitationCode", code);//邀请码
		KLog.e("params ", params.toString());
		MyHttp.getInstance(this).post(MyHttp.REGIST_USER, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonObject :");
				KLog.json(Utils.TAG_DEBUG, response.toString());
				try {
					if (response.getInt("Code") == 1) {
						Utils.ToastShort(BaseInformationActivity.this, response.getString("Reason"));
						Utils.SPPutBoolean(BaseInformationActivity.this, userName, true);
						Intent intent = new Intent(BaseInformationActivity.this, LoginActivity.class);
						intent.putExtra("username", userName);
						intent.putExtra("password", passWord);
						intent.putExtra("isLogin", true);
						startActivity(intent);
					} else {
						Utils.ToastShort(BaseInformationActivity.this, response.getString("Reason"));
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void getSchool(View view) {
		Intent intent = new Intent(this, ChooseSchoolActivity1.class);
		startActivityForResult(intent, 998);
	}

	String[] types = new String[]{"C1", "C2"};

	public void getType(View view) {
		new AlertDialog.Builder(this).setItems(types, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {
				licenseType.setText(types[i]);
				dialogInterface.dismiss();
			}
		}).show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 998 && resultCode == RESULT_OK) {
			Bundle b = data.getBundleExtra("bun");
			DrivingSchoolId = b.getString("schoolId");
			schoolName.setText(b.getString("schoolName"));
		}
	}

	public void toLogin(View view) {
		regist();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		ButterKnife.reset(this);
	}
}
