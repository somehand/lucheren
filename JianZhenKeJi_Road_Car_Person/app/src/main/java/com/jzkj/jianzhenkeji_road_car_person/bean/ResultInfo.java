package com.jzkj.jianzhenkeji_road_car_person.bean;

/**
 * Created by Administrator on 2016/3/1.
 */
public class ResultInfo {
	private String UserId;
	private String UserName;
	private String NickName;
	private int UserLogo;
	private String RealName;
	private String IDCardNumber;
	private String Birthday;
	private String UserSex;
	private String UserAddress;
	private String DrivingSchoolId;
	private String CoachId;
	private int Schedule;
	private int State;

	@Override
	public String toString() {
		return "ResultInfo{" +
				"UserId='" + UserId + '\'' +
				", UserName='" + UserName + '\'' +
				", NickName='" + NickName + '\'' +
				", UserLogo=" + UserLogo +
				", RealName='" + RealName + '\'' +
				", IDCardNumber='" + IDCardNumber + '\'' +
				", Birthday='" + Birthday + '\'' +
				", UserSex='" + UserSex + '\'' +
				", UserAddress='" + UserAddress + '\'' +
				", DrivingSchoolId='" + DrivingSchoolId + '\'' +
				", CoachId='" + CoachId + '\'' +
				", Schedule=" + Schedule +
				", State=" + State +
				'}';
	}

	public String getUserId() {
		return UserId;
	}

	public void setUserId(String userId) {
		UserId = userId;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getNickName() {
		return NickName;
	}

	public void setNickName(String nickName) {
		NickName = nickName;
	}

	public int getUserLogo() {
		return UserLogo;
	}

	public void setUserLogo(int userLogo) {
		UserLogo = userLogo;
	}

	public String getRealName() {
		return RealName;
	}

	public void setRealName(String realName) {
		RealName = realName;
	}

	public String getIDCardNumber() {
		return IDCardNumber;
	}

	public void setIDCardNumber(String IDCardNumber) {
		this.IDCardNumber = IDCardNumber;
	}

	public String getBirthday() {
		return Birthday;
	}

	public void setBirthday(String birthday) {
		Birthday = birthday;
	}

	public String getUserSex() {
		return UserSex;
	}

	public void setUserSex(String userSex) {
		UserSex = userSex;
	}

	public String getUserAddress() {
		return UserAddress;
	}

	public void setUserAddress(String userAddress) {
		UserAddress = userAddress;
	}

	public String getDrivingSchoolId() {
		return DrivingSchoolId;
	}

	public void setDrivingSchoolId(String drivingSchoolId) {
		DrivingSchoolId = drivingSchoolId;
	}

	public String getCoachId() {
		return CoachId;
	}

	public void setCoachId(String coachId) {
		CoachId = coachId;
	}

	public int getSchedule() {
		return Schedule;
	}

	public void setSchedule(int schedule) {
		Schedule = schedule;
	}

	public int getState() {
		return State;
	}

	public void setState(int state) {
		State = state;
	}
}
