package com.jzkj.jianzhenkeji_road_car_person.bean;

/**
 * Created by Administrator on 2016/3/8.
 */
public class SecurityOfficerInfo {
	/**
	 * SecurityOfficerId：安全员ID
	 * ExaminationId：考场ID
	 * OfficerName：姓名
	 * LogoURL：头像
	 * OfficerSex：性别
	 * Mobile:联系电话
	 * IDCardNumber：身份证号码
	 * Signature：个性签名
	 * Introduce：介绍
	 */
	private String SecurityOfficerId;
	private String ExaminationId;
	private String OfficerName;
	private String LogoURL;
	private String OfficerSex;
	private String Mobile;
	private String IDCardNumber;
	private String Signature;
	private String Introduce;

	public String getSecurityOfficerId() {
		return SecurityOfficerId;
	}

	public void setSecurityOfficerId(String securityOfficerId) {
		SecurityOfficerId = securityOfficerId;
	}

	public String getExaminationId() {
		return ExaminationId;
	}

	public void setExaminationId(String examinationId) {
		ExaminationId = examinationId;
	}

	public String getOfficerName() {
		return OfficerName;
	}

	public void setOfficerName(String officerName) {
		OfficerName = officerName;
	}

	public String getLogoURL() {
		return LogoURL;
	}

	public void setLogoURL(String logoURL) {
		LogoURL = logoURL;
	}

	public String getOfficerSex() {
		return OfficerSex;
	}

	public void setOfficerSex(String officerSex) {
		OfficerSex = officerSex;
	}

	public String getMobile() {
		return Mobile;
	}

	public void setMobile(String mobile) {
		Mobile = mobile;
	}

	public String getIDCardNumber() {
		return IDCardNumber;
	}

	public void setIDCardNumber(String IDCardNumber) {
		this.IDCardNumber = IDCardNumber;
	}

	public String getSignature() {
		return Signature;
	}

	public void setSignature(String signature) {
		Signature = signature;
	}

	public String getIntroduce() {
		return Introduce;
	}

	public void setIntroduce(String introduce) {
		Introduce = introduce;
	}

	@Override
	public String toString() {
		return "SecurityOfficerInfo{" +
				"SecurityOfficerId='" + SecurityOfficerId + '\'' +
				", ExaminationId='" + ExaminationId + '\'' +
				", OfficerName='" + OfficerName + '\'' +
				", LogoURL='" + LogoURL + '\'' +
				", OfficerSex='" + OfficerSex + '\'' +
				", Mobile='" + Mobile + '\'' +
				", IDCardNumber='" + IDCardNumber + '\'' +
				", Signature='" + Signature + '\'' +
				", Introduce='" + Introduce + '\'' +
				'}';
	}

	public SecurityOfficerInfo(String securityOfficerId, String examinationId, String officerName, String logoURL, String officerSex, String mobile, String IDCardNumber, String signature, String introduce) {
		SecurityOfficerId = securityOfficerId;
		ExaminationId = examinationId;
		OfficerName = officerName;
		LogoURL = logoURL;
		OfficerSex = officerSex;
		Mobile = mobile;
		this.IDCardNumber = IDCardNumber;
		Signature = signature;
		Introduce = introduce;
	}
}
