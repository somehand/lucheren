package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.BaseActivity;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.adapter.MyCollectAdapter;
import com.jzkj.jianzhenkeji_road_car_person.bean.MyCollectBean;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.kanak.emptylayout.EmptyLayout;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MyCollectActivity extends BaseActivity {

	private ImageButton ibBack;
	private TextView tvTitle;
	private ListView myListview;
	MyCollectAdapter mAdapter;
	ArrayList<MyCollectBean> mArrayList;
	EmptyLayout emptyLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_collect);
		initView();
		initData();
		initListener();

	}


	private void initView() {
		//绑定ID
		ibBack = (ImageButton) findViewById(R.id.headframe_ib);
		tvTitle = (TextView) findViewById(R.id.headframe_title);
		myListview = (ListView) findViewById(R.id.my_collect_lv);
		emptyLayout = new EmptyLayout(this, myListview);
		mAdapter = new MyCollectAdapter(this, R.layout.listitem_mycollect);
		myListview.setAdapter(mAdapter);
		tvTitle.setText("我的收藏");
	}

	private void initListener() {
		ibBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});

//		myListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//			@Override
//			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//
//			}
//		});
//		myListview.setOnLongClickListener(new View.OnLongClickListener() {
//			@Override
//			public boolean onLongClick(final View view) {
//
//
//				return true;
//			}
//		});
	}


	/**
	 * 取消收藏文章
	 */

	public void CollectCancel(String articelId) {
		System.out.println("Blank.CollectCancel");
		RequestParams params = new RequestParams();
		params.put("UserId", Utils.SPGetString(this, Utils.userId, ""));
		params.put("AppIndexArticleId", articelId);
		KLog.e(Utils.TAG_DEBUG, params.toString());
		//System.out.println("params : " + params.toString());
		MyHttp.getInstance(this).post(MyHttp.COLLECT_CANCEL, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonObject :");
				KLog.json(Utils.TAG_DEBUG, response.toString());
				try {
					if (response.getInt("Code") == 1) { //取消收藏成功
//						collectImg.setImageResource(R.drawable.icon_star_black);
//						collectText.setText("收藏");
//						isCollected = false;
//						Utils.ToastShort(Blank.this , response.getString("Reason"));
						getCollectList();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		mArrayList = new ArrayList<MyCollectBean>();
		getCollectList();
	}

	private void initData() {
	}

	/**
	 * 获取收藏列表
	 */
	public void getCollectList() {
		RequestParams params = new RequestParams();
		params.put("UserId", Utils.SPGetString(this, Utils.userId, ""));
		KLog.e(Utils.TAG_DEBUG, params.toString());
		System.out.println("params : " + params.toString());

		MyHttp.getInstance(this).post(MyHttp.GET_MY_COLLECT, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonObject :");
				KLog.json(Utils.TAG_DEBUG, response.toString());
				mArrayList.clear();
				mAdapter.clear();
				emptyLayout.setEmptyMessage("你还没有收藏任何文章哟~");
				emptyLayout.showEmpty();
				mAdapter.notifyDataSetChanged();
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonArray :");
				KLog.json(Utils.TAG_DEBUG, response.toString());
				mArrayList.clear();
				mAdapter.clear();
				try {
					for (int i = 0; i < response.length(); i++) {
						JSONObject object = response.getJSONObject(i);
						MyCollectBean bean = new MyCollectBean();
						bean.setAppIndexArticleId(object.getString("AppIndexArticleId"));
						bean.setHeaderUrl(object.getString("UserLogo"));
						bean.setTitle(object.getString("ArticleTitle"));
						bean.setName(object.getString("NickName"));
						bean.setUrl(object.getString("ArticleURL"));
						SimpleDateFormat format = new SimpleDateFormat("yyyy-M-dd HH:mm:ss");
						Date date = format.parse(object.getString("CollectionDate"));
						format = new SimpleDateFormat("yyyy/MM/dd");
						String time = format.format(date);
						bean.setTime(time);
						String urls = object.getString("ImgURL");
						String[] ss = urls.split("\\|");
						bean.setImgurl1(ss[0]);
						bean.setImgurl2(ss[1]);
						bean.setImgurl3(ss[2]);
						mArrayList.add(bean);
					}
					if (mArrayList.size() == 0) {
						emptyLayout.setEmptyMessage("你还没有收藏任何文章哟~");
						emptyLayout.showEmpty();
						return;
					}
					mAdapter.addAll(mArrayList);
					mAdapter.notifyDataSetChanged();
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				mArrayList.clear();
				mAdapter.clear();
				emptyLayout.setEmptyMessage("你还没有收藏任何文章哟~");
				emptyLayout.showEmpty();
				mAdapter.notifyDataSetChanged();
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
				super.onFailure(statusCode, headers, throwable, errorResponse);
				mArrayList.clear();
				mAdapter.clear();
				emptyLayout.setEmptyMessage("你还没有收藏任何文章哟~");
				emptyLayout.showEmpty();
				mAdapter.notifyDataSetChanged();
			}
		});
	}


}
