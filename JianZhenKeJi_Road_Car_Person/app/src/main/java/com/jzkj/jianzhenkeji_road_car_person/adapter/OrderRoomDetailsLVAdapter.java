package com.jzkj.jianzhenkeji_road_car_person.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.activity.OrderRoomInfoActivity;
import com.jzkj.jianzhenkeji_road_car_person.bean.OrderRoomDetailsBean;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OrderRoomDetailsLVAdapter extends QuickAdapter<OrderRoomDetailsBean>{

	public static final int TYPE_ROOM = 0;
	public static final int TYPE_COACH = 1;
	private int adapterType =  0;

	public void setAdapterType(int type) {
		adapterType = type;
	}

	public OrderRoomDetailsLVAdapter(Context context, int layoutResId) {
		super(context, layoutResId);
	}

	@Override
	protected void convert(BaseAdapterHelper helper, OrderRoomDetailsBean item) {
		TextView time = helper.getView(R.id.order_room_item_time);
		TextView class1 = helper.getView(R.id.order_room_item_class);
		TextView roomName = helper.getView(R.id.order_room_item_roomname);
		TextView num = helper.getView(R.id.order_room_item_ordernum);
		TextView totalNum = helper.getView(R.id.order_room_item_totalnum);
		Button button = helper.getView(R.id.order_room_item_btn);
		ImageView tag = helper.getView(R.id.order_room_item_tag);

		time.setText(item.getTime());
		class1.setText(item.getClass1());
		roomName.setText(item.getName());
		num.setText(item.getOrderNum() + "");
		totalNum.setText("/" + item.getTotalNum() + "人");
		button.setOnClickListener(new MyOnclickListener(item , tag , button , num , totalNum));

		if(!item.isEnable()){
			button.setVisibility(View.GONE);
			tag.setVisibility(View.VISIBLE);
		}
		if(item.isFull()){
			tag.setVisibility(View.VISIBLE);
			tag.setImageResource(R.drawable.order_full);
			button.setVisibility(View.GONE);
		}

	}


	class MyOnclickListener implements View.OnClickListener{
		OrderRoomDetailsBean mBean;
		ImageView mImageView;
		Button mButton;
		TextView num;
		TextView totalNum;

		public MyOnclickListener(OrderRoomDetailsBean bean , ImageView tag ,Button button , TextView num , TextView totalNm) {
			mBean = bean;
			mImageView = tag;
			mButton = button;
			this.num = num;
			this.totalNum = totalNm;
		}

		@Override
		public void onClick(View view) {

			if (adapterType == TYPE_ROOM) {
//				Toast.makeText(context, "yuyue", Toast.LENGTH_SHORT).show();
				Intent intent = new Intent(context, OrderRoomInfoActivity.class);
//				intent.putExtra()
				context.startActivity(intent);
			}else{
				orderCoach(mBean , mImageView , mButton , num , totalNum);
			}
		}
	}

	/**
	 * 预约教练
	 */
	public void orderCoach(final OrderRoomDetailsBean bean , final ImageView imageView , final Button button , final TextView num , final TextView totalNum){
		RequestParams params = new RequestParams();
		params.put("UserId" , Utils.SPGetString(context , Utils.userId , ""));
		params.put("CoachBookingsDateId" , bean.getTimeId());
		KLog.e(Utils.TAG_DEBUG , params.toString());
		//System.out.println("params : " + params.toString());
		MyHttp.getInstance(context).post(MyHttp.ORDER_COACH, params , new JsonHttpResponseHandler(){
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG , "jsonObject :");
				KLog.json(Utils.TAG_DEBUG ,response.toString());
				try {
					if(response.getInt("Code") == 1){
						Utils.ToastShort(context , response.getString("Reason"));
						imageView.setImageResource(R.drawable.order_ok);
						imageView.setVisibility(View.VISIBLE);
						button.setVisibility(View.GONE);
						sendJpush();
					}else if(response.getInt("Code") == 2){
						Utils.ToastShort(context , response.getString("Reason"));
						imageView.setImageResource(R.drawable.order_full);
						imageView.setVisibility(View.VISIBLE);
						button.setVisibility(View.GONE);
						num.setText(totalNum.getText());
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG , "jsonArray :");
				KLog.json(Utils.TAG_DEBUG ,response.toString());

			}

		});
	}

	private void sendJpush() {
		RequestParams params = new RequestParams();
		params.put("RealName",Utils.SPGetString(context,Utils.realName,"realName"));
		params.put("Name",Utils.SPGetString(context,Utils.CoachName,"CoachName"));
		params.put("StudentBookingsId","");
		params.put("Type",1);
		Log.e("params",params.toString());
		MyHttp.getInstance(context).post(MyHttp.ORDER_SEND_JPUSH,params,new JsonHttpResponseHandler(){
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
				/*try {
					if (response.getInt("Code") == 1) {

					}
				} catch (JSONException e) {
					e.printStackTrace();
				}*/
				/*try {
					Utils.ToastShort(context,response.getString("Reason"));
				} catch (JSONException e) {
					e.printStackTrace();
				}*/
			}
		});
	}
}
