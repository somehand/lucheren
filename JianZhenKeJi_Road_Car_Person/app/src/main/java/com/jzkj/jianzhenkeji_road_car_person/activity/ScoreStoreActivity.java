package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;
import com.jzkj.jianzhenkeji_road_car_person.CircleImageView;
import com.jzkj.jianzhenkeji_road_car_person.MyApplication;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.adapter.ScoreStoreAdapter;
import com.jzkj.jianzhenkeji_road_car_person.bean.RefreshEvent;
import com.jzkj.jianzhenkeji_road_car_person.bean.ScoreStoreBean;
import com.jzkj.jianzhenkeji_road_car_person.util.MyGridView;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.kanak.emptylayout.EmptyLayout;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Administrator on 2016/4/5.
 */
public class ScoreStoreActivity extends Activity implements View.OnClickListener {
	@InjectView(R.id.headframe_ib)
	ImageButton mHeadframeIb;
	@InjectView(R.id.headframe_title)
	TextView mHeadframeTitle;
	@InjectView(R.id.score_store_tvcurrentscore)
	TextView tvCurrentScore;
	@InjectView(R.id.score_store_headimg)
	CircleImageView mScoreStoreHeadimg;
	@InjectView(R.id.score_store_gridview)
	MyGridView mScoreStoreGridview;
	@InjectView(R.id.score_store_tvhead_name)
	TextView mScoreStoreTvheadName;
	@InjectView(R.id.score_store_tvcurrent_score)
	LinearLayout mScoreStoreTvcurrentScore;
	@InjectView(R.id.score_store_tvscore_record)
	LinearLayout mScoreStoreTvscoreRecord;
	@InjectView(R.id.score_store_tvrecord_exchange)
	LinearLayout mScoreStoreTvrecordExchange;
	@InjectView(R.id.score_store_ptrfsv)
	PullToRefreshScrollView mPullToRefreshScrollView;
	@InjectView(R.id.img_exam_head_vip)
	ImageView imgVip;

	private ArrayList<ScoreStoreBean> list;
	private ScoreStoreAdapter adapter;
	int nowPage = 1;
	EmptyLayout mEmptyLayout;
	String currentScore;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_score_store);
		ButterKnife.inject(this);
		init();
		mEmptyLayout = new EmptyLayout(ScoreStoreActivity.this, mScoreStoreGridview);
		initdata();
		initListener();
		EventBus.getDefault().register(this);
	}

	private void initListener() {
		mHeadframeIb.setOnClickListener(this);
		mScoreStoreTvcurrentScore.setOnClickListener(this);
		mScoreStoreTvscoreRecord.setOnClickListener(this);
		mScoreStoreTvrecordExchange.setOnClickListener(this);
		mPullToRefreshScrollView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ScrollView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ScrollView> refreshView) {
				String label = DateUtils.formatDateTime(ScoreStoreActivity.this, System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_ALL);
				refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);
				nowPage++;
				getStoreInfo();
			}
		});
	}

	private void initdata() {
		/*final int img = R.drawable.timg4;
		final String[] score = {"15000", "16000", "17000", "18000", "19000", "20000", "22000"};
		for (int i = 0; i < score.length; i++) {
			ScoreStoreBean bean = new ScoreStoreBean();
			bean.setImage(img);
			bean.setScore(score[i]);
			list.add(bean);
		}*/
		/*adapter = new ScoreStoreAdapter(this, R.layout.acriviry_score_store_gv_item);
		mScoreStoreGridview.setAdapter(adapter);
		adapter.addAll(list);*/
		getStoreInfo();
		mPullToRefreshScrollView.setMode(PullToRefreshBase.Mode.PULL_FROM_END);
		mScoreStoreGridview.setFocusable(false);
		mScoreStoreGridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				Intent intent = new Intent(ScoreStoreActivity.this, WareDetailActivity.class);
				intent.putExtra("id", list.get(i).getGoodsId());
				startActivityForResult(intent, 0x111);
			}
		});

	}

	ProgressDialog mDialog;
	private void getStoreInfo() {
		RequestParams params = new RequestParams();
		mDialog = new ProgressDialog(this);
		mDialog.setMessage("正在加载中...");
		mDialog.setIndeterminate(true);
		mDialog.setCanceledOnTouchOutside(false);
		mDialog.show();

		params.put("type", 1);
		params.put("pageIndex", nowPage);
		params.put("pageSize", 6);
		KLog.e("params", params.toString());
		MyHttp.getInstance(this).post(MyHttp.GET_STORE_INFO, params, new JsonHttpResponseHandler() {

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e("Debug", "JSONArray:");
				KLog.json(response.toString());
				for (int i = 0; i < response.length(); i++) {
					try {
						JSONObject object = response.getJSONObject(i);
						ScoreStoreBean bean = new ScoreStoreBean();
						bean.setGoodsId(object.getString("id"));
						bean.setGoodsName(object.getString("GoodsName"));
						bean.setImgUrl(object.getString("pic"));
						bean.setPrice(object.getDouble("Price"));
						bean.setScores(object.getString("Integral"));
						bean.setRemark(object.getString("Remark"));
						list.add(bean);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				adapter = new ScoreStoreAdapter(ScoreStoreActivity.this, R.layout.acriviry_score_store_gv_item);
				mScoreStoreGridview.setAdapter(adapter);
				adapter.addAll(list);
				mPullToRefreshScrollView.postDelayed(new Runnable() {
					@Override
					public void run() {
						mPullToRefreshScrollView.onRefreshComplete();
						mDialog.cancel();
					}
				}, 1000);
				if (list.size() == 0) {
					mEmptyLayout.setEmptyMessage("还没有任何商品数据~");
					mEmptyLayout.showEmpty();
				}
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				mPullToRefreshScrollView.postDelayed(new Runnable() {
					@Override
					public void run() {
						mPullToRefreshScrollView.onRefreshComplete();
					}
				}, 1000);
				if (nowPage != 1) {
					Utils.ToastShort(ScoreStoreActivity.this, "当前已是最后一页!");
				}
				mEmptyLayout.setEmptyMessage("还没有任何商品数据~");
				mEmptyLayout.showEmpty();
				mDialog.cancel();

			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				mPullToRefreshScrollView.postDelayed(new Runnable() {
					@Override
					public void run() {
						mPullToRefreshScrollView.onRefreshComplete();
					}
				}, 1000);
				mEmptyLayout.setEmptyMessage("还没有任何商品数据~");
				mEmptyLayout.showEmpty();
				mDialog.cancel();

			}
		});
	}

	private void init() {
		if (Utils.SPGetInt(ScoreStoreActivity.this, Utils.IsVip, 2) == 1) {
			imgVip.setVisibility(View.VISIBLE);
		} else {
			imgVip.setVisibility(View.GONE);
		}
		mHeadframeTitle.setText("积分商城");
		getHeaderName();
		list = new ArrayList<ScoreStoreBean>();
		getCurrentInfo();
	}

	/**
	 * 获取用户信息
	 */
	private void getHeaderName() {
		RequestParams params = new RequestParams();
		params.put("UserId", Utils.SPGetString(this, Utils.userId, ""));
		MyHttp.getInstance(this).post(MyHttp.GET_USER_HEADER, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, response.toString());

				try {
					JSONObject object = response.getJSONObject(0);
					final String name = object.getString("NickName");
					final String logoUrl = object.getString("UserLogo");
					Utils.SPutString(ScoreStoreActivity.this, Utils.realName, name);
					Utils.SPutString(ScoreStoreActivity.this, Utils.userLogo, logoUrl);
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							mScoreStoreTvheadName.setText(name);
							ImageLoader.getInstance().displayImage(logoUrl, mScoreStoreHeadimg, MyApplication.options_user);
						}
					});
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				KLog.e("errorcode　", statusCode + " " + responseString);

			}
		});
	}

	/**
	 * 获取学员当前积分
	 */
	private void getCurrentInfo() {
		RequestParams params = new RequestParams();
		params.put("UserId", Utils.SPGetString(ScoreStoreActivity.this, Utils.userId, ""));
		KLog.e("params", params.toString());
		MyHttp.getInstance(ScoreStoreActivity.this).post(MyHttp.GET_CURRENT_INFO, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e("Debug", "JSONArray:");
				KLog.json(response.toString());
				try {
					JSONObject object = response.getJSONObject(0);
					currentScore = object.getString("Integral");
					tvCurrentScore.setText(currentScore);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	public void onClick(View view) {
		Intent intent;
		switch (view.getId()) {
			case R.id.headframe_ib:
				EventBus.getDefault().post(new RefreshEvent());
				finish();
				break;
			case R.id.score_store_tvcurrent_score:

				break;
			case R.id.score_store_tvscore_record:
				intent = new Intent(ScoreStoreActivity.this, IntegralDetailsActivity.class);
				intent.putExtra("ss", 1);
				intent.putExtra("score", currentScore);
				startActivity(intent);
				break;
			case R.id.score_store_tvrecord_exchange:
				intent = new Intent(ScoreStoreActivity.this, ExchangeRecordActivity.class);
				startActivity(intent);
				break;

			default:
				break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		EventBus.getDefault().unregister(this);
	}


	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onRefreshScore(RefreshEvent event) {
		getCurrentInfo();
	}
}
