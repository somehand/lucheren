package com.jzkj.jianzhenkeji_road_car_person.bean;

/**
 * Created by Administrator on 2016/4/6.
 */
public class ExchangeRecordBean {
	private String goodsId;
	private String Goodname;
	private String Price;
	private String pic;
	private String Createdate;


	public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}

	public String getGoodname() {
		return Goodname;
	}

	public void setGoodname(String goodname) {
		Goodname = goodname;
	}

	public String getPrice() {
		return Price;
	}

	public void setPrice(String price) {
		Price = price;
	}


	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getCreatedate() {
		return Createdate;
	}

	public void setCreatedate(String createdate) {
		Createdate = createdate;
	}

	public ExchangeRecordBean() {
	}
}
