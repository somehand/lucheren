package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Administrator on 2016/3/11.
 */
public class AboutUs extends Activity {
	@InjectView(R.id.headframe_ib)
	ImageButton mHeadframeIb;
	@InjectView(R.id.headframe_title)
	TextView mHeadframeTitle;
	@InjectView(R.id.about_phone)
	LinearLayout mAboutPhone;
	@InjectView(R.id.about_to_webview)
	LinearLayout mAboutWeb;
	@InjectView(R.id.about_to_tvurl)
	TextView mTvUrl;
	@InjectView(R.id.text_current_version)
	TextView currentVersion;
	/*@InjectView(R.id.is_webview)
	WebView mIsWebview;*/

	AlertDialog mDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about_us);
		ButterKnife.inject(this);
		mHeadframeTitle.setText("关于我们");
		try {
			currentVersion.setText("V" + getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
		/*WebSettings webSettings = mIsWebview.getSettings();
		//设置加载显示的URL
		mIsWebview.loadUrl("http://www.zgjzkj.net/");
		//支持JavaScript
		webSettings.setJavaScriptEnabled(true);
		webSettings.setSupportZoom(true);
		webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
		webSettings.setBuiltInZoomControls(true);//support zoom
		webSettings.setUseWideViewPort(true);// 这个很关键
		webSettings.setLoadWithOverviewMode(true);
		mIsWebview.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
		//设置WebView的切换方式
		mIsWebview.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}
		});*/

		mHeadframeIb.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});
		mAboutPhone.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				mDialog = new AlertDialog.Builder(AboutUs.this).create();
				mDialog.setMessage("是否拨打电话?");
				mDialog.setButton(DialogInterface.BUTTON_POSITIVE, "确定", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:4000064666"));
						startActivity(intent);
					}
				});
				mDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						mDialog.dismiss();
					}
				});
				mDialog.show();
			}
		});
		mAboutWeb.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(AboutUs.this, WebPageActivity.class);
				intent.putExtra("titleName", "路车人");
				intent.putExtra("type", 5);
				intent.putExtra("url", "http://www.lucheren.net/");
				startActivity(intent);
			}
		});
//		mAboutWeb.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View view) {
////				Intent intent = new Intent(AboutUs.this,ToCompanyActivity.class);
//				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.zgjzkj.net/"));
//				startActivity(intent);
//			}
//		});
	}
}
