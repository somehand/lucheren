package com.jzkj.jianzhenkeji_road_car_person.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.activity.AboutUs;
import com.jzkj.jianzhenkeji_road_car_person.activity.FeedBackActivity;
import com.jzkj.jianzhenkeji_road_car_person.activity.LookroomRecordActivity;
import com.jzkj.jianzhenkeji_road_car_person.activity.ModifyPasswordActivity;
import com.jzkj.jianzhenkeji_road_car_person.activity.MyDrivingSchoolActivity;
import com.jzkj.jianzhenkeji_road_car_person.activity.MyNewsActivity;
import com.jzkj.jianzhenkeji_road_car_person.activity.MyOrderActivity;
import com.jzkj.jianzhenkeji_road_car_person.bean.ItIsUser;

import java.util.ArrayList;

public class MineLVAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<ItIsUser> list;
	private LayoutInflater inflater;

	public MineLVAdapter(Context context, ArrayList<ItIsUser> list) {
		super();
		this.context = context;
		this.list = list;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		viewHolder holder = null;
		if (arg1 == null) {
			arg1 = inflater.inflate(R.layout.mine_lv_item, null);
		}
		holder = (viewHolder) arg1.getTag();
		if (holder == null) {
			holder = new viewHolder();
			holder.imgLeft = (ImageView) arg1.findViewById(R.id.mine_item_img);
			holder.tvText = (TextView) arg1.findViewById(R.id.mine_lv_tv);
			holder.imgLineOne = (ImageView) arg1
					.findViewById(R.id.mine_item_img_line1);
			holder.imgLineTwo = (View) arg1
					.findViewById(R.id.mine_item_img_line2);
			holder.ll = (LinearLayout) arg1.findViewById(R.id.mine_item_ll);
		}
		holder.clear();
		final int index = arg0;
		ItIsUser ituser = list.get(arg0);
		holder.imgLineTwo.setVisibility(View.GONE);
		if (arg0 == 0) {
			holder.ll.setVisibility(View.VISIBLE);
			holder.imgLineOne.setVisibility(View.VISIBLE);
		}
		if (arg0 == 3) {
			holder.imgLineOne.setVisibility(View.GONE);
//			holder.imgLineTwo.setVisibility(View.VISIBLE);
		}
		if (arg0 == 4) {
			holder.ll.setVisibility(View.GONE);
			holder.imgLineOne.setVisibility(View.GONE);
			holder.imgLineTwo.setVisibility(View.VISIBLE);
		}
		holder.imgLeft.setImageResource(ituser.getImg());
		holder.tvText.setText(ituser.getMineText());
		holder.ll.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				switch (index) {
				/*case 0:
					Intent intent = new Intent(context, MyDataActivity.class);
					context.startActivity(intent);

					break;*/
					case 0:
						Intent intent1 = new Intent(context, MyOrderActivity.class);
						context.startActivity(intent1);
						break;
					case 3:
						Intent intent2 = new Intent(context, MyDrivingSchoolActivity.class);
						context.startActivity(intent2);
						break;
					case 1:
						Intent intent4 = new Intent(context,
								LookroomRecordActivity.class);
						context.startActivity(intent4);

						break;
					case 2:
						Intent intent3 = new Intent(context, MyNewsActivity.class);
						context.startActivity(intent3);
						break;
				/*case 3:
					Intent intent5 = new Intent(context, MyCollectActivity.class);
					context.startActivity(intent5);
					break;*/
					case 5:
						Intent intent7 = new Intent(context, FeedBackActivity.class);
						context.startActivity(intent7);
						break;
					case 6:
						Intent intent8 = new Intent(context, ModifyPasswordActivity.class);
						context.startActivity(intent8);
						break;
					case 7:
						Intent intent9 = new Intent(context, AboutUs.class);
						context.startActivity(intent9);
						break;

					default:
						break;
				}
			}
		});

		return arg1;
	}

	class viewHolder {
		private ImageView imgLeft, imgLineOne;
		private View imgLineTwo;
		private TextView tvText;
		private LinearLayout ll;

		void clear() {
			imgLeft.setBackgroundDrawable(null);
			tvText.setText("");
		}
	}
}
