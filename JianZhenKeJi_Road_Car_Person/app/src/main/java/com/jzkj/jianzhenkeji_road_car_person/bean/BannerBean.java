package com.jzkj.jianzhenkeji_road_car_person.bean;

/**
 * Created by Administrator on 2016/4/13.
 */
public class BannerBean {
    public String SchoolName;
    public String ImgURL;
    public String verson;
    public String ArticleURL;


    public String getSchoolName() {
        return SchoolName;
    }

    public void setSchoolName(String schoolName) {
        SchoolName = schoolName;
    }

    public String getImgURL() {
        return ImgURL;
    }

    public void setImgURL(String imgURL) {
        ImgURL = imgURL;
    }

    public String getVerson() {
        return verson;
    }

    public void setVerson(String verson) {
        this.verson = verson;
    }

    public String getArticleURL() {
        return ArticleURL;
    }

    public void setArticleURL(String articleURL) {
        ArticleURL = articleURL;
    }
}
