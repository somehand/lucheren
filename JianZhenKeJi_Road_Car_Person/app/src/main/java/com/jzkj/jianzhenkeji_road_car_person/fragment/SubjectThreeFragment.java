package com.jzkj.jianzhenkeji_road_car_person.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RadioButton;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.activity.OrderDetailActivity;
import com.jzkj.jianzhenkeji_road_car_person.adapter.MyOrderLVAdapter;
import com.jzkj.jianzhenkeji_road_car_person.bean.MyOrderDetail;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.kanak.emptylayout.EmptyLayout;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class SubjectThreeFragment extends Fragment implements View.OnClickListener {
	private ListView lv;
	private ArrayList<MyOrderDetail> list;
	private MyOrderLVAdapter adapter;
	private RadioButton rbLookRoom, rbCoach, rbNoOrder, rbOrdered, rbNoEvaluate, rbEvaluate;
	private int progress = 1;//1未预约 2已预约 3未评价 4已评价
	private int isAgree = 1;//是否同意，0：已预约，1：已取消
	private int isEvaluate = -1;//是否评价，0：未评价，1：已评价, -1关联此条件
	EmptyLayout layout;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		EventBus.getDefault().register(getActivity());
		list = new ArrayList<MyOrderDetail>();

		/*String time = "2016-05-22  12:00-14:00";
		String coachname = "奇偶打算";
		String[] state = {"未预约","已预约","未预约","已预约","未预约","未预约","未预约"};
		String[] iseval = {"是","是","是","是","是","是","是"};
		for (int i = 0; i < state.length; i++) {
			MyOrderDetail bean = new MyOrderDetail();
			bean.setCoachName(coachname);
			bean.setIsEvaluate(iseval[i]);
			bean.setState(state[i]);
			bean.setStartDate(time);
			list.add(bean);
		}*/
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_look_room, container, false);
		lv = (ListView) view.findViewById(R.id.look_room_lv);
		layout = new EmptyLayout(getActivity(), lv);
		rbNoOrder = (RadioButton) view.findViewById(R.id.my_order_rbno_orde3);
		rbOrdered = (RadioButton) view.findViewById(R.id.my_order_rbordered3);
		rbNoEvaluate = (RadioButton) view.findViewById(R.id.my_order_rbno_evaluate3);
		rbEvaluate = (RadioButton) view.findViewById(R.id.my_order_rbevaluated3);

		Drawable da = getActivity().getResources().getDrawable(R.drawable.bule_line_img);
		//调用setCompoundDrawables时，必须调用Drawable.setBounds()方法,否则图片不显示
		da.setBounds(0, 0, da.getIntrinsicWidth(), da.getIntrinsicHeight());
		rbNoOrder.setCompoundDrawables(null, null, null, da);

//		adapter = new MyOrderLVAdapter(getActivity(),list);
//		lv.setAdapter(adapter);
//		getLookRoomInfo();
		getOrderInfo();
		bindClickListener();
		return view;
	}

	private void bindClickListener() {
		rbEvaluate.setOnClickListener(this);
		rbNoEvaluate.setOnClickListener(this);
		rbOrdered.setOnClickListener(this);
		rbNoOrder.setOnClickListener(this);
		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				if (progress == 2) {
					Intent intent = new Intent(getActivity(), OrderDetailActivity.class);
					intent.putExtra("CoachBookingsDateId", list.get(i).getCoachBookingsDateId());
					intent.putExtra("Studentbookingsid", list.get(i).getStudentBookingsId());
					intent.putExtra("coachName", list.get(i).getName());
					intent.putExtra("flag", 3);
					startActivity(intent);
				}
			}
		});
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
//		EventBus.getDefault().unregister(this);

	}

	ProgressDialog mDialog;

	public void getOrderInfo() {
		list.clear();
		mDialog = new ProgressDialog(getActivity());
		mDialog.setMessage("正在加载中...");
		mDialog.setIndeterminate(true);
		mDialog.setCanceledOnTouchOutside(false);
		mDialog.show();
		RequestParams params = new RequestParams();
		params.put("UserId", Utils.SPGetString(getActivity(), Utils.userId, "MyOrder_userId"));
		params.put("Subjectnum", 3);
		params.put("Isagree", isAgree);
		params.put("Ispj", isEvaluate);
		Log.e("params", params.toString());
		MyHttp.getInstance(getActivity()).post(MyHttp.GET_ORDER_INFO, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				Log.e("Debug", "JSONArray:");
				KLog.json(response.toString());
//				list = new ArrayList<MyOrderDetail>();
				KLog.e("response", response.length() + "");
				try {
					for (int i = 0; i < response.length(); i++) {
						JSONObject jsonObject = response.getJSONObject(i);
						MyOrderDetail bean = new MyOrderDetail();
						bean.setStudentBookingsId(jsonObject.getString("StudentBookingsId"));
						bean.setStartDate(jsonObject.getString("CoachDay"));
						bean.setIsAgree(jsonObject.getString("IsAgree"));
						bean.setName(jsonObject.getString("RealName"));
						bean.setEvaluateId(jsonObject.getString("pjid"));
						bean.setEvaluateState(jsonObject.getString("pj"));
						bean.setCoachBookingsDateId(jsonObject.getString("CoachBookingsDateId"));
						list.add(bean);

					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				if (list.size() == 0) {
					layout.setEmptyMessage("还没有任何数据哟~");
					layout.showEmpty();
					return;
				}
				adapter = new MyOrderLVAdapter(getActivity(), list, progress);
				lv.setAdapter(adapter);
				adapter.notifyDataSetChanged();
				mDialog.cancel();
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				Log.e("Debug", "JSONObject:");
				KLog.json(response.toString());
				list.clear();
				mDialog.cancel();
				adapter = new MyOrderLVAdapter(getActivity(), list, progress);
				lv.setAdapter(adapter);
				adapter.notifyDataSetChanged();
				layout.setEmptyMessage("还没有任何数据哟~");
				layout.showEmpty();
			}
		});
	}

	@Override
	public void onClick(View view) {
		Drawable da = getActivity().getResources().getDrawable(R.drawable.bule_line_img);
		//调用setCompoundDrawables时，必须调用Drawable.setBounds()方法,否则图片不显示
		da.setBounds(0, 0, da.getIntrinsicWidth(), da.getIntrinsicHeight());
		switch (view.getId()) {
			case R.id.my_order_rbno_orde3:
				if (rbNoOrder.isChecked()) {
					isAgree = 1;
					progress = 1;
					isEvaluate = -1;
					getOrderInfo();
					rbNoOrder.setCompoundDrawables(null, null, null, da);
					rbOrdered.setCompoundDrawables(null, null, null, null);
					rbNoEvaluate.setCompoundDrawables(null, null, null, null);
					rbEvaluate.setCompoundDrawables(null, null, null, null);
				}
				break;
			case R.id.my_order_rbordered3:
				if (rbOrdered.isChecked()) {
					isAgree = 0;
					progress = 2;
					isEvaluate = -1;
					getOrderInfo();
					rbNoOrder.setCompoundDrawables(null, null, null, null);
					rbOrdered.setCompoundDrawables(null, null, null, da);
					rbNoEvaluate.setCompoundDrawables(null, null, null, null);
					rbEvaluate.setCompoundDrawables(null, null, null, null);
				}
				break;
			case R.id.my_order_rbno_evaluate3:
				if (rbNoEvaluate.isChecked()) {
					isAgree = 0;
					isEvaluate = 0;
					progress = 3;
					getOrderInfo();
					rbNoOrder.setCompoundDrawables(null, null, null, null);
					rbOrdered.setCompoundDrawables(null, null, null, null);
					rbNoEvaluate.setCompoundDrawables(null, null, null, da);
					rbEvaluate.setCompoundDrawables(null, null, null, null);
				}
				break;
			case R.id.my_order_rbevaluated3:
				if (rbEvaluate.isChecked()) {
					isAgree = 0;
					isEvaluate = 1;
					progress = 4;
					getOrderInfo();
					rbNoOrder.setCompoundDrawables(null, null, null, null);
					rbOrdered.setCompoundDrawables(null, null, null, null);
					rbNoEvaluate.setCompoundDrawables(null, null, null, null);
					rbEvaluate.setCompoundDrawables(null, null, null, da);
				}
				break;
			default:
				break;
		}
	}

//	@Subscribe(threadMode = ThreadMode.MAIN)
//	public void onEvaluate(MyEvaluateActivity.EvaluateSecurity security) {
//		getOrderInfo();
//	}
}
