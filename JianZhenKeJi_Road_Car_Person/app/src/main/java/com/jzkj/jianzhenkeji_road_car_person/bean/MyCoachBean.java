package com.jzkj.jianzhenkeji_road_car_person.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/4/6.
 */
public class MyCoachBean implements Serializable{
	private String commentid;//评论记录ID
	private String studentid;//学员id
	private int serviceAttitude;//服务态度
	private String contents;//评论内容
	private String commentDate;//评论日期
	private String studentLogo;//学员头像
	private int major;//专业知识
	private int MYD;//满意度
	private String studentName;//学员名字
	private String StudentBookingsId;//预约教练记录ID
	private String evaluated;//0：好评，1：中评，2：差评

	public int getMYD() {
		return MYD;
	}

	public void setMYD(int MYD) {
		this.MYD = MYD;
	}

	public String getCommentid() {
		return commentid;
	}

	public void setCommentid(String commentid) {
		this.commentid = commentid;
	}

	public String getStudentid() {
		return studentid;
	}

	public void setStudentid(String studentid) {
		this.studentid = studentid;
	}


	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getCommentDate() {
		return commentDate;
	}

	public void setCommentDate(String commentDate) {
		this.commentDate = commentDate;
	}

	public String getStudentLogo() {
		return studentLogo;
	}

	public void setStudentLogo(String studentLogo) {
		this.studentLogo = studentLogo;
	}

	public int getServiceAttitude() {
		return serviceAttitude;
	}

	public void setServiceAttitude(int serviceAttitude) {
		this.serviceAttitude = serviceAttitude;
	}

	public int getMajor() {
		return major;
	}

	public void setMajor(int major) {
		this.major = major;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentBookingsId() {
		return StudentBookingsId;
	}

	public void setStudentBookingsId(String studentBookingsId) {
		StudentBookingsId = studentBookingsId;
	}

	public String getEvaluated() {
		return evaluated;
	}

	public void setEvaluated(String evaluated) {
		this.evaluated = evaluated;
	}

	public MyCoachBean() {
	}
}
