package com.jzkj.jianzhenkeji_road_car_person.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/2/29 0029.
 */
public class ExamBean implements Serializable {


	/**
	 * id : 11
	 * question : 这个标志是何含义？
	 * answer : 3
	 * item1 : 省道编号
	 * item2 : 国道编号
	 * item3 : 县道编号
	 * item4 : 乡道编号
	 * explains : 乡道是白底Y字开头的，县道是白底X开头的，省道是黄底的，国道是红底的。
	 * url : http://images.juheapi.com/jztk/c1c2subject1/11.jpg
	 */

	private int id;
	private String question;
	private int answer;
	private String item1;
	private String item2;
	private String item3;
	private String item4;
	private String explains;
	private String url;

	public void setId(int id) {
		this.id = id;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public void setAnswer(int answer) {
		this.answer = answer;
	}

	public void setItem1(String item1) {
		this.item1 = item1;
	}

	public void setItem2(String item2) {
		this.item2 = item2;
	}

	public void setItem3(String item3) {
		this.item3 = item3;
	}

	public void setItem4(String item4) {
		this.item4 = item4;
	}

	public void setExplains(String explains) {
		this.explains = explains;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getId() {
		return id;
	}

	public String getQuestion() {
		return question;
	}

	public int getAnswer() {
		return answer;
	}

	public String getItem1() {
		return item1;
	}

	public String getItem2() {
		return item2;
	}

	public String getItem3() {
		return item3;
	}

	public String getItem4() {
		return item4;
	}

	public String getExplains() {
		return explains;
	}

	public String getUrl() {
		return url;
	}

	@Override
	public String toString() {
		return "ExamBean{" +
				"id=" + id +
				", question='" + question + '\'' +
				", answer=" + answer +
				", item1='" + item1 + '\'' +
				", item2='" + item2 + '\'' +
				", item3='" + item3 + '\'' +
				", item4='" + item4 + '\'' +
				", explains='" + explains + '\'' +
				", url='" + url + '\'' +
				'}';
	}
}
