package com.jzkj.jianzhenkeji_road_car_person.adapter;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.MyApplication;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.bean.ScoreStoreBean;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Administrator on 2016/4/6.
 */
public class ScoreStoreAdapter extends QuickAdapter<ScoreStoreBean>{
	public ScoreStoreAdapter(Context context, int layoutResId) {
		super(context, layoutResId);
	}

	@Override
	protected void convert(BaseAdapterHelper helper, ScoreStoreBean item) {
		ImageView img = helper.getView(R.id.score_store_gv_item_img);
		TextView textScore = helper.getView(R.id.score_store_gv_item_text_score);
		ImageLoader.getInstance().displayImage(item.getImgUrl(),img, MyApplication.options_other);
		textScore.setText(item.getScores());
	}
}
