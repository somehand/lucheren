package com.jzkj.jianzhenkeji_road_car_person.alipay;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;
import com.jzkj.jianzhenkeji_road_car_person.MyPayCompleteListener;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by Administrator on 2015/10/14.
 */
public class MyAlipayManager {


	public static MyAlipayManager mAlipayManager;

	public static MyPayCompleteListener mPayCompleteListener;
	Activity mContext;

	// 商户PID
	public static final String PARTNER = "2088121801625769";
	// 商户收款账号
	public static final String SELLER = "13808304222@139.com";
	// 商户私钥，pkcs8格式
	public static final String RSA_PRIVATE = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAOC+Jgoy3l8X+qog\n" +
			"Tk8dkbJzHXxGk35MK+GGtrY/FbE08Wgx8bM+qWKp97ncYXEp5gKoSvIZQCLvEnkI\n" +
			"I1SMB4g/7QfXDXdanOYaW/c748Ma2z0i6ibeSqONLos27G7/2exktp6/03H+HIkr\n" +
			"Yiu5b4IgaXvehtawONYJ1AEjYcxBAgMBAAECf1KFf3I/O2evZlsRga+LK/RaUHlR\n" +
			"cNbvIS2uSWihwVVQG1QnApjOaWPRBSU5xo3G1K4lHV/H5cTuA9ptIovo3tp4dHKy\n" +
			"PSLrMGUM8qrQtWeHx2+u38TFe3tISmZSUUIQ6/AMmf/miB1ob96zmhEz6VxJhUG6\n" +
			"vCGuufFBGs91OdkCQQDzC2OBCXyQcY1zYXT1gzjBvuNFZpSth2FE+5/lOUSCztGL\n" +
			"/cM2egGtM/1UfLetQ0b7XPMqAwRNQKmmD9TAJ7kDAkEA7LkDPluVmEMA2cpBjuKq\n" +
			"YMgELl7vwGtrxhiNDmKb07PyEysbh+cDi1IAkYbDjvQ6mnq2Un7A6M2e00wq2vUo\n" +
			"awJBAOrjtNT+wB4xQE2kgI7OtUm6HJf0HfjfgAhVTyEQCcCJ9Ubgh4vcukYUmt7O\n" +
			"cg3HmZBRMskFhWDL/6l4X1seiNUCQG3pNU1Vvq6Upad5lljee1c2Z2KUZ+NCvSac\n" +
			"LZo5RLrV+RbXVBAUvfuJFoR7Ov97/F3PjindenpdRlo7KkbBv4MCQQCh2NOrI00R\n" +
			"vY1TAzAOZ7AeTxhePSWM+ecjpmNMXECSTwSnit/M0N73BKLqIsSazPRA7Vlc5tvt\n" +
			"8AvNqLS5vCvZ";
	// 支付宝公钥
	public static final String RSA_PUBLIC = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC" +
			"nxj/9qwVfgoUh/y2W89L6BkRAFljhNhgPdyPuBV64bfQNN1PjbCzkIM6qRdKBoLPXmKKMiF" +
			"Ynkd6rAoprih3/PrQEB/VsW8OoM8fxn67UDYuyBTqA23MML9q1+ilIZwBC2AQ2UBVOrFXfFl" +
			"75p6/B5KsiNG9zpgmLCUYuLkxpLQIDAQAB";
	private static final int SDK_PAY_FLAG = 1;
	private static final int SDK_CHECK_FLAG = 2;

	private MyAlipayManager(Activity context) {
		this.mContext = context;
	}

	public static MyAlipayManager getInstance(Activity activity, MyPayCompleteListener payCompleteListener) {
		mAlipayManager = new MyAlipayManager(activity);
		mPayCompleteListener = payCompleteListener;
		return mAlipayManager;
	}

	int payTypeId;

	public MyAlipayManager setPayType(int payTypeId) {
		this.payTypeId = payTypeId;
		return mAlipayManager;
	}

	/*public MyAlipayManager setOnPayCompleteListener(MyPayCompleteListener payCompleteListener){
		this.mPayCompleteListener = payCompleteListener;
		return mAlipayManager;
	}*/

	/**
	 * 处理支付结果
	 */
	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case SDK_PAY_FLAG: {
					PayResult payResult = new PayResult((String) msg.obj);

					// 支付宝返回此次支付结果及加签，建议对支付宝签名信息拿签约时支付宝提供的公钥做验签
					String resultInfo = payResult.getResult();

					String resultStatus = payResult.getResultStatus();

					// 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
					if (TextUtils.equals(resultStatus, "9000")) {
						Toast.makeText(mContext, "支付成功",
								Toast.LENGTH_SHORT).show();
						mPayCompleteListener.onPayComplete(payTypeId);
					} else {
						// 判断resultStatus 为非“9000”则代表可能支付失败
						// “8000”代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
						if (TextUtils.equals(resultStatus, "8000")) {
							Toast.makeText(mContext, "支付结果确认中",
									Toast.LENGTH_SHORT).show();

						} else {
							// 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
							Toast.makeText(mContext, "支付失败" /*+ resultStatus*/,
									Toast.LENGTH_SHORT).show();

						}
					}
					break;
				}
				case SDK_CHECK_FLAG: {
					Toast.makeText(mContext, "检查结果为：" + msg.obj,
							Toast.LENGTH_SHORT).show();
					break;
				}
				default:
					break;
			}
		}

		;
	};


	/**
	 * call alipay sdk pay. 调用SDK支付
	 */
	public void pay(String productName, String productDescribe, String price) {
		if (TextUtils.isEmpty(PARTNER) || TextUtils.isEmpty(RSA_PRIVATE)
				|| TextUtils.isEmpty(SELLER)) {
			new AlertDialog.Builder(mContext)
					.setTitle("警告")
					.setMessage("需要配置PARTNER | RSA_PRIVATE| SELLER")
					.setPositiveButton("确定",
							new DialogInterface.OnClickListener() {
								public void onClick(
										DialogInterface dialoginterface, int i) {
									//

								}
							}).show();
			return;
		}
		// 订单
		String orderInfo = getOrderInfo(productName, productDescribe, price);
//		String orderInfo = getOrderInfo("测试的商品", "该测试商品的详细描述", "0.01");

		// 对订单做RSA 签名
		String sign = sign(orderInfo);
		try {
			// 仅需对sign 做URL编码
			sign = URLEncoder.encode(sign, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		// 完整的符合支付宝参数规范的订单信
		// 息
		final String payInfo = orderInfo + "&sign=\"" + sign + "\"&"
				+ getSignType();

		Runnable payRunnable = new Runnable() {

			@Override
			public void run() {
				// 构造PayTask 对象
				PayTask alipay = new PayTask(mContext);
				// 调用支付接口，获取支付结果
				String result = alipay.pay(payInfo, true);

				Message msg = new Message();
				msg.what = SDK_PAY_FLAG;
				msg.obj = result;
				mHandler.sendMessage(msg);
			}
		};

		// 必须异步调用
		Thread payThread = new Thread(payRunnable);
		payThread.start();
	}

	/**
	 * check whether the device has authentication alipay account.
	 * 查询终端设备是否存在支付宝认证账户
	 */
	public void check() {
		Runnable checkRunnable = new Runnable() {

			@Override
			public void run() {
				// 构造PayTask 对象
				PayTask payTask = new PayTask(mContext);
				// 调用查询接口，获取查询结果
//				boolean isExist = payTask.;
//
//				Message msg = new Message();
//				msg.what = SDK_CHECK_FLAG;
//				msg.obj = isExist;
//				mHandler.sendMessage(msg);
			}
		};

		Thread checkThread = new Thread(checkRunnable);
		checkThread.start();

	}

	/**
	 * get the sdk version. 获取SDK版本号
	 */
	public void getSDKVersion() {
		PayTask payTask = new PayTask(mContext);
		String version = payTask.getVersion();
		Toast.makeText(mContext, version, Toast.LENGTH_SHORT).show();
	}

	/**
	 * create the order info. 创建订单信息
	 */
	public String getOrderInfo(String subject, String body, String price) {

		// 签约合作者身份ID
		String orderInfo = "partner=" + "\"" + PARTNER + "\"";

		// 签约卖家支付宝账号
		orderInfo += "&seller_id=" + "\"" + SELLER + "\"";

		// 商户网站唯一订单号
		orderInfo += "&out_trade_no=" + "\"" + getOutTradeNo() + "\"";

		// 商品名称
		orderInfo += "&subject=" + "\"" + subject + "\"";

		// 商品详情
		orderInfo += "&body=" + "\"" + body + "\"";

		// 商品金额
		orderInfo += "&total_fee=" + "\"" + price + "\"";

		// 服务器异步通知页面路径
		/*orderInfo += "&notify_url=" + "\"" + "http://notify.msp.hk/notify.htm"
				+ "\"";*/
		orderInfo += "&notify_url=" + "\"" + Utils.SPGetString(mContext,Utils.payUrl,"")
				+ "\"";

		// 服务接口名称， 固定值
		orderInfo += "&service=\"mobile.securitypay.pay\"";

		// 支付类型， 固定值
		orderInfo += "&payment_type=\"1\"";

		// 参数编码， 固定值
		orderInfo += "&_input_charset=\"utf-8\"";

		// 设置未付款交易的超时时间
		// 默认30分钟，一旦超时，该笔交易就会自动被关闭。
		// 取值范围：1m～15d。
		// m-分钟，h-小时，d-天，1c-当天（无论交易何时创建，都在0点关闭）。
		// 该参数数值不接受小数点，如1.5h，可转换为90m。
		orderInfo += "&it_b_pay=\"30m\"";

		// extern_token为经过快登授权获取到的alipay_open_id,带上此参数用户将使用授权的账户进行支付
		// orderInfo += "&extern_token=" + "\"" + extern_token + "\"";

		// 支付宝处理完请求后，当前页面跳转到商户指定页面的路径，可空
		orderInfo += "&return_url=\"m.alipay.com\"";

		// 调用银行卡支付，需配置此参数，参与签名， 固定值 （需要签约《无线银行卡快捷支付》才能使用）
		// orderInfo += "&paymethod=\"expressGateway\"";

		return orderInfo;
	}

	/**
	 * get the out_trade_no for an order. 生成商户订单号，该值在商户端应保持唯一（可自定义格式规范）
	 */
	public String getOutTradeNo() {
		/*SimpleDateFormat format = new SimpleDateFormat("MMddHHmmss",
				Locale.getDefault());
		Date date = new Date();
		String key = format.format(date);

		Random r = new Random();
		key = key + r.nextInt();
		key = key.substring(0, 15);
		Utils.SPutString(mContext , Utils.payCode , "");*/
		return Utils.SPGetString(mContext , Utils.orderCode , "");
	}

	/**
	 * sign the order info. 对订单信息进行签名
	 *
	 * @param content 待签名订单信息
	 */
	public String sign(String content) {
		return SignUtils.sign(content, RSA_PRIVATE);
	}

	/**
	 * get the sign type we use. 获取签名方式
	 */
	public String getSignType() {
		return "sign_type=\"RSA\"";
	}
}
