package com.jzkj.jianzhenkeji_road_car_person.bean;

public class ItIsUser {
	private int img;
	private String mineText;
	public ItIsUser(int img, String mineText) {
		super();
		this.img = img;
		this.mineText = mineText;
	}
	public int getImg() {
		return img;
	}
	public void setImg(int img) {
		this.img = img;
	}
	public String getMineText() {
		return mineText;
	}
	public void setMineText(String mineText) {
		this.mineText = mineText;
	}
	
}
