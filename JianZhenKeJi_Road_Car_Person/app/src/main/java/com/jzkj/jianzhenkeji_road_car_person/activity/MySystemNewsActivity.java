package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.adapter.MySystemNewsAdapter;
import com.jzkj.jianzhenkeji_road_car_person.bean.MySystemNewsBean;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.kanak.emptylayout.EmptyLayout;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MySystemNewsActivity extends Activity {
	private ImageButton ibBack;
	private TextView tvTitle;
	private ListView lv;
	private MySystemNewsAdapter mAdapter;
	private ArrayList<MySystemNewsBean> list;

	private EmptyLayout mEmptyLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_system_news);
		init();
		initDate();
		bindListener();

	}

	private void initDate() {
		list = new ArrayList<MySystemNewsBean>();
		/*String text = "系统消息来了...";
		String[] dates = {"2016-03-22","2016-03-28","2016-03-29","2016-03-30"};
		for (int i = 0; i < dates.length; i++) {
			MySystemNewsBean bean = new MySystemNewsBean();
			bean.setDate(dates[i]);
			bean.setTextInfo(text);
			list.add(bean);
		}*/
		mAdapter = new MySystemNewsAdapter(this, R.layout.my_news_details_lv_item);
		getSystemNews();
		lv.setAdapter(mAdapter);
	}

	private void bindListener() {
		ibBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});
	}

	private void init() {
		ibBack = (ImageButton) findViewById(R.id.headframe_ib);
		tvTitle = (TextView) findViewById(R.id.headframe_title);
		lv = (ListView) findViewById(R.id.my_news_details_lv);
		tvTitle.setText("系统消息");
		mEmptyLayout = new EmptyLayout(MySystemNewsActivity.this, lv);
	}

	private void getSystemNews() {
		final ProgressDialog dialog = new ProgressDialog(MySystemNewsActivity.this);
		dialog.setMessage("正在加载中...");
		dialog.show();
		RequestParams params = new RequestParams();
		params.put("messagetype", 2);//系统消息
		params.put("type", 1);
		params.put("pageindex", 1);
		params.put("pagesize", 20);
		params.put("Userid", Utils.SPGetString(MySystemNewsActivity.this, Utils.userId, ""));
		KLog.e("params", params.toString());
		MyHttp.getInstance(this).post(MyHttp.GET_MYNEWS_INFO, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e("Debug", "JSONObject:");
				KLog.json(response.toString());
				/*try {
					Utils.ToastShort(MySystemNewsActivity.this,response.getString("Reason"));
				} catch (JSONException e) {
					e.printStackTrace();
				}*/
				new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							Thread.sleep(300);
							dialog.cancel();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}).start();
				mEmptyLayout.setEmptyMessage("宝宝好伤心，你还没有系统消息~");
				mEmptyLayout.showEmpty();
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e("Debug", "JSONArray:");
				KLog.json(response.toString());
				dialog.cancel();
				for (int i = 0; i < response.length(); i++) {
					try {
						JSONObject object = response.getJSONObject(i);
						MySystemNewsBean bean = new MySystemNewsBean();
						bean.setMessageType(object.getInt("MessageType"));
						bean.setRC_MessageId(object.getInt("RC_MessageId"));
						bean.setMsgContent(object.getString("MsgContent"));
						bean.setPushDate(object.getString("PushDate"));
						bean.setUserId(object.getString("UserId"));
						list.add(bean);

					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				mAdapter.addAll(list);
				if (list.size() == 0) {
					mEmptyLayout.setEmptyMessage("宝宝好伤心，你还没有系统消息~");
					mEmptyLayout.showEmpty();
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							Thread.sleep(300);
							dialog.cancel();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}).start();
				mEmptyLayout.setEmptyMessage("宝宝好伤心，你还没有系统消息~");
				mEmptyLayout.showEmpty();
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
				super.onFailure(statusCode, headers, throwable, errorResponse);
				new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							Thread.sleep(300);
							dialog.cancel();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}).start();
				mEmptyLayout.setEmptyMessage("宝宝好伤心，你还没有系统消息~");
				mEmptyLayout.showEmpty();
			}
		});
	}
}
