package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.bean.SchoolBean;
import com.jzkj.jianzhenkeji_road_car_person.bean.SchoolName;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.jzkj.jianzhenkeji_road_car_person.util.sortlist.CharacterParser;
import com.jzkj.jianzhenkeji_road_car_person.util.sortlist.PinyinComparator;
import com.jzkj.jianzhenkeji_road_car_person.util.sortlist.SideBar;
import com.jzkj.jianzhenkeji_road_car_person.util.sortlist.SortAdapter;
import com.jzkj.jianzhenkeji_road_car_person.util.sortlist.SortModel;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ChooseSchoolActivity1 extends Activity {

	private static final String BACK_FROM_CHOOSECITY = "0X999";

	private TextView title;
	private EditText searchText;
	private ListView sortListView;
	private ListView searchListView;
	private SideBar sideBar;
	private TextView dialog;
	private SortAdapter adapter;
	private ViewGroup layout;

	/**
	 * 汉字转换成拼音的类
	 */
	private CharacterParser characterParser;
	private List<SortModel> SourceDateList;

	private List<SchoolBean> hotSchools;
	private List<SchoolBean> schools;
	private List<SchoolBean> allSchools;
	private List<String> searchStringList;

	AlertDialog mDialog;

	ArrayAdapter<String> arrayAdapter;
	/**
	 * 根据拼音来排列ListView里面的数据类
	 */
	private PinyinComparator pinyinComparator;
	private boolean isSearchIng;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_choose_school);
		title = (TextView) findViewById(R.id.headframe_title);
		findViewById(R.id.headframe_ib).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
		searchText = (EditText) findViewById(R.id.choose_school_search);
		sortListView = (ListView) findViewById(R.id.country_lvcountry);
		searchListView = (ListView) findViewById(R.id.choose_school_list2);
		layout = (ViewGroup) findViewById(R.id.choose_school_layout);

		hotSchools = new ArrayList<SchoolBean>();
		schools = new ArrayList<SchoolBean>();
		getHotSchool();
		title.setText("选择驾校");

	}


	/**
	 * initView
	 */
	private void initViews() {
		System.out.println("ChooseSchoolActivity.initViews");
//		title.setText("选择驾校");

		//实例化汉字转拼音类
		characterParser = CharacterParser.getInstance();
		pinyinComparator = new PinyinComparator();

		sideBar = (SideBar) findViewById(R.id.sidrbar);
		dialog = (TextView) findViewById(R.id.dialog);
		sideBar.setTextView(dialog);

		initListener();

		initData();
	}

	private void initData() {

		String[] s = new String[schools.size()];
		for (int i = 0; i < s.length; i++) {
			s[i] = schools.get(i).getName();
		}
		SourceDateList = filledData(s);

		// 根据a-z进行排序源数据
		Collections.sort(SourceDateList, pinyinComparator);

		/**
		 * 对正常数据排序后, 列表顶部加入热门驾校
		 */
		for (int i = hotSchools.size() - 1; i > -1; i--) {
			SortModel model = new SortModel();
			model.setName(hotSchools.get(i).getName());
			model.setSortLetters("热门驾校");
			SourceDateList.add(0, model);
		}

		adapter = new SortAdapter(this, SourceDateList);
		sortListView.setAdapter(adapter);
	}


	/**
	 * 为ListView填充数据
	 *
	 * @param date
	 * @return
	 */
	private List<SortModel> filledData(String[] date) {
		List<SortModel> mSortList = new ArrayList<SortModel>();


		for (int i = 0; i < date.length; i++) {
			SortModel sortModel = new SortModel();
			sortModel.setName(date[i]);
			//汉字转换成拼音
			String pinyin;
			String sortString;
			if (date[i].equals("重庆")) {
				pinyin = characterParser.getSelling(date[i]);
				sortString = /*pinyin.substring(0, 1).toUpperCase()*/ "C";
			} else {
				pinyin = characterParser.getSelling(date[i]);
				sortString = pinyin.substring(0, 1).toUpperCase();
			}

			// 正则表达式，判断首字母是否是英文字母
			if (sortString.matches("[A-Z]")) {
				sortModel.setSortLetters(sortString.toUpperCase());
			} else {
				sortModel.setSortLetters("#");
			}
			mSortList.add(sortModel);
		}

		return mSortList;

	}

	/**
	 * 事件监听
	 */
	private void initListener() {
		System.out.println("ChooseSchoolActivity.initListener");
		//设置右侧触摸监听
		sideBar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {

			@Override
			public void onTouchingLetterChanged(String s) {
				//该字母首次出现的位置
				int position = adapter.getPositionForSection(s.charAt(0));
				if (position != -1) {
					sortListView.setSelection(position);
				}

			}
		});

		/**
		 * item 点击事件
		 */
		sortListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
			                        int position, long id) {
				//这里要利用adapter.getItem(position)来获取当前position所对应的对象
				String schoolName = "";

				//Toast.makeText(getApplication(), ((SortModel)adapter.getItem(position)).getName() + "  " + position, Toast.LENGTH_SHORT).show();
				schoolName = ((SortModel) adapter.getItem(position)).getName();

				String schoolId = "";
				String schoolUrl = "";
				for (int i = 0; i < allSchools.size(); i++) {
					if (allSchools.get(i).getName().equals(schoolName)) {
						schoolId = allSchools.get(i).getId();
						schoolUrl = allSchools.get(i).getSchoolUrl();
					}
				}
				mDialog = new AlertDialog.Builder(ChooseSchoolActivity1.this).create();
				mDialog.setMessage("是否绑定 : " + schoolName+"驾校");
				final String finalSchoolId = schoolId;
				final String finalSchoolUrl = schoolUrl;
				final String finalSchoolName = schoolName;
				mDialog.setButton(DialogInterface.BUTTON_POSITIVE, "确定", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
//						bindSchool(finalSchoolId, finalSchoolName, finalSchoolUrl);
						Intent intent = new Intent(ChooseSchoolActivity1.this,BaseInformationActivity.class);
						Bundle bundle = new Bundle();
						bundle.putString("schoolName",finalSchoolName);
						bundle.putString("schoolId",finalSchoolId);
						intent.putExtra("bun",bundle);
						setResult(RESULT_OK, intent);
						ChooseSchoolActivity1.this.finish();
					}
				});
				mDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						mDialog.cancel();
					}
				});
				mDialog.show();


			}
		});


		/**
		 * 搜索列表item 点击事件
		 */
		searchListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
			                        int position, long id) {
				//这里要利用adapter.getItem(position)来获取当前position所对应的对象
				String schoolName = "";

				//Toast.makeText(getApplication(), ((String)arrayAdapter.getItem(position).toString()) + "  " + position, Toast.LENGTH_SHORT).show();
				schoolName = arrayAdapter.getItem(position).toString();

				String schoolId = "";
				String schoolUrl = "";
				for (int i = 0; i < allSchools.size(); i++) {
					if (allSchools.get(i).getName().equals(schoolName)) {
						schoolId = allSchools.get(i).getId();
						schoolUrl = allSchools.get(i).getSchoolUrl();
					}
				}
				mDialog = new AlertDialog.Builder(ChooseSchoolActivity1.this).create();
				mDialog.setMessage("是否绑定 : " + schoolName+"驾校");
				final String finalSchoolId = schoolId;
				final String finalSchoolName = schoolName;
				final String finalSchoolUrl = schoolUrl;
				mDialog.setButton(DialogInterface.BUTTON_POSITIVE, "确定", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
//						bindSchool(finalSchoolId, finalSchoolName, finalSchoolUrl);
						Intent intent = new Intent(ChooseSchoolActivity1.this,BaseInformationActivity.class);
						Bundle bundle = new Bundle();
						bundle.putString("schoolName",finalSchoolName);
						bundle.putString("schoolId",finalSchoolId);
						intent.putExtra("bun",bundle);
						setResult(998, intent);
						ChooseSchoolActivity1.this.finish();
					}
				});
				mDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						mDialog.cancel();
					}
				});
				mDialog.show();


			}
		});

		searchText.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

			}

			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

			}

			@Override
			public void afterTextChanged(Editable editable) {
				if (editable.toString().length() == 0) {
					KLog.e(Utils.TAG_DEBUG, "ChooseSchoolActivity.afterTextChanged  1");

					//searchListView.setVisibility(View.GONE);
					//sortListView.setVisibility(View.VISIBLE);
					layout.setVisibility(View.VISIBLE);
				} else {
					KLog.e(Utils.TAG_DEBUG, "ChooseSchoolActivity.afterTextChanged  2");
					layout.setVisibility(View.GONE);

					searchSchool(editable.toString());
				}
			}
		});
	}

	/**
	 * 获取热门驾校
	 */
	private void getHotSchool() {
		MyHttp.getInstance(this).post(MyHttp.GET_SCHOOL, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers,
			                      JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, response.toString());
				try {
					for (int i = 0; i < response.length(); i++) {
						JSONObject object = response.getJSONObject(i);
						SchoolBean bean = new SchoolBean();
						bean.setId(object.getString("DrivingSchoolId"));
						bean.setName(object.getString("SchoolName"));
						bean.setSchoolUrl(object.getString("SchoolProfiles"));

						hotSchools.add(bean);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				KLog.e(Utils.TAG_DEBUG, response.toString());
				getSchools();
			}

		});
	}

	/**
	 * 获取驾校列表
	 */
	private void getSchools() {
		RequestParams params = new RequestParams();
		params.put("Type", 1);
		KLog.e(params.toString());
		MyHttp.getInstance(this).post(MyHttp.GET_SCHOOL, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers,
			                      JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, response.toString());
				try {
					for (int i = 0; i < response.length(); i++) {
						JSONObject object = response.getJSONObject(i);
						SchoolBean bean = new SchoolBean();
						bean.setId(object.getString("DrivingSchoolId"));
						bean.setName(object.getString("SchoolName"));
						bean.setSchoolUrl(object.getString("SchoolProfiles"));
						schools.add(bean);
					}
					allSchools = new ArrayList<SchoolBean>();
					allSchools.addAll(schools);
					allSchools.addAll(0, hotSchools);

					initViews();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				KLog.e(Utils.TAG_DEBUG, response.toString());

			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, String responseString) {
				super.onSuccess(statusCode, headers, responseString);
				KLog.e(Utils.TAG_DEBUG, responseString);

			}

			@Override
			public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
				super.onFailure(statusCode, headers, throwable, errorResponse);
				KLog.e(Utils.TAG_DEBUG, errorResponse.toString());
			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
			                      String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				KLog.e(Utils.TAG_DEBUG, responseString);
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
				super.onFailure(statusCode, headers, throwable, errorResponse);
				KLog.e(Utils.TAG_DEBUG, errorResponse.toString());
			}
		});
	}


	/**
	 * 搜索驾校
	 */
	private void searchSchool(String name) {
	/*	sortListView.setVisibility(View.GONE);
		searchListView.setVisibility(View.VISIBLE);*/
		searchStringList = new ArrayList<String>();
		arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
		searchListView.setAdapter(arrayAdapter);
		allSchools.clear();

		RequestParams params = new RequestParams();
		params.put("Name", name);
		System.out.println("搜索驾校参数: " + params.toString());
		MyHttp.getInstance(this).post(MyHttp.SEARCH_SCHOOL, params, new JsonHttpResponseHandler() {

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, response.toString());

			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
			                      JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, response.toString());
				searchStringList.clear();
				arrayAdapter.notifyDataSetChanged();
				try {
					for (int i = 0; i < response.length(); i++) {
						JSONObject object = response.getJSONObject(i);
						SchoolBean bean = new SchoolBean();
						bean.setId(object.getString("DrivingSchoolId"));
						bean.setName(object.getString("SchoolName"));
						bean.setSchoolUrl(object.getString("SchoolProfiles"));
						searchStringList.add(bean.getName());
						allSchools.add(bean);
					}
					System.out.println(searchStringList.size() + "------");
					arrayAdapter.addAll(searchStringList);
					arrayAdapter.notifyDataSetChanged();

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}


//	/**
//	 * 绑定驾校
//	 */
//	private void bindSchool(final String id, final String name, final String schoolUrl) {
//
//		RequestParams params = new RequestParams();
//		params.put("UserId", Utils.SPGetString(this, Utils.userId, ""));
//		params.put("DrivingSchoolId", id);
//		System.out.println("bind驾校参数: " + params.toString());
//		MyHttp.getInstance(this).post(MyHttp.BIND_SCHOOL, params, new JsonHttpResponseHandler() {
//
//			@Override
//			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
//				super.onSuccess(statusCode, headers, response);
//				KLog.e(Utils.TAG_DEBUG, response.toString());
//				try {
//					Utils.ToastShort(ChooseSchoolActivity1.this, response.getString("Reason"));
//					if (response.getInt("Code") == 1) {
//						Utils.SPutString(ChooseSchoolActivity1.this, Utils.schoolName, name);
//						Utils.SPutString(ChooseSchoolActivity1.this, Utils.schoolId, id);
//						Utils.SPutString(ChooseSchoolActivity1.this, Utils.SchoolUrl, schoolUrl);
//						EventBus.getDefault().post(new SchoolName(name));
//						finish();
//					}
//				} catch (JSONException e) {
//					e.printStackTrace();
//				}
//
//			}
//
//
//			@Override
//			public void onFailure(int statusCode, Header[] headers,
//			                      String responseString, Throwable throwable) {
//				super.onFailure(statusCode, headers, responseString, throwable);
//
//			}
//		});
//	}


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}


	@Override
	public void finish() {
		/*Intent intent = new Intent();
		intent.putExtra("p", location);
		setResult(0x12, intent);*/
		super.finish();
	}

}
