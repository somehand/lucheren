package com.jzkj.jianzhenkeji_road_car_person.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.jzkj.jianzhenkeji_road_car_person.MyApplication;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.activity.ChooseSchoolActivity;
import com.jzkj.jianzhenkeji_road_car_person.activity.ExamActivity;
import com.jzkj.jianzhenkeji_road_car_person.bean.BannerBean;
import com.jzkj.jianzhenkeji_road_car_person.bean.SchoolName;
import com.jzkj.jianzhenkeji_road_car_person.bean.UserInformationExchange;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.socks.library.KLog;
import com.zanlabs.widget.infiniteviewpager.InfinitePagerAdapter;
import com.zanlabs.widget.infiniteviewpager.InfiniteViewPager;
import com.zanlabs.widget.infiniteviewpager.indicator.CirclePageIndicator;

import org.apache.http.Header;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;

public class HomePage extends Fragment implements View.OnTouchListener, PullToRefreshBase.OnRefreshListener<ScrollView> {
	/*@InjectView(R.id.home_page_vp)
	InfiniteViewPager mVp;*/
	private View view;
	//声明控件
	private InfiniteViewPager vp;
	private ImageView mImageView;
	private CirclePageIndicator mPageIndicator;
/*	private int[] images = new int[]{R.drawable.home_page_vp_img, R.drawable.home_page_vp_img
			, R.drawable.home_page_vp_img, R.drawable.home_page_vp_img, R.drawable.home_page_vp_img
	};*/

	private LinearLayout btnCourseOne, btnCourseTwo, btnCourseThree, btnCourseFour, btnMyBusiness, btnMyCoach, btnDriveSchool, btnScoreStore;
	private ImageView ivPracticeExam;
//	private PullToRefreshScrollView mPullToRefreshScrollView;
//	private ListView lv;

	private RelativeLayout rlTrainCar, rlTestQuestion, rlBreakRules, rlChelianwang, rlIntegralStore, rlLookRoom, rlInsure, rlLearnCar;
	private ArrayList<UserInformationExchange> lvList;
	private ArrayList<BannerBean> urls;
//	HomePageLVAdapter listAdapter;

	MyInfiniteAdapter mAdapter;
	private FragmentTransaction fragmentTransaction;
	HomePageFragmentOne fragmentOne = new HomePageFragmentOne();

	//Timer类
	private Timer timer;
	private int i; //当前的item
	private boolean isFirst = true; //第一次初始化


	@Override
	public void onCreate(Bundle savedInstanceState) {
		mAdapter = new MyInfiniteAdapter();
		getLunbo();
		urls = new ArrayList<BannerBean>();
		EventBus.getDefault().register(this);
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {

		view = inflater.inflate(R.layout.fragment_home, container, false);
		vp = (InfiniteViewPager) view.findViewById(R.id.home_page_vp);
		mPageIndicator = (CirclePageIndicator) view.findViewById(R.id.home_page_indicator);

//		fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
		fragmentTransaction = getChildFragmentManager().beginTransaction();
		fragmentTransaction.add(R.id.fragment_home_frame_vp, fragmentOne);
		fragmentTransaction.commit();


		//*************start之前的******************************************************************
		btnCourseOne = (LinearLayout) view.findViewById(R.id.home_page_curseone);
		btnCourseTwo = (LinearLayout) view.findViewById(R.id.home_page_cursetwo);
		btnCourseThree = (LinearLayout) view.findViewById(R.id.home_page_cursethree);
		btnCourseFour = (LinearLayout) view.findViewById(R.id.home_page_cursefour);

		btnMyBusiness = (LinearLayout) view.findViewById(R.id.home_page_mybusiness);
		btnDriveSchool = (LinearLayout) view.findViewById(R.id.home_page_drive_school_info);
		btnMyCoach = (LinearLayout) view.findViewById(R.id.home_page_mycoach);
		btnScoreStore = (LinearLayout) view.findViewById(R.id.home_page_score_store);
		ivPracticeExam = (ImageView) view.findViewById(R.id.home_page_iv_practice_exam);
//		lv = (ListView) view.findViewById(R.id.home_page_lv);
		//*************end******************************************************************

		init();
		setListener();
		ButterKnife.inject(this, view);
		return view;
	}

	/**
	 * 绑定监听
	 */
	private void setListener() {


		//************************之前的**********************************
//		btnCourseOne.setOnClickListener(this);
//		btnCourseTwo.setOnClickListener(this);
//		btnCourseThree.setOnClickListener(this);
//		btnCourseFour.setOnClickListener(this);
//
//		btnScoreStore.setOnClickListener(this);
//		btnMyCoach.setOnClickListener(this);
//		btnDriveSchool.setOnClickListener(this);
//		btnMyBusiness.setOnClickListener(this);
//
//		ivPracticeExam.setOnClickListener(this);
	/*	lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				addReadNum(lvList.get(i).getId());
				Intent intent = new Intent(getActivity(), Blank.class);
				intent.putExtra("url", lvList.get(i).getUrl());
				intent.putExtra("title", lvList.get(i).getTitle());
				intent.putExtra("articelId", lvList.get(i).getId());
				startActivity(intent);
			}
		});*/
	/*	vp.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

			}

			@Override
			public void onPageSelected(int position) {

			}

			@Override
			public void onPageScrollStateChanged(int state) {

			}
		})*/
		;
	}

	/**
	 * 绑定id
	 */
	void init() {
		vp.setAdapter(mAdapter);
		vp.setAutoScrollTime(2000);
		vp.startAutoScroll();
		//	vp.setCurrentItem(0);
		mPageIndicator.setViewPager(vp);

		//设置相应科目以及模拟考试的监听

		lvList = new ArrayList<UserInformationExchange>();
//		listAdapter = new HomePageLVAdapter(getActivity(), R.layout.home_page_lv_item);
//		getHomeArticle();
//		lv.setAdapter(listAdapter);
		//解决scrollview与listview的高度冲突
		//Util.setListViewHeight(lv);

		//解决焦点在listview的问题
//		lv.setFocusable(false);

	}

	/**
	 * 弹出选择驾校dialog
	 */
	private void showDialog() {
		Boolean firstlogin = Utils.SPGetBoolean(getActivity(), Utils.SPGetString(getActivity(), Utils.userName, ""), false);
		if (firstlogin) {
			AlertDialog.Builder aBuilder = new AlertDialog.Builder(getActivity());
			aBuilder.setMessage("是否进行科目一的学习?");
			aBuilder.setPositiveButton("是", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					startActivity(new Intent(getActivity(), ExamActivity.class));
					Utils.SPPutBoolean(getActivity(), Utils.SPGetString(getActivity(), Utils.userName, ""), false);
				}
			});
			aBuilder.setNegativeButton("否", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					Utils.SPPutBoolean(getActivity(), Utils.SPGetString(getActivity(), Utils.userName, ""), false);
					dialogInterface.dismiss();
				}
			});
			AlertDialog dialog = aBuilder.create();
			dialog.show();
		}
		if (Utils.SPGetString(getActivity(), Utils.schoolId, "").length() == 0 && Utils.SPGetBoolean(getActivity(), Utils.noUpdate, true)) {
			AlertDialog.Builder aBuilder = new AlertDialog.Builder(getActivity());
			aBuilder.setMessage("您还未绑定驾校，是否现在绑定？");
			aBuilder.setPositiveButton("是", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					startActivity(new Intent(getActivity(), ChooseSchoolActivity.class));
				}
			});
			aBuilder.setNegativeButton("否", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					dialogInterface.dismiss();
				}
			});
			AlertDialog dialog = aBuilder.create();
			dialog.show();
		}
	}


	@Override
	public void onStart() {
		super.onStart();
		if (vp != null) {
			vp.startAutoScroll();
		}
	}


	@Override
	public void onStop() {
		super.onStop();
		if (vp != null) {
			vp.stopAutoScroll();
		}
	}


	@Override
	public void onRefresh(PullToRefreshBase<ScrollView> refreshView) {
		String label = DateUtils.formatDateTime(getActivity(),
				System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME
						| DateUtils.FORMAT_SHOW_DATE
						| DateUtils.FORMAT_ABBREV_ALL);

		// Update the LastUpdatedLabel
		refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);
		//mHandler.sendEmptyMessageDelayed(1, 1800);
		lvList.clear();
//		getHomeArticle();
		getLunbo();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		ButterKnife.reset(this);
	}

	/**
	 * 初始化PagerAdapter
	 */

	public class MyInfiniteAdapter extends InfinitePagerAdapter {


		@Override
		public int getItemCount() {
			return urls == null ? 0 : urls.size();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup container) {
			final BannerBean bean = urls.get(position);
			ImageView imageView = new ImageView(getActivity());
			ImageLoader.getInstance().displayImage(bean.getImgURL(), imageView, MyApplication.options_other);
			imageView.setAdjustViewBounds(true);
			imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
		/*	imageView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					Intent intent = new Intent(getActivity(), WebPageActivity.class);
					intent.putExtra("titleName", "详情");
					intent.putExtra("type", 3);
					intent.putExtra("url", bean.getArticleURL());
					startActivity(intent);
				}
			});*/
			container.addView(imageView);
			return imageView;
		}

		/*@Override
		public Object instantiateItem(View container, int position) {

			return super.instantiateItem(container, position);
		}*/
	}

	Handler mHandler2 = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			//vp.setCurrentItem(i);
			//mPageIndicator.setCurrentItem(i);
			//System.out.println("HomePage.run  当前位置: " + i);
		}
	};

	boolean isTimerRuning;

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		System.out.println("HomePage.onHiddenChanged  " + hidden);
		if (!hidden) {
			timer = null;
			timer = new Timer();
			if (!isTimerRuning) {
				timer.schedule(new TimerTask() {
					@Override
					public void run() {
						i = vp.getCurrentItem();
						i = i < urls.size() - 1 ? i + 1 : 0;
						mHandler2.sendMessage(mHandler2.obtainMessage());
						/*vp.post(new Runnable() {
							@Override
							public void run() {
								vp.setCurrentItem(i);
								mPageIndicator.setCurrentItem(i);
								System.out.println("HomePage.run  当前位置: " + i);
							}
						});*/
					}
				}, 2000, 3000);

				if (isFirst) {
					showDialog();
					isFirst = false;
				}
			}
		} else if (hidden && timer != null) {
			timer.cancel();
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		System.out.println("HomePage.onPause");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		EventBus.getDefault().unregister(this);
		if (timer != null) {
			timer.cancel();
		}
	}

	public boolean onTouch(View v, MotionEvent event) {
		return true;
	}


	String schoolName = "";

	/**
	 * 获取轮播图 和驾校名
	 */
	public void getLunbo() {
		RequestParams params = new RequestParams();
		params.put("DrivingSchoolId", Utils.SPGetString(getActivity(), Utils.schoolId, ""));
		params.put("AppType", 1);
		params.put("UserId", Utils.SPGetString(getActivity(), Utils.userId, ""));
		KLog.e(Utils.TAG_DEBUG, params.toString());

		MyHttp.getInstance(getActivity()).post(MyHttp.GET_BANNER, params, new JsonHttpResponseHandler() {

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonArray :");
				System.out.println(response.toString() + "----------");
				KLog.json(Utils.TAG_DEBUG, response.toString());
				urls.clear();
				if (statusCode == 200) {
					try {
						for (int i = 0; i < response.length(); i++) {
							JSONObject object = response.getJSONObject(i);
							BannerBean bean = new BannerBean();
							bean.setArticleURL(object.getString("ArticleURL"));
							bean.setImgURL(object.getString("ImgURL"));
							bean.setSchoolName(object.getString("SchoolName"));
							bean.setVerson(object.getString("verson"));

							if (schoolName.length() == 0) {
								schoolName = object.getString("SchoolName");
								EventBus.getDefault().post(new SchoolName(schoolName));
							}
							urls.add(bean);
							mAdapter.notifyDataSetChanged();
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}

			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(Utils.TAG_DEBUG, response.toString());
							BannerBean bean = new BannerBean();
							bean.setImgURL(MyHttp.STABLE_BANNER);
							urls.add(bean);
							mAdapter.notifyDataSetChanged();
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				KLog.json(Utils.TAG_DEBUG, responseString);

			}
		});
	}

	/**
	 * 获取首页listview信息
	 */
	/*public void getHomeArticle() {
		MyHttp.getInstance(getActivity()).post(MyHttp.GET_HOME_ARTICLE, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonObject :");
				KLog.json(Utils.TAG_DEBUG, response.toString());
//				mHandler.sendEmptyMessageDelayed(1, 0);

			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonArray :");
				KLog.json(Utils.TAG_DEBUG, response.toString());
				try {
					for (int i = 0; i < response.length(); i++) {
						JSONObject object = response.getJSONObject(i);
						String title = object.getString("ArticleTitle");
						String id = object.getString("AppIndexArticleId");
						String url = object.getString("ArticleURL");
						String imgUrls = object.getString("ImgURL");
						String readNum = object.getString("ReadNum");

						String[] ss = imgUrls.split("\\|");
						lvList.add(new UserInformationExchange(id, title, ss[0], ss[1], ss[2], readNum, url));
						//KLog.e(lvList.size() + "--------------- " + lvList.get(0).toString());

					}
					listAdapter.clear();
					listAdapter.addAll(lvList);
					Util.setListViewHeight(lv);
					mHandler.sendEmptyMessageDelayed(1, 800);

				} catch (JSONException e) {
					e.printStackTrace();
				}


			}

		});
	}*/

	/**
	 * 增加阅读量
	 *//*
	public void addReadNum(String AppIndexArticleId) {
		RequestParams params = new RequestParams();
		params.put("AppIndexArticleId", AppIndexArticleId);
		KLog.e(Utils.TAG_DEBUG, params.toString());
		MyHttp.getInstance(getActivity()).post(MyHttp.ARTICLE_READNUM_ADD, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonObject :");
				KLog.json(Utils.TAG_DEBUG, response.toString());
				try {
					if (response.getInt("Code") == 1) {
						KLog.e(Utils.TAG_DEBUG, "点击成功");
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonArray :");
				KLog.json(Utils.TAG_DEBUG, response.toString());

			}

		});
	}*/

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void setSchoolName(SchoolName schoolName) {
		getLunbo();
	}


}


