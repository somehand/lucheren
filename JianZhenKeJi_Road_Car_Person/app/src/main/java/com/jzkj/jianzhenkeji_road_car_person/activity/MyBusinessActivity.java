package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.CircleImageView;
import com.jzkj.jianzhenkeji_road_car_person.MyApplication;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Administrator on 2016/4/6.
 */
public class MyBusinessActivity extends Activity implements View.OnClickListener {
	@InjectView(R.id.headframe_ib)
	ImageButton mHeadframeIb;
	@InjectView(R.id.headframe_title)
	TextView mHeadframeTitle;
	@InjectView(R.id.my_business_headimg)
	CircleImageView mMyBusinessHeadimg;
	@InjectView(R.id.my_business_name)
	TextView mMyBusinessName;
	@InjectView(R.id.my_business_driveschool)
	TextView mMyBusinessDriveschool;
	@InjectView(R.id.my_business_requestcode)
	TextView mMyBusinessRequestcode;
	@InjectView(R.id.my_business_choosetemplate)
	ImageButton mMyBusinessChoosetemplate;
	@InjectView(R.id.my_business_etexplain)
	EditText mMyBusinessEtexplain;
	@InjectView(R.id.my_business_btnshare)
	Button mMyBusinessBtnshare;

	private int i = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_business);
		ButterKnife.inject(this);
		init();
		initData();
		initListener();
	}

	private void initListener() {
		mHeadframeIb.setOnClickListener(this);
		mMyBusinessBtnshare.setOnClickListener(this);
		mMyBusinessChoosetemplate.setOnClickListener(this);
	}

	private void initData() {
	}

	private void init() {
		mHeadframeTitle.setText("我的名片");
		ImageLoader.getInstance().displayImage(Utils.SPGetString(MyBusinessActivity.this,Utils.userLogo,""),mMyBusinessHeadimg, MyApplication.options_user);
		mMyBusinessName.setText(Utils.SPGetString(MyBusinessActivity.this,Utils.realName,"name"));
		mMyBusinessDriveschool.setText(Utils.SPGetString(MyBusinessActivity.this,Utils.schoolName,"schoolname"));

	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.headframe_ib:
				finish();
				break;
			case R.id.my_business_choosetemplate:
				final String[] text = {"今天，我跟奶奶玩词语开花结果的游戏。我用白字做种子，开了一朵三层共12瓣的“联字”花。奶奶用“花”做种子，也开了一朵同样的“联字”花。然后我们各自用自己的12片花瓣去谱写一段文字。","我觉得这个游戏新鲜有趣，便很用心地谱写。奶奶说我写的“干净”，“利落”而且１２瓣全都用上了。你看，下面就是我的成绩，可以打多少分呢？\n" +
						"冬天的早晨，刚刚下过一场鹅毛大雪，空气格外清新。白雪汇集在陆地上、屋顶上、电线上、树枝上，......到处白茫茫的一片。清洁工们一起出动，把雪赶到了马路边上......"};
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				LayoutInflater inflater = LayoutInflater.from(MyBusinessActivity.this);
				View adview = inflater.inflate(R.layout.my_business_dialog_template,null);
				Button btnNext = (Button) adview.findViewById(R.id.btn_next);
				final TextView tvContent = (TextView) adview.findViewById(R.id.text_content);
				AlertDialog dialog = builder.create();
				dialog.show();
				dialog.setContentView(adview);
				btnNext.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						tvContent.setText(text[i]);
						i++;
						if (i == text.length) {
							i = 0;
						}
					}
				});
				break;
			case R.id.my_business_btnshare:

				break;
			default:
				break;
		}
	}
}
