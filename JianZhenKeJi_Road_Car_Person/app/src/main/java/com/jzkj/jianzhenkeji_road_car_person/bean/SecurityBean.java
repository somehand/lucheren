package com.jzkj.jianzhenkeji_road_car_person.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/4/14.
 * SecurityOfficerId：安全员id
 ExaminationId--考场信息ID
 OfficerName--安全员姓名
 LogoURL--头像
 OfficerSex--性别
 Mobile--联系电话，
 IDCardNumber--身份证号码，
 Signature--个性签名，
 Introduce--介绍

 */
public class SecurityBean implements Serializable {
    private  String SecurityOfficerId;
    private  String OfficerName;
    private  String LogoURL;
    private  String OfficerSex;
    private  String Mobile;
    private  String IDCardNumber;
    private  String Signature;
    private  String Introduce;

    public String getSecurityOfficerId() {
        return SecurityOfficerId;
    }

    public void setSecurityOfficerId(String securityOfficerId) {
        SecurityOfficerId = securityOfficerId;
    }

    public String getOfficerName() {
        return OfficerName;
    }

    public void setOfficerName(String officerName) {
        OfficerName = officerName;
    }

    public String getLogoURL() {
        return LogoURL;
    }

    public void setLogoURL(String logoURL) {
        LogoURL = logoURL;
    }

    public String getOfficerSex() {
        return OfficerSex;
    }

    public void setOfficerSex(String officerSex) {
        OfficerSex = officerSex;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getIDCardNumber() {
        return IDCardNumber;
    }

    public void setIDCardNumber(String IDCardNumber) {
        this.IDCardNumber = IDCardNumber;
    }

    public String getSignature() {
        return Signature;
    }

    public void setSignature(String signature) {
        Signature = signature;
    }

    public String getIntroduce() {
        return Introduce;
    }

    public void setIntroduce(String introduce) {
        Introduce = introduce;
    }
}
