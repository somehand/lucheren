package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.adapter.OrderNewsAdapter;
import com.jzkj.jianzhenkeji_road_car_person.bean.OrderNewsBean;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.kanak.emptylayout.EmptyLayout;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Administrator on 2016/3/16.
 */
public class MyOrderNewsActivity extends Activity {
	@InjectView(R.id.headframe_ib)
	ImageButton mHeadframeIb;
	@InjectView(R.id.headframe_title)
	TextView mHeadframeTitle;
	@InjectView(R.id.headframe_btn)
	Button mHeadframeBtn;
	@InjectView(R.id.headframe_layout)
	LinearLayout mHeadframeLayout;
	@InjectView(R.id.headframe_gray_line)
	TextView mHeadframeGrayLine;
	@InjectView(R.id.check_order_lv)
	ListView mCheckOrderLv;
	OrderNewsAdapter adapter;
	boolean isable;

	private ArrayList<OrderNewsBean> list;
	private String[] studentNum;
	private EmptyLayout mEmptyLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_order_news);
		ButterKnife.inject(this);
		initDate();
		bindListener();
	}

	private void bindListener() {
		mHeadframeIb.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});
	}

	private void initDate() {
		mHeadframeTitle.setText("预约消息");
		mEmptyLayout = new EmptyLayout(MyOrderNewsActivity.this, mCheckOrderLv);
		isable = getIntent().getBooleanExtra("enable", true);
		list = new ArrayList<OrderNewsBean>();
		getInfo();

		mCheckOrderLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				/*if (list.get(i).getTypes() == 3) {
					Intent intent = new Intent(MyOrderNewsActivity.this, MyOrderNewsDetailsActivity.class);
					intent.putExtra("bean", list.get(i));
					startActivity(intent);
				}*/
			}
		});
	}

	/**
	 * 获取消息列表
	 */
	private void getInfo() {
		RequestParams params = new RequestParams();
		params.put("messagetype", 1);//预约消息
		params.put("type", 1);
		params.put("pageindex", 1);
		params.put("pagesize", 10);
		params.put("Userid", Utils.SPGetString(MyOrderNewsActivity.this, Utils.userId, ""));
		KLog.e("params", params.toString());
		MyHttp.getInstance(this).post(MyHttp.GET_MYNEWS_INFO, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				/*try {
					Utils.ToastShort(MyOrderNewsActivity.this, response.getString("Reason"));
				} catch (JSONException e) {
					e.printStackTrace();
				}*/
				mEmptyLayout.setEmptyMessage("宝宝好伤心，你还没有预约消息~");
				mEmptyLayout.showEmpty();
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
				for (int i = 0; i < response.length(); i++) {
					try {
						JSONObject object = response.getJSONObject(i);
						OrderNewsBean bean = new OrderNewsBean();
						bean.setHeaderUrl(object.getString("UserLogo"));
						bean.setName(object.getString("UserId"));
						bean.setContent(object.getString("MsgContent"));
						bean.setName(object.getString("UserName"));
						bean.setTime(object.getString("PushDate"));
//						bean.setNewsId(object.getInt("RC_MessageId"));
						/*if (TextUtils.isEmpty(object.getString("types"))) {
							bean.setTypes(0);
						}else{
							bean.setTypes(Integer.parseInt(object.getString("types")));
						}*/
						list.add(bean);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}

				adapter = new OrderNewsAdapter(MyOrderNewsActivity.this, R.layout.order_news_lv_item);
				mCheckOrderLv.setAdapter(adapter);
				adapter.addAll(list);
				if (list.size() == 0) {
					mEmptyLayout.setEmptyMessage("宝宝好伤心，你还没有预约消息~");
					mEmptyLayout.showEmpty();
				}
			}
		});
	}

}
