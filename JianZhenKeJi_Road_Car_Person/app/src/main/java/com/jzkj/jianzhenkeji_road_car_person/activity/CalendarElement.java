package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Administrator on 2016/2/24 0024.
 */
public class CalendarElement extends TextView {
	public CalendarElement(Context context) {
		super(context);
	}

	public CalendarElement(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CalendarElement(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		Paint paint = new Paint();
		paint.setColor(Color.BLACK);
		int startX = this.getWidth();
		int stopX = startX;
		int stopY = this.getHeight();
		//画边框
		canvas.drawLine(0, 0, this.getWidth() - 1, 0, paint);
		canvas.drawLine(0, 0, 0, this.getHeight() - 1, paint);
		canvas.drawLine(this.getWidth() - 1, 0, this.getWidth() - 1, this.getHeight() - 1, paint);
		canvas.drawLine(0, this.getHeight() - 1, this.getWidth() - 1, this.getHeight() - 1, paint);
	}
}
