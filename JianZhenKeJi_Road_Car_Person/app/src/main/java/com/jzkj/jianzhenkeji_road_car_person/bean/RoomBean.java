package com.jzkj.jianzhenkeji_road_car_person.bean;

/**
 * Created by Administrator on 2016/4/8.
 *   ExaminationId：考场信息表ID
     ExaminationName：考场名称
     ExaminationAddress：考场地址
     ExaminationMobile：考场联系电话
     ExaminationURL：考场网站介绍

 */
public class RoomBean {
    private String examinationId;
    private String examinationName;
    private String examinationAddress;
    private String examinationMobile;
    private String examinationURL;

    public String getExaminationId() {
        return examinationId;
    }

    public void setExaminationId(String examinationId) {
        this.examinationId = examinationId;
    }

    public String getExaminationName() {
        return examinationName;
    }

    public void setExaminationName(String examinationName) {
        this.examinationName = examinationName;
    }

    public String getExaminationAddress() {
        return examinationAddress;
    }

    public void setExaminationAddress(String examinationAddress) {
        this.examinationAddress = examinationAddress;
    }

    public String getExaminationMobile() {
        return examinationMobile;
    }

    public void setExaminationMobile(String examinationMobile) {
        this.examinationMobile = examinationMobile;
    }

    public String getExaminationURL() {
        return examinationURL;
    }

    public void setExaminationURL(String examinationURL) {
        this.examinationURL = examinationURL;
    }
}
