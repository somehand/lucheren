package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.MyMd5;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

public class ModifyPasswordActivity extends Activity implements View.OnClickListener {
	private ImageButton ibBack;
	private TextView tvTitle;
	private EditText etOldPassword, etNewPw, etSurePw;
	private Button btnSubmit;

	private Context context;
	private String oldPw;
	private String newPw;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_modify_password);
		init();
		initData();
		bindListener();
	}

	private void bindListener() {
		ibBack.setOnClickListener(this);
		btnSubmit.setOnClickListener(this);
	}

	private void initData() {
	}

	private void init() {
		ibBack = (ImageButton) findViewById(R.id.headframe_ib);
		tvTitle = (TextView) findViewById(R.id.headframe_title);
		etOldPassword = (EditText) findViewById(R.id.modify_password);
		etNewPw = (EditText) findViewById(R.id.modify_et_newpw);
		etSurePw = (EditText) findViewById(R.id.modify_et_surepw);
		btnSubmit = (Button) findViewById(R.id.modify_btn_submit);
		context = ModifyPasswordActivity.this;
		tvTitle.setText("修改密码");
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.headframe_ib:
				finish();
				break;
			case R.id.modify_btn_submit:
				oldPw = etOldPassword.getText().toString().trim();
				newPw = etNewPw.getText().toString().trim();
				String surePw = etSurePw.getText().toString().trim();
				if (TextUtils.isEmpty(oldPw)) {
					Utils.ToastShort(context, "原密码不能为空");
				} else if (TextUtils.isEmpty(newPw) || TextUtils.isEmpty(surePw)) {
					Utils.ToastShort(context, "新密码或确认密码不能为空");
				} else if (oldPw.equals(newPw) || oldPw == newPw) {
					Utils.ToastShort(context, "新密码与旧密码相同");
				} else if (newPw.length() < 6){
					Utils.ToastShort(context, "密码至少6位");

				}
				else {
					if (newPw == surePw || newPw.equals(surePw)) {
						getModifyResult();
						/*AlertDialog.Builder builder = new AlertDialog.Builder(context);
						builder.setMessage("修改成功");
						builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialogInterface, int i) {
								finish();
							}
						});
						builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialogInterface, int i) {
								dialogInterface.dismiss();
							}
						});
						AlertDialog dialog = builder.create();
						dialog.show();
						dialog.setCanceledOnTouchOutside(false);*/
					} else {
						Utils.ToastShort(context, "两次输入的密码不一致");
					}
				}
				break;
			default:
				break;
		}
	}

	private void getModifyResult() {
		String md5OldPw = MyMd5.getMd5(oldPw);
		String md5NewPw = MyMd5.getMd5(newPw);
		RequestParams params = new RequestParams();
		params.put("UserId", Utils.SPGetString(context, Utils.userId, "333333"));
		params.put("OldPassWord", md5OldPw);
		params.put("NewPassWord", md5NewPw);
		MyHttp.getInstance(context).post(MyHttp.MODIFY_PASSWORD, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
				try {
					if (response.getInt("Code") == 1) {
						Utils.ToastShort(context, response.getString("Reason"));
						Utils.SPutString(ModifyPasswordActivity.this,Utils.passWord,newPw);
						ModifyPasswordActivity.this.finish();
					} else {
						Utils.ToastShort(context, response.getString("Reason"));
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}
}
