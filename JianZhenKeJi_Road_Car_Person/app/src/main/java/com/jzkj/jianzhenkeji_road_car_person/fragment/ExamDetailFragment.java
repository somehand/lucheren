package com.jzkj.jianzhenkeji_road_car_person.fragment;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.MyApplication;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.bean.ExamBean;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.socks.library.KLog;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

public class ExamDetailFragment extends Fragment implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

	@InjectView(R.id.exam_detail_problem)
	TextView mExamQuestion;
	@InjectView(R.id.exam_detail_img)
	GifImageView mExamImg;
	@InjectView(R.id.exam_detail_box1)
	CheckBox mExamBox1;
	@InjectView(R.id.exam_detail_txt1)
	TextView mExamTxt1;
	@InjectView(R.id.exam_detail_box2)
	CheckBox mExamBox2;
	@InjectView(R.id.exam_detail_txt2)
	TextView mExamTxt2;
	@InjectView(R.id.exam_detail_box3)
	CheckBox mExamBox3;
	@InjectView(R.id.exam_detail_txt3)
	TextView mExamTxt3;
	@InjectView(R.id.exam_detail_box4)
	CheckBox mExamBox4;
	@InjectView(R.id.exam_detail_txt4)
	TextView mExamTxt4;
	@InjectView(R.id.exam_detail_divider3)
	View mExamDivider3;
	@InjectView(R.id.exam_detail_divider4)
	View mExamlDivider4;
	@InjectView(R.id.exam_detail_layout1)
	LinearLayout mExamDetailLayout1;
	@InjectView(R.id.exam_detail_layout2)
	LinearLayout mExamDetailLayout2;
	@InjectView(R.id.exam_detail_layout3)
	LinearLayout mExamDetailLayout3;
	@InjectView(R.id.exam_detail_btn)
	Button mExamDetailBtn;
	@InjectView(R.id.exam_detail_layout4)
	LinearLayout mExamDetailLayout4;
	public static final int TYPE_SINGLE = 0;
	public static final int TYPE_JUDGE = 1;
	public static final int TYPE_MUTIL = 2;
	private int itemType;

	boolean isFromUndo; //如果是选题号查看, 需要加载原答案;

	ExamBean item;
	ArrayList<CheckBox> boxList;

	int index; //题号

	int subject ; //科目1 或科目4

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bundle = getArguments();
		if (!bundle.isEmpty() && bundle.containsKey("item")) {
			item = (ExamBean) bundle.get("item");
			index = bundle.getInt("index");
			subject = bundle.getInt("class");
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.simulation_exam_details_item, null);
		ButterKnife.inject(this, view);
		//KLog.e(Utils.TAG_DEBUG , "-------" + index + "---onCreateView--------" + item.toString());
		setData(item);
		initListener();
		return view;
	}

	/**
	 * 设置监听
	 */
	private void initListener() {
		mExamBox1.setOnCheckedChangeListener(this);
		mExamBox2.setOnCheckedChangeListener(this);
		mExamBox3.setOnCheckedChangeListener(this);
		mExamBox4.setOnCheckedChangeListener(this);

		mExamDetailLayout1.setOnClickListener(this);
		mExamDetailLayout2.setOnClickListener(this);
		mExamDetailLayout3.setOnClickListener(this);
		mExamDetailLayout4.setOnClickListener(this);
		mExamDetailBtn.setOnClickListener(this);
	}

	/**
	 * 加载问题数据
	 *
	 * @param data
	 */
	public void setData(ExamBean data) {
		String textImg;
		if (item.getUrl().length() != 0) {
			String url = item.getUrl();
			KLog.e(Utils.TAG_DEBUG , "图片地址 : " + url);

			String end = url.substring(url.length() -3 , url.length());
			KLog.e(Utils.TAG_DEBUG , end);

			if(subject == 4){
				//KLog.e(Utils.TAG_DEBUG , "科目四");

				if(end.equals("swf")){
					KLog.e(Utils.TAG_DEBUG , "动画");

					try {
						KLog.e(Utils.TAG_DEBUG , item.getId() + "");
						GifDrawable drawable = new GifDrawable(getActivity().getAssets() , item.getId() + ".gif");
						mExamImg.setImageDrawable(drawable);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}else{
					ImageLoader.getInstance().displayImage(item.getUrl(), mExamImg, MyApplication.options_other);
				}
			}else{
				ImageLoader.getInstance().displayImage(item.getUrl(), mExamImg , MyApplication.options_other);
			}

		} else {
			mExamImg.setVisibility(View.GONE);
		}

		if (data.getAnswer() < 7) {

			if (data.getItem4().length() != 0) {
				itemType = TYPE_SINGLE;
				//单选
				textImg = "<img src=\"" + R.drawable.single_selection + "\" />" + " " + item.getQuestion();
				mExamTxt1.setText(item.getItem1());
				mExamTxt2.setText(item.getItem2());
				mExamTxt3.setText(item.getItem3());
				mExamTxt4.setText(item.getItem4());

			} else {
				itemType = TYPE_JUDGE;
				//判断
				mExamDetailLayout3.setVisibility(View.GONE);
				mExamDetailLayout4.setVisibility(View.GONE);
				mExamDivider3.setVisibility(View.GONE);
				mExamlDivider4.setVisibility(View.GONE);

				textImg = "<img src=\"" + R.drawable.judge + "\" />" + " " + item.getQuestion();
				mExamTxt1.setText(item.getItem1());
				mExamTxt2.setText(item.getItem2());
				if (TextUtils.isEmpty(item.getItem1()) || TextUtils.isEmpty(item.getItem2())) {
					mExamTxt1.setText("正确");
					mExamTxt2.setText("错误");
				}
			}
		} else {
			itemType = TYPE_MUTIL;
			//多选
			textImg = "<img src=\"" + R.drawable.mutil_choice + "\" />" + " " + item.getQuestion();
			mExamTxt1.setText(item.getItem1());
			mExamTxt2.setText(item.getItem2());
			mExamTxt3.setText(item.getItem3());
			mExamTxt4.setText(item.getItem4());
			mExamDetailBtn.setVisibility(View.VISIBLE);
		}
		mExamQuestion.setText(Html.fromHtml(textImg, imageGetter, null));

		boxList = new ArrayList<CheckBox>();
		boxList.add(mExamBox1);
		boxList.add(mExamBox2);
		boxList.add(mExamBox3);
		boxList.add(mExamBox4);
	}


	/**
	 * 此处获取个人答案, 并判断对错
	 */
	boolean showed = false;
	int myAnswer;  // 用户的答案

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		//KLog.e("index : " + index + " " + isVisibleToUser);
		if (isVisibleToUser) {
			showed = true; //fragment 都是先隐藏 再显示 再隐藏, 所以显示过之后再次隐藏就可以获取答案
		}
		if (showed && !isVisibleToUser) {//再次隐藏就可以获取答案
			/*getAnswer(); //获取用户的答案
			//参数对应 : 题号, 答案对应的数字, 是否作答, 是否正确;
			mCompleteListener.onAnswerComplete(index , myAnswer , myAnswer != 0 , myAnswer == item.getAnswer());*/

		}

	}

	/**
	 * 设定为单选
	 *
	 * @param a
	 */
	private void setSingleChoice(int a) {
		for (int i = 0; i < boxList.size(); i++) {
			if (i != a) {
				boxList.get(i).setChecked(false);
			}
		}
	}

	/**
	 * text中加载图片
	 */
	private Html.ImageGetter imageGetter = new Html.ImageGetter() {

		@Override
		public Drawable getDrawable(String arg0) {
			Drawable drawable;
			int dId = Integer.parseInt(arg0);
			drawable = getResources().getDrawable(dId);
			drawable.setBounds(0, 0, sp2px(getActivity(), 19 * 2),
					sp2px(getActivity(), 19));
			return drawable;
		}
	};

	/**
	 * sp转换为px
	 *
	 * @param context
	 * @param spValue
	 * @return
	 */
	public static int sp2px(Context context, float spValue) {
		float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
		return (int) (spValue * fontScale + 0.5f);
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		ButterKnife.reset(this);
	}

	/**
	 * checkbox监听
	 *
	 * @param compoundButton
	 * @param b
	 */
	@Override
	public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
		//在单选和判断题中 四个checkbox设置为单选
		if (b == true && (itemType == TYPE_SINGLE || itemType == TYPE_JUDGE)) {
			switch (compoundButton.getId()) {
				case R.id.exam_detail_box1:
					setSingleChoice(0);
					break;
				case R.id.exam_detail_box2:
					setSingleChoice(1);
					break;
				case R.id.exam_detail_box3:
					setSingleChoice(2);
					break;
				case R.id.exam_detail_box4:
					setSingleChoice(3);
					break;
			}

			getAnswer(); //获取用户的答案
			//参数对应 : 题号, 答案对应的数字, 是否作答, 是否正确;
			mCompleteListener.onAnswerComplete(index, myAnswer, myAnswer != 0, myAnswer == item.getAnswer());
			//切换下一页
			EventBus.getDefault().post(new MutilBtnClick());
			//KLog.e(Utils.TAG_DEBUG , "myanswer :" + myAnswer + "  index : " + index);
		}

	}

	/**
	 * 点击监听
	 *
	 * @param view
	 */
	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.exam_detail_layout1:
				mExamBox1.toggle();
				break;
			case R.id.exam_detail_layout2:
				mExamBox2.toggle();

				break;
			case R.id.exam_detail_layout3:
				mExamBox3.toggle();

				break;
			case R.id.exam_detail_layout4:
				mExamBox4.toggle();

				break;
			case R.id.exam_detail_btn:
				int j = 0;
				for (int i = 0; i < boxList.size(); i++) {
					if (boxList.get(i).isChecked()) {
						j++;
					}
				}
				if (j > 1) {
					getAnswer(); //获取用户的答案
					//参数对应 : 题号, 答案对应的数字, 是否作答, 是否正确;
					mCompleteListener.onAnswerComplete(index, myAnswer, myAnswer != 0, myAnswer == item.getAnswer());
					//切换下一页
					EventBus.getDefault().post(new MutilBtnClick());
				} else {
					Utils.ToastShort(getActivity(), "至少选择两项");
				}
				break;
		}
	}

	/**
	 * 获取用户单个题目答案
	 */
	private void getAnswer() {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < boxList.size(); i++) {
			if (boxList.get(i).isChecked()) {
				builder.append((char) (i + 65));
			}
		}
		if (builder.toString().equals("A")) {
			myAnswer = 1;
		} else if (builder.toString().equals("B")) {
			myAnswer = 2;
		} else if (builder.toString().equals("C")) {
			myAnswer = 3;
		} else if (builder.toString().equals("D")) {
			myAnswer = 4;
		} else if (builder.toString().equals("AB")) {
			myAnswer = 7;
		} else if (builder.toString().equals("AC")) {
			myAnswer = 8;
		} else if (builder.toString().equals("AD")) {
			myAnswer = 9;
		} else if (builder.toString().equals("BC")) {
			myAnswer = 10;
		} else if (builder.toString().equals("BD")) {
			myAnswer = 11;
		} else if (builder.toString().equals("CD")) {
			myAnswer = 12;
		} else if (builder.toString().equals("ABC")) {
			myAnswer = 13;
		} else if (builder.toString().equals("ABD")) {
			myAnswer = 14;
		} else if (builder.toString().equals("ACD")) {
			myAnswer = 15;
		} else if (builder.toString().equals("BCD")) {
			myAnswer = 16;
		} else if (builder.toString().equals("ABCD")) {
			myAnswer = 17;
		} else if (builder.toString().length() == 0) {
			myAnswer = 0;
		}


	}

	OnAnswerCompleteListener mCompleteListener;

	public void setAnswerCompleteListener(OnAnswerCompleteListener answerCompleteListener) {
		mCompleteListener = answerCompleteListener;
	}

	public interface OnAnswerCompleteListener {

		void onAnswerComplete(int index, int answer, boolean isAnswered, boolean isCorrect);
	}

	public class MutilBtnClick {

	}
}
