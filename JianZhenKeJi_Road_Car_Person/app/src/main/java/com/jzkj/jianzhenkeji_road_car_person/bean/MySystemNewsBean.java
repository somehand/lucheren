package com.jzkj.jianzhenkeji_road_car_person.bean;

/**
 * Created by Administrator on 2016/3/21.
 */
public class MySystemNewsBean {
	private int RC_MessageId;
	private int MessageType;
	private String MsgContent;
	private String PushDate;
	private String UserId;

	public int getRC_MessageId() {
		return RC_MessageId;
	}

	public void setRC_MessageId(int RC_MessageId) {
		this.RC_MessageId = RC_MessageId;
	}

	public int getMessageType() {
		return MessageType;
	}

	public void setMessageType(int messageType) {
		MessageType = messageType;
	}

	public String getMsgContent() {
		return MsgContent;
	}

	public void setMsgContent(String msgContent) {
		MsgContent = msgContent;
	}

	public String getPushDate() {
		return PushDate;
	}

	public void setPushDate(String pushDate) {
		PushDate = pushDate;
	}

	public String getUserId() {
		return UserId;
	}

	public void setUserId(String userId) {
		UserId = userId;
	}

	public MySystemNewsBean() {
	}
}
