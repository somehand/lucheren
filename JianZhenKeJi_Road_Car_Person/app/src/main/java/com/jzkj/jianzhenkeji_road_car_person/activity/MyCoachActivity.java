package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;
import com.jzkj.jianzhenkeji_road_car_person.CircleImageView;
import com.jzkj.jianzhenkeji_road_car_person.MyApplication;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.adapter.MyCoachAdapter;
import com.jzkj.jianzhenkeji_road_car_person.bean.MyCoachBean;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.MyListView;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.kanak.emptylayout.EmptyLayout;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.socks.library.KLog;
import com.umeng.socialize.Config;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.utils.Log;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Administrator on 2016/4/6.
 */
public class MyCoachActivity extends Activity implements View.OnClickListener {
	@InjectView(R.id.headframe_ib)
	ImageButton mHeadframeIb;
	@InjectView(R.id.headframe_title)
	TextView mHeadframeTitle;
	@InjectView(R.id.my_coach_headimg)
	CircleImageView mMyCoachHeadimg;
	@InjectView(R.id.my_coach_ratingbar)
	RatingBar mMyCoachRatingbar;
	@InjectView(R.id.my_coach_lv)

	MyListView mMyCoachLv;
	@InjectView(R.id.my_coach_coachname)
	TextView mMyCoachCoachname;
	@InjectView(R.id.my_coach_accept_times)
	TextView mMyCoachAcceptTimes;
	@InjectView(R.id.my_coach_totle_sudents)
	TextView mMyCoachTotleSudents;
	@InjectView(R.id.text_plate_number)
	TextView plateNumber;
	@InjectView(R.id.my_coach_ptrfsv)
	PullToRefreshScrollView mPullToRefreshScrollView;
	@InjectView(R.id.headframe_layout_mycoach_share)
	LinearLayout mHeadframeLayoutMycoachShare;

	private ArrayList<MyCoachBean> list;
	private MyCoachAdapter adapter;
	int nowPage = 1;
	int loadNum = 10;
	EmptyLayout mEmptyLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_coach);
		ButterKnife.inject(this);
		getLogin();
		init();
		initData();
		initListener();
	}

	private void initListener() {
		mHeadframeIb.setOnClickListener(this);
		mHeadframeLayoutMycoachShare.setOnClickListener(this);
		mPullToRefreshScrollView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ScrollView>() {
			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ScrollView> refreshView) {
				String label = DateUtils.formatDateTime(MyCoachActivity.this, System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_ALL);
				refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);
				nowPage = 1;
				getCoachInfo(4, nowPage, loadNum);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ScrollView> refreshView) {
				String label = DateUtils.formatDateTime(MyCoachActivity.this, System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_ALL);
				refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);
				nowPage++;
				getCoachInfo(4, nowPage, loadNum);
			}
		});
		mMyCoachLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				MyCoachBean bean = (MyCoachBean) adapterView.getAdapter().getItem(i);
				Intent intent = new Intent(MyCoachActivity.this, CommentedActivity.class);
				intent.putExtra("bean", bean);
				startActivity(intent);

			}
		});
	}

	public void GET_DailyTask() {
		KLog.e(Utils.TAG_DEBUG, "GET_DailyTask :");
		RequestParams params = new RequestParams();
		params.put("userid", Utils.SPGetString(this, Utils.userId, ""));
		params.put("type", 1);
		KLog.e(Utils.TAG_DEBUG, params.toString());
		//System.out.println("params : " + params.toString());
		MyHttp.getInstance(this).post(MyHttp.GET_DailyTask, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonObject :");
				KLog.json(Utils.TAG_DEBUG, response.toString());
			}

		});
	}

	private void initData() {
		getCoachInfo(4, 1, loadNum);
//		mMyCoachLv = (MyListView) ptrfLv.getRefreshableView();
		mMyCoachLv.setFocusable(false);
		mMyCoachLv.setAdapter(adapter);
		mEmptyLayout = new EmptyLayout(MyCoachActivity.this, mMyCoachLv);
		mPullToRefreshScrollView.setMode(PullToRefreshBase.Mode.BOTH);
	}

	View view;


	private void getCoachInfo(final int mtype, final int pageindex, final int pagesize) {
		RequestParams params = new RequestParams();
		params.put("coachid", Utils.SPGetString(MyCoachActivity.this, Utils.coachId, ""));
		if (mtype != 4) {
			params.put("type", mtype);
			params.put("pageindex", "");
			params.put("pagesize", "");
		} else {
			params.put("type", mtype);
			params.put("pageindex", pageindex);
			params.put("pagesize", pagesize);
		}
		KLog.e("params", params.toString());
		MyHttp.getInstance(this).post(MyHttp.GET_COACH_INFO, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e("Debug", "JSONArray:");
				KLog.json(response.toString());
				if (pageindex == 1) {
					list.clear();
					adapter.clear();
				}
				if (mtype != 4) {
					try {
						JSONObject object = response.getJSONObject(0);
						if (mtype == 1) {
							if (TextUtils.isEmpty(object.getString("Star"))) {
								mMyCoachRatingbar.setRating(0);
							} else {
								mMyCoachRatingbar.setRating(Float.parseFloat(object.getString("Star")));
							}
							mMyCoachCoachname.setText(object.getString("RealName"));//object.getString("RealName")
							ImageLoader.getInstance().displayImage(object.getString("UserLogo"), mMyCoachHeadimg, MyApplication.options_user);
							Utils.SPutString(MyCoachActivity.this, Utils.coachLogo, object.getString("UserLogo"));
						} else if (mtype == 2) {
							mMyCoachAcceptTimes.setText(object.getString("total"));
						} else {
							mMyCoachTotleSudents.setText(object.getString("total"));
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}

				} else {
					for (int i = 0; i < response.length(); i++) {
						try {
							JSONObject object = response.getJSONObject(i);
							MyCoachBean bean = new MyCoachBean();
							bean.setCommentid(object.getString("id"));
							bean.setCommentDate(object.getString("createdate"));
							bean.setContents(object.getString("contents"));
							if ("1".equals(object.getString("type"))) {//0：好评，1：中评，2：差评
								bean.setEvaluated("中评");
							} else if ("2".equals(object.getString("type"))) {
								bean.setEvaluated("差评");
							} else {
								bean.setEvaluated("好评");
							}
							bean.setMajor(object.getInt("zyzs"));
							bean.setServiceAttitude(object.getInt("fwtd"));
							bean.setStudentBookingsId(object.getString("StudentBookingsId"));
							bean.setStudentid(object.getString("userid"));
							bean.setStudentLogo(object.getString("UserLogo"));
							bean.setStudentName(object.getString("UserName"));
							bean.setMYD(object.getInt("MYD"));
							list.add(bean);
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
					adapter.addAll(list);
					mPullToRefreshScrollView.postDelayed(new Runnable() {
						@Override
						public void run() {
							mPullToRefreshScrollView.onRefreshComplete();
						}
					}, 1000);
					if (list.size() == 0) {
						mEmptyLayout.setEmptyMessage("宝宝好伤心，还没有评论~");
						mEmptyLayout.showEmpty();
					}

				}
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());

				mPullToRefreshScrollView.postDelayed(new Runnable() {
					@Override
					public void run() {
						mPullToRefreshScrollView.onRefreshComplete();
					}
				}, 1000);
				if (list.size() == 0) {
					mEmptyLayout.setEmptyMessage("宝宝好伤心，还没有评论~");
					mEmptyLayout.showEmpty();
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				mPullToRefreshScrollView.postDelayed(new Runnable() {
					@Override
					public void run() {
						mPullToRefreshScrollView.onRefreshComplete();
					}
				}, 1000);
				if (list.size() == 0) {
					mEmptyLayout.setEmptyMessage("宝宝好伤心，还没有评论~");
					mEmptyLayout.showEmpty();
				}
			}
		});
	}

	private View.OnClickListener mErrorClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			Toast.makeText(MyCoachActivity.this, "Try again button clicked", Toast.LENGTH_LONG).show();
		}
	};

	private void init() {
		mHeadframeTitle.setText("我的教练");
		plateNumber.setText("车牌号:" + getIntent().getStringExtra("plateNumber"));
		mHeadframeLayoutMycoachShare.setVisibility(View.VISIBLE);
		mMyCoachRatingbar.setEnabled(false);
		list = new ArrayList<MyCoachBean>();
		adapter = new MyCoachAdapter(this, R.layout.activity_my_coach_lv_item);
		//判断是否有教练
		isCoach();
		getCoachInfo(1, 0, 0);
		getCoachInfo(2, 0, 0);
		getCoachInfo(3, 0, 0);
	}

	private void isCoach() {
		if (TextUtils.isEmpty(Utils.SPGetString(MyCoachActivity.this, Utils.coachId, ""))) {
			AlertDialog.Builder builder = new AlertDialog.Builder(MyCoachActivity.this);
			builder.setMessage("未绑定教练，暂无教练信息~");
			builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					dialogInterface.dismiss();
					MyCoachActivity.this.finish();
				}
			});
			AlertDialog alertDialog = builder.create();
			alertDialog.show();
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.headframe_ib:
				finish();
				break;
			case R.id.headframe_layout_mycoach_share:
				initShare();
				break;
			default:
				break;
		}
	}

	/**
	 * 弹出分享界面
	 */
	public void initShare() {
		final SHARE_MEDIA[] displaylist = new SHARE_MEDIA[]{
				SHARE_MEDIA.WEIXIN, SHARE_MEDIA.WEIXIN_CIRCLE, SHARE_MEDIA.QQ, SHARE_MEDIA.QZONE, SHARE_MEDIA.SINA
		};
		/*注意新浪分享的title是不显示的，URL链接只能加在分享文字后显示，并且需要确保withText()不为空
		当同时传递URL参数和图片时，注意确保图片不能超过32K，否则无法分享，不传递URL参数时图片不受32K限制*/
		Log.LOG = true;
		Config.OpenEditor = false;
		Config.IsToastTip = true;
		ProgressDialog dialog = new ProgressDialog(this);
		dialog.setMessage("正在分享中");
		Config.dialog = dialog;
		new ShareAction(this).setDisplayList(displaylist)
				.withText("路车人教练分享")
				.withTitle("我的教练")
				.withTargetUrl(MyHttp.SHARE_URL + Utils.SPGetString(MyCoachActivity.this, Utils.coachId, ""))
				.withMedia(new UMImage(this, R.drawable.ic_launcher))
				.setListenerList(mumshare, mumshare, mumshare, mumshare, mumshare)
				.open();
	}

	private UMShareListener mumshare = new UMShareListener() {
		@Override
		public void onResult(SHARE_MEDIA share_media) {
			Utils.ToastShort(MyCoachActivity.this, "分享成功");
			GET_DailyTask();
		}

		@Override
		public void onError(SHARE_MEDIA share_media, Throwable throwable) {
			Utils.ToastShort(MyCoachActivity.this, "分享失败");
			GET_DailyTask();
		}

		@Override
		public void onCancel(SHARE_MEDIA share_media) {
			Utils.ToastShort(MyCoachActivity.this, "分享已取消");

		}
	};

	/**
	 * 获取登录数据
	 */
	private void getLogin() {
		KLog.e("getLogin()");


		RequestParams params = new RequestParams();
		params.put("UserName", Utils.SPGetString(this, Utils.userName, ""));
		params.put("PassWord", Utils.SPGetString(this, Utils.passWord, ""));
		params.put("Type", 1);
		KLog.e("params", params.toString());
		MyHttp.getInstance(MyCoachActivity.this).post(MyHttp.USER_LOGIN, params, new JsonHttpResponseHandler(
		) {

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());

			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				try {
					JSONObject jsonObject = response.getJSONObject(0);
					KLog.e(Utils.TAG_DEBUG, "jsonArray :");
					KLog.json(Utils.TAG_DEBUG, response.toString());

					//Utils.SPPutBoolean(HomeActivity.this, "isLogin", true);
					//Toast.makeText(HomeActivity.this, "登录成功(^_^)", Toast.LENGTH_LONG).show();
					Utils.SPutString(MyCoachActivity.this, Utils.userId, jsonObject.getString("UserId"));
					Utils.SPutString(MyCoachActivity.this, Utils.userName, jsonObject.getString("UserName"));

					Utils.SPutString(MyCoachActivity.this, Utils.realName, jsonObject.getString("RealName"));
					Utils.SPutString(MyCoachActivity.this, Utils.nickName, jsonObject.getString("NickName"));
					Utils.SPutString(MyCoachActivity.this, Utils.idCarNumber, jsonObject.getString("IDCardNumber"));
					Utils.SPutString(MyCoachActivity.this, Utils.userSex, jsonObject.getString("UserSex"));
					Utils.SPutString(MyCoachActivity.this, Utils.userAddress, jsonObject.getString("UserAddress"));
					Utils.SPutString(MyCoachActivity.this, Utils.userLogo, jsonObject.getString("UserLogo"));
					Utils.SPutString(MyCoachActivity.this, Utils.schoolName, jsonObject.getString("SchoolName"));

					Utils.SPPutBoolean(MyCoachActivity.this, Utils.isLogin, true);
					Utils.SPutString(MyCoachActivity.this, Utils.schoolId, jsonObject.getString("DrivingSchoolId"));
					Utils.SPutString(MyCoachActivity.this, Utils.coachId, jsonObject.getString("CoachId"));
					Utils.SPutString(MyCoachActivity.this, Utils.CoachName, jsonObject.getString("CoachName"));
					Utils.SPutString(MyCoachActivity.this, Utils.SchoolUrl, jsonObject.getString("SchoolProfiles"));
					Utils.SPutString(MyCoachActivity.this, Utils.TeachType, jsonObject.getString("TeachType"));
					if (jsonObject.getInt("DrivingType") == 1) {
						Utils.SPutString(MyCoachActivity.this, Utils.DrivingType, "C1");
					} else {
						Utils.SPutString(MyCoachActivity.this, Utils.DrivingType, "C2");
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);

			}
		});
//		}
	}
}
