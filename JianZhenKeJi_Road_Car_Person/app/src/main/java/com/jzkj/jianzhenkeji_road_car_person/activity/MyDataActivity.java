package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.Selection;
import android.text.Spannable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.BaseActivity;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.adapter.SelectSchoolLVAdapter;
import com.jzkj.jianzhenkeji_road_car_person.bean.MyDrivingSchoolBean;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyDataActivity extends BaseActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
	private ImageButton ibBack, ibSelectSchool;
	private LinearLayout etLl, tvLl;
	private TextView tvSubmit, /*etSex, tvSex,  tvNickName,*/tvName, tvID, tvAddress;
	private EditText etName, /*etNickName,*/ etID, etAddress;
	private ListView lv;
	private String sex;
	private String realName;
	private String nickName;
	private String id;
	private String address;
	private ArrayList<EditText> etList;
	private int sexTag = 1;//性别 1.男 2.女

	private ArrayList<MyDrivingSchoolBean> list;
	private PopupWindow popupWindow;
	private String[] name;
	private Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_data);

		init();
		initData();
		bindListener();

//		setPopupWindow();

	}

	/**
	 * 获取数据
	 */
	private void initData() {
		/*String sex = "男";
		if (Utils.SPGetString(context, Utils.userSex, "sex") == "2" || Utils.SPGetString(context,Utils.userSex,"sex").equals("2")) {
			sex = "女";
		}*/
		tvName.setText(Utils.SPGetString(context, Utils.realName, "name"));
//		tvNickName.setText(Utils.SPGetString(context, Utils.nickName, "nickname"));
		tvID.setText(Utils.SPGetString(context, Utils.idCarNumber, "18wei"));
//		tvSex.setText(sex);
		tvAddress.setText(Utils.SPGetString(context, Utils.userAddress, "chongqing"));

		etName.setText(Utils.SPGetString(context, Utils.realName, "name"));
//		etNickName.setText(Utils.SPGetString(context, Utils.nickName, "nickname"));
		etID.setText(Utils.SPGetString(context, Utils.idCarNumber, "18wei"));
//		etSex.setText(sex);
		etAddress.setText(Utils.SPGetString(context, Utils.userAddress, "chongqing"));
	}

	/**
	 * 绑定监听
	 */
	private void bindListener() {
		ibBack.setOnClickListener(this);
//		tvSchoolName.setOnClickListener(this);
		tvSubmit.setOnClickListener(this);
		tvLl.setOnClickListener(this);
		/*etSex.setOnClickListener(this);
		tvSex.setOnClickListener(this);*/
		for (int i = 0; i < etList.size(); i++) {
			etList.get(i).setOnClickListener(this);
		}
	}

	/**
	 * 初始化ID
	 */
	private void init() {
		ibBack = (ImageButton) findViewById(R.id.my_data_back);
		tvSubmit = (TextView) findViewById(R.id.my_data_finish);
		etName = (EditText) findViewById(R.id.my_data_name);
//		etNickName = (EditText) findViewById(R.id.my_data_nickname);
		etID = (EditText) findViewById(R.id.my_data_personid);
//		etSex = (TextView) findViewById(R.id.my_data_sex);
		etAddress = (EditText) findViewById(R.id.my_data_address);
		etLl = (LinearLayout) findViewById(R.id.my_data_et_ll);

		tvName = (TextView) findViewById(R.id.my_data_tv_name);
//		tvNickName = (TextView) findViewById(R.id.my_data_tv_nickname);
		tvID = (TextView) findViewById(R.id.my_data_tv_personid);
//		tvSex = (TextView) findViewById(R.id.my_data_tv_sex);
		tvAddress = (TextView) findViewById(R.id.my_data_tv_address);
		tvLl = (LinearLayout) findViewById(R.id.my_data_tv_ll);
		context = MyDataActivity.this;
		etList = new ArrayList<EditText>();
		etList.add(etName);
//		etList.add(etNickName);
		etList.add(etID);
		etList.add(etAddress);
	}

	/**
	 * 设置PopupWindow的方法
	 */
	private void setPopupWindow() {
		LayoutInflater inflater = LayoutInflater.from(context);
		View view = inflater.inflate(R.layout.select_driving_school_popwindow, null);

		lv = (ListView) view.findViewById(R.id.select_driving_school_lv);
		list = new ArrayList<MyDrivingSchoolBean>();
		name = new String[]{"贵峰驾校", "渝快驾校", "启富驾校", "嘉信驾校", "嘉州驾校"};
		for (int k = 0; k < name.length; k++) {
			list.add(new MyDrivingSchoolBean(name[k]));
		}
		SelectSchoolLVAdapter Adapter = new SelectSchoolLVAdapter(context, list);
		lv.setAdapter(Adapter);

		//监听
		lv.setOnItemClickListener(this);
		popupWindow = new PopupWindow(view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		ibSelectSchool.setOnClickListener(this);
		popupWindow.setFocusable(true);
		popupWindow.setOutsideTouchable(true);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());//这句话必须有，否则按返回键popupWindow不消失

	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.my_data_back:
				/*Intent intent = new Intent(MyDataActivity.this,HomeActivity.class);
				intent.putExtra("nickname",nickName);
				startActivity(intent);*/
				finish();
				break;
			case R.id.my_data_finish:
				setModifyData();
				tvName.setText(realName);
//				tvNickName.setText(nickName);
				tvID.setText(id);
//				tvSex.setText(sex);
				tvAddress.setText(address);
				Utils.SPutString(context, Utils.realName, realName);
				Utils.SPutString(context, Utils.nickName, nickName);
				Utils.SPutString(context, Utils.idCarNumber, id);
				Utils.SPutString(context, Utils.userAddress, address);
				tvSubmit.setVisibility(View.GONE);
				tvLl.setVisibility(View.VISIBLE);
				etLl.setVisibility(View.GONE);
				System.out.println("post  " + realName + "  "
				+ nickName );
				EventBus.getDefault().post(new String(realName));
				finish();
				break;
			/*case R.id.my_data_tv_sex:
			case R.id.my_data_sex:
				if (tvSubmit.getVisibility() == View.GONE) {
					tvSubmit.setVisibility(View.VISIBLE);
				}
				if (etLl.getVisibility() == View.GONE) {
					etLl.setVisibility(View.VISIBLE);
				}
				if (tvLl.getVisibility() == View.VISIBLE) {
					tvLl.setVisibility(View.GONE);
				}
				String currentSex = etSex.getText().toString().trim();
				int j = 1;
				if (currentSex.equals("男") || currentSex == "男") {
					j = 0;
				}else{
					j = 1;
				}
				final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
				final String[] sexText = {"男", "女"};
				dialog.setTitle("请选择性别：");
				dialog.setSingleChoiceItems(sexText, j, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						etSex.setText(sexText[i]);
						dialogInterface.dismiss();
					}
				});
				AlertDialog alertDialog = dialog.create();
				alertDialog.show();
				break;*/
			case R.id.my_data_tv_ll:

				tvLl.setVisibility(View.GONE);
				etLl.setVisibility(View.VISIBLE);
				tvSubmit.setVisibility(View.VISIBLE);
				/*etName.setFocusable(true);
				etName.setFocusableInTouchMode(true);
				etName.requestFocus();*/
				break;
			case R.id.my_data_name:
			case R.id.my_data_nickname:
			case R.id.my_data_personid:
			case R.id.my_data_address:
				for (int i = 0; i < etList.size(); i++) {
					etList.get(i).setText(etList.get(i).getText().toString());
					Spannable spannable = etList.get(i).getText();
					Selection.selectAll(spannable);
				}
				break;

			default:
				break;
		}
	}

	/**
	 * 用户修改资料
	 */
	private void setModifyData() {
		realName = etName.getText().toString().trim();
//		nickName = etNickName.getText().toString().trim();
		id = etID.getText().toString().trim();
		address = etAddress.getText().toString().trim();
		/*sex = etSex.getText().toString().trim();
		if (sex == "男" || sex.equals("男")) {
			sexTag = 1;
		} else {
			sexTag = 2;
		}*/
		Utils.SPutString(context, Utils.userSex, sexTag+"");
		RequestParams params = new RequestParams();
		params.put("UserId", Utils.SPGetString(context, Utils.userId, "333333"));
		params.put("RealName", realName);
//		params.put("NickName", nickName);
//		params.put("UserSex", sexTag);
		params.put("IDCardNumber", id);
		params.put("UserAddress", address);
		KLog.e("params",params.toString());
		MyHttp.getInstance(context).post(MyHttp.MODIFY_DATA, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
				try {
					if (response.getInt("Code") == 1) {
						Utils.ToastShort(context, response.getString("Reason"));
					} else {
						Utils.ToastShort(context, response.getString("Reason"));
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
				try {
					JSONObject object = response.getJSONObject(0);
					if (object.getInt("Code") == 1) {
						Utils.ToastShort(context, object.getString("Reason"));
					} else {
						Utils.ToastShort(context, object.getString("Reason"));
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
//		}

	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//		tvSchoolName.setText(name[i]);
		popupWindow.dismiss();
	}
}
