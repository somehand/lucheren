package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.MyApplication;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.bean.ADBean;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Administrator on 2016/3/9 0009.
 */
public class ADActivity extends Activity {

	@InjectView(R.id.splash_imageview)
	ImageView mSplashImageview;
	@InjectView(R.id.splash_textview)
	TextView mTextView;
	MyCount count = new MyCount(3000, 1000);
	Class cls;
	int type;
	int item = 0;
	ADBean bean;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		ButterKnife.inject(this);
		Intent intent = getIntent();
		cls = (Class) intent.getSerializableExtra("class");
		type = intent.getIntExtra("type", 0);
		item = intent.getIntExtra("item", 0);
		bean = Utils.ADMap.get("ad" + type);
		count.start();
		if (bean != null) {
			Log.d("bean.toString", bean.toString());
			getHeaderName();
		}
		mTextView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
//				count.cancel();
				count.onFinish();
			}
		});
	}

	private void getHeaderName() {
		ImageLoader.getInstance().displayImage(bean.getIconImgUrl(), mSplashImageview, MyApplication.options_other);
	}

	class MyCount extends CountDownTimer {

		public MyCount(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}

		@Override
		public void onTick(long l) {
			mTextView.setText("跳过" + (l / 1000) + "s");
		}

		@Override
		public void onFinish() {
			mTextView.setText("跳过" + 0 + "s");
			count.cancel();
			Intent intent = new Intent(ADActivity.this, cls);
			if (type == 3) {
				intent.putExtra("titleName", "交通违章查询");
				intent.putExtra("url", Utils.ADMap.get("ad3").getIconUrl());
				intent.putExtra("type", 3);
			} else if (type == 4) {
				try {
					//Intent Intent = new Intent();
					intent.setClassName("com.jzkj.jianzhenkeji_passengers", "com.jzkj.jianzhenkeji_passengers.activity.LoginActivity");
					startActivity(intent);
//					count.cancel();
					ADActivity.this.finish();
					return;
				} catch (Exception ex) {
					intent.setClass(ADActivity.this, WebPageActivity.class);
					intent.putExtra("url", Utils.ADMap.get("ad4").getIconUrl());
					intent.putExtra("titleName", "车联网");
					intent.putExtra("type", 3);
				}

			} else if (type == 1 || type == 6) {
				intent.putExtra("item", item);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
			}
			startActivity(intent);
			ADActivity.this.finish();
		}
	}

	@Override
	public void onBackPressed() {
		count.cancel();
		super.onBackPressed();
	}
}
