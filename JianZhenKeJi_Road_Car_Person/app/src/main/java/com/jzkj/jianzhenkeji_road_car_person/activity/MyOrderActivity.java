package com.jzkj.jianzhenkeji_road_car_person.activity;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.bean.RefreshEvent;
import com.jzkj.jianzhenkeji_road_car_person.fragment.SubjectThreeFragment;
import com.jzkj.jianzhenkeji_road_car_person.fragment.SubjectTwoFragment;

import org.greenrobot.eventbus.EventBus;

public class MyOrderActivity extends FragmentActivity implements View.OnClickListener {
	private ImageButton ibBack;
	private RadioGroup rg2;
	private RadioButton rbLookRoom, rbCoach, rbNoOrder, rbOrdered, rbNoEvaluate, rbEvaluate;
	private Context context;
	private FragmentTransaction transaction;
	private SubjectThreeFragment lookRoomFragment = new SubjectThreeFragment();
	private SubjectTwoFragment coachFragment = new SubjectTwoFragment();


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_order);
//		EventBus.getDefault().register(this);
		init();
		bindClickListener();

	}

	private void bindClickListener() {
		ibBack.setOnClickListener(this);
		rbLookRoom.setOnClickListener(this);
		rbCoach.setOnClickListener(this);
		/*rbEvaluate.setOnClickListener(this);
		rbNoEvaluate.setOnClickListener(this);
		rbOrdered.setOnClickListener(this);
		rbNoOrder.setOnClickListener(this);*/
	}

	private void init() {
		ibBack = (ImageButton) findViewById(R.id.my_order_ib);
		context = MyOrderActivity.this;
		rbCoach = (RadioButton) findViewById(R.id.my_order_rb1);
		rbLookRoom = (RadioButton) findViewById(R.id.my_order_rb2);


//		rg2 = (RadioGroup) findViewById(R.id.my_order_rg2);

		transaction = getSupportFragmentManager().beginTransaction();
		transaction.add(R.id.my_order_fl, lookRoomFragment);
		transaction.hide(lookRoomFragment);
		transaction.commit();
		transaction = getSupportFragmentManager().beginTransaction();
		transaction.add(R.id.my_order_fl, coachFragment);
//		transaction.hide(coachFragment);
		transaction.commit();

	}


	@Override
	public void onClick(View view) {
		/*Drawable da = getResources().getDrawable(R.drawable.bule_line_img);
		//调用setCompoundDrawables时，必须调用Drawable.setBounds()方法,否则图片不显示
		da.setBounds(0,0,da.getIntrinsicWidth(),da.getIntrinsicHeight());*/
		switch (view.getId()) {
			case R.id.my_order_ib:
//				finish();
				Intent intent = new Intent(MyOrderActivity.this, HomeActivity.class);
				EventBus.getDefault().post(new RefreshEvent());
				intent.putExtra("item", 3);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				finish();
				break;
			case R.id.my_order_rb1:
				transaction = getSupportFragmentManager().beginTransaction();
				transaction.show(coachFragment);
				transaction.hide(lookRoomFragment);

				transaction.commit();
				break;
			case R.id.my_order_rb2:
				transaction = getSupportFragmentManager().beginTransaction();
				transaction.show(lookRoomFragment);
				transaction.hide(coachFragment);
				transaction.commit();
				break;
			/*case R.id.my_order_rbno_order:
				if (rbNoOrder.isChecked()) {
					rbNoOrder.setCompoundDrawables(null,null,null,da);
					rbOrdered.setCompoundDrawables(null,null,null,null);
					rbNoEvaluate.setCompoundDrawables(null,null,null,null);
					rbEvaluate.setCompoundDrawables(null,null,null,null);
				}
				break;
			case R.id.my_order_rbordered:
				if (rbOrdered.isChecked()) {
					rbNoOrder.setCompoundDrawables(null,null,null,null);
					rbOrdered.setCompoundDrawables(null,null,null,da);
					rbNoEvaluate.setCompoundDrawables(null,null,null,null);
					rbEvaluate.setCompoundDrawables(null,null,null,null);
				}
				break;
			case R.id.my_order_rbno_evaluate:
				if (rbNoEvaluate.isChecked()) {
					rbNoOrder.setCompoundDrawables(null,null,null,null);
					rbOrdered.setCompoundDrawables(null,null,null,null);
					rbNoEvaluate.setCompoundDrawables(null,null,null,da);
					rbEvaluate.setCompoundDrawables(null,null,null,null);
				}
				break;
			case R.id.my_order_rbevaluated:
				if (rbEvaluate.isChecked()) {
					rbNoOrder.setCompoundDrawables(null,null,null,null);
					rbOrdered.setCompoundDrawables(null,null,null,null);
					rbNoEvaluate.setCompoundDrawables(null,null,null,null);
					rbEvaluate.setCompoundDrawables(null,null,null,da);
				}
				break;*/
			default:
				break;
		}
	}
}
