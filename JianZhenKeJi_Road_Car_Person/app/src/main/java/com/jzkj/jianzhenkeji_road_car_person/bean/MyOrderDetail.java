package com.jzkj.jianzhenkeji_road_car_person.bean;

public class MyOrderDetail {
	private String StudentBookingsId;
	private String StartDate;
	private String name;
	private String evaluateId;
	private String isAgree;
	private String evaluateState;
	private String CoachBookingsDateId;

	public String getCoachBookingsDateId() {
		return CoachBookingsDateId;
	}

	public void setCoachBookingsDateId(String coachBookingsDateId) {
		CoachBookingsDateId = coachBookingsDateId;
	}

	public String getStudentBookingsId() {
		return StudentBookingsId;
	}

	public void setStudentBookingsId(String studentBookingsId) {
		StudentBookingsId = studentBookingsId;
	}

	public String getStartDate() {
		return StartDate;
	}

	public void setStartDate(String startDate) {
		StartDate = startDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEvaluateId() {
		return evaluateId;
	}

	public void setEvaluateId(String evaluateId) {
		this.evaluateId = evaluateId;
	}

	public String getIsAgree() {
		return isAgree;
	}

	public void setIsAgree(String isAgree) {
		this.isAgree = isAgree;
	}

	public String getEvaluateState() {
		return evaluateState;
	}

	public void setEvaluateState(String evaluateState) {
		this.evaluateState = evaluateState;
	}

	public MyOrderDetail() {
	}
}
