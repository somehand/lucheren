package com.jzkj.jianzhenkeji_road_car_person.adapter;

import android.content.Context;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.CircleImageView;
import com.jzkj.jianzhenkeji_road_car_person.MyApplication;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.bean.ChooseCoachBean;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Administrator on 2016/5/13 0013.
 */
public class ChooseCoachAdapter extends QuickAdapter<ChooseCoachBean> {
	public ChooseCoachAdapter(Context context, int layoutResId) {
		super(context, layoutResId);
	}

	@Override
	protected void convert(BaseAdapterHelper helper, ChooseCoachBean item) {
		CircleImageView headImg = helper.getView(R.id.item_head_img);
		TextView name = helper.getView(R.id.item_text_name);
		TextView plateNumber = helper.getView(R.id.item_text_plate_number);
		TextView address = helper.getView(R.id.item_text_address);

		ImageLoader.getInstance().displayImage(item.getCoachLogo(), headImg, MyApplication.options_other);
		name.setText(item.getCoachName());
		plateNumber.setText(item.getLicensePlate());
		address.setText(item.getCoachAddress());
	}
}
