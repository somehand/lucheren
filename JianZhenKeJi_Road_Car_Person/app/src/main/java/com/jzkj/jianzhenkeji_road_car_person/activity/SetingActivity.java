package com.jzkj.jianzhenkeji_road_car_person.activity;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.BaseActivity;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.adapter.SetingLVAdapter;
import com.jzkj.jianzhenkeji_road_car_person.bean.MySeting;

import java.util.ArrayList;

public class SetingActivity extends BaseActivity implements View.OnClickListener ,AdapterView.OnItemClickListener{

	private ImageButton ibBack;
	private TextView tvTitle;
	private ListView lv;
	private ArrayList<MySeting> list;
	private String[] itemNames;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.seting);
		init();

		list = new ArrayList<MySeting>();
		itemNames = new String[]{"修改密码"/*, "退出登录"*/};
		for (int i = 0; i < itemNames.length; i++) {
			list.add(new MySeting(itemNames[i]));//
		}
		SetingLVAdapter adapter = new SetingLVAdapter(this, list);
		lv.setAdapter(adapter);

		lv.setOnItemClickListener(this);


		ibBack.setOnClickListener(this);
	}

	/**
	 * 初始化绑定id
	 */
	private void init() {
		ibBack = (ImageButton) findViewById(R.id.headframe_ib);
		tvTitle = (TextView) findViewById(R.id.headframe_title);
		lv = (ListView) findViewById(R.id.seting_lv);
		tvTitle.setText("设置");

	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.headframe_ib:
				finish();
				break;


			default:
				break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
		switch (i) {
			case 0:
				Intent intent = new Intent(SetingActivity.this,ModifyPasswordActivity.class);
				startActivity(intent);
				break;
			/*case 1:
				Intent intent1 = new Intent(SetingActivity.this, LoginActivity.class);
				intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent1);
				Utils.SPPutBoolean(this,Utils.isLogin , false);

				break;*/
			default:
				break;
		}
	}
}
