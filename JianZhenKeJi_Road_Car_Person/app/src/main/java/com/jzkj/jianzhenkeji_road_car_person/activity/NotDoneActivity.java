package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.adapter.NotDoneAdapter;
import com.jzkj.jianzhenkeji_road_car_person.bean.NotDoneBean;
import com.socks.library.KLog;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016/3/1.
 */
public class NotDoneActivity extends Activity{
	private LinearLayout llBack;
	private GridView gv;
	private ArrayList<Integer> answeredList;
	private ArrayList<NotDoneBean> beans;
	NotDoneAdapter mAdapter;
	private int[] nums = new int[101];

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.not_done);
		initView();
		initData();
		initListener();
	}

	private void initListener() {
		llBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});

		gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				Intent intent = new Intent();
				intent.putExtra("index" , i);
				setResult(RESULT_OK, intent);
				finish();
			}
		});
	}

	private void initView() {
		llBack = (LinearLayout) findViewById(R.id.not_done_back_ll);
		gv = (GridView) findViewById(R.id.not_done_gv);
	}


	private void initData() {
		Intent intent = getIntent();
		answeredList = intent.getIntegerArrayListExtra("list");
		KLog.e(answeredList.toString() + "------------");
		beans = new ArrayList<NotDoneBean>();
		int num = intent.getIntExtra("num" , 0);
		mAdapter = new NotDoneAdapter(this, R.layout.not_done_gv_item , answeredList);
		gv.setAdapter(mAdapter);
		for (int i = 0; i < num; i++) {
			NotDoneBean notDoneBean = new NotDoneBean();
			notDoneBean.setNumber(i + 1);
			beans.add(notDoneBean);
		}
		mAdapter.addAll(beans);
	}
}
