package com.jzkj.jianzhenkeji_road_car_person.bean;

/**
 * Created by Administrator on 2016/2/24 0024.
 */
public class OrderRoomDetailsBean {

	private int studentBookingId;
	private int timeId;

	private String name;
	private int orderNum;
	private int totalNum;
	private String time;
	private String class1, class2;
	private boolean enable = true;
	private boolean full = false;

	public boolean isFull() {
		return full;
	}

	public void setFull(boolean full) {
		this.full = full;
	}

	public int getStudentBookingId() {
		return studentBookingId;
	}

	public void setStudentBookingId(int studentBookingId) {
		this.studentBookingId = studentBookingId;
	}

	public int getTimeId() {
		return timeId;
	}

	public void setTimeId(int timeId) {
		this.timeId = timeId;
	}

	public int getOrderNum() {
		return orderNum;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public void setOrderNum(int orderNum) {
		this.orderNum = orderNum;
	}

	public int getTotalNum() {
		return totalNum;
	}

	public void setTotalNum(int totalNum) {
		this.totalNum = totalNum;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getClass1() {
		return class1;
	}

	public void setClass1(String class1) {
		this.class1 = class1;
	}

	public String getClass2() {
		return class2;
	}

	public void setClass2(String class2) {
		this.class2 = class2;
	}

	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}

}
