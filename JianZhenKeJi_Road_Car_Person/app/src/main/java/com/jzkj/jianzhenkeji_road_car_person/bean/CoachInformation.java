package com.jzkj.jianzhenkeji_road_car_person.bean;

import android.graphics.drawable.Drawable;

public class CoachInformation {
	private int imgHead;
	private int imgGrade;
	private int starLevel;
	private String coachName;
	private String orderNum;
	private String evaluateNum;
	public int getImgHead() {
		return imgHead;
	}
	public void setImgHead(int imgHead) {
		this.imgHead = imgHead;
	}
	public int getImgGrad() {
		return imgGrade;
	}
	public void setImgGrad(int imgGrad) {
		this.imgGrade = imgGrad;
	}
	public int getStarLevel() {
		return starLevel;
	}
	public void setStarLevel(int starLevel) {
		this.starLevel = starLevel;
	}
	public String getCoachName() {
		return coachName;
	}
	public void setCoachName(String coachName) {
		this.coachName = coachName;
	}
	public String getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}
	public String getEvaluateNum() {
		return evaluateNum;
	}
	public void setEvaluateNum(String evaluateNum) {
		this.evaluateNum = evaluateNum;
	}
	public CoachInformation(int imgHead, int imgGrad, int starLevel,
			String coachName, String orderNum, String evaluateNum) {
		super();
		this.imgHead = imgHead;
		this.imgGrade = imgGrad;
		this.starLevel = starLevel;
		this.coachName = coachName;
		this.orderNum = orderNum;
		this.evaluateNum = evaluateNum;
	}
	
}
