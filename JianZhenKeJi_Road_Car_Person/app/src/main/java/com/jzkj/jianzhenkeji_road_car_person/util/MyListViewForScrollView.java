package com.jzkj.jianzhenkeji_road_car_person.util;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public class MyListViewForScrollView extends ListView {

	public MyListViewForScrollView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public MyListViewForScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public MyListViewForScrollView(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
				MeasureSpec.AT_MOST);
		super.onMeasure(widthMeasureSpec, expandSpec);
	}
}
