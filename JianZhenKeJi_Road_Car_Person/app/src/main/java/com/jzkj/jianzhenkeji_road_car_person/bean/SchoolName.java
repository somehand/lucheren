package com.jzkj.jianzhenkeji_road_car_person.bean;

/**
 * Created by monster on 2016/3/25.
 */
public class SchoolName {
	String name;

	public SchoolName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
