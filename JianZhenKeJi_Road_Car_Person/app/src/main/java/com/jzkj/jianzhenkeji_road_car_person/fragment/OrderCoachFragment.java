package com.jzkj.jianzhenkeji_road_car_person.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.activity.ChooseSchoolActivity;
import com.jzkj.jianzhenkeji_road_car_person.activity.OrderCoachListActivity;
import com.jzkj.jianzhenkeji_road_car_person.activity.TipActivity_1;
import com.jzkj.jianzhenkeji_road_car_person.caldroid.CaldroidSampleCustomAdapter;
import com.jzkj.jianzhenkeji_road_car_person.caldroid.CaldroidSampleCustomFragment;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;
import com.roomorama.caldroid.CalendarHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import butterknife.ButterKnife;
import hirondelle.date4j.DateTime;

public class OrderCoachFragment extends Fragment implements View.OnTouchListener{
	private CaldroidFragment caldroidFragment;
	int[] s = {26 , 27, 29};
	HashMap<String, Object> mMap = new HashMap<String, Object>();
	HashMap<DateTime , Integer> mDateEnableMap = new HashMap<DateTime, Integer>();
	ArrayList<Date> mDatesEnable = new ArrayList<Date>();
	ArrayList<Integer> mDatesDisable = new ArrayList<Integer>();
	boolean isFirst = true;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		caldroidFragment = new CaldroidSampleCustomFragment();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_order_room, null);
		ButterKnife.inject(this, view);
		initView();
		//添加額外的数据 比如哪天预约了
		caldroidFragment.setExtraData(mMap);
		initData();
		caldroidFragment.setCaldroidListener(caldroidListener);

		return view;
	}

	private void initData() {
		Date date = new Date();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		//mDatesEnable.add(date);
		for (int i = 0; i < 10; i++) {
			calendar.add(Calendar.DAY_OF_MONTH , 1);
			calendar.set(Calendar.HOUR_OF_DAY , 0);
			calendar.set(Calendar.MINUTE , 0);
			calendar.set(Calendar.SECOND , 0);
			mDatesEnable.add(calendar.getTime());

		}
		for (Date date1 : mDatesEnable){
			mDateEnableMap.put(CalendarHelper.convertDateToDateTime(date1) , 1);
		}
		mMap.put("datesEnable" , mDateEnableMap);

//		mDatesDisable.add(28);
		mMap.put("datesDisable" , mDatesDisable);
		mMap.put("type" , CaldroidSampleCustomAdapter.TYPE_EXAM_ROOM);
	}

	private void initView() {
		Bundle bundle = new Bundle();
		Calendar calendar = Calendar.getInstance();
		bundle.putInt(CaldroidFragment.MONTH , calendar.get(Calendar.MONTH) + 1);
		bundle.putInt(caldroidFragment.YEAR, calendar.get(Calendar.YEAR));
		bundle.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
		bundle.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, false);
		bundle.putBoolean(CaldroidFragment.SHOW_NAVIGATION_ARROWS, false);
		bundle.putBoolean(CaldroidFragment.ENABLE_CLICK_ON_DISABLED_DATES, false);
		caldroidFragment.setArguments(bundle);
		FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
		transaction.replace(R.id.order_room_ly , caldroidFragment);
		transaction.commit();
	}

	CaldroidListener caldroidListener = new CaldroidListener() {
		@Override
		public void onSelectDate(Date date, View view) {
			//System.out.println("------------");
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(date);
			int day = calendar.get(Calendar.DAY_OF_MONTH);
			//如果属于可预约的日期
			for (int i = 0; i < mDatesEnable.size(); i++) {
				if(mDatesEnable.get(i).toString() .equals(date.toString())){
					SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
//					Toast.makeText(getActivity() , format.format(date) , Toast.LENGTH_SHORT).show();

					Intent intent = new Intent(getActivity(), OrderCoachListActivity.class);
					intent.putExtra("date" , format.format(date));
					getActivity().startActivity(intent);
					break;
				}
			}

		}
		@Override
		public void onCaldroidViewCreated() {
			super.onCaldroidViewCreated();
			TextView textView = caldroidFragment.getMonthTitleTextView();
//				RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) textView.getLayoutParams();
//				layoutParams.bottomMargin = -10;
//				textView.setPadding(0,0,0,0);
//				textView.setLayoutParams(layoutParams);
			textView.setTextSize(TypedValue.COMPLEX_UNIT_SP , 14);
			textView.setTextColor(getResources().getColor(R.color.caldroid_darker_gray));
			GridView weekGrid = caldroidFragment.getWeekdayGridView();
			weekGrid.setBackgroundColor(getResources().getColor(R.color.common_blue));

		}
	};

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		ButterKnife.reset(this);
	}

	public boolean onTouch(View v, MotionEvent event) {
		return true;
	}


	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if(!hidden && isFirst){
			if (Utils.SPGetString(getActivity(),Utils.schoolId,"").length() == 0) {
				AlertDialog.Builder aBuilder = new AlertDialog.Builder(getActivity());
				aBuilder.setMessage("您还未绑定驾校，是否现在绑定？");
				aBuilder.setPositiveButton("是", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						startActivity(new Intent(getActivity() , ChooseSchoolActivity.class));

					}
				});
				aBuilder.setNegativeButton("否", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						dialogInterface.dismiss();
					}
				});
				AlertDialog dialog = aBuilder.create();
				dialog.show();
			}
			isFirst = false;
			/**
			 * 用户点取消后, 不会每次 可见fragment的时候都 弹出
			 */

			if(!Utils.SPGetBoolean(getActivity(),Utils.tips_4_Showed , false)){
				startActivity(new Intent(getActivity() , TipActivity_1.class).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY));
				getActivity().overridePendingTransition(0 , 0);
			}
		}

	}
}
