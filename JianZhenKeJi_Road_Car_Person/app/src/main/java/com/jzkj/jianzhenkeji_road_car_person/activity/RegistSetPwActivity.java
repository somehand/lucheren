package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.jzkj.jianzhenkeji_road_car_person.BaseActivity;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.MyMd5;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Set;

import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;

public class RegistSetPwActivity extends BaseActivity implements OnClickListener {

	private EditText etPassword, etPasswordAgain;
	private ImageButton ibBack;
	private TextView tvTitle;
	private Button btnSubmit;
	//请求码
	public static final int RESULT_CODE = 101;
	/**
	 * 使用handler发送消息
	 */
	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			super.handleMessage(msg);
			switch (msg.what) {
				case 1:
					showToast(RegistSetPwActivity.this, "设置密码", Toast.LENGTH_SHORT);
					break;

				default:
					break;
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.regist_setpw);
		init();

		btnSubmit.setOnClickListener(this);
		ibBack.setOnClickListener(this);
	}

	private void init() {
		etPassword = (EditText) findViewById(R.id.regist_setpw_et1);
		etPasswordAgain = (EditText) findViewById(R.id.regist_setpw_et2);
		ibBack = (ImageButton) findViewById(R.id.headframe_ib);
		tvTitle = (TextView) findViewById(R.id.headframe_title);
		btnSubmit = (Button) findViewById(R.id.regist_setpw_submit);
		tvTitle.setText("设置密码");
		tvTitle.setVisibility(View.VISIBLE);
		ibBack.setVisibility(View.VISIBLE);
	}

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
			case R.id.headframe_ib:
				finish();
				break;
			case R.id.regist_setpw_submit:
				regist();
				break;

			default:
				break;
		}

	}


	/**
	 * 注册
	 */
	public void regist() {
		String pw = etPassword.getText().toString().trim();
		String surePw = etPasswordAgain.getText().toString().trim();
		final String mobile = getIntent().getStringExtra("username");
		System.out.println(mobile + "<<<<<<<<");
		if (pw.length() != 0 && surePw.length() != 0) {

			if (pw.length() < 6) {
				Utils.ToastShort(this, "密码至少需要6位");
			} else {
				if (pw.equals(surePw)) {
					final String md5pw = MyMd5.getMd5(pw);
					RequestParams params = new RequestParams();
					params.put("UserName", mobile);
					params.put("PassWord", md5pw);
					params.put("RegChannel", 2);
					params.put("UserType", 1);
					KLog.e("pwd " + pw + " md5 :" + md5pw);
					MyHttp.getInstance(this).post(MyHttp.REGIST_USER, params, new JsonHttpResponseHandler() {
						@Override
						public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
							super.onSuccess(statusCode, headers, response);
							KLog.e(Utils.TAG_DEBUG, "jsonObject :");
							KLog.json(Utils.TAG_DEBUG, response.toString());
							try {
								if (response.getInt("Code") == 1) {
									Utils.ToastShort(RegistSetPwActivity.this, response.getString("Reason"));
									Utils.SPPutBoolean(RegistSetPwActivity.this, Utils.isLogin, true);
									Intent intent = new Intent(RegistSetPwActivity.this, HomeActivity.class);
									startActivity(intent);
									getLogin();
								} else {
									Utils.ToastShort(RegistSetPwActivity.this, response.getString("Reason"));
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					});

				} else {
					Utils.ToastShort(this, "两次密码不相同");
				}
			}
		} else {
			Utils.ToastShort(this, "密码不能为空");
		}
	}


	/**
	 * 获取登录数据
	 */
	private void getLogin() {

		String pw = etPassword.getText().toString().trim();
		String surePw = etPasswordAgain.getText().toString().trim();
		final String mobile = getIntent().getStringExtra("username");
		RequestParams params = new RequestParams();
		params.put("UserName", mobile);
		params.put("PassWord", pw);
		params.put("Type", 1);

		MyHttp.getInstance(RegistSetPwActivity.this).post(MyHttp.USER_LOGIN, params, new JsonHttpResponseHandler(
		) {

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());

			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString() + "response  responseresponseresponseresponse");

				try {
					JSONObject jsonObject = response.getJSONObject(0);
					KLog.e(Utils.TAG_DEBUG, "jsonArray :");
					KLog.json(Utils.TAG_DEBUG, response.toString());

					Utils.SPPutBoolean(RegistSetPwActivity.this, "isLogin", true);
					Toast.makeText(RegistSetPwActivity.this, "登录成功(^_^)", Toast.LENGTH_LONG).show();
					Utils.SPutString(RegistSetPwActivity.this, Utils.userId, jsonObject.getString("UserId"));
					Utils.SPutString(RegistSetPwActivity.this, Utils.userName, jsonObject.getString("UserName"));

					Utils.SPutString(RegistSetPwActivity.this, Utils.realName, jsonObject.getString("RealName"));
					Utils.SPutString(RegistSetPwActivity.this, Utils.nickName, jsonObject.getString("NickName"));
					Utils.SPutString(RegistSetPwActivity.this, Utils.idCarNumber, jsonObject.getString("IDCardNumber"));
					Utils.SPutString(RegistSetPwActivity.this, Utils.userSex, jsonObject.getString("UserSex"));
					Utils.SPutString(RegistSetPwActivity.this, Utils.userAddress, jsonObject.getString("UserAddress"));
					Utils.SPutString(RegistSetPwActivity.this, Utils.userLogo, jsonObject.getString("UserLogo"));
					Utils.SPutString(RegistSetPwActivity.this, Utils.schoolName, jsonObject.getString("SchoolName"));

					Utils.SPPutBoolean(RegistSetPwActivity.this, Utils.isLogin, true);
					Utils.SPutString(RegistSetPwActivity.this, Utils.schoolId, jsonObject.getString("DrivingSchoolId"));
					Utils.SPutString(RegistSetPwActivity.this, Utils.coachId, jsonObject.getString("CoachId"));
					Utils.SPutString(RegistSetPwActivity.this, Utils.CoachName, jsonObject.getString("CoachName"));
					Utils.SPutString(RegistSetPwActivity.this, Utils.SchoolUrl, jsonObject.getString("SchoolProfiles"));
					Utils.SPutString(RegistSetPwActivity.this, Utils.TeachType, jsonObject.getString("TeachType"));
					if (jsonObject.getInt("DrivingType") == 1) {
						Utils.SPutString(RegistSetPwActivity.this, Utils.DrivingType, "C1");
					} else {
						Utils.SPutString(RegistSetPwActivity.this, Utils.DrivingType, "C2");
					}
					setAlias(mobile);

					finish();
				} catch (JSONException e) {
					e.printStackTrace();
				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);

			}
		});
//		}
	}


	private void setAlias(String name) {
		mHandler.sendMessage(mHandler.obtainMessage(12, name));

	}

	TagAliasCallback mAliasCallback = new TagAliasCallback() {
		@Override
		public void gotResult(int code, String alias, Set<String> tags) {
			switch (code) {
				case 0:
					KLog.e("alias", "别名设置成功");
					break;
				case 6002:
					KLog.e("alias", "别名设置失败， 60秒后重试");
					mHandler.sendMessageDelayed(mHandler.obtainMessage(12, alias), 1000 * 60);
					break;
				default:
					KLog.e("alias", "别名设置失败， errorCode = " + code);
					break;
			}
		}
	};

	Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
				case 12:
					JPushInterface.setAlias(getApplicationContext(), (String) msg.obj, mAliasCallback);
					break;
			}

		}
	};
}


