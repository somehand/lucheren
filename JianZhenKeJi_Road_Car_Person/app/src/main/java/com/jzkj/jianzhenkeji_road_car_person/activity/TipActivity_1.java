package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;

/**
 * Created by monster on 2016/3/27.
 */
public class TipActivity_1 extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tip_1_layout);
		Utils.SPPutBoolean(this,Utils.tips_1_Showed ,true);

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		finish();
		overridePendingTransition(0 , 0);
		return true;
	}
}
