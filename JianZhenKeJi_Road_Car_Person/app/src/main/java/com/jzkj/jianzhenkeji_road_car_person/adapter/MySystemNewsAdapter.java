package com.jzkj.jianzhenkeji_road_car_person.adapter;

import android.content.Context;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.bean.MySystemNewsBean;

/**
 * Created by Administrator on 2016/3/21.
 */
public class MySystemNewsAdapter extends QuickAdapter<MySystemNewsBean>{
	public MySystemNewsAdapter(Context context, int layoutResId) {
		super(context, layoutResId);
	}

	@Override
	protected void convert(BaseAdapterHelper helper, MySystemNewsBean item) {
		TextView tvDate = helper.getView(R.id.my_system_news_tvdate);
		TextView tvText = helper.getView(R.id.my_system_news_tvtextinfo);

		tvDate.setText(item.getPushDate());
		tvText.setText(item.getMsgContent());
	}
}
