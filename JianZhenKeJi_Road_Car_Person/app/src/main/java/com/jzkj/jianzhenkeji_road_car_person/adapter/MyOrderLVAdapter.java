package com.jzkj.jianzhenkeji_road_car_person.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.activity.CommentedActivity;
import com.jzkj.jianzhenkeji_road_car_person.bean.MyOrderDetail;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyOrderLVAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<MyOrderDetail> list;
	private LayoutInflater inflater;
	private int progress;//1未预约 2已预约 3未评价 4已评价

	public MyOrderLVAdapter(Context context, ArrayList<MyOrderDetail> list, int progress) {
		super();
		this.context = context;
		this.list = list;
		this.progress = progress;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(final int arg0, View arg1, ViewGroup arg2) {
		viewHolder holder = null;
		if (arg1 == null) {
			arg1 = inflater.inflate(R.layout.my_order_item, null);
		}
		holder = (viewHolder) arg1.getTag();
		if (holder == null) {
			holder = new viewHolder();
			holder.tvHour = (TextView) arg1.findViewById(R.id.my_order_tvhour);
			holder.tvLearnCoach = (TextView) arg1
					.findViewById(R.id.my_order_coach);
			holder.tvState = (TextView) arg1
					.findViewById(R.id.my_order_tvstate);
			holder.tvYear = (TextView) arg1.findViewById(R.id.my_order_tvyear);
			holder.ivSure = (ImageView) arg1.findViewById(R.id.my_order_ivsure);
			holder.no_answered = (ImageView) arg1.findViewById(R.id.my_order_unanswered);
			holder.ivOverDue = (ImageView) arg1.findViewById(R.id.my_order_ivoverdue);
			holder.tvEvaluateCoach = (TextView) arg1.findViewById(R.id.my_order_tvevaluate_coach);
			holder.tvEvaluated = (TextView) arg1.findViewById(R.id.my_order_tvevaluated_coach);
		}
		holder.clear();
		final MyOrderDetail myOrderDetail = list.get(arg0);
		holder.tvYear.setText(myOrderDetail.getStartDate());
		holder.tvLearnCoach.setText(myOrderDetail.getName());
		holder.tvState.setText(myOrderDetail.getIsAgree());
		if (!"已预约".equals(String.valueOf(myOrderDetail.getIsAgree()))) {
			holder.tvState.setTextColor(context.getResources().getColor(R.color.globalframe_rb_blue));
		}
		if (progress == 1) {
			holder.tvEvaluateCoach.setVisibility(View.GONE);
			holder.tvEvaluated.setVisibility(View.GONE);
		} else if (progress == 2) {
			if (!TextUtils.isEmpty(myOrderDetail.getEvaluateId())) {
				holder.tvEvaluateCoach.setVisibility(View.GONE);
				holder.tvEvaluated.setVisibility(View.VISIBLE);
//				holder.tvEvaluated.setText(myOrderDetail.getEvaluateState());
				holder.tvEvaluated.setText("已评价");
				holder.tvEvaluated.setTextColor(Color.parseColor("#999999"));
				/*if ("好评".equals(myOrderDetail.getEvaluateState())) {
					holder.tvEvaluated.setTextColor(Color.parseColor("#f15549"));
				} else if ("中评".equals(myOrderDetail.getEvaluateState())) {
					holder.tvEvaluated.setTextColor(Color.parseColor("#01891e"));
				} else {
					holder.tvEvaluated.setTextColor(Color.parseColor("#999999"));
				}*/
			} else {
				holder.tvEvaluateCoach.setVisibility(View.VISIBLE);
				holder.tvEvaluated.setVisibility(View.GONE);
			}

		} else if (progress == 3) {
			Log.e("paramsfdsafdsafa", progress + "********");
			holder.tvEvaluateCoach.setVisibility(View.VISIBLE);
			holder.tvEvaluated.setVisibility(View.GONE);
		} else {
			holder.tvEvaluateCoach.setVisibility(View.GONE);
			holder.tvEvaluated.setVisibility(View.VISIBLE);
			holder.tvEvaluated.setText("已评价");
			holder.tvEvaluated.setTextColor(Color.parseColor("#999999"));
			/*if ("好评".equals(myOrderDetail.getEvaluateState())) {
				holder.tvEvaluated.setTextColor(Color.parseColor("#f15549"));
			} else if ("中评".equals(myOrderDetail.getEvaluateState())) {
				holder.tvEvaluated.setTextColor(Color.parseColor("#01891e"));
			} else {
				holder.tvEvaluated.setTextColor(Color.parseColor("#999999"));
			}*/
		}
//		holder.tvHour.setText(myOrderDetail.getTimeHour());
		/*if ("未预约".equals(String.valueOf(myOrderDetail.getState()))) {
			holder.tvState.setTextColor(context.getResources().getColor(R.color.globalframe_rb_blue));
		}
		holder.tvState.setText(myOrderDetail.getState());
		holder.tvYear.setText(myOrderDetail.getStartDate());
		holder.tvLearnCoach.setText(myOrderDetail.getCoachName());
		if ("是".equals(String.valueOf(myOrderDetail.getIsEvaluate()))) {
			holder.tvEvaluated.setVisibility(View.VISIBLE);
			holder.tvEvaluateCoach.setVisibility(View.GONE);
		}else{
			holder.tvEvaluated.setVisibility(View.GONE);
			holder.tvEvaluateCoach.setVisibility(View.VISIBLE);
		}*/
		//设置标记图标
		/*if (myOrderDetail.getIsAgree().equals("已同意")
				|| myOrderDetail.getIsAgree() == "已同意") {

			holder.ivSure.setVisibility(View.VISIBLE);
			holder.ivOverDue.setVisibility(View.GONE);
			holder.no_answered.setVisibility(View.GONE);
			holder.tvEvaluateCoach.setVisibility(View.VISIBLE);

			if(myOrderDetail.getStudentCommentId() != 0){
				holder.tvEvaluateCoach.setVisibility(View.GONE);
			}

		} else if (myOrderDetail.getIsAgree().equals("已拒绝")
				|| myOrderDetail.getIsAgree() == "已拒绝") {
			holder.ivOverDue.setVisibility(View.VISIBLE);
			holder.ivSure.setVisibility(View.GONE);
			holder.no_answered.setVisibility(View.GONE);
			holder.tvEvaluateCoach.setVisibility(View.GONE);
			holder.tvEvaluateCoach.setVisibility(View.GONE);

		} else if (myOrderDetail.getIsAgree().equals("未答复")
				|| myOrderDetail.getIsAgree() == "未答复") {
			holder.ivOverDue.setVisibility(View.GONE);
			holder.ivSure.setVisibility(View.GONE);
			holder.no_answered.setVisibility(View.VISIBLE);
			holder.tvEvaluateCoach.setVisibility(View.GONE);
			holder.tvEvaluateCoach.setVisibility(View.GONE);

		}*/


		final viewHolder finalHolder = holder;
		holder.tvEvaluateCoach.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				/*AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setMessage("请对该教练进行评价：");
				builder.setPositiveButton("满意", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
//						evaluateCoach(1, myOrderDetail, finalHolder.tvEvaluateCoach);
					}
				});
				builder.setNegativeButton("不满意", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
//						evaluateCoach(2, myOrderDetail, finalHolder.tvEvaluateCoach);
					}
				});
				AlertDialog dialog = builder.create();
				dialog.show();*/
				Intent intent = new Intent(context, CommentedActivity.class);
				intent.putExtra("StudentBookingsId", myOrderDetail.getStudentBookingsId());
				context.startActivity(intent);

			}
		});
		//button监听
		/*holder.btnOrder.setTag(arg0 + "");
		holder.btnOrder.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (view.getTag().toString().equals(arg0 + "")) {

				}
			}
		});*/

		//根据教练等级设置字体颜色
		/*if (myOrderDetail.getState().equals("体验教练")
				|| myOrderDetail.getLearnCoach() == "体验教练") {
			holder.tvLearnCoach.setTextColor(Color.parseColor("#eb6100"));
		} else if (myOrderDetail.getLearnCoach().equals("二级教练")
				|| myOrderDetail.getLearnCoach() == "二级教练") {
			holder.tvLearnCoach.setTextColor(Color.parseColor("#018a25"));
		} else if (myOrderDetail.getLearnCoach().equals("一级教练")
				|| myOrderDetail.getLearnCoach() == "一级教练") {
			holder.tvLearnCoach.setTextColor(Color.parseColor("#0e93f6"));
		}
		holder.tvLearnCoach.setText(myOrderDetail.getLearnCoach());*/
		return arg1;
	}

	private void evaluateCoach(int i, MyOrderDetail myOrderDetail, final TextView text) {
		RequestParams params = new RequestParams();
//		params.put("StudentBookingsId", myOrderDetail.getStudentBookingsId());
		params.put("Comment", i);
		MyHttp.getInstance(context).post(MyHttp.EVALUATE_COACH, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
				try {
					if (response.getInt("Code") == 1) {
						text.setVisibility(View.GONE);
						text.setEnabled(false);
						text.setTextColor(context.getResources().getColor(R.color.globalframe_rb_gray));
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}

	class viewHolder {
		private TextView tvYear, tvHour, tvLearnCoach, tvState, tvEvaluateCoach, tvEvaluated;
		private ImageView ivSure, ivOverDue, no_answered;

		void clear() {
			tvHour.setText("");
			tvYear.setText("");
			tvState.setText("");
			tvLearnCoach.setText("");
			tvEvaluated.setText("");
		}
	}
}
