package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.BaseActivity;
import com.jzkj.jianzhenkeji_road_car_person.R;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MyNewsActivity extends BaseActivity implements View.OnClickListener {
	private ImageButton ibBack;
	private TextView tvTitle, tvTextContent, tvPushDate, tvOrderTextContent, tvOrderDate, tvLookRoomTextContent, tvLookRoomDate;
	private RelativeLayout myNewsll, systemNewsll, groupBuyll;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_news);
		init();
		bindListener();

	}

	private void bindListener() {
		ibBack.setOnClickListener(this);
		myNewsll.setOnClickListener(this);
		systemNewsll.setOnClickListener(this);
		groupBuyll.setOnClickListener(this);
	}

	private void init() {
		ibBack = (ImageButton) findViewById(R.id.headframe_ib);
		tvTitle = (TextView) findViewById(R.id.headframe_title);
		tvTextContent = (TextView) findViewById(R.id.my_news_system_textcontent);
		tvPushDate = (TextView) findViewById(R.id.my_news_system_pushdate);
		tvLookRoomTextContent = (TextView) findViewById(R.id.my_news_lookroom_textcontent);
		tvLookRoomDate = (TextView) findViewById(R.id.my_news_lookroom_date);
		tvOrderTextContent = (TextView) findViewById(R.id.my_news_order_textcontent);
		tvOrderDate = (TextView) findViewById(R.id.my_news_order_date);
		tvTitle.setText("我的消息");
		myNewsll = (RelativeLayout) findViewById(R.id.my_news_ll);
		systemNewsll = (RelativeLayout) findViewById(R.id.my_system_news_ll);
		groupBuyll = (RelativeLayout) findViewById(R.id.my_news_tuang_ll);
		tvTextContent.setText("路车人提醒您");
		tvPushDate.setText(new SimpleDateFormat("yyyy/MM/dd").format(new Date()));
		tvOrderTextContent.setText("已预约消息");
		tvOrderDate.setText(new SimpleDateFormat("yyyy/MM/dd").format(new Date()));
		tvLookRoomTextContent.setText("教练发布的合场团购消息");
		tvLookRoomDate.setText(new SimpleDateFormat("yyyy/MM/dd").format(new Date()));
//		getSystemNews();
	}


	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.headframe_ib:
				finish();
				break;
			case R.id.my_system_news_ll:
				Intent intent = new Intent(this, MySystemNewsActivity.class);
				startActivity(intent);
				break;
			case R.id.my_news_ll:
				Intent intent2 = new Intent(this, MyOrderNewsActivity.class);
				startActivity(intent2);
				break;
			case R.id.my_news_tuang_ll:
				Intent intent3 = new Intent(this, GroupBuyActivity.class);
				startActivity(intent3);
				break;

			default:

				break;
		}
	}

	/*private void getSystemNews() {
		RequestParams params = new RequestParams();
		params.put("messagetype", 2);
		params.put("type", 1);
		params.put("pageindex", 1);
		params.put("pagesize", 20);
		params.put("Userid", Utils.SPGetString(MyNewsActivity.this, Utils.userId, ""));
		KLog.e("params", params.toString());
		MyHttp.getInstance(this).post(MyHttp.GET_MYNEWS_INFO, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
				tvTextContent.setText("暂无系统消息");
				tvPushDate.setText(new SimpleDateFormat("yyyy/MM/dd").format(new Date()));
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
				for (int i = 0; i < response.length(); i++) {
					try {
						if (i == 0) {
							JSONObject object = response.getJSONObject(i);
							tvTextContent.setText(object.getString("MsgContent"));
							SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							Date date = null;
							try {
								date = format.parse(object.getString("PushDate"));
								SimpleDateFormat formatNow = new SimpleDateFormat("yyyy/MM/dd");
								tvPushDate.setText(formatNow.format(date) + "");
								Log.e("date", formatNow.format(date) + "");
							} catch (ParseException e) {
								e.printStackTrace();
							}
						}

					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}*/
}
