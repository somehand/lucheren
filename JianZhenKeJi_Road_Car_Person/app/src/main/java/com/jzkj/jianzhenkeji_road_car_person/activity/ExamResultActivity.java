package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.BaseActivity;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ExamResultActivity extends BaseActivity implements View.OnClickListener {
	@InjectView(R.id.exam_result_back)
	ImageButton mExamResultBack;
	@InjectView(R.id.exam_result_score)
	TextView mExamResultScore;
	@InjectView(R.id.exam_result_tag)
	TextView mExamResultTag;
	@InjectView(R.id.exam_result_date)
	TextView mExamResultDate;
	@InjectView(R.id.exam_result_time)
	TextView mExamResultTime;
	@InjectView(R.id.exam_result_totalNum)
	TextView mExamResultTotalNum;
	@InjectView(R.id.exam_result_correctNum)
	TextView mExamResultCorrectNum;
	@InjectView(R.id.exam_result_wrongNum)
	TextView mExamResultWrongNum;
	@InjectView(R.id.exam_result_percent)
	TextView mExamResultPercent;
	@InjectView(R.id.exam_result_btn)
	Button mExamResultBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.exam_result);
		ButterKnife.inject(this);
		initData();
		initListener();
		GET_DailyTask();
	}

	private void initData() {
		Intent intent = getIntent();
		mExamResultScore.setText((int) (intent.getDoubleExtra("score", 0)) + "");
//		mExamResultTag.setBackgroundResource();
		mExamResultTime.setText(intent.getStringExtra("time"));
		mExamResultDate.setText(intent.getStringExtra("date"));
		mExamResultTotalNum.setText(intent.getIntExtra("total", 0) + "");
		mExamResultCorrectNum.setText(intent.getIntExtra("correctNum", 0) + "");
		mExamResultWrongNum.setText(intent.getIntExtra("wrongNum", 0) + "");
		mExamResultPercent.setText(mExamResultScore.getText().toString());
		if (intent.getDoubleExtra("score", 0) >= Double.parseDouble("90")) {
			mExamResultTag.setText("通过");
		} else {
			mExamResultTag.setText("未通过");
		}

	}

	private void initListener() {
		mExamResultBack.setOnClickListener(this);
		mExamResultBtn.setOnClickListener(this);

	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.exam_result_back:
				finish();
				break;
			case R.id.exam_result_btn:
				finish();
				break;
		}
	}

	public void GET_DailyTask() {
		KLog.e(Utils.TAG_DEBUG, "GET_DailyTask :");
		RequestParams params = new RequestParams();
		params.put("userid", Utils.SPGetString(this, Utils.userId, ""));
		params.put("type", 2);
		KLog.e(Utils.TAG_DEBUG, params.toString());
		//System.out.println("params : " + params.toString());
		MyHttp.getInstance(this).post(MyHttp.GET_DailyTask, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonObject :");
				KLog.json(Utils.TAG_DEBUG, response.toString());
			}

		});
	}

	@Override
	public void finish() {
		startActivity(new Intent(this, ExamActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
		super.finish();
	}
}
