package com.jzkj.jianzhenkeji_road_car_person.bean;

public class UserLoginResult {

	private String Code;
	private String Count;
	private String Reason;
	private ResultInfo Result;

	public String getCode() {
		return Code;
	}

	public void setCode(String code) {
		Code = code;
	}

	public String getCount() {
		return Count;
	}

	public void setCount(String count) {
		Count = count;
	}

	public String getReason() {
		return Reason;
	}

	public void setReason(String reason) {
		Reason = reason;
	}

	public ResultInfo getResult() {
		return Result;
	}

	public void setResult(ResultInfo result) {
		Result = result;
	}

	@Override
	public String toString() {
		return "UserLoginResult{" +
				"Code='" + Code + '\'' +
				", Count='" + Count + '\'' +
				", Reason='" + Reason + '\'' +
				", Result=" + Result +
				'}';
	}
}
