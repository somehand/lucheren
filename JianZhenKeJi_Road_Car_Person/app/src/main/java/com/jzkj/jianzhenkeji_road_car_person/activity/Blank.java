package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.BaseActivity;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;
import com.umeng.socialize.Config;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.utils.Log;


import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class Blank extends BaseActivity implements View.OnClickListener {
	@InjectView(R.id.headframe_ib)
	ImageButton mback;
	@InjectView(R.id.headframe_title)
	TextView mHeadframeTitle;
	@InjectView(R.id.blank_web)
	WebView mBlankWeb;

	String url; //网址
	String articelId; //文章id
	String title;
	@InjectView(R.id.headframe_layout)
	LinearLayout mHeadframeLayout;

	ImageView collectImg;//收藏的图标
	TextView collectText;//收藏的文字
	boolean isCollected; // 是否收藏过
	PopupWindow popupWindow;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.blank);
		ButterKnife.inject(this);
		mHeadframeLayout.setVisibility(View.VISIBLE);
		initData();
		initListener();
	}

	private void initData() {
		Intent intent = getIntent();
		url = intent.getStringExtra("url");
		title = intent.getStringExtra("title");
		articelId = intent.getStringExtra("articelId");
		mBlankWeb.loadUrl(url);
		mHeadframeTitle.setText(intent.getStringExtra("title"));

		initPopupWindow();
		judgeCollect();
	}

	private void initListener() {
		mback.setOnClickListener(this);
		mHeadframeLayout.setOnClickListener(this);
	}


	/**
	 * 点击监听
	 * @param view
	 */
	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.headframe_ib:
				finish();
				break;
			case R.id.headframe_layout:
				if(popupWindow.isShowing()){
					popupWindow.dismiss();
				}else{
					popupWindow.showAsDropDown(mHeadframeLayout);
				}
				break;
			case R.id.popup_article_collect:
//				System.out.println("点击收藏布局");
				//调用收藏和取消收藏
				if(isCollected){
					//取消收藏
//					CollectCancel();
				}else{
					//收藏
					Collect();
				}
				popupWindow.dismiss();
				break;
			case R.id.popup_article_share:
				initShare();
				popupWindow.dismiss();
				break;
		}
	}

	/**
	 * popupWindow
	 */
	private void initPopupWindow() {
		View view1 = LayoutInflater.from(Blank.this).inflate(R.layout.popup_article_layout, null);
		popupWindow = new PopupWindow(view1, ViewGroup.LayoutParams.WRAP_CONTENT , ViewGroup.LayoutParams.WRAP_CONTENT);
		ViewGroup layoutCollect = (ViewGroup) view1.findViewById(R.id.popup_article_collect);
		ViewGroup layoutShare = (ViewGroup) view1.findViewById(R.id.popup_article_share);
		collectImg = (ImageView) view1.findViewById(R.id.popup_article_img1);
		collectText = (TextView) view1.findViewById(R.id.popup_article_txt);
		layoutCollect.setOnClickListener(Blank.this);
		layoutShare.setOnClickListener(Blank.this);
		popupWindow.setFocusable(true);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
	}



	public void GET_DailyTask(){
		KLog.e(Utils.TAG_DEBUG , "GET_DailyTask :");
		RequestParams params = new RequestParams();
		params.put("userid" , Utils.SPGetString(this , Utils.userId , ""));
		params.put("type" , 3);
		KLog.e(Utils.TAG_DEBUG , params.toString());
		//System.out.println("params : " + params.toString());
		MyHttp.getInstance(this).post(MyHttp.GET_DailyTask , params , new JsonHttpResponseHandler(){
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG , "jsonObject :");
				KLog.json(Utils.TAG_DEBUG ,response.toString());
			}

		});
	}

	/**
	 * 弹出分享界面
	 */
	public void initShare() {
		final SHARE_MEDIA[] displaylist = new SHARE_MEDIA[]{
		SHARE_MEDIA.WEIXIN , SHARE_MEDIA.WEIXIN_CIRCLE , SHARE_MEDIA.QQ ,SHARE_MEDIA.QZONE ,  SHARE_MEDIA.SINA
		};
		/*注意新浪分享的title是不显示的，URL链接只能加在分享文字后显示，并且需要确保withText()不为空
		当同时传递URL参数和图片时，注意确保图片不能超过32K，否则无法分享，不传递URL参数时图片不受32K限制*/
		Log.LOG = true;
		Config.OpenEditor = false;
		Config.IsToastTip = true;
		ProgressDialog dialog = new ProgressDialog(this);
		dialog.setMessage("正在分享中");
		Config.dialog = dialog;
		new ShareAction(this).setDisplayList(displaylist)
				.withText(title)
				.withTitle("路车人")
				.withTargetUrl(url)
//				.withMedia(new UMImage(this, "http://i.ce.cn/ce/xwzx/gnsz/gdxw/201508/07/W020150807558131488790.jpg"))
				.withMedia(new UMImage(this , R.drawable.ic_launcher_big))

				.setListenerList(mumshare,mumshare,mumshare,mumshare,mumshare)
				.open();
	}
	private UMShareListener mumshare = new UMShareListener() {
		@Override
		public void onResult(SHARE_MEDIA share_media) {
			GET_DailyTask();
			Utils.ToastShort(Blank.this, "分享成功");
		}

		@Override
		public void onError(SHARE_MEDIA share_media, Throwable throwable) {
			GET_DailyTask();
			Utils.ToastShort(Blank.this, "分享失败");
		}

		@Override
		public void onCancel(SHARE_MEDIA share_media) {
			Utils.ToastShort(Blank.this, "分享已取消");

		}
	};
	/**
	 * 检查是否收藏
	 */

	public void judgeCollect(){
		RequestParams params = new RequestParams();
		params.put("UserId" , Utils.SPGetString(this , Utils.userId , ""));
		params.put("AppIndexArticleId" , articelId);
		KLog.e(Utils.TAG_DEBUG , params.toString());
		//System.out.println("params : " + params.toString());
		MyHttp.getInstance(this).post(MyHttp.COLLECT_JUDGE , params , new JsonHttpResponseHandler(){
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG , "jsonObject :");
				KLog.json(Utils.TAG_DEBUG ,response.toString());
				try {
					if (response.getInt("Code") == 1) {
						if (response.getString("Reason").equals("已收藏")) {
							collectImg.setImageResource(R.drawable.icon_star_blue);
							collectText.setText("已收藏");
							isCollected = true;
						}else{
							collectImg.setImageResource(R.drawable.icon_star_black);
							collectText.setText("收藏");
							isCollected = false;
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

		});
	}



	/**
	 * 收藏文章
	 */

	public void Collect(){
		System.out.println("Blank.Collect");
		RequestParams params = new RequestParams();
		params.put("UserId" , Utils.SPGetString(this , Utils.userId , ""));
		params.put("AppIndexArticleId" , articelId);
		KLog.e(Utils.TAG_DEBUG , params.toString());
		//System.out.println("params : " + params.toString());
		MyHttp.getInstance(this).post(MyHttp.COLLECT , params , new JsonHttpResponseHandler(){
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG , "jsonObject :");
				KLog.json(Utils.TAG_DEBUG ,response.toString());
				try {
					if (response.getInt("Code") == 1) { //收藏成功
						collectImg.setImageResource(R.drawable.icon_star_blue);
						collectText.setText("已收藏");
						isCollected = true;
						Utils.ToastShort(Blank.this , response.getString("Reason"));
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

		});
	}




}
