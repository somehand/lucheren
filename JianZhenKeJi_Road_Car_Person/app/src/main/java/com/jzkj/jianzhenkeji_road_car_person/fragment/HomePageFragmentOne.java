package com.jzkj.jianzhenkeji_road_car_person.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.activity.ADActivity;
import com.jzkj.jianzhenkeji_road_car_person.activity.ApplyDrivingSchoolActivity;
import com.jzkj.jianzhenkeji_road_car_person.activity.ExamActivity;
import com.jzkj.jianzhenkeji_road_car_person.activity.HomeActivity;
import com.jzkj.jianzhenkeji_road_car_person.activity.LearnCarInsureActivity;
import com.jzkj.jianzhenkeji_road_car_person.activity.MyCoachActivity;
import com.jzkj.jianzhenkeji_road_car_person.activity.MyDrivingHomepageActivity;
import com.jzkj.jianzhenkeji_road_car_person.activity.ScoreStoreActivity;
import com.jzkj.jianzhenkeji_road_car_person.activity.WebPageActivity;

/**
 * Created by Administrator on 2016/5/12 0012.
 */
public class HomePageFragmentOne extends Fragment implements View.OnClickListener, View.OnTouchListener {
	private RelativeLayout rlTrainCar, rlTestQuestion, rlBreakRules, rlChelianwang, rlIntegralStore, rlLookRoom, rlInsure, rlLearnCar;
	private View view;
	private LinearLayout left1;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fragment_home_page_one, container, false);
		rlTrainCar = (RelativeLayout) view.findViewById(R.id.rl_order_train_car);
		rlTestQuestion = (RelativeLayout) view.findViewById(R.id.rl_test_question);
		rlBreakRules = (RelativeLayout) view.findViewById(R.id.rl_break_rules);
		rlChelianwang = (RelativeLayout) view.findViewById(R.id.rl_chelianwang);

		rlIntegralStore = (RelativeLayout) view.findViewById(R.id.rl_integral_store);
		rlLookRoom = (RelativeLayout) view.findViewById(R.id.rl_order_lookroom);
		rlInsure = (RelativeLayout) view.findViewById(R.id.rl_learn_car_insure);
		rlLearnCar = (RelativeLayout) view.findViewById(R.id.rl_apply_learn_car);
		left1 = (LinearLayout) view.findViewById(R.id.left1);
		initListener();
		return view;
	}

	private void initListener() {
		rlTrainCar.setOnClickListener(this);
		rlTestQuestion.setOnClickListener(this);
		rlBreakRules.setOnClickListener(this);
		rlChelianwang.setOnClickListener(this);

		rlIntegralStore.setOnClickListener(this);
		rlLookRoom.setOnClickListener(this);
		rlInsure.setOnClickListener(this);
		rlLearnCar.setOnClickListener(this);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onClick(View arg0) {
		Intent intent;
		switch (arg0.getId()) {
			case R.id.rl_order_train_car://预约练车
				intent = new Intent(getActivity(), ADActivity.class);
				intent.putExtra("class", HomeActivity.class);
				intent.putExtra("item", 1);
				intent.putExtra("type", 1);
				startActivity(intent);
				break;
			case R.id.rl_order_lookroom://预约合场
				intent = new Intent(getActivity(), ADActivity.class);
				intent.putExtra("class", HomeActivity.class);
				intent.putExtra("item", 2);
				intent.putExtra("type", 6);
				startActivity(intent);
				break;
			case R.id.rl_learn_car_insure://学车保险
				intent = new Intent(getActivity(), ADActivity.class);
				intent.putExtra("class", LearnCarInsureActivity.class);
				intent.putExtra("type", 7);
				startActivity(intent);
				break;
			case R.id.rl_apply_learn_car://报名学车
				intent = new Intent(getActivity(), ADActivity.class);
				intent.putExtra("class", ApplyDrivingSchoolActivity.class);
				intent.putExtra("type", 8);
				startActivity(intent);
				break;
			case R.id.rl_test_question://练习试题
				intent = new Intent(getActivity(), ADActivity.class);
				intent.putExtra("class", ExamActivity.class);
				intent.putExtra("type", 2);
				startActivity(intent);
				break;
			case R.id.rl_break_rules://交通违章查询
				intent = new Intent(getActivity(), ADActivity.class);
				intent.putExtra("class", WebPageActivity.class);
				intent.putExtra("type", 3);
//				intent.putExtra("titleName", "交通违章处理");
//				intent.putExtra("url", Utils.ADMap.get("ad3").getIconUrl());
				startActivity(intent);
				break;
			case R.id.home_page_mycoach:
				intent = new Intent(getActivity(), ADActivity.class);
				intent.putExtra("class", MyCoachActivity.class);
				startActivity(intent);
				break;
			case R.id.home_page_drive_school_info:
				intent = new Intent(getActivity(), ADActivity.class);
				intent.putExtra("class", MyDrivingHomepageActivity.class);
				startActivity(intent);
				break;
			case R.id.rl_integral_store://积分商城
				intent = new Intent(getActivity(), ADActivity.class);
				intent.putExtra("class", ScoreStoreActivity.class);
				intent.putExtra("type", 5);
				startActivity(intent);
				break;
			case R.id.rl_chelianwang://车联网
				intent = new Intent(getActivity(), ADActivity.class);
				intent.putExtra("class", WebPageActivity.class);
				intent.putExtra("type",4);
				startActivity(intent);
				break;
			default:
				break;
		}
	}
	int screenWidth;
	int screenHeight;
	int lastX;
	int lastY;
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		int action=event.getAction();
		switch(action){
			case MotionEvent.ACTION_DOWN:
				lastX = (int) event.getRawX();
				lastY = (int) event.getRawY();
				screenWidth = left1.getWidth();
				screenHeight = left1.getHeight();
				Log.i("============","lastX  " + lastX +",\n "
						+ "lastY  " + lastY +",\n "
						+"screenWidth  " + screenWidth +",\n "
						+ "screenHeight   " + screenHeight
						+"\n_root w"+left1.getWidth()
						+"\n_root h"+left1.getHeight());
				break;
			case MotionEvent.ACTION_MOVE:
				int dx =(int)event.getRawX() - lastX;
				int dy =(int)event.getRawY() - lastY;

				int left = v.getLeft() + dx;
				int top = v.getTop() + dy;
				int right = v.getRight() + dx;
				int bottom = v.getBottom() + dy;
				if(left < 0){
					left = 0;
					right = left + v.getWidth();
				}
				if(right > screenWidth){
					right = screenWidth;
					left = right - v.getWidth();
				}
				if(top < 0){
					top = 0;
					bottom = top + v.getHeight();
				}
				if(bottom > screenHeight){
					bottom = screenHeight;
					top = bottom - v.getHeight();
				}
				v.layout(left, top, right, bottom);
				Log.i("@@@@@@", "position  " + left +", " + top + ", " + right + ", " + bottom);
				lastX = (int) event.getRawX();
				lastY = (int) event.getRawY();

				break;
			case MotionEvent.ACTION_UP:
				break;
		}
		return true;
	}
}
