package com.jzkj.jianzhenkeji_road_car_person.adapter;

import android.content.Context;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.bean.IntegralDetailsBean;

/**
 * Created by Administrator on 2016/4/8.
 */
public class IntegralDetailsAdapter extends QuickAdapter<IntegralDetailsBean> {
	public IntegralDetailsAdapter(Context context, int layoutResId) {
		super(context, layoutResId);
	}

	@Override
	protected void convert(BaseAdapterHelper helper, IntegralDetailsBean item) {
		TextView tvName = helper.getView(R.id.integral_lv_item_name);
		TextView tvScore = helper.getView(R.id.integral_lv_item_score);
		TextView tvTime = helper.getView(R.id.integral_lv_item_time);

		if (item.getScore().contains("-")) {
			tvScore.setTextColor(context.getResources().getColor(R.color.color_f1584c));
		} else {
			tvScore.setTextColor(context.getResources().getColor(R.color.color_55f149));
		}
		tvName.setText(item.getTypeDetail());
		tvScore.setText(item.getScore());
		tvTime.setText(item.getTime());
	}
}
