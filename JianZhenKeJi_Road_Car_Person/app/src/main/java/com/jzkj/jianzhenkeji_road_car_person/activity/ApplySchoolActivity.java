package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ApplySchoolActivity extends Activity {
	@InjectView(R.id.headframe_title)
	TextView title;
	@InjectView(R.id.username)
	TextView username;
	@InjectView(R.id.schoolname)
	TextView schoolname;
	@InjectView(R.id.phone)
	TextView phone;
	@InjectView(R.id.iDCardNum)
	TextView iDCardNum;
	@InjectView(R.id.type)
	TextView type;
	@InjectView(R.id.pay)
	Button pay;
	@InjectView(R.id.headframe_ib)
	ImageButton mHeadframeIb;
	@InjectView(R.id.text_remark)
	EditText remark;
	@InjectView(R.id.rl_type)
	RelativeLayout rlType;

	private PopupWindow popupWindow;
	private TextView tvPopText1;
	private TextView tvPopText2;
	private TextView tvPopTextLine;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_apply_school);
		ButterKnife.inject(this);
		initViews();
		initListener();
	}

	private void initListener() {
		mHeadframeIb.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});
		pay.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				applyDrivingSchool();
			}
		});
		rlType.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (popupWindow.isShowing()) {
					popupWindow.dismiss();
				} else {
					popupWindow.showAsDropDown(rlType, 630, 3);
				}
			}
		});
	}

	public void initViews() {
		title.setText("报名" + getIntent().getStringExtra("schoolname"));
		schoolname.setText(getIntent().getStringExtra("schoolname"));
		type.setText(Utils.SPGetString(ApplySchoolActivity.this, Utils.DrivingType, ""));
		username.setText(Utils.SPGetString(ApplySchoolActivity.this, Utils.realName, ""));
		phone.setText(Utils.SPGetString(ApplySchoolActivity.this, Utils.userName, ""));
		iDCardNum.setText(Utils.SPGetString(ApplySchoolActivity.this, Utils.idCarNumber, ""));
		setPopupWindow();
	}

	private void applyDrivingSchool() {
		RequestParams params = new RequestParams();
		params.put("DrivingSchoolId", getIntent().getStringExtra("DrivingSchoolId"));
		params.put("Userid", Utils.SPGetString(ApplySchoolActivity.this, Utils.userId, ""));
		if ("C1".equals(type.getText().toString())) {
			params.put("Gear", 1);//课程 （1.C1   2.C2）
		} else {
			params.put("Gear", 2);//课程 （1.C1   2.C2）
		}
		params.put("Content", remark.getText().toString());//学员对驾校的备注
		params.put("PhoneType", 2);//手机类型（1.IOS，2.安卓）
		params.put("PayChannel", 1);//类型int	//支付渠道（1.支付宝）现在只有支付宝
		KLog.e("params", params.toString());
		MyHttp.getInstance(ApplySchoolActivity.this).post(MyHttp.WRITE_APPLY_INFO, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
				try {
					Utils.SPutString(ApplySchoolActivity.this, Utils.payUrl, response.getString("Reason"));
					JSONArray array = response.getJSONArray("Result");
					JSONObject object = array.getJSONObject(0);
					Utils.SPutString(ApplySchoolActivity.this, Utils.orderCode, object.getString("OrderCode"));
					Utils.SPutString(ApplySchoolActivity.this, Utils.orderCodeId, object.getString("OrderId"));
					Utils.SPutString(ApplySchoolActivity.this, Utils.createtime, object.getString("createtime"));
					Intent intent = new Intent(ApplySchoolActivity.this, PayPageActivity.class);
					intent.putExtra("price", object.getString("Money"));
					intent.putExtra("orderName", "报名-" + schoolname.getText().toString());
					intent.putExtra("tag", 1);
					intent.putExtra("type", 2);
					intent.putExtra("DrivingSchoolId", getIntent().getStringExtra("DrivingSchoolId"));
					intent.putExtra("schoolname", getIntent().getStringExtra("schoolname"));
					startActivity(intent);
//					MyAlipayManager.getInstance(ApplySchoolActivity.this, mMyPayCompleteListener).setPayType(0).pay("路车人", "支付", object.getString("Money"));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
			}
		});
	}

	/*MyPayCompleteListener mMyPayCompleteListener = new MyPayCompleteListener() {
		@Override
		public void onPayComplete(int payTypeId) {
			Utils.SPutString(ApplySchoolActivity.this, Utils.schoolId, getIntent().getStringExtra("DrivingSchoolId"));
			Utils.SPutString(ApplySchoolActivity.this, Utils.schoolName, getIntent().getStringExtra("schoolname"));
			Intent intent = new Intent(ApplySchoolActivity.this, PaySuccessActivity.class);
			intent.putExtra("PAY_TYPE", payTypeId);
			intent.putExtra("type", 2);//支付成功后只显示订单编号和支付方式
			startActivity(intent);
			System.out.println(Utils.SPGetString(ApplySchoolActivity.this, Utils.schoolId, getIntent().getStringExtra("DrivingSchoolId")) + "id");
			System.out.println(Utils.SPGetString(ApplySchoolActivity.this, Utils.schoolName, getIntent().getStringExtra("schoolname")) + "name");
			finish();
		}
	};*/

	@Override
	protected void onDestroy() {
		super.onDestroy();
		ButterKnife.reset(this);
	}

	/**
	 * 设置PopupWindow的方法
	 */
	private void setPopupWindow() {
		LayoutInflater inflater = LayoutInflater.from(this);
		View view = inflater.inflate(R.layout.simulation_exam_popwindow, null);
		tvPopText1 = (TextView) view.findViewById(R.id.simulation_exam_poptext1);
		tvPopText2 = (TextView) view.findViewById(R.id.simulation_exam_poptext2);
		tvPopTextLine = (TextView) view.findViewById(R.id.simulation_exam_line);
		tvPopText1.setText("C1");
		tvPopText2.setText("C2");
		popupWindow = new PopupWindow(view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		popupWindow.setFocusable(true);
		popupWindow.setOutsideTouchable(true);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());

		//监听
		tvPopText1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				type.setText(tvPopText1.getText().toString());
				popupWindow.dismiss();
			}
		});
		tvPopText2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				type.setText(tvPopText2.getText().toString());
				popupWindow.dismiss();
			}
		});
	}
}
