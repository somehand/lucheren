package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.bean.ExamBean;
import com.jzkj.jianzhenkeji_road_car_person.fragment.ExamDetailFragment;
import com.jzkj.jianzhenkeji_road_car_person.network.HttpUtils;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ExamDetailsActivity extends FragmentActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {

	@InjectView(R.id.exam_activity_currentNum)
	TextView mExamActivityCurrentNum;
	@InjectView(R.id.exam_activity_totalNum)
	TextView mExamActivityTotalNum;
	@InjectView(R.id.exam_activity_undo)
	LinearLayout mExamActivityUndo;
	@InjectView(R.id.exam_activity_commit)
	LinearLayout mExamActivityCommit;
	@InjectView(R.id.exam_activity_time)
	TextView mExamActivityTime;
	@InjectView(R.id.exam_activity_previous)
	LinearLayout mExamActivityPrevious;
	@InjectView(R.id.exam_activity_next)
	LinearLayout mExamActivityNext;
	private ViewPager vp;
	private ImageButton ibBack;//返回
	private SparseArray<ExamDetailFragment> array = new SparseArray<ExamDetailFragment>();

	MypageAdapter adapter;
	private ArrayList<ExamBean> examArray;
	private ArrayList<Integer> AnsweredList; // 已作答
	private ArrayList<Integer> WrongAnswerList; //回答错误

	int totalNum; // 总题数

	int hour = 45;
	int minute = 0;

	Timer mTimer;
	ProgressDialog mProgressDialog;

/*
	public static final int VIEWSINGLE = 0;
	public static final int VIEWMULTIPLE = 1;
	public static final int VIEWJUBGE = 2;
*/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.simulation_exam_detais);
		ButterKnife.inject(this);
		examArray = new ArrayList<ExamBean>();
		AnsweredList = new ArrayList<Integer>();
		WrongAnswerList = new ArrayList<Integer>();
		initView();
		initData();
		initListener();
		EventBus.getDefault().register(this);
	}

	private void initListener() {
		//返回的监听
		ibBack.setOnClickListener(this);
		vp.setOnPageChangeListener(this);
		mExamActivityUndo.setOnClickListener(this);
		mExamActivityCommit.setOnClickListener(this);
		mExamActivityPrevious.setOnClickListener(this);
		mExamActivityNext.setOnClickListener(this);
	}

	public void initView() {
		vp = (ViewPager) findViewById(R.id.exam_pager);
		/**
		 * 解决选题后不恢复页面的问题
		 */
		vp.setOffscreenPageLimit(50);
		ibBack = (ImageButton) findViewById(R.id.simulation_exam_details_ibback);
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("正在加载中...");
		mProgressDialog.setCanceledOnTouchOutside(true);

	/*	ImageSpan span = new ImageSpan(this, R.drawable.single_selection);
		SpannableString spannableString = new SpannableString("涉嫌酒后驾车，醉酒后驾车罚款（），记（）分");
		spannableString.setSpan(span, 0, 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
		tvExamProblem.setText(spannableString);
		*/
		String textImg = "<img src=\"" + R.drawable.single_selection + "\" />" + " 涉嫌酒后驾车，醉酒后驾车罚款（），记（）分";
		//tvExamProblem.setText(Html.fromHtml(textImg, imageGetter, null));
	}

	/**
	 * 获取模拟题
	 */
	public void initData() {
		String url;
		if (getIntent().getIntExtra("class", 1) == 1) {
			url = MyHttp.QUESTION_CLASS_1;
		} else {
			url = MyHttp.QUESTION_CLASS_4;
		}
		mProgressDialog.show();
		if (HttpUtils.checkNetworkAvailable(ExamDetailsActivity.this) == false) {
			mProgressDialog.cancel();
			Utils.ToastShort(ExamDetailsActivity.this, "网络异常，请检查...");
		}
		MyHttp.getInstance(this).post(url, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
				try {
					if (statusCode == 200 && response.getInt("error_code") == 0) {
						JSONArray jsonArray = response.getJSONArray("result");
						Gson gson = new Gson();
						examArray = gson.fromJson(jsonArray.toString(), new TypeToken<List<ExamBean>>() {
						}.getType());
//						adapter = new MypageAdapter(getSupportFragmentManager());
//						vp.setAdapter(adapter);

						totalNum = examArray.size();
						mExamActivityTotalNum.post(new Runnable() {
							@Override
							public void run() {
								mExamActivityTotalNum.setText(totalNum + "");
							}
						});

						mTimer = new Timer();
						mTimer.schedule(new TimerTask() {
							@Override
							public void run() {
								mHandler.sendEmptyMessage(0);
							}
						}, 0, 1000);

						for (int i = 0; i < examArray.size(); i++) {
							ExamDetailFragment examDetailFragment = new ExamDetailFragment();
							Bundle bundle = new Bundle();
							bundle.putInt("index", i);
							bundle.putInt("class", getIntent().getIntExtra("class", 1));
							bundle.putSerializable("item", examArray.get(i));
							examDetailFragment.setArguments(bundle);

							//实现监听, 添加或删除 未作答列表 和 错误列表;
							examDetailFragment.setAnswerCompleteListener(new ExamDetailFragment.OnAnswerCompleteListener() {
								@Override
								public void onAnswerComplete(int index, int answer, boolean isAnswered, boolean isCorrect) {
									if (isAnswered) {
										if (!AnsweredList.contains(index)) {
											AnsweredList.add(index);
										}
									} else {
										if (AnsweredList.contains(index)) {
											AnsweredList.remove(AnsweredList.indexOf(index));
										}
									}

									if (isCorrect == false) {
										if (!WrongAnswerList.contains(index)) {
											WrongAnswerList.add(index);
										}
									} else {
										if (WrongAnswerList.contains(index)) {
											WrongAnswerList.remove(WrongAnswerList.indexOf(index));
										}
									}
								}
							});
							array.put(i, examDetailFragment);

						}
						adapter = new MypageAdapter(getSupportFragmentManager());
						vp.setAdapter(adapter);
						mProgressDialog.cancel();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * 显示计时
	 */
	Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			minute--;
			if (minute < 0) {
				hour--;
				minute = 59;
			}

			if (hour == 0 && minute == 0) {
				commitAnswer();
			}
			DecimalFormat decimalFormat = new DecimalFormat("00");
			String hourString = decimalFormat.format(hour);
			String minuteString = decimalFormat.format(minute);
			mExamActivityTime.setText(hourString + " : " + minuteString);
		}
	};

	/**
	 * 点击监听
	 *
	 * @param view
	 */
	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.simulation_exam_details_ibback: //返回
				final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
				alertDialog.setMessage("答案未提交, 是否退出?");
				alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "确认", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						alertDialog.dismiss();
						finish();
					}
				});
				alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						alertDialog.dismiss();
					}
				});
				alertDialog.show();
				break;
			case R.id.exam_activity_undo: //未做
				Intent intent = new Intent(this, NotDoneActivity.class);
				intent.putExtra("list", AnsweredList);
				intent.putExtra("num", examArray.size());
				startActivityForResult(intent, 0x1);

				break;
			case R.id.exam_activity_commit: //提交
				final AlertDialog dialog = new AlertDialog.Builder(this).create();
				dialog.setMessage("是否确定提交?");
				dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						dialog.dismiss();
					}
				});
				dialog.setButton(DialogInterface.BUTTON_POSITIVE, "确定", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						commitAnswer();
					}
				});
				dialog.show();


				break;
			case R.id.exam_activity_previous://上一题
				if (vp.getCurrentItem() != 0) {

					vp.setCurrentItem(vp.getCurrentItem() - 1, true);
				} else {
					Utils.ToastShort(this, "当前已经是第一题");
				}
				break;
			case R.id.exam_activity_next://下一题
				if (vp.getCurrentItem() != totalNum - 1) {
					vp.setCurrentItem(vp.getCurrentItem() + 1, true);
				} else {
					Utils.ToastShort(this, "当前已经是最后一题");
				}

				break;

			default:
				break;
		}
	}

	@Override
	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

	}

	@Override
	public void onPageSelected(int position) {
		mExamActivityCurrentNum.setText(++position + "");

	}

	@Override
	public void onPageScrollStateChanged(int state) {

	}

	/**
	 * 提交答案
	 */
	public void commitAnswer() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");
		String date = format.format(new Date());
		int wrongNum = WrongAnswerList.size();
		int correctNum = AnsweredList.size() - wrongNum;
		correctNum = correctNum < 0 ? 0 : correctNum;
		KLog.e(correctNum + " --------------- ");
		double a = correctNum * 1.00;
		double score = 100 * (a / (double) examArray.size());
		KLog.e(Utils.TAG_DEBUG, score + "");
		Intent intent1 = new Intent(ExamDetailsActivity.this, ExamResultActivity.class);
		intent1.putExtra("date", date);
		int minute2;
		int hour2;
		if (minute != 0) {
			minute2 = 60 - minute;
			hour2 = 45 - hour - 1;
		} else {
			minute2 = 0;
			hour2 = 45 - hour;
		}

		DecimalFormat decimalFormat = new DecimalFormat("00");
		String minuteString = decimalFormat.format(minute2);
		String hourString = decimalFormat.format(hour2);
		intent1.putExtra("time", hourString + " : " + minuteString);
		intent1.putExtra("score", score);
		intent1.putExtra("total", AnsweredList.size());
		intent1.putExtra("wrongNum", wrongNum);
		intent1.putExtra("correctNum", correctNum);
		startActivity(intent1);
	}

	/**
	 * viewpager 适配器
	 */
	private class MypageAdapter extends FragmentStatePagerAdapter {

		public MypageAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int arg0) {
//			return fragments[arg0];

//			return examDetailFragment;
			return array.get(arg0);
		}

		@Override
		public int getCount() {

			return examArray.size();
		}

		@Override
		public Object instantiateItem(View container, int position) {
			//array.put(position, new ExamDetailFragment());
			return super.instantiateItem(container, position);
		}

		@Override
		public void destroyItem(View container, int position, Object object) {
			//array.remove(position);
			super.destroyItem(container, position, object);
		}

		/*@Override
		public int getItemPosition(Object object) {
			return POSITION_NONE;
		}*/


	}


	/**
	 * 在未做页面中选题后
	 *
	 * @param requestCode
	 * @param resultCode
	 * @param data
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 0x1 && resultCode == RESULT_OK) {
			int index = data.getIntExtra("index", vp.getCurrentItem());
			vp.setCurrentItem(index, true);
		}
	}

	/**
	 * 返回键
	 *
	 * @param keyCode
	 * @param event
	 * @return
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setMessage("答案未提交, 是否退出?");
			alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "确认", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					alertDialog.dismiss();
					finish();
				}
			});
			alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					alertDialog.dismiss();
				}
			});
			alertDialog.show();
		}
		return false;
	}

	@Override
	protected void onDestroy() {
		EventBus.getDefault().unregister(this);
		super.onDestroy();
	}

	/**
	 * 多选题目特有按钮点击之后  /  单选答题之后 换下一页
	 *
	 * @param click
	 */
	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMutilBtnClick(ExamDetailFragment.MutilBtnClick click) {
		//KLog.e(Utils.TAG_DEBUG , "answerlist.size :" + AnsweredList.size() + "  examarray : " + examArray.size());

		if (vp.getCurrentItem() != examArray.size() - 1) {
			vp.setCurrentItem(vp.getCurrentItem() + 1, true);
		} else if (AnsweredList.size() == examArray.size()) {
			//Utils.ToastShort(this , "答完所有题");
			final AlertDialog dialog = new AlertDialog.Builder(this).create();
			dialog.setMessage("题目全部答完, 是否交卷?");
			dialog.setButton(DialogInterface.BUTTON_POSITIVE, "确定", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					commitAnswer();
				}
			});
			dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					dialog.dismiss();
				}
			});
			dialog.show();
		}
	}
}
