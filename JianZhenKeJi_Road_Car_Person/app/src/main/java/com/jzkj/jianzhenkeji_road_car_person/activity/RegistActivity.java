package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.BaseActivity;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

public class RegistActivity extends BaseActivity implements OnClickListener {

	private EditText etMobile, etCode, etPassword, etSurePassword, etRequestCode;
	private TextView tvGainCode, tvTitle, tvUserAgreement;
	private Button btnNext;
	private ImageButton ibLeft;
	private int code;
	private String pw;
	private String mobile;
	private boolean flag = false;
	private CheckBox agree;
	private TextView yhxy;

	private Timer timer;
	private TimerTask mTask;
	MyCount count = new MyCount(60000, 1000);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.regist);
		init();
		bindListener();

	}

	/**
	 * 设置监听
	 */
	private void bindListener() {
		btnNext.setOnClickListener(this);
		tvGainCode.setOnClickListener(this);
		ibLeft.setOnClickListener(this);
		yhxy.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(RegistActivity.this,WebPageActivity.class);
				intent.putExtra("type",3);
				intent.putExtra("url",MyHttp.USER_AGREEMENT);
				intent.putExtra("titleName","用户协议");
				startActivity(intent);
			}
		});
	}

	/**
	 * 初始化绑定id
	 */
	private void init() {
		etCode = (EditText) findViewById(R.id.regist_etcode);
		etMobile = (EditText) findViewById(R.id.regist_etmobile);
		agree = (CheckBox) findViewById(R.id.agree);
		yhxy = (TextView) findViewById(R.id.yhxy);
		etPassword = (EditText) findViewById(R.id.regist_etpasswword);
		etSurePassword = (EditText) findViewById(R.id.regist_etsurepassword);
		etRequestCode = (EditText) findViewById(R.id.regist_etrequest_code);
		tvGainCode = (TextView) findViewById(R.id.regist_gaincode);
		tvTitle = (TextView) findViewById(R.id.headframe_title);
		btnNext = (Button) findViewById(R.id.regist_regist);
		ibLeft = (ImageButton) findViewById(R.id.headframe_ib);
		tvTitle.setVisibility(View.VISIBLE);
		ibLeft.setVisibility(View.VISIBLE);
		pw = etPassword.getText().toString().trim();
	}

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
			case R.id.regist_gaincode://获取验证码
				System.out.println(etMobile.getText().toString().trim());
				if (!TextUtils.isEmpty(etMobile.getText().toString().trim())) {
					getCode();
					count.start();
				} else {
					Utils.ToastShort(this, "请输入手机号");
				}
				break;
			case R.id.regist_regist://注册
				if (etCode.getText().toString().trim().length() == 0) {
					Utils.ToastShort(this, "请输入验证码");
				} else if (!etCode.getText().toString().trim().equals(String.valueOf(code))) {
					Utils.ToastShort(this, "验证码错误, 请重新获取");
				} else if (!agree.isChecked()) {
					Utils.ToastShort(this, "请同意用户协议");
				} else {
					Intent intent = new Intent(RegistActivity.this, BaseInformationActivity.class);
					intent.putExtra("username", etMobile.getText().toString().trim());
					intent.putExtra("password", etPassword.getText().toString().trim());
					intent.putExtra("code", etRequestCode.getText().toString().trim());
					startActivity(intent);
				}
				break;
			case R.id.headframe_ib://返回
				finish();
				break;

			default:
				break;
		}

	}

	/**
	 * 获取验证码
	 */
	public void getCode() {
		RequestParams params = new RequestParams();
		params.put("Mobile", etMobile.getText().toString().trim());
		MyHttp.getInstance(this).post(MyHttp.GET_CODE, params, new JsonHttpResponseHandler() {

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonObject :");
				KLog.json(Utils.TAG_DEBUG, response.toString());
				try {
					if (response.getInt("Code") == 1) {
						code = response.getInt("Result");
						Utils.ToastShort(RegistActivity.this, response.getString("Reason"));
					} else {
						count.cancel();
						count.onFinish();
						Utils.ToastShort(RegistActivity.this, response.getString("Reason"));
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}

			}
		});
	}


	class MyCount extends CountDownTimer {

		public MyCount(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}

		@Override
		public void onTick(long l) {
			tvGainCode.setText((l / 1000) + "s");
			tvGainCode.setTextColor(getResources().getColor(R.color.regist_hintcolor));
			tvGainCode.setEnabled(false);
		}

		@Override
		public void onFinish() {
			tvGainCode.setText("获取验证码");
			tvGainCode.setEnabled(true);
			tvGainCode.setTextColor(getResources().getColor(R.color.regist_gaincodecolor));
		}
	}
}
