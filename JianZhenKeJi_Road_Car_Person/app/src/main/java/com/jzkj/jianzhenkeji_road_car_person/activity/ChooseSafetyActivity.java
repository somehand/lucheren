package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jzkj.jianzhenkeji_road_car_person.BaseActivity;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.adapter.ChooseSafetyLVAdapter;
import com.jzkj.jianzhenkeji_road_car_person.bean.ChooseSafetyDetails;
import com.jzkj.jianzhenkeji_road_car_person.bean.SecurityBean;
import com.jzkj.jianzhenkeji_road_car_person.bean.SecurityOfficerInfo;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.kanak.emptylayout.EmptyLayout;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.OnClick;

public class ChooseSafetyActivity extends BaseActivity {

	private ImageButton ibBack;
	private TextView tvTitle;
	private ListView lv;
	private ArrayList<ChooseSafetyDetails> list;
	private Context context;
	ChooseSafetyLVAdapter adapter;
	EmptyLayout mEmptyLayout;

	private ArrayList<SecurityOfficerInfo> safetyInfo;
	private ArrayList<SecurityBean> securitys =null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choose_safety);
		init();
		adapter = new ChooseSafetyLVAdapter(this, R.layout.choose_safety_item);
		lv.setAdapter(adapter);

	}

	private void init() {
		ibBack = (ImageButton) findViewById(R.id.headframe_ib);
		tvTitle = (TextView) findViewById(R.id.headframe_title);
		lv = (ListView) findViewById(R.id.choose_safety_lv);
		tvTitle.setText("选择安全员");
		ibBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});
		context = ChooseSafetyActivity.this;
		initEmptyLayout();
//		getSafetyInfo();
		get_Securitys(getIntent().getStringExtra("examinationId"));
	}

	private void initEmptyLayout() {
		mEmptyLayout = new EmptyLayout(this,lv);
		//对应自定义布局中的textview的id
		mEmptyLayout.setEmptyMessage("对不起,暂时没有安全员可供选择。", R.id.text);
		ViewGroup group = (ViewGroup) LayoutInflater.from(this).inflate(R.layout.layout_no_data, null);
		/*ImageView imageView = (ImageView) group.findViewById(R.id.img);
		imageView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				isRefreshing = true;
				initData();
			}
		});*/
		mEmptyLayout.setEmptyView(group);
		mEmptyLayout.setShowEmptyButton(false);
	}

	@OnClick(R.id.headframe_ib)
	public void back(){
		finish();
	}

	/**
	 * 获取安全员列表(old)
	 */
	private void getSafetyInfo() {
		RequestParams params = new RequestParams();
		params.put("ExaminationId", Utils.SPGetInt(this , Utils.currentExamRoomId , 1));
		KLog.e(Utils.TAG_DEBUG , params.toString());
		MyHttp.getInstance(context).post(MyHttp.GET_SAFETY_INFO,params,new JsonHttpResponseHandler(){
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json("JSONObject"+response.toString());
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json("JSONArray"+response.toString());
				Log.e("JSONArray_length",response.length()+"");
				Gson gson = new Gson();
				safetyInfo = gson.fromJson(response.toString(), new TypeToken<List<SecurityOfficerInfo>>() {
				}.getType());
//				adapter.addAll(safetyInfo);

			}
		});
	}
	//得到考场的安全员信息
	public void get_Securitys(final String eId){
		RequestParams params = new RequestParams();
		params.put("ExaminationId",eId);
		MyHttp.getInstance(ChooseSafetyActivity.this).post(MyHttp.GET_SECURITY_INFO,params,new JsonHttpResponseHandler(){
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				try {
					String code = response.getString("Code");
					KLog.json(response.toString());
					if("1".equals(code)){
						securitys = new ArrayList<SecurityBean>();
						JSONArray array = response.getJSONArray("Result"); // 集合转为json
						for (int i=0;i<array.length();i++){
							JSONObject jsonObject = (JSONObject) array.get(i);
							SecurityBean secBean = new SecurityBean();
							secBean.setIDCardNumber(jsonObject.getString("IDCardNumber"));
							secBean.setIntroduce(jsonObject.getString("Introduce"));
							secBean.setLogoURL(jsonObject.getString("LogoURL"));
							secBean.setMobile(jsonObject.getString("Mobile"));
							secBean.setOfficerName(jsonObject.getString("OfficerName"));
							secBean.setSecurityOfficerId(jsonObject.getString("SecurityOfficerId"));
							secBean.setSignature(jsonObject.getString("Signature"));
							securitys.add(secBean);
						}
						adapter.addAll(securitys);
					}else {
						mEmptyLayout.showEmpty();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				mEmptyLayout.showEmpty();
			}
		});
	}

}
