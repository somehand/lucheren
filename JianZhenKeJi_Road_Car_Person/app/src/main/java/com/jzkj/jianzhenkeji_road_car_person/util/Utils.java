package com.jzkj.jianzhenkeji_road_car_person.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.jzkj.jianzhenkeji_road_car_person.bean.ADBean;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Administrator on 2015/9/10.
 */
public class Utils {
	public static final String TAG_DEBUG = "debug";

	public static final String isLogin = "isLogin";
	public static final String userId = "userId";
	public static final String userName = "userName";
	public static final String integral = "integral";
	public static final String passWord = "passWord";
	public static final String nickName = "nickName";
	public static final String realName = "realName";
	public static final String idCarNumber = "idCarNumber";
	public static final String userAddress = "userAddress";
	public static final String userSex = "userSex";
	public static final String schoolId = "schoolId";
	public static final String schoolName = "schoolName";
	public static final String coachId = "coachId";
	public static final String coachLogo = "coachLogo";
	public static final String userLogo = "userLogo";
	public static final String currentExamRoomId = "currentExamRoomId";
	public static final String currentExamRoomName = "currentExamRoomName";
	public static final String carType = "carType";
	public static final String yearAndDate = "yearAndDate";
	public static final String orderTime = "orderTime";
	public static final String pushId = "pushId";
	public static final String SchoolUrl = "SchoolUrl";
	public static final String payCode = "payCode";
	public static final String CoachName = "CoachName";
	public static final String DrivingType = "DrivingType";
	public static final String CoachRealName = "CoachRealName";
	public static final String noUpdate = "noUpdate";
	public static final String tips_1_Showed = "tips_1_Showed";
	public static final String tips_2_Showed = "tips_2_Showed";
	public static final String tips_3_Showed = "tips_3_Showed";
	public static final String tips_4_Showed = "tips_4_Showed";

	public static final String TeachType = "TeachType";//TeachType  string  //基础训练学员or 已预约考试学员

	public static final String orderCode = "OrderCode";//支付订单
	public static final String orderCodeId = "OrderCodeId";//支付订单id
	public static final String createtime = "createTime";//创建时间
	public static final String payUrl = "PayUrl";//回调

	public static final String IsVip = "IsVip";//1vip  2非vip

	//所有需要跳到支付界面的
	public static final String toPayPageid_1 = "toPayPageid_1";//预约合场
	public static final int toPayPageid_2 = 2;//积分充值
	public static final String toPayPageid_3 = "toPayPageid_3";//合场记录



	//private static  LocationManagerProxy mLocationManagerProxy;

	public static boolean isPhoneNumber(String input) {
		String regex = "^1[3|4|5|8|7]{1}[0-9]{9}$";
		//  这里的1是代表，手机号码要以1开头，因为中国目前还没有以其他数字开头的手机号，[3|4|5|8]是表示
		// 在1的后面可能是3,4,5或者8，比如：13/14/15/18等，[0-9]表示第三位数字可以是0到9的任意整数。
		// \d{8}的意思是0到9的整数长度为8！
		Pattern p = Pattern.compile(regex);
		Matcher matcher = p.matcher(input);
		return matcher.matches();
	}

	public static boolean isAllNumber(String input) {
		String regex = "^[0-9]+$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(input);
		return matcher.matches();
	}

	public static boolean isTel(String input) {
		String regex = "^(\\d{11}|((\\d{3}|\\d{4})-(\\d{7}|\\d{8}))|((\\d{3}|\\d{4})-(\\d{3}|\\d{4})-(\\d{3}|\\d{4})))$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(input);
		return matcher.matches();
	}

	public static boolean isUrl(String input) {
		String regex = "^(http|https|ftp)\\://([a-zA-Z0-9\\.\\-]+(\\:[a-zA-Z0-9\\.&amp;%\\$\\-]+)*@)?((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\\-]+\\.)*[a-zA-Z0-9\\-]+\\.[a-zA-Z]{2,4})(\\:[0-9]+)?(/[^/][a-zA-Z0-9\\.\\,\\?\\'\\\\/\\+&amp;%\\$#\\=~_\\-@]*)*$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(input);
		return matcher.matches();
	}

	/**
	 * @param str
	 * @return是否是邮箱:是为true，否则false
	 */
	public static Boolean isEmail(String str) {
		Boolean isEmail = false;
		String expr = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
		if (str.matches(expr)) {
			isEmail = true;
		}
		return isEmail;
	}

	public static Map<String, ADBean> ADMap = new HashMap<String, ADBean>();


	public static void ToastShort(Context context, String content) {
		Toast.makeText(context, content, Toast.LENGTH_SHORT).show();
	}


	public static void SPutString(Context context, String key, String value) {
		SharedPreferences sharedPreferences = context.getSharedPreferences("UserData", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public static void SPPutInt(Context context, String key, int value) {
		SharedPreferences sharedPreferences = context.getSharedPreferences("UserData", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putInt(key, value);
		editor.commit();
	}

	public static void SPPutBoolean(Context context, String key, boolean value) {
		SharedPreferences sharedPreferences = context.getSharedPreferences("UserData", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}

	public static String SPGetString(Context context, String key, String defaultValue) {
		SharedPreferences sharedPreferences = context.getSharedPreferences("UserData", Context.MODE_PRIVATE);
		return sharedPreferences.getString(key, defaultValue);
	}

	public static int SPGetInt(Context context, String key, int defaultValue) {
		SharedPreferences sharedPreferences = context.getSharedPreferences("UserData", Context.MODE_PRIVATE);
		return sharedPreferences.getInt(key, defaultValue);
	}

	public static boolean SPGetBoolean(Context context, String key, boolean defaultValue) {
		SharedPreferences sharedPreferences = context.getSharedPreferences("UserData", Context.MODE_PRIVATE);
		return sharedPreferences.getBoolean(key, defaultValue);
	}


}
