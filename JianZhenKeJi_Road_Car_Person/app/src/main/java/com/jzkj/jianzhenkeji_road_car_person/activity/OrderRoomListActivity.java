package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.BaseActivity;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.adapter.OrderRoomDetailsLVAdapter;
import com.jzkj.jianzhenkeji_road_car_person.bean.OrderRoomDetailsBean;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016/2/24 0024.
 */
public class OrderRoomListActivity extends BaseActivity {
	private ImageButton ibBack;
	private TextView tvTitle;
	private ListView lv;
	OrderRoomDetailsLVAdapter adapter;
	private ArrayList<OrderRoomDetailsBean> list;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_order_room_list);
		init();

		list = new ArrayList<OrderRoomDetailsBean>();

		for (int i = 0; i < 5; i++) {
			OrderRoomDetailsBean roomDetailsBean = new OrderRoomDetailsBean();
			roomDetailsBean.setTime("00:00 - 24:00");
			roomDetailsBean.setClass1("(科目一 科目二)");
			roomDetailsBean.setName("第一考场");
			roomDetailsBean.setOrderNum(100);
			roomDetailsBean.setTotalNum(1000);
			roomDetailsBean.setEnable(true);
			list.add(roomDetailsBean);
		}
		 adapter = new OrderRoomDetailsLVAdapter(this,R.layout.order_room_details_lv_item);
		lv.setAdapter(adapter);
		adapter.addAll(list);

	}

	private void init() {
		ibBack = (ImageButton) findViewById(R.id.headframe_ib);
		tvTitle = (TextView) findViewById(R.id.headframe_title);
		lv = (ListView) findViewById(R.id.order_room_details_lv);
		tvTitle.setText("2016/01/29");

		ibBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});
	}
}
