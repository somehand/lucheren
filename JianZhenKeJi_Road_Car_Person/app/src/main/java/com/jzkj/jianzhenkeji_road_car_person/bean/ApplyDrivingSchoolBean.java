package com.jzkj.jianzhenkeji_road_car_person.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/5/12 0012.
 */
public class ApplyDrivingSchoolBean implements Serializable {
	private String SchoolUrl;
	private String SchoolLogo;
	private String SchoolAddress;
	private String SchoolName;
	private String Price;
	private String SchoolProfiles;
	private String SchoolPhone;
	private String DrivingSchoolId;
	private int star;
	private int IsRecommend;

	public ApplyDrivingSchoolBean() {
	}

	public String getDrivingSchoolId() {
		return DrivingSchoolId;
	}

	public void setDrivingSchoolId(String drivingSchoolId) {
		DrivingSchoolId = drivingSchoolId;
	}

	public String getSchoolPhone() {
		return SchoolPhone;
	}

	public void setSchoolPhone(String schoolPhone) {
		SchoolPhone = schoolPhone;
	}

	public String getSchoolProfiles() {
		return SchoolProfiles;
	}

	public void setSchoolProfiles(String schoolProfiles) {
		SchoolProfiles = schoolProfiles;
	}

	public int getIsRecommend() {
		return IsRecommend;
	}

	public void setIsRecommend(int isRecommend) {
		IsRecommend = isRecommend;
	}

	public String getSchoolLogo() {
		return SchoolLogo;
	}

	public void setSchoolLogo(String schoolLogo) {
		SchoolLogo = schoolLogo;
	}

	public String getSchoolUrl() {
		return SchoolUrl;
	}

	public void setSchoolUrl(String schoolUrl) {
		SchoolUrl = schoolUrl;
	}

	public String getSchoolAddress() {
		return SchoolAddress;
	}

	public void setSchoolAddress(String schoolAddress) {
		SchoolAddress = schoolAddress;
	}

	public String getSchoolName() {
		return SchoolName;
	}

	public void setSchoolName(String schoolName) {
		SchoolName = schoolName;
	}

	public String getPrice() {
		return Price;
	}

	public void setPrice(String price) {
		Price = price;
	}

	public int getStar() {
		return star;
	}

	public void setStar(int star) {
		this.star = star;
	}
}
