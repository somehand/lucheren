package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.adapter.ChooseCoachAdapter;
import com.jzkj.jianzhenkeji_road_car_person.bean.ChooseCoachBean;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Administrator on 2016/5/13 0013.
 */
public class ChooseCoachActivity extends Activity {
	@InjectView(R.id.headframe_ib)
	ImageButton mHeadframeIb;
	@InjectView(R.id.headframe_title)
	TextView mHeadframeTitle;
	@InjectView(R.id.list_choose_coach)
	PullToRefreshListView mListChooseCoach;

	ListView lv;
	ArrayList<ChooseCoachBean> list;
	ChooseCoachAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_choose_coach);
		ButterKnife.inject(this);
		initView();
		initListener();
	}

	private void initListener() {
		mHeadframeIb.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});
		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				ChooseCoachBean item = (ChooseCoachBean) adapterView.getAdapter().getItem(i);
				bindCoach(item.getCoachName(), item.getCoachId());
			}
		});
	}

	private void initView() {
		mHeadframeTitle.setText("选择教练");
		list = new ArrayList<ChooseCoachBean>();
//		mListChooseCoach.setMode(PullToRefreshBase.Mode.BOTH);
		lv = mListChooseCoach.getRefreshableView();
		getCoachInfo(this);
	}

	private void getCoachInfo(final Context context) {
		if (TextUtils.isEmpty(Utils.SPGetString(ChooseCoachActivity.this, Utils.schoolId, ""))) {
			new AlertDialog.Builder(ChooseCoachActivity.this).setMessage("请先绑定驾校！").create().show();
			/*ProgressDialog dialog = new ProgressDialog(ChooseCoachActivity.this);
			dialog.setMessage("请先绑定驾校！");
			dialog.setIndeterminate(true);
			dialog.setCanceledOnTouchOutside(true);
			dialog.show();*/
		} else {
			RequestParams params = new RequestParams();
			params.put("DrivingSchoolId", Utils.SPGetString(ChooseCoachActivity.this, Utils.schoolId, ""));
			KLog.e("params", params.toString());
			MyHttp.getInstance(context).post(MyHttp.GET_COACHINFO, params, new JsonHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
					super.onSuccess(statusCode, headers, response);
					KLog.json(response.toString());

					try {

						JSONArray array = response.getJSONArray("Result");
						for (int i = 0; i < array.length(); i++) {
							JSONObject object = array.getJSONObject(i);
							ChooseCoachBean bean = new ChooseCoachBean();
							bean.setCoachAddress(object.getString("UserAddress"));
							bean.setCoachId(object.getString("UserId"));
							bean.setCoachLogo(object.getString("UserLogo"));
							bean.setCoachName(object.getString("RealName"));
							bean.setLicensePlate(object.getString("LicensePlate"));
							list.add(bean);
						}

					} catch (JSONException e) {
						e.printStackTrace();
					}
					adapter = new ChooseCoachAdapter(context, R.layout.activity_choose_coach_item);
					lv.setAdapter(adapter);
					adapter.addAll(list);
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
					super.onFailure(statusCode, headers, responseString, throwable);

				}
			});
		}
	}

	/**
	 * 绑定教练dialog
	 */
	private void bindCoach(final String coachname, final String id) {
		AlertDialog.Builder aBuilder = new AlertDialog.Builder(ChooseCoachActivity.this);
		aBuilder.setMessage("是否绑定教练：" + coachname);
		aBuilder.setPositiveButton("是", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {
				bindingCoach(ChooseCoachActivity.this, id, coachname);

			}
		});
		aBuilder.setNegativeButton("否", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {
				dialogInterface.dismiss();
			}
		});
		AlertDialog dialog = aBuilder.create();
		dialog.show();

	}

	private void bindingCoach(final Context context, final String id, final String coachName) {
		RequestParams params = new RequestParams();
		params.put("UserId", Utils.SPGetString(ChooseCoachActivity.this, Utils.userId, ""));
		params.put("CoachId", id);
		KLog.e("params", params.toString());
		MyHttp.getInstance(context).post(MyHttp.BIND_COACH, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
				try {
					Utils.ToastShort(context, response.getString("Reason"));
					if (response.getInt("Code") == 1) {
						Utils.SPutString(ChooseCoachActivity.this, Utils.coachId, id);
						Utils.SPutString(ChooseCoachActivity.this, Utils.CoachName, coachName);
						finish();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}

}
