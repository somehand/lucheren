package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.BaseActivity;
import com.jzkj.jianzhenkeji_road_car_person.CircleImageView;
import com.jzkj.jianzhenkeji_road_car_person.MyApplication;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.bean.RefreshEvent;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ExamActivity extends BaseActivity implements OnClickListener {

	//声明控件
	private ImageButton ibBack, ibSelectCourse;
	private TextView tvTitle, tvUserName, tvExamSchool, tvWhichCourse, tvPopText1, tvPopText2;
	private Button btnCourseExam;
	private CircleImageView headImg;
	private ViewGroup layout;
	private ImageView imgHeadVip;

	private PopupWindow popupWindow;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		if (!Utils.SPGetBoolean(this, Utils.tips_4_Showed, false)) {
			startActivity(new Intent(this, TipActivity_3.class).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY));
			this.overridePendingTransition(0, 0);
		}
		setContentView(R.layout.simulation_exam);
		init();
		initdata();
		initListener();

		setPopupWindow();
	}

	private void initListener() {
		//设置监听事件
		ibBack.setOnClickListener(this);
		ibSelectCourse.setOnClickListener(this);
		btnCourseExam.setOnClickListener(this);
		tvWhichCourse.setOnClickListener(this);
		layout.setOnClickListener(this);

	}

	private void initdata() {
		getHeaderName();

	}

	/**
	 * 设置PopupWindow的方法
	 */
	private void setPopupWindow() {
		LayoutInflater inflater = LayoutInflater.from(this);
		View view = inflater.inflate(R.layout.simulation_exam_popwindow, null);
		tvPopText1 = (TextView) view.findViewById(R.id.simulation_exam_poptext1);
		tvPopText2 = (TextView) view.findViewById(R.id.simulation_exam_poptext2);
		popupWindow = new PopupWindow(view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		popupWindow.setFocusable(true);
		popupWindow.setOutsideTouchable(true);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());

		//监听
		tvPopText1.setOnClickListener(this);
		tvPopText2.setOnClickListener(this);
	}

	/**
	 * 绑定id
	 */
	private void init() {
		layout = (ViewGroup) findViewById(R.id.simulation_exam_layout1);
		ibBack = (ImageButton) findViewById(R.id.headframe_ib);
		ibSelectCourse = (ImageButton) findViewById(R.id.simulation_exam_ib_course);
		tvTitle = (TextView) findViewById(R.id.headframe_title);
		tvUserName = (TextView) findViewById(R.id.simulation_exam_name);
		tvWhichCourse = (TextView) findViewById(R.id.simulation_exam_course_which);
		tvExamSchool = (TextView) findViewById(R.id.simulation_exam_driving_school);
		btnCourseExam = (Button) findViewById(R.id.simulation_exam_btn);
		headImg = (CircleImageView) findViewById(R.id.simulation_exam_civ);
		imgHeadVip = (ImageView) findViewById(R.id.img_exam_head_vip);
		if (Utils.SPGetInt(ExamActivity.this, Utils.IsVip, 2) == 1) {
			imgHeadVip.setVisibility(View.VISIBLE);
		} else {
			imgHeadVip.setVisibility(View.GONE);
		}
		tvTitle.setText("模拟考试");
		ImageLoader.getInstance().displayImage(Utils.SPGetString(this, Utils.userLogo, "userlogo"), headImg, MyApplication.options_user);
		tvUserName.setText(Utils.SPGetString(this, Utils.realName, "realname"));
		tvExamSchool.setText(Utils.SPGetString(this, Utils.schoolName, "schoolname"));
	}

	/**
	 * 监听内容
	 *
	 * @param arg0
	 */
	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
			case R.id.headframe_ib:
				Intent intentM = new Intent(ExamActivity.this, HomeActivity.class);
				EventBus.getDefault().post(new RefreshEvent());
				intentM.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intentM);
				finish();
				break;
			case R.id.simulation_exam_btn:
				Intent intent = new Intent(this, ExamDetailsActivity.class);
				if (tvWhichCourse.getText().toString().equals("科目一考试")) {
					intent.putExtra("class", 1);
				} else if (tvWhichCourse.getText().toString().equals("科目四考试")) {
					intent.putExtra("class", 4);
				}
				startActivity(intent);
				break;
			case R.id.simulation_exam_layout1:
			case R.id.simulation_exam_course_which:
			case R.id.simulation_exam_ib_course:
				if (popupWindow.isShowing()) {
					popupWindow.dismiss();
				} else {
					popupWindow.showAsDropDown(tvWhichCourse, 0, 30);
				}
				break;
			case R.id.simulation_exam_poptext1:
				tvWhichCourse.setText(tvPopText1.getText().toString());
				popupWindow.dismiss();
				break;
			case R.id.simulation_exam_poptext2:
				tvWhichCourse.setText(tvPopText2.getText().toString());
				popupWindow.dismiss();
				break;

			default:
				break;
		}

	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void photoChange(Uri phontUri) {
		headImg.setImageURI(phontUri);
	}


	/**
	 * 获取用户信息
	 */
	private void getHeaderName() {
		RequestParams params = new RequestParams();
		params.put("UserId", Utils.SPGetString(this, Utils.userId, ""));
		MyHttp.getInstance(this).post(MyHttp.GET_USER_HEADER, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(Utils.TAG_DEBUG, response.toString());

				try {
					JSONObject object = response.getJSONObject(0);
					final String name = object.getString("NickName");
					final String logoUrl = object.getString("UserLogo");
					Utils.SPutString(ExamActivity.this, Utils.realName, name);
					Utils.SPutString(ExamActivity.this, Utils.userLogo, logoUrl);
					headImg.post(new Runnable() {
						@Override
						public void run() {
							tvUserName.setText(name);
							ImageLoader.getInstance().displayImage(logoUrl, headImg, MyApplication.options_user);
						}
					});
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				KLog.e("errorcode　", statusCode + " " + responseString);

			}
		});


	}


}
