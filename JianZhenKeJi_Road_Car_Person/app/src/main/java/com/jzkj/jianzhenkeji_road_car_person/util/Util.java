package com.jzkj.jianzhenkeji_road_car_person.util;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

public class Util {
	public static User user = new User();
	public static boolean skipTag = false;
	public static void setListViewHeight(ListView listView){
		if (listView ==null) {
			return;
		}
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter ==null) {
			return;
		}
		int toTalHeight = 0;
//		for (int i = 0; i < listAdapter.getCount() ; i++) {
		for (int i = 0; i < listAdapter.getCount() ; i++) {
			View listItem = listAdapter.getView(i, null, listView);
			listItem.measure(0, 0);
			toTalHeight += listItem.getMeasuredHeight();
		}
		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = toTalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
	}
}
