package com.jzkj.jianzhenkeji_road_car_person.bean;

public class UserRegistResult {
	private boolean registerresult;
	private String registerdetail;
	public UserRegistResult() {
	}
	public UserRegistResult(boolean registerresult, String registerdetail) {
		super();
		this.registerresult = registerresult;
		this.registerdetail = registerdetail;
	}
	public boolean isRegisterresult() {
		return registerresult;
	}
	public void setRegisterresult(boolean registerresult) {
		this.registerresult = registerresult;
	}
	public String getRegisterdetail() {
		return registerdetail;
	}
	public void setRegisterdetail(String registerdetail) {
		this.registerdetail = registerdetail;
	}
	
}
