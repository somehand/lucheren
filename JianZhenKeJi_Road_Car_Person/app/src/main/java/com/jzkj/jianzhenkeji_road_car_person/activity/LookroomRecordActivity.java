package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.BaseActivity;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.adapter.LookRoomLvAdapter;
import com.jzkj.jianzhenkeji_road_car_person.bean.LookRoomBean;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.kanak.emptylayout.EmptyLayout;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class LookroomRecordActivity extends BaseActivity {

	@InjectView(R.id.lookroom_record_lv)
	ListView mLookroomRecordLv;
	private ImageButton ibBack;
	private TextView tvTitle;
	private ArrayList<LookRoomBean> list;
	private LookRoomLvAdapter adapter;

	private EmptyLayout mEmptyLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		EventBus.getDefault().register(this);
		setContentView(R.layout.activity_lookroom_record);
		ButterKnife.inject(this);
		init();
		initData();
//		getMyExamCount();

		ibBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});
	}

	private void initData() {
		getLookRoomInfo();
	}

	private void init() {
		ibBack = (ImageButton) findViewById(R.id.headframe_ib);
		tvTitle = (TextView) findViewById(R.id.headframe_title);
		tvTitle.setText("合场记录");
		mEmptyLayout = new EmptyLayout(LookroomRecordActivity.this, mLookroomRecordLv);
		adapter = new LookRoomLvAdapter(this, R.layout.look_room_item);
		list = new ArrayList<LookRoomBean>();
	}

	/**
	 * 获取我的统计数据
	 */
	private void getMyExamCount() {
		RequestParams params = new RequestParams();
		params.put("UserId", Utils.SPGetString(this, Utils.userId, ""));

		MyHttp.getInstance(this).post(MyHttp.MY_EXAM_COUNT, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e("debug", statusCode + " " + response.toString());
				try {

					JSONObject object = response.getJSONObject(0);
					int total = object.getInt("Totle");
					int fail = object.getInt("shibai");
					int success = 0;
					if (total != 0) {
						success = total - fail;
					}
/*
						mMyStatisticsTxt1.setText(total + "");
						mMyStatisticsTxt2.setText(success + "");
						mMyStatisticsTxt3.setText(fail + "");*/


				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				KLog.e("debug　", statusCode + " " + responseString);

			}
		});
	}

	/**
	 * 获取合场信息
	 */
	private void getLookRoomInfo() {
		RequestParams params = new RequestParams();
		params.put("Userid", Utils.SPGetString(LookroomRecordActivity.this, Utils.userId, "userid"));
		list.clear();
		adapter.clear();
		MyHttp.getInstance(LookroomRecordActivity.this).post(MyHttp.LOOK_ROOM, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e("Debug", "JSONObject:");
				KLog.json(response.toString());
				/*try {
					Utils.ToastShort(getActivity(),response.getString("Reason"));
				} catch (JSONException e) {
					e.printStackTrace();
				}*/
				mEmptyLayout.setEmptyMessage("宝宝好伤心，还没有合场数据~");
				mEmptyLayout.showEmpty();
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e("Debug", "JSONArray:");
				KLog.json(response.toString());
				list = new ArrayList<LookRoomBean>();
				for (int i = 0; i < response.length(); i++) {
					try {
						JSONObject object = response.getJSONObject(i);
						LookRoomBean bean = new LookRoomBean();
						bean.setExaminationName(object.getString("ExaminationName"));
						bean.setIsPay(object.getString("IsPay"));
						bean.setStartDate(object.getString("StartDate"));
						bean.setPracticeId(object.getString("PracticeId"));
						bean.setPayMoney(object.getString("PayMoney"));
						list.add(bean);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				mLookroomRecordLv.setAdapter(adapter);
				adapter.addAll(list);
				if (list.size() == 0) {
					mEmptyLayout.setEmptyMessage("宝宝好伤心，还没有合场数据~");
					mEmptyLayout.showEmpty();
				}
			}
		});
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onEvaluate(MyEvaluateActivity.EvaluateSecurity security) {
		getLookRoomInfo();
	}
}
