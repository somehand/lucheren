package com.jzkj.jianzhenkeji_road_car_person.adapter;

import android.content.Context;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.CircleImageView;
import com.jzkj.jianzhenkeji_road_car_person.MyApplication;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.bean.OrderDetailGvstudentBean;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Administrator on 2016/4/13.
 */
public class OrderDetailGvstudentAdapter extends QuickAdapter<OrderDetailGvstudentBean>{
	public OrderDetailGvstudentAdapter(Context context, int layoutResId) {
		super(context, layoutResId);
	}

	@Override
	protected void convert(BaseAdapterHelper helper, OrderDetailGvstudentBean item) {
		CircleImageView civ = helper.getView(R.id.gv_student_item_civ);
		TextView studentName = helper.getView(R.id.gv_student_item_tvname);
		ImageLoader.getInstance().displayImage(item.getImg(),civ, MyApplication.options_other);
		studentName.setText(item.getStudentName());
	}
}
