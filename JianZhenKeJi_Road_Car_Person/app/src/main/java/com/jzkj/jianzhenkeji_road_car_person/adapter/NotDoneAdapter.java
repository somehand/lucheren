package com.jzkj.jianzhenkeji_road_car_person.adapter;

import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.bean.NotDoneBean;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016/3/1 0001.
 */
public class NotDoneAdapter extends QuickAdapter<NotDoneBean>{

	ArrayList<java.lang.Integer> anwseredList;

	public NotDoneAdapter(Context context, int layoutResId , ArrayList mAnwseredList) {
		super(context, layoutResId);
		anwseredList = mAnwseredList;
	}

	@Override
	protected void convert(BaseAdapterHelper helper, NotDoneBean item) {
		int position = helper.getPosition();
		TextView textView = helper.getView(R.id.not_done_gv_item_tvnum);
		RelativeLayout layout = helper.getView(R.id.not_done_gv_item_ly);
		if(anwseredList.contains(position)){
			layout.setBackgroundResource(R.drawable.shape_circle_light_green_bg);
		}else{
			layout.setBackgroundResource(R.drawable.shape_circle_red_bg);
		}
		textView.setText(item.getNumber() + "");

	}
}
