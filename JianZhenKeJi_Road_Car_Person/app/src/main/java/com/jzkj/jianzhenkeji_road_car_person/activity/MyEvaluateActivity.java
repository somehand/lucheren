package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.BaseActivity;
import com.jzkj.jianzhenkeji_road_car_person.CircleImageView;
import com.jzkj.jianzhenkeji_road_car_person.MyApplication;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class MyEvaluateActivity extends BaseActivity {
	@InjectView(R.id.choose_safety_civ)
	CircleImageView mSafetyHeader;
	@InjectView(R.id.choose_safety_item_name)
	TextView mSafetyName;
	@InjectView(R.id.choose_safety_item_sign)
	TextView mSafetySign;
	@InjectView(R.id.choose_safety_radio1)
	RatingBar mSafetyRadio1;
	@InjectView(R.id.choose_safety_radio2)
	RatingBar mSafetyRadio2;
	@InjectView(R.id.choose_safety_radio3)
	RatingBar mSafetyRadio3;
	@InjectView(R.id.choose_safety_btn)
	Button mChooseSafetyBtn;

	private ImageButton ibBack;
	private TextView tvTitle;
	int PracticeId ;

	int securityId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_evaluate);
		ButterKnife.inject(this);
		Intent intent = getIntent();
		PracticeId = intent.getIntExtra("PracticeId" , 0);
		mSafetyName.setText(intent.getStringExtra("SecurityName"));
		mSafetySign.setText(intent.getStringExtra("SecuritySign"));
		ImageLoader.getInstance().displayImage(intent.getStringExtra("SecurityLogo"), mSafetyHeader, MyApplication.options_user);
		securityId = intent.getIntExtra("SecurityId", 0);
		init();
	}

	private void init() {
		ibBack = (ImageButton) findViewById(R.id.headframe_ib);
		tvTitle = (TextView) findViewById(R.id.headframe_title);
		tvTitle.setText("我的评价");

	}

	/**
	 * 提交评价
	 */
	@OnClick(R.id.choose_safety_btn)
	public void comit() {
		RequestParams params = new RequestParams();
		params.put("UserId", Utils.SPGetString(this, Utils.userId, ""));
		params.put("SecurityOfficerId", securityId);
		params.put("ServiceAttitude",(int) mSafetyRadio1.getRating());
		params.put("Teaching", (int)mSafetyRadio2.getRating());
		params.put("CarScience", (int)mSafetyRadio3.getRating());
		params.put("PracticeId", PracticeId);
		KLog.e("debug", params.toString());

		MyHttp.getInstance(this).post(MyHttp.EVALUATE_SECURITY, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e("debug", statusCode + " " + response.toString());

				try {
					Utils.ToastShort(MyEvaluateActivity.this , response.getString("Reason"));
					EventBus.getDefault().post(new EvaluateSecurity());
					finish();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				KLog.e("debug　", statusCode + " " + responseString);

			}
		});

	}

	@OnClick(R.id.headframe_ib)
	public void back() {
		finish();
	}

	public class EvaluateSecurity{

	}
}
