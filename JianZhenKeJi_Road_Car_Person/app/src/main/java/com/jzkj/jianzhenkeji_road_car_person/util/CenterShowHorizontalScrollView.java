package com.jzkj.jianzhenkeji_road_car_person.util;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

import com.jzkj.jianzhenkeji_road_car_person.R;

/**
 * Created by Administrator on 2016/4/10.
 */
public class CenterShowHorizontalScrollView extends HorizontalScrollView {
    private LinearLayout mShowLinear;
    private Context context;
    private int itemCount=0;

    public CenterShowHorizontalScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        mShowLinear = new LinearLayout(context);
        mShowLinear.setOrientation(LinearLayout.HORIZONTAL);
        LayoutParams params = new LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
        mShowLinear.setGravity(Gravity.CENTER_VERTICAL);
        this.addView(mShowLinear, params);
    }

    public void onClicked(View v) {
        if (v.getTag(R.id.item_position) != null) {
            int position = (Integer) v.getTag(R.id.item_position);
            View itemView = mShowLinear.getChildAt(position);
            int itemWidth = itemView.getWidth();
            int screenWidth = ScreenUtils.getScreenWidth(context);
            int left = itemView.getLeft();
            smoothScrollTo(left - (screenWidth / 2 - itemWidth / 2), 0);
        }
    }

    public LinearLayout getLinear() {
        return mShowLinear;
    }

    public void addItemView(View itemView, int position) {
        itemView.setTag(R.id.item_position, position);
        mShowLinear.addView(itemView);
    }

    public void removeAll(){
        mShowLinear.removeAllViews();
    }

}
