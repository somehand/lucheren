package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.jzkj.jianzhenkeji_road_car_person.BaseActivity;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by Administrator on 2016/4/7.
 */
public class OrderDetailActivity extends BaseActivity implements View.OnClickListener {
	@InjectView(R.id.headframe_ib)
	ImageButton mHeadframeIb;
	@InjectView(R.id.headframe_title)
	TextView mHeadframeTitle;
	@InjectView(R.id.order_detail_ll_coachname)
	LinearLayout mOrderDetailCoachname;
	@InjectView(R.id.order_detail_subnum)
	TextView mOrderDetailSubnum;
	@InjectView(R.id.order_detail_ordertime)
	TextView mOrderDetailOrdertime;
	@InjectView(R.id.order_detail_orderstate)
	TextView mOrderDetailOrderstate;
	@InjectView(R.id.order_detail_applytime)
	TextView mOrderDetailApplytime;
	@InjectView(R.id.order_detail_cancelorder)
	Button mOrderDetailCancelorder;
	@InjectView(R.id.order_detail_checkother)
	TextView mOrderDetailCheckother;
	@InjectView(R.id.RealName)
	TextView RealName;
	@InjectView(R.id.headframe_text_location)
	TextView location;
	@InjectView(R.id.call)
	ImageView call;

	LocationClient mLocClient;
	boolean isFirstLoc = true; // 是否首次定位
	public MyLocationListenner myListener = new MyLocationListenner();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_order_detail);
		ButterKnife.inject(this);
		init();
		initData();
		initListener();
	}

	private void initListener() {
		mHeadframeIb.setOnClickListener(this);
		mOrderDetailCheckother.setOnClickListener(this);
		call.setOnClickListener(this);
		location.setOnClickListener(this);
	}

	private void initData() {
		getAppointmentDetail3();
	}

	String studentbookingsid;
	String coachBookingsDateId;
	String coachName;
	int qiandao;
	int flag = 3;

	private void init() {
		mHeadframeTitle.setText("预约详情");
		Intent intent = getIntent();
		studentbookingsid = intent.getStringExtra("Studentbookingsid");
		coachBookingsDateId = intent.getStringExtra("CoachBookingsDateId");
		coachName = intent.getStringExtra("coachName");
		flag = intent.getIntExtra("flag", 3);
		location.setVisibility(View.VISIBLE);
		initLocation();
	}

	//取得预约详情
	public void getAppointmentDetail3() {
		super.showProgressDialog();
		RequestParams params = new RequestParams();
		params.put("UserId", Utils.SPGetString(this, Utils.userId, ""));
		params.put("CoachBookingsDateId", coachBookingsDateId);
		if (flag == 3) {
			params.put("studentbookingsid", studentbookingsid);
		}
		params.put("Type", flag);
		MyHttp.getInstance(this).post(MyHttp.GET_ORDER_DETAIL, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				OrderDetailActivity.super.removeProgressDialog();
				Log.d("Test", response.toString());
				try {
					JSONObject jsonObject = (JSONObject) response.get(0);
					mOrderDetailSubnum.setText(jsonObject.getString("SubjectNum"));
					RealName.setText(jsonObject.getString("RealName"));
					mOrderDetailApplytime.setText(jsonObject.getString("Createdate"));
					mOrderDetailOrdertime.setText(jsonObject.getString("CoachDay"));
					mOrderDetailOrderstate.setText(jsonObject.getString("status"));
					studentbookingsid = jsonObject.getString("StudentBookingsId");
					phoneNum = jsonObject.getString("UserName");
					qiandao = jsonObject.getInt("qiandao");
					if (qiandao == 1) {//qiandao  （0.未签到，1.已经签到）
						location.setText("已签到");
						location.setTextColor(getResources().getColor(R.color.globalframe_rb_gray));
					} else {
						location.setText("练车签到");
						location.setTextColor(getResources().getColor(R.color.black));
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				OrderDetailActivity.super.removeProgressDialog();
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);

				OrderDetailActivity.super.removeProgressDialog();

			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, String responseString) {
				super.onSuccess(statusCode, headers, responseString);
				OrderDetailActivity.super.removeProgressDialog();

			}

			@Override
			public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
				super.onFailure(statusCode, headers, throwable, errorResponse);
				OrderDetailActivity.super.removeProgressDialog();

			}
		});
	}


	String phoneNum = "";

	//取消本次预约
	@OnClick(R.id.order_detail_cancelorder)
	public void resetAppointment() {
		new AlertDialog.Builder(this).setMessage("取得取消本次预约？").setNegativeButton("确定", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {
				RequestParams params = new RequestParams();
				params.put("UserId", Utils.SPGetString(OrderDetailActivity.this, Utils.userId, ""));
				params.put("CoachBookingsDateId", coachBookingsDateId);
				params.put("studentbookingsid", studentbookingsid);
				params.put("Type", 2);
				KLog.e("params", params.toString());
				MyHttp.getInstance(OrderDetailActivity.this).post(MyHttp.GET_ORDER_DETAIL, params, new JsonHttpResponseHandler() {
					@Override
					public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
						super.onSuccess(statusCode, headers, response);
						try {
							if ("1".equals(response.getString("Code"))) {
								new AlertDialog.Builder(OrderDetailActivity.this).setMessage("取消成功").setNegativeButton("确定", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialogInterface, int i) {
										OrderDetailActivity.this.finish();
									}
								}).create().show();
							} else {
								new AlertDialog.Builder(OrderDetailActivity.this).setMessage(response.getString("Reason")).setNegativeButton("确定", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialogInterface, int i) {
										dialogInterface.dismiss();
									}
								}).create().show();
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}

					@Override
					public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
						super.onSuccess(statusCode, headers, response);
					}
				});
				dialogInterface.dismiss();
			}
		}).setNeutralButton("取消", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {
				dialogInterface.dismiss();
			}
		}).create().show();
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.headframe_ib:
				finish();
				break;
			case R.id.order_detail_checkother://查看其它
				Intent intent = new Intent(OrderDetailActivity.this, MyOrderActivity.class);
				startActivity(intent);
				break;
			case R.id.call://call教练
				Intent call = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phoneNum));
				call.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(call);
				break;
			case R.id.headframe_text_location:
				mLocClient.start();
				break;
			default:
				break;
		}
	}

	public void initLocation() {

		// 定位初始化
		mLocClient = new LocationClient(this);
		mLocClient.registerLocationListener(myListener);
		LocationClientOption option = new LocationClientOption();
		option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);//可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
		option.setCoorType("bd09ll");//可选，默认gcj02，设置返回的定位结果坐标系，如果配合百度地图使用，建议设置为bd09ll;
		option.setScanSpan(0);//可选，默认0，即仅定位一次，设置发起定位请求的间隔需要大于等于1000ms才是有效的
		option.setIsNeedAddress(true);//可选，设置是否需要地址信息，默认不需要
		option.setIsNeedLocationDescribe(true);//可选，设置是否需要地址描述
		option.setNeedDeviceDirect(false);//可选，设置是否需要设备方向结果
		option.setLocationNotify(false);//可选，默认false，设置是否当gps有效时按照1S1次频率输出GPS结果
		option.setIgnoreKillProcess(true);//可选，默认true，定位SDK内部是一个SERVICE，并放到了独立进程，设置是否在stop的时候杀死这个进程，默认不杀死
		option.setIsNeedLocationDescribe(true);//可选，默认false，设置是否需要位置语义化结果，可以在BDLocation.getLocationDescribe里得到，结果类似于“在北京天安门附近”
		option.setIsNeedLocationPoiList(true);//可选，默认false，设置是否需要POI结果，可以在BDLocation.getPoiList里得到
		option.SetIgnoreCacheException(false);//可选，默认false，设置是否收集CRASH信息，默认收集
		option.setOpenGps(true);
		mLocClient.setLocOption(option);
	}

	/**
	 * 定位SDK监听函数
	 */
	public class MyLocationListenner implements BDLocationListener {

		@Override
		public void onReceiveLocation(BDLocation location) {
			StringBuffer sb = new StringBuffer();
//			Utils.ToastShort(OrderDetailActivity.this, location.getLocationDescribe());
			/*List<Poi> list = location.getPoiList();// POI数据
			if (list != null) {
				for (Poi p : list) {
					sb.append("\npoi= : ");
					sb.append(p.getId() + " " + p.getName() + " " + p.getRank());

				}

			}
			Utils.ToastShort(OrderDetailActivity.this, sb.toString());*/

			signIn(location.getLocationDescribe(), location.getLongitude(), location.getLatitude());
			mLocClient.stop();
		}

		public void onReceivePoi(BDLocation poiLocation) {
		}
	}

	/**
	 * string UserId = context.Request.Params["UserId"];//UserId
	 * string WeiD = context.Request.Params["WeiD"];//纬度
	 * string JingD = context.Request.Params["JingD"];//经度
	 * string Adress = context.Request.Params["Adress"];//详细地址
	 * string CreateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
	 *
	 * @param address
	 * @param longitude
	 * @param latitude
	 */
	private void signIn(String address, double longitude, double latitude) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		RequestParams params = new RequestParams();
		params.put("UserId", Utils.SPGetString(OrderDetailActivity.this, Utils.userId, ""));
		params.put("Adress", address);
		params.put("JingD", longitude);
		params.put("WeiD", latitude);
		params.put("studentbookingsid", studentbookingsid);//预约时间段id
		params.put("CreateTime", format.format(new Date()));
		KLog.e(params.toString());
		MyHttp.getInstance(OrderDetailActivity.this).post(MyHttp.SIGN_IN, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
				try {
					Utils.ToastShort(OrderDetailActivity.this, response.getString("Reason"));
					if (response.getInt("Code") == 1) {
						location.setText("已签到");
						location.setTextColor(getResources().getColor(R.color.globalframe_rb_gray));
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mLocClient.stop();
	}
}
