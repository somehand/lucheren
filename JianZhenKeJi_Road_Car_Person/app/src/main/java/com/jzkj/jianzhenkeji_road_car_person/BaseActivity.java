package com.jzkj.jianzhenkeji_road_car_person;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import com.jzkj.jianzhenkeji_road_car_person.util.ActivityCollect;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.socks.library.KLog;

/**
 * 所有Activity的基础类，还可以定义一些其他方法
 * @author zsh
 *
 */
public class BaseActivity extends Activity{
	//加载框的文字说明
	private String progressMessage = "数据加载中...";
	//声明一个ProgressDialog
	public ProgressDialog mProgressDialog;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActivityCollect.addActivity(this);
	}
	/**
	 * Toast提示文本
	 */
	private Toast toast = null;
	public void showToast(Context context, String text, int duration){
		if (toast == null) {
			toast = Toast.makeText(context, text, duration);
		}else{
			toast.setText(text);
			toast.setDuration(duration);
		}
		toast.show();
	}
	/**
	 * 显示进度框
	 */
	public void showProgressDialog(){
		showProgressDialog(null);
	}
	private void showProgressDialog(String message) {
		//创建一个显示的Dialog
		if (message != null) {
			progressMessage = message;
		}
		if (mProgressDialog == null) {
			mProgressDialog = new ProgressDialog(BaseActivity.this);
			// 设置点击屏幕Dialog不消失
			mProgressDialog.setCanceledOnTouchOutside(false);
		}
		mProgressDialog.setMessage(progressMessage);
		mProgressDialog.show();
	}
	/**
	 * 移除进度框
	 */
	public void removeProgressDialog() {
		if (mProgressDialog.isShowing()) {
			mProgressDialog.cancel();
		}
	}
	//获取进度框文字
	public String getProgressMessage() {
		return progressMessage;
	}
	/**
	 * 设置进度框文字
	 * @param message
	 */
	public void setProgressMessage(String message){
		BaseActivity.this.progressMessage = message;
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
		ActivityCollect.removeActivity(this);
	}

	public void LLog(String s){
		KLog.e(Utils.TAG_DEBUG , s);
	}
}
