package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.MyApplication;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class MyDrivingSchoolActivity extends Activity implements View.OnClickListener {

	@InjectView(R.id.headframe_ib)
	ImageButton mHeadframeIb;
	@InjectView(R.id.headframe_title)
	TextView mHeadframeTitle;
	@InjectView(R.id.drive_img_logo)
	ImageView mDriveImgLogo;
	@InjectView(R.id.drive_tvabout)
	TextView mDriveTvabout;
	@InjectView(R.id.tv_address)
	TextView mTvAddress;
	@InjectView(R.id.tv_telephone)
	TextView mTvTelephone;
	@InjectView(R.id.tv_url)
	TextView mTvUrl;
	@InjectView(R.id.ll_telephone)
	LinearLayout mllTele;
	@InjectView(R.id.ll_url)
	LinearLayout mllUrl;
/*	@InjectView(R.id.btn_now_apply)
	Button btnApply;*/

	AlertDialog dialog;
	String schoolname;
	String DrivingSchoolId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_driving_school);
		ButterKnife.inject(this);
		mHeadframeIb.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});
//		ApplyDrivingSchoolBean item = (ApplyDrivingSchoolBean) getIntent().getSerializableExtra("bean");
//		schoolname = item.getSchoolName();
//		mHeadframeTitle.setText(schoolname);
//		ImageLoader.getInstance().displayImage(item.getSchoolLogo(), mDriveImgLogo, MyApplication.options_other);
//		mDriveTvabout.setText(item.getSchoolProfiles());
//		mTvTelephone.setText(Html.fromHtml("<u>" + item.getSchoolPhone() + "</u>"));
//		mTvUrl.setText(Html.fromHtml("<u>" + item.getSchoolUrl() + "</u>"));
//		mTvAddress.setText(item.getSchoolAddress());
//		DrivingSchoolId = item.getDrivingSchoolId();
		/*if (TextUtils.isEmpty(Utils.SPGetString(MyDrivingSchoolActivity.this, Utils.schoolName, ""))) {
		} else {
			if (Utils.SPGetString(MyDrivingSchoolActivity.this, Utils.schoolName, "").equals(schoolname)) {
				btnApply.setVisibility(View.GONE);
			}
		}*/
		initListener();
		getDriveInfo();
	}

	private void initListener() {
		mllTele.setOnClickListener(this);
		mllUrl.setOnClickListener(this);
//		btnApply.setOnClickListener(this);
	}

	private void getDriveInfo() {
		String schoolid = Utils.SPGetString(MyDrivingSchoolActivity.this, Utils.schoolId, "");
		if (TextUtils.isEmpty(schoolid)) {
			mHeadframeTitle.setText("我的驾校");
//			Utils.ToastShort(MyDrivingHomepageActivity.this, "您还未绑定驾校，请先绑定驾校");
			AlertDialog.Builder aBuilder = new AlertDialog.Builder(MyDrivingSchoolActivity.this);
			aBuilder.setMessage("您还未绑定驾校，是否现在绑定？");
			aBuilder.setPositiveButton("是", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					startActivity(new Intent(MyDrivingSchoolActivity.this, ChooseSchoolActivity.class));
					getDriveInfo();
				}
			});
			aBuilder.setNegativeButton("否", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					dialogInterface.dismiss();
					MyDrivingSchoolActivity.this.finish();
				}
			});
			dialog = aBuilder.create();
			dialog.show();
		} else {
			RequestParams params = new RequestParams();
			params.put("DrivingSchoolId", schoolid);
			KLog.e("params", params.toString());
			MyHttp.getInstance(this).post(MyHttp.GET_DRIVING_INFO, params, new JsonHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
					super.onSuccess(statusCode, headers, response);
					KLog.e("Debug", "JSONArray:");
					KLog.json(response.toString());
					try {
						JSONObject object = response.getJSONObject(0);
						mHeadframeTitle.setText(object.getString("SchoolName"));
						ImageLoader.getInstance().displayImage(object.getString("SchoolLogo"), mDriveImgLogo, MyApplication.options_other);
						mDriveTvabout.setText(object.getString("SchoolProfiles"));
						mTvTelephone.setText(Html.fromHtml("<u>" + object.getString("SchoolPhone") + "</u>"));
						mTvUrl.setText(Html.fromHtml("<u>" + object.getString("SchoolUrl") + "</u>"));
						mTvAddress.setText(object.getString("SchoolAddress"));
					/*	if (Utils.SPGetString(MyDrivingSchoolActivity.this, Utils.schoolName, "").equals(object.getString("SchoolName"))) {
							btnApply.setVisibility(View.GONE);
						}*/
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			});
		}
	}

	@Override
	public void onClick(View view) {
		Intent intent;
		switch (view.getId()) {
			case R.id.ll_telephone:
//				String[] str = mTvTelephone.getText().toString().split("\\-");
				intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mTvTelephone.getText().toString()));
				startActivity(intent);
				break;
			case R.id.ll_url:
				intent = new Intent(MyDrivingSchoolActivity.this, WebPageActivity.class);
				intent.putExtra("titleName", "我的驾校");
				intent.putExtra("type", 4);
				intent.putExtra("url", mTvUrl.getText().toString());
				startActivity(intent);
				break;
			/*case R.id.btn_now_apply:
//				if (TextUtils.isEmpty(Utils.SPGetString(MyDrivingHomepageActivity.this, Utils.schoolId, ""))) {
				intent = new Intent(MyDrivingSchoolActivity.this, ApplySchoolActivity.class);
				intent.putExtra("schoolname", schoolname);
				intent.putExtra("DrivingSchoolId", DrivingSchoolId);
				startActivity(intent);
//				} else {
//					Utils.ToastShort(MyDrivingHomepageActivity.this, "你已绑定" + Utils.SPGetString(MyDrivingHomepageActivity.this, Utils.schoolName, "") + "，不能再报名其他驾校");
//				}
				break;*/
			default:
				break;
		}
	}


	@Override
	protected void onPause() {
		super.onPause();
//		if (TextUtils.isEmpty(Utils.SPGetString(MyDrivingHomepageActivity.this, Utils.schoolId, ""))) {
//			dialog.dismiss();
//		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		getDriveInfo();
	}

	@Override
	protected void onRestart() {
		super.onRestart();

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}
