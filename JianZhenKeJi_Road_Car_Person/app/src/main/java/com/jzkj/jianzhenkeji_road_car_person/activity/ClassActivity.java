package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.adapter.HomePageLVAdapter;
import com.jzkj.jianzhenkeji_road_car_person.bean.UserInformationExchange;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ClassActivity extends Activity implements PullToRefreshBase.OnRefreshListener<ListView>{

	@InjectView(R.id.headframe_ib)
	ImageButton mHeadframeIb;
	@InjectView(R.id.headframe_title)
	TextView mHeadframeTitle;
	@InjectView(R.id.headframe_btn)
	Button mHeadframeBtn;
	@InjectView(R.id.class_list)
	PullToRefreshListView mPullToRefreshListView;
	ListView mClassList;

	int classNum;
	HomePageLVAdapter mAdapter;
	ArrayList<UserInformationExchange> mArrayList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_class);
		ButterKnife.inject(this);

		initView();
		initData();
		initlistener();
	}

	private void initData() {
		mAdapter = new HomePageLVAdapter(this, R.layout.home_page_lv_item);
		mClassList.setAdapter(mAdapter);
		mArrayList = new ArrayList<UserInformationExchange>();
		getArticle();
	}

	private void initView() {
		mClassList = mPullToRefreshListView.getRefreshableView();
		mPullToRefreshListView.setOnRefreshListener(this);
		mPullToRefreshListView.setMode(PullToRefreshBase.Mode.PULL_FROM_START);
		Intent intent = getIntent();
		classNum = intent.getIntExtra("classNum", 1);
		switch (classNum) {
			case 1:
				mHeadframeTitle.setText("科目一");

				break;
			case 2:
				mHeadframeTitle.setText("科目二");

				break;
			case 3:
				mHeadframeTitle.setText("科目三");

				break;
			case 4:
				mHeadframeTitle.setText("科目四");

				break;
		}
	}

	private void initlistener() {
		mHeadframeIb.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});
		mClassList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				System.out.println( "个数 ： " + adapterView.getAdapter().getCount() +  " 位置 ：" + i);
				UserInformationExchange userInformationExchange = (UserInformationExchange) adapterView.getAdapter().getItem(i);
				addReadNum(userInformationExchange.getId());
				Intent intent = new Intent(ClassActivity.this, Blank.class);
				intent.putExtra("url", userInformationExchange.getUrl());
				intent.putExtra("articelId", userInformationExchange.getId());
				startActivity(intent);
			}
		});
	}

	/**
	 * 获取listview信息
	 */
	public void getArticle(){
		RequestParams params = new RequestParams();
		params.put("SubjectNum", classNum);
		KLog.e(Utils.TAG_DEBUG , params.toString());
		MyHttp.getInstance(this).post(MyHttp.GET_CLASS_ARTICLE , params, new JsonHttpResponseHandler(){
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG , "jsonObject :");
				KLog.json(Utils.TAG_DEBUG ,response.toString());
				mHandler.sendEmptyMessageDelayed(1, 0);

			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG , "jsonArray :");
				KLog.json(Utils.TAG_DEBUG ,response.toString());
				try {
					for (int i = 0; i < response.length(); i++) {
						JSONObject object = response.getJSONObject(i);
						String title = object.getString("ArticleTitle");
						String id = object.getString("AppIndexArticleId");
						String url = object.getString("ArticleURL");
						String imgUrls = object.getString("ImgURL");
						String readNum = object.getString("ReadNum");
						String[] ss = imgUrls.split("\\|");
						mArrayList.add(new UserInformationExchange(id, title , ss[0] , ss[1] , ss[2] , readNum , url) );
						//KLog.e(lvList.size() + "--------------- " + lvList.get(0).toString());

					}
					mAdapter.clear();
					mAdapter.addAll(mArrayList);
					mHandler.sendEmptyMessageDelayed(1, 800);
					//Util.setListViewHeight(lv);
				} catch (JSONException e) {
					e.printStackTrace();
				}


			}

		});
	}

	/**
	 * 增加阅读量
	 */
	public void addReadNum(String AppIndexArticleId ){
		RequestParams params = new RequestParams();
		params.put("AppIndexArticleId" , AppIndexArticleId);
		KLog.e(Utils.TAG_DEBUG , params.toString());
		MyHttp.getInstance(this).post(MyHttp.ARTICLE_READNUM_ADD , params , new JsonHttpResponseHandler(){
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG , "jsonObject :");
				KLog.json(Utils.TAG_DEBUG ,response.toString());
				try {
					if(response.getInt("Code") == 1){
						KLog.e(Utils.TAG_DEBUG , "点击成功");
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG , "jsonArray :");
				KLog.json(Utils.TAG_DEBUG ,response.toString());

			}

		});
	}

	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		String label = DateUtils.formatDateTime(this,
				System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME
						| DateUtils.FORMAT_SHOW_DATE
						| DateUtils.FORMAT_ABBREV_ALL);

		// Update the LastUpdatedLabel
		refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);
		mArrayList.clear();
		getArticle();
	}

	Handler mHandler = new Handler(){
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			mPullToRefreshListView.onRefreshComplete();
		}
	};




}
