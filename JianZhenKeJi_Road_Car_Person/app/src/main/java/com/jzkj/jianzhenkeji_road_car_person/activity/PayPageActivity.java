package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.BaseActivity;
import com.jzkj.jianzhenkeji_road_car_person.MyPayCompleteListener;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.alipay.MyAlipayManager;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class PayPageActivity extends BaseActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
	@InjectView(R.id.pay_name)
	TextView mPayName;
	@InjectView(R.id.pay_price)
	TextView mPayPrice;
	@InjectView(R.id.pay_box_1)
	CheckBox mPayBox1;
	@InjectView(R.id.pay_layout_ali)
	LinearLayout mPayLayoutAli;
	@InjectView(R.id.pay_box_2)
	CheckBox mPayBox2;
	@InjectView(R.id.pay_layout_common)
	LinearLayout mPayLayoutCommon;
	@InjectView(R.id.pay_box_3)
	CheckBox mPayBox3;
	@InjectView(R.id.pay_layout_weichat)
	LinearLayout mPayLayoutWeichat;
	@InjectView(R.id.pay_btn)
	Button mPayBtn;
	private ImageButton ibBack;
	private TextView tvTitle;

	List<CheckBox> mBoxList;
	String price;
	int PracticeId;
	int type;
	int payType = 0;//0 支付宝支付 1 普通支付 2 微信支付

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pay_order_form);
		ButterKnife.inject(this);
		init();
		initListener();

	}

	private void initListener() {
		ibBack.setOnClickListener(this);
		mPayLayoutAli.setOnClickListener(this);
		mPayLayoutCommon.setOnClickListener(this);
		mPayLayoutWeichat.setOnClickListener(this);
		mPayBox1.setOnCheckedChangeListener(this);
		mPayBox2.setOnCheckedChangeListener(this);
		mPayBox3.setOnCheckedChangeListener(this);
		mPayBtn.setOnClickListener(this);
	}

	private void init() {
		ibBack = (ImageButton) findViewById(R.id.headframe_ib);
		tvTitle = (TextView) findViewById(R.id.headframe_title);
		tvTitle.setText("支付订单");
		mBoxList = new ArrayList<CheckBox>();
		mBoxList.add(mPayBox1);
		mBoxList.add(mPayBox2);
		mBoxList.add(mPayBox3);
		mPayBox1.setChecked(true);

		Intent intent = getIntent();
		Double getprice = Double.valueOf(intent.getStringExtra("price"));
		DecimalFormat format = new DecimalFormat("#.00");
		price = format.format(getprice);
		type = intent.getIntExtra("type", 2);
		Log.e("payMoney", price + PracticeId);
		mPayName.setText(intent.getStringExtra("orderName"));
		mPayPrice.setText(price);
		tag = intent.getIntExtra("tag", 0);
		if (tag == 1 || tag == 2) {//报名驾校和购买保险  不需要调payAfter()、也不需要PracticeId
		} else {
			PracticeId = Integer.parseInt(intent.getStringExtra("PracticeId"));
			payAfter();
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.headframe_ib:
				showConfirmDialog();
				break;
			case R.id.pay_layout_ali:
				mPayBox1.setChecked(true);
				break;
			case R.id.pay_layout_common:
				mPayBox2.setChecked(true);
				break;
			case R.id.pay_layout_weichat:
				mPayBox3.setChecked(true);
				break;
			case R.id.pay_btn:
//				payAfter();
				if (mPayBox1.isChecked()) { //调用支付宝
					payType = 0;
					MyAlipayManager.getInstance(this, mMyPayCompleteListener).setPayType(0).pay("路车人", "支付", price);
				} else if (mPayBox2.isChecked()) { //调用普通支付
					payType = 1;
				} else if (mPayBox3.isChecked()) { //调用微信支付
					payType = 2;
				} else {
					Utils.ToastShort(PayPageActivity.this, "当前只支持支付宝支付，请选择!");

				}
				break;
		}
	}

	public void singleButton(int a) {
		for (int i = 0; i < mBoxList.size(); i++) {
			if (i != a) {
				mBoxList.get(i).setChecked(false);
			}
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
		if (b) {
			switch (compoundButton.getId()) {
				case R.id.pay_box_1:
					singleButton(0);
					break;
				case R.id.pay_box_2:
					singleButton(1);
					break;
				case R.id.pay_box_3:
					singleButton(2);
					break;
			}
		}
	}

	int tag;
	MyPayCompleteListener mMyPayCompleteListener = new MyPayCompleteListener() {
		@Override
		public void onPayComplete(int payTypeId) {
			Intent intent = new Intent(PayPageActivity.this, PaySuccessActivity.class);
			intent.putExtra("PAY_TYPE", payTypeId);
			intent.putExtra("type", type);
			if (tag == 1) {//报名驾校
				Utils.SPutString(PayPageActivity.this, Utils.schoolId, getIntent().getStringExtra("DrivingSchoolId"));
				Utils.SPutString(PayPageActivity.this, Utils.schoolName, getIntent().getStringExtra("schoolname"));
				//支付成功后只显示订单编号和支付方式
				startActivity(intent);
				finish();
			} else if (tag == 2) {//学车保险
				//支付成功后只显示订单编号和支付方式
				startActivity(intent);
				finish();
			}
			startActivity(intent);
			finish();
		}
	};

	/**
	 * 支付（调支付接口）
	 */
	public void payAfter() {
		RequestParams params = new RequestParams();
		params.put("UserId", Utils.SPGetString(this, Utils.userId, ""));
		params.put("PayMoney", price);
		params.put("PracticeId", PracticeId);
		params.put("PayChannel", 1);//1支付宝
		params.put("paytype", type);
		params.put("PhoneType", 2);//1IOS  2安卓
//		params.put("Ordername", mPayName.getText().toString());
		KLog.e(Utils.TAG_DEBUG, params.toString());
		MyHttp.getInstance(this).post(MyHttp.PAY_AFTER, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonObject :");
				KLog.json(Utils.TAG_DEBUG, response.toString());
				try {
					if (response.getInt("Code") == 1) {
						Utils.SPutString(PayPageActivity.this, Utils.payUrl, response.getString("Reason"));
						JSONArray array = response.getJSONArray("Result");
						JSONObject object = array.getJSONObject(0);
						Utils.SPutString(PayPageActivity.this, Utils.orderCode, object.getString("OrderCode"));
						Utils.SPutString(PayPageActivity.this, Utils.orderCodeId, object.getString("OrderId"));
						Utils.SPutString(PayPageActivity.this, Utils.createtime, object.getString("createtime"));
						KLog.e(Utils.TAG_DEBUG, object.getString("OrderCode") + object.getString("OrderId") + object.getString("createtime") + response.getString("Reason"));
						String type = "0";
						if (payType == 0) {
							type = "支付宝支付";
						} /*else if (payType == 1) {
							type = "普通支付";
						} else if (payType == 2) {
							type = "微信支付";
						}*/
						/*Intent intent = new Intent(PayPageActivity.this, PaySuccessActivity.class);
						intent.putExtra("PAY_TYPE", type);

						startActivity(intent);*/
//						finish();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonArray :");
				KLog.json(Utils.TAG_DEBUG, response.toString());

			}

		});
	}

	public void showConfirmDialog() {
		final AlertDialog dialog = new AlertDialog.Builder(this).create();
		dialog.setMessage("您还没有完成支付, 是否放弃?");
		dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {
				dialog.dismiss();
			}
		});
		dialog.setButton(DialogInterface.BUTTON_POSITIVE, "确定", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {
				Intent intent = new Intent(PayPageActivity.this, HomeActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
				if (type == 1) {
					intent.putExtra("item", 3);
				} else {
					intent.putExtra("item", 0);
				}
				startActivity(intent);
				finish();
			}
		});
		dialog.show();
	}

	@Override
	public void onBackPressed() {
		showConfirmDialog();
	}
}
