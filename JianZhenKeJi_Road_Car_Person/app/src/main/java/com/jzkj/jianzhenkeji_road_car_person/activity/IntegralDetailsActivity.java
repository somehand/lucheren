package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.jzkj.jianzhenkeji_road_car_person.CircleImageView;
import com.jzkj.jianzhenkeji_road_car_person.MyApplication;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.adapter.IntegralDetailsAdapter;
import com.jzkj.jianzhenkeji_road_car_person.bean.IntegralDetailsBean;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.kanak.emptylayout.EmptyLayout;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class IntegralDetailsActivity extends Activity {

	private ImageButton btnBlack;
	TextView tvTitle, tvscore;
	private ListView lv;
	private PullToRefreshListView ptrfLv;
	private CircleImageView head_img;
	private ArrayList<IntegralDetailsBean> list;
	private IntegralDetailsAdapter adapter;
	private TextView name_tv;
	int nowPage = 1;
	EmptyLayout mEmptyLayout;
	private ImageView imgVip;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_integral_details);
		init();
		initData();
		initListener();

	}

	private void initListener() {
		btnBlack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});
	}

	private void initData() {
		/*String name = "苹果6plus手机 兑换";
		String[] score = {"-500000", "+500000", "+2000", "-6000"};
		String time = "2016-05-16";
		for (int i = 0; i < score.length; i++) {
			IntegralDetailsBean bean = new IntegralDetailsBean();
			bean.setTime(time);
			bean.setName(name);
			bean.setScore(score[i]);
			list.add(bean);
		}*/
		getHeaderName();
		getScoreRecordInfo(this);
		lv = ptrfLv.getRefreshableView();
		ptrfLv.setMode(PullToRefreshBase.Mode.PULL_FROM_END);
		ptrfLv.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				String label = DateUtils.formatDateTime(IntegralDetailsActivity.this, System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_ALL);
				refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);
				nowPage++;
				getScoreRecordInfo(IntegralDetailsActivity.this);
			}
		});
	}

	private void init() {
		btnBlack = (ImageButton) findViewById(R.id.headframe_ib);
		tvTitle = (TextView) findViewById(R.id.headframe_title);
		ptrfLv = (PullToRefreshListView) findViewById(R.id.integral_details_lv);
		tvscore = (TextView) findViewById(R.id.integral_details_tvscore);
		head_img = (CircleImageView) findViewById(R.id.head_img);
		name_tv = (TextView) findViewById(R.id.name);
		imgVip = (ImageView) findViewById(R.id.img_exam_head_vip);

		if (Utils.SPGetInt(IntegralDetailsActivity.this, Utils.IsVip, 2) == 1) {
			imgVip.setVisibility(View.VISIBLE);
		} else {
			imgVip.setVisibility(View.GONE);
		}
		tvTitle.setText("积分记录");
		if (getIntent().getIntExtra("ss", 0) == 1) {
			tvscore.setText(getIntent().getStringExtra("score"));
		} else {
			tvscore.setText(getIntent().getStringExtra("currentscore"));
		}

		list = new ArrayList<IntegralDetailsBean>();
		ImageLoader.getInstance().displayImage(Utils.SPGetString(IntegralDetailsActivity.this, Utils.userLogo, ""), head_img, MyApplication.options_user);
		name_tv.setText(Utils.SPGetString(IntegralDetailsActivity.this, Utils.realName, "name"));
	}

	/**
	 * 获取用户信息
	 */
	private void getHeaderName() {
		RequestParams params = new RequestParams();
		params.put("UserId", Utils.SPGetString(this, Utils.userId, ""));
		MyHttp.getInstance(this).post(MyHttp.GET_USER_HEADER, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, response.toString());

				try {
					JSONObject object = response.getJSONObject(0);
					final String name = object.getString("NickName");
					final String logoUrl = object.getString("UserLogo");
					Utils.SPutString(IntegralDetailsActivity.this, Utils.realName, name);
					Utils.SPutString(IntegralDetailsActivity.this, Utils.userLogo, logoUrl);
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							name_tv.setText(name);
							ImageLoader.getInstance().displayImage(logoUrl, head_img, MyApplication.options_user);
						}
					});
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				KLog.e("errorcode　", statusCode + " " + responseString);

			}
		});
	}

	private void getScoreRecordInfo(Context context) {
		RequestParams params = new RequestParams();
		params.put("Userid", Utils.SPGetString(context, Utils.userId, ""));
		params.put("PageIndex", nowPage);
		params.put("pageSize", 15);
		KLog.e("params", params.toString());
		MyHttp.getInstance(context).post(MyHttp.GET_SCORERECORD_INFO, params, new JsonHttpResponseHandler() {

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e("Debug", "JSONArray:");
				KLog.json(response.toString());
				for (int i = 0; i < response.length(); i++) {
					try {
						JSONObject object = response.getJSONObject(i);
						IntegralDetailsBean bean = new IntegralDetailsBean();
						bean.setId(object.getString("Id"));
						bean.setScore(object.getString("Integral"));
						if (object.getInt("Type") == 1) {
							bean.setTypeDetail("充值");
						} else if (object.getInt("Type") == 2) {
							bean.setTypeDetail("赠送他人");
						} else if (object.getInt("Type") == 3) {
							bean.setTypeDetail("兑换礼品");
						} else if (object.getInt("Type") == 4) {
							bean.setTypeDetail("自己分享");
						} else if (object.getInt("Type") == 5) {
							bean.setTypeDetail("分享注册");
						} else if (object.getInt("Type") == 6) {
							bean.setTypeDetail("他人赠送");
						} else if (object.getInt("Type") == 7) {
							bean.setTypeDetail("每日登陆");
						} else if (object.getInt("Type") == 8) {
							bean.setTypeDetail("预约教练");
						} else if (object.getInt("Type") == 9) {
							bean.setTypeDetail("评价");
						} else if (object.getInt("Type") == 10) {
							bean.setTypeDetail("在线答题");
						} else if (object.getInt("Type") == 11) {
							bean.setTypeDetail("驾考攻略");
						}
						bean.setTime(object.getString("Createdate"));
						list.add(bean);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				adapter = new IntegralDetailsAdapter(IntegralDetailsActivity.this, R.layout.integral_lv_item);
				lv.setAdapter(adapter);
				adapter.addAll(list);
				ptrfLv.postDelayed(new Runnable() {
					@Override
					public void run() {
						ptrfLv.onRefreshComplete();
					}
				}, 1000);
				if (list.size() == 0) {
					mEmptyLayout = new EmptyLayout(IntegralDetailsActivity.this, lv);
					/*LayoutInflater inflater = LayoutInflater.from(IntegralDetailsActivity.this);
					View view = inflater.inflate(R.layout.empty_layout_half,null);
					mEmptyLayout.setEmptyView((ViewGroup) view);*/
					mEmptyLayout.setEmptyMessage("还没有任何积分记录~");
					mEmptyLayout.showEmpty();
				}
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e("Debug", "JSONObject:");
				KLog.json(response.toString());
				if (nowPage != 1) {
					Utils.ToastShort(IntegralDetailsActivity.this, "当前已是最后一页!");
				}
				ptrfLv.postDelayed(new Runnable() {
					@Override
					public void run() {
						ptrfLv.onRefreshComplete();
					}
				}, 1000);
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				ptrfLv.postDelayed(new Runnable() {
					@Override
					public void run() {
						ptrfLv.onRefreshComplete();
					}
				}, 1000);
			}
		});
	}
}
