package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.adapter.ExchangeRecordAdapter;
import com.jzkj.jianzhenkeji_road_car_person.bean.ExchangeRecordBean;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.kanak.emptylayout.EmptyLayout;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Administrator on 2016/4/6.
 */
public class ExchangeRecordActivity extends Activity {
	@InjectView(R.id.exchange_record_lv)
	PullToRefreshListView ptrfLv;

	@InjectView(R.id.headframe_ib)
	ImageButton mHeadframeIb;
	@InjectView(R.id.headframe_title)
	TextView mHeadframeTitle;

	ListView mExchangeRecordLv;
	private ArrayList<ExchangeRecordBean> list;
	private ExchangeRecordAdapter adapter;
	private int pageSize = 8;
	private int nowPage = 1;
	EmptyLayout mEmptyLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_exchange_record);
		ButterKnife.inject(this);
		init();
		initData();
		initListener();
	}

	private void initListener() {
		mHeadframeIb.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});
		ptrfLv.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				String label = DateUtils.formatDateTime(ExchangeRecordActivity.this,System.currentTimeMillis(),DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_ALL);
				refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);
				nowPage++;
				getExchangeRecordInfo();
			}
		});
		mExchangeRecordLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				Intent intent = new Intent(ExchangeRecordActivity.this, WareDetailActivity.class);
				intent.putExtra("id", ((ExchangeRecordBean)adapterView.getAdapter().getItem(i)).getGoodsId());
				startActivity(intent);
			}
		});
	}

	private void initData() {
		/*int img = R.drawable.example_img;
		String name = "Apple/苹果ipone6plus64G全网通原装苹果6手机";
		String[] scores = {"20000","21000","22000","23000"};
		String time = "2016-05-23   15:30";
		for (int i = 0; i < scores.length; i++) {
			ExchangeRecordBean bean = new ExchangeRecordBean();
			bean.setImg(img);
			bean.setNameTitle(name);
			bean.setScores(scores[i]);
			bean.setTime(time);
			list.add(bean);
		}*/
		getExchangeRecordInfo();
		mExchangeRecordLv = ptrfLv.getRefreshableView();
		ptrfLv.setMode(PullToRefreshBase.Mode.PULL_FROM_END);
	}

	private void init() {
		mHeadframeTitle.setText("兑换记录");
		list = new ArrayList<ExchangeRecordBean>();
	}

	private void getExchangeRecordInfo() {
		RequestParams params = new RequestParams();
		params.put("Userid", Utils.SPGetString(ExchangeRecordActivity.this, Utils.userId, ""));
		params.put("PageIndex", nowPage);
		params.put("PageSize", pageSize);
		KLog.e("params", params.toString());
		MyHttp.getInstance(this).post(MyHttp.GET_EXCHANGERECORD_INFO, params, new JsonHttpResponseHandler() {

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e("Debug", "JSONArray:");
				KLog.json(response.toString());
				for (int i = 0; i < response.length(); i++) {
					try {
						JSONObject object = response.getJSONObject(i);
						ExchangeRecordBean bean = new ExchangeRecordBean();
						bean.setGoodsId(object.getString("Goodsid"));
						bean.setCreatedate(object.getString("Createdate"));
						bean.setGoodname(object.getString("GoodsName"));
						bean.setPic(object.getString("pic"));
						bean.setPrice(object.getString("Integral"));
						list.add(bean);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				adapter = new ExchangeRecordAdapter(ExchangeRecordActivity.this, R.layout.exchange_record_lv_item);
				mExchangeRecordLv.setAdapter(adapter);
				adapter.addAll(list);
				ptrfLv.postDelayed(new Runnable() {
					@Override
					public void run() {
						ptrfLv.onRefreshComplete();
					}
				},1000);
				if (list.size() == 0) {
					mEmptyLayout = new EmptyLayout(ExchangeRecordActivity.this,mExchangeRecordLv);
					/*LayoutInflater inflater = LayoutInflater.from(ExchangeRecordActivity.this);
					View view = inflater.inflate(R.layout.empty_layout_half,null);
					mEmptyLayout.setEmptyView((ViewGroup) view);
					TextView text = (TextView) view.findViewById(R.id.empty_tvtext);
					text.setText(" 宝宝好伤心，还没有兑换记录~");*/
					mEmptyLayout.setEmptyMessage("宝宝好伤心，还没有兑换记录~");
					mEmptyLayout.showEmpty();
				}
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e("Debug", "JSONObject:");
				KLog.json(response.toString());
				if (nowPage != 1) {
					Utils.ToastShort(ExchangeRecordActivity.this, "当前已是最后一页!");
				}
				ptrfLv.postDelayed(new Runnable() {
					@Override
					public void run() {
						ptrfLv.onRefreshComplete();
					}
				},1000);
				mEmptyLayout = new EmptyLayout(ExchangeRecordActivity.this,mExchangeRecordLv);
				/*LayoutInflater inflater = LayoutInflater.from(ExchangeRecordActivity.this);
				View view = inflater.inflate(R.layout.empty_layout_half,null);
				mEmptyLayout.setEmptyView((ViewGroup) view);
				TextView text = (TextView) view.findViewById(R.id.empty_tvtext);
				text.setText(" 宝宝好伤心，还没有兑换记录~");*/
				mEmptyLayout.setEmptyMessage("宝宝好伤心，还没有兑换记录~");
				mEmptyLayout.showEmpty();
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				ptrfLv.postDelayed(new Runnable() {
					@Override
					public void run() {
						ptrfLv.onRefreshComplete();
					}
				},1000);
			}
		});
	}

}
