package com.jzkj.jianzhenkeji_road_car_person.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.CircleImageView;
import com.jzkj.jianzhenkeji_road_car_person.MyApplication;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.activity.IntegralDetailsActivity;
import com.jzkj.jianzhenkeji_road_car_person.activity.LoginActivity;
import com.jzkj.jianzhenkeji_road_car_person.activity.MyDataActivity;
import com.jzkj.jianzhenkeji_road_car_person.activity.RechargeActivity;
import com.jzkj.jianzhenkeji_road_car_person.adapter.MineLVAdapter;
import com.jzkj.jianzhenkeji_road_car_person.bean.ItIsUser;
import com.jzkj.jianzhenkeji_road_car_person.bean.RefreshEvent;
import com.jzkj.jianzhenkeji_road_car_person.bean.SchoolName;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Util;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.jzkj.jianzhenkeji_road_car_person.util.photocrop.CropHandler;
import com.jzkj.jianzhenkeji_road_car_person.util.photocrop.CropHelper;
import com.jzkj.jianzhenkeji_road_car_person.util.photocrop.CropParams;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class Mine extends Fragment implements View.OnTouchListener, CropHandler, View.OnClickListener {

	@InjectView(R.id.mine_header)
	CircleImageView mMineHeader;
	@InjectView(R.id.mine_name)
	TextView mMineName;
	@InjectView(R.id.mine_tvscore_record)
	TextView mScorerecord;
	@InjectView(R.id.mine_tvrecharge)
	TextView mRecharge;
	@InjectView(R.id.mine_tvcurrentscore)
	TextView tvCurrentScore;
	@InjectView(R.id.mine_head_right_rl)
	RelativeLayout mMyData;
	@InjectView(R.id.mine_btn_exitlogin)
	Button btnExit;
	@InjectView(R.id.img_head_vip)
	ImageView isVip;
	/*@InjectView(R.id.mine_driving_school)
	TextView mMineDrivingSchool;*/
	private ListView lv;
	private ScrollView sv;
	private ArrayList<ItIsUser> list;
	private String[] texts;
	private String[] s = {"从相册选取", "拍照"};
	CropParams mCropParams;

	String currentScore;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		mCropParams = new CropParams(getActivity());
		list = new ArrayList<ItIsUser>();
		int[] imgs = new int[]{R.drawable.mine_img_order,
				R.drawable.mine_img_statistics, R.drawable.mine_img_news,
				/*R.drawable.mine_img_collect,*/ R.drawable.mine_img_school, 0, R.drawable.mine_img_opinion,
				R.drawable.mine_img_set, R.drawable.mine_img_abut_us};
		texts = new String[]{"预约列表", "合场记录", "消息通知", "我的驾校",
				"", "意见反馈", "安全设置", "关于我们"};
		for (int i = 0; i < texts.length; i++) {
			list.add(new ItIsUser(imgs[i], texts[i]));
		}
		super.onCreate(savedInstanceState);
		getCurrentInfo();
		EventBus.getDefault().register(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_mine, container, false);
		ButterKnife.inject(this, view);
		mMineName.setText(Utils.SPGetString(getActivity(), Utils.realName, "realName"));
//		mMineDrivingSchool.setText(Utils.SPGetString(getActivity(), Utils.schoolName, "schoolName"));
		sv = (ScrollView) view.findViewById(R.id.mine_sv);
		lv = (ListView) view.findViewById(R.id.mine_lv);

		if (Utils.SPGetInt(getActivity(), Utils.IsVip, 2) == 1) {
			isVip.setVisibility(View.VISIBLE);
		} else {
			isVip.setVisibility(View.GONE);
		}
		MineLVAdapter adapter = new MineLVAdapter(getActivity(), list);
		lv.setAdapter(adapter);
		Util.setListViewHeight(lv);
		lv.setFocusable(false);
		initListener();
		getHeaderName();
		return view;
	}

	AlertDialog alertDialog;

	private void initListener() {

		mMineHeader.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				alertDialog = new AlertDialog.Builder(getActivity()).setItems(s, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						if (i == 0) {
							mCropParams.refreshUri();
							mCropParams.enable = true;
							mCropParams.compress = false;
							Intent intent = CropHelper.buildGalleryIntent(mCropParams);
							CropHelper.clearCachedCropFile(mCropParams.uri);
							startActivityForResult(intent, CropHelper.REQUEST_CROP);
							alertDialog.dismiss();
						} else {
							mCropParams.refreshUri();
							mCropParams.enable = true;
							mCropParams.compress = false;
							Intent intent = CropHelper.buildCameraIntent(mCropParams);
							CropHelper.clearCachedCropFile(mCropParams.uri);
							startActivityForResult(intent, CropHelper.REQUEST_CAMERA);
							alertDialog.cancel();
						}
					}
				}).create();
				alertDialog.show();
			}
		});
		mScorerecord.setOnClickListener(this);
		mRecharge.setOnClickListener(this);
		mMyData.setOnClickListener(this);
		btnExit.setOnClickListener(this);
	}

	/**
	 * j接受返回的数据。
	 */

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		CropHelper.handleResult(this, requestCode, resultCode, data);
		super.onActivityResult(requestCode, resultCode, data);
		System.out.println("Mine.onActivityResult");
	}

	String uri;

	@Override
	public void onPhotoCropped(Uri uri) {
		mMineHeader.setImageURI(uri);
		uploadHeader(uri);
	}

	@Override
	public void onCompressed(Uri uri) {

	}

	@Override
	public void onCancel() {

	}

	@Override
	public void onFailed(String message) {

	}

	@Override
	public void handleIntent(Intent intent, int requestCode) {
		startActivityForResult(intent, requestCode);
	}

	@Override
	public CropParams getCropParams() {
		return mCropParams;
	}


	public boolean onTouch(View v, MotionEvent event) {
		return true;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		ButterKnife.reset(this);
		EventBus.getDefault().unregister(this);

	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void setSchoolName(SchoolName schoolName) {
//		mMineDrivingSchool.setText(schoolName.getName());
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void OnRefreshEvent(RefreshEvent event) {
		System.out.println("Mine.OnRefreshEvent");
		getCurrentInfo();
	}

	/**
	 * event 修改资料后该昵称
	 *
	 * @param name
	 */
	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onNameChange(String name) {
		System.out.println("post 2 " + name);
		mMineName.setText(name);
	}


	/**
	 * 上传头像
	 */
	private void uploadHeader(Uri uri) {
		RequestParams params = new RequestParams();
		EventBus.getDefault().post(uri);

		KLog.e(uri.getPath());
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Bitmap bitmap = BitmapFactory.decodeFile(uri.getPath());
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
		try {
			baos.close();
			byte[] buffer = baos.toByteArray();
			String s = new String(buffer);

			String s2 = Base64.encodeToString(buffer, Base64.DEFAULT);
			params.put("UserID", Utils.SPGetString(getActivity(), Utils.userId, ""));
			params.put("UserLogo", s2);

//			File file2 = new File(uri.getPath());
//			params.put("UserLogo" , file2);
//			System.out.println("参数 ： " + params.toString());
//
//			File file = new File(Environment.getEx ternalStorageDirectory().getPath() , "String.txt");
//			FileOutputStream fops = new FileOutputStream(file);
//			fops.write(buffer);
//			fops.flush();
//			fops.close();
//
//			File file1 = new File(Environment.getExternalStorageDirectory().getPath() , "Base64String.txt");
//			FileOutputStream fops2 = new FileOutputStream(file1);
//			fops2.write(s2.getBytes());
//			fops2.flush();
//			fops2.close();
			MyHttp.getInstance(getActivity()).post(MyHttp.UPLOAD_HEADER, params, new JsonHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
					super.onSuccess(statusCode, headers, response);
					KLog.e(Utils.TAG_DEBUG, response.toString());
				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 获取学员当前积分
	 */
	private void getCurrentInfo() {
		RequestParams params = new RequestParams();
		params.put("UserId", Utils.SPGetString(getActivity(), Utils.userId, ""));
		KLog.e("params", params.toString());
		MyHttp.getInstance(getActivity()).post(MyHttp.GET_CURRENT_INFO, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e("Debug", "JSONArray:");
				KLog.json(response.toString());
				try {
					JSONObject object = response.getJSONObject(0);
					currentScore = object.getString("Integral");
					tvCurrentScore.setText(currentScore);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (!hidden) {
			getHeaderName();
		}
	}

	/**
	 * 获取用户信息
	 */
	private void getHeaderName() {
		RequestParams params = new RequestParams();
		params.put("UserId", Utils.SPGetString(getActivity(), Utils.userId, ""));
		MyHttp.getInstance(getActivity()).post(MyHttp.GET_USER_HEADER, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(Utils.TAG_DEBUG, response.toString());

				try {
					JSONObject object = response.getJSONObject(0);
					final String name = object.getString("NickName");
					final String logoUrl = object.getString("UserLogo");
					Utils.SPutString(getActivity(), Utils.realName, name);
					Utils.SPutString(getActivity(), Utils.userLogo, logoUrl);
					mMineHeader.post(new Runnable() {
						@Override
						public void run() {
							mMineName.setText(name);
							ImageLoader.getInstance().displayImage(logoUrl, mMineHeader, MyApplication.options_user);
						}
					});
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				KLog.e("errorcode　", statusCode + " " + responseString);

			}
		});
	}

	@Override
	public void onClick(View view) {
		Intent intent;
		switch (view.getId()) {
			case R.id.mine_tvscore_record:
				intent = new Intent(getActivity(), IntegralDetailsActivity.class);
				intent.putExtra("currentscore", currentScore);
				startActivity(intent);
				break;
			case R.id.mine_tvrecharge:
				intent = new Intent(getActivity(), RechargeActivity.class);
				startActivityForResult(intent, 0x111);
				break;
			case R.id.mine_head_right_rl:
				intent = new Intent(getActivity(), MyDataActivity.class);
				startActivity(intent);
				break;
			case R.id.mine_btn_exitlogin:
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder.setMessage("是否确定退出？");
				builder.setPositiveButton("是", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						Intent intent1 = new Intent(getActivity(), LoginActivity.class);
						intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(intent1);
						Utils.SPPutInt(getActivity(), Utils.IsVip, 2);
						Utils.SPPutBoolean(getActivity(), Utils.isLogin, false);
						Utils.SPutString(getActivity(), Utils.schoolId, "");

					}
				});
				builder.setNegativeButton("否", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						dialogInterface.dismiss();
					}
				});
				AlertDialog dialog = builder.create();
				dialog.show();
				break;
			default:
				break;
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		getCurrentInfo();
	}
}



