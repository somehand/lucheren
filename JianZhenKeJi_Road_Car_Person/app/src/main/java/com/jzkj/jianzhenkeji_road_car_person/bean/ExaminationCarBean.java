package com.jzkj.jianzhenkeji_road_car_person.bean;

/**
 * Created by Administrator on 2016/4/9.
 *   ExaminationPriceId：考场费用及车型表ID
     ExaminationId：考场信息ID
     CarType：车型
     Gear：排挡类型1.自动2.手动
     Price：Price

 */
public class ExaminationCarBean {
    public String examinationPriceId;
    public String examinationId;
    public String carType;
    public String gear;
    public String price;

    public String getExaminationPriceId() {
        return examinationPriceId;
    }

    public void setExaminationPriceId(String examinationPriceId) {
        this.examinationPriceId = examinationPriceId;
    }

    public String getExaminationId() {
        return examinationId;
    }

    public void setExaminationId(String examinationId) {
        this.examinationId = examinationId;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public String getGear() {
        return gear;
    }

    public void setGear(String gear) {
        this.gear = gear;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
