package com.jzkj.jianzhenkeji_road_car_person.caldroid;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.roomorama.caldroid.CaldroidGridAdapter;

import java.util.Date;
import java.util.HashMap;

import hirondelle.date4j.DateTime;

public class CaldroidSampleCustomAdapter extends CaldroidGridAdapter {

	public static final int TYPE_EXAM_ROOM = 0;
	public static final int TYPE_EXAM_COACH = 1;

	public CaldroidSampleCustomAdapter(Context context, int month, int year,
	                                   HashMap<String, Object> caldroidData,
	                                   HashMap<String, Object> extraData) {
		super(context, month, year, caldroidData, extraData);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View cellView = convertView;

		// For reuse
		if (convertView == null) {
			cellView = inflater.inflate(R.layout.custom_cell, null);
		}
		HashMap<Date , Integer> datesEnable = (HashMap<Date , Integer>) extraData.get("datesEnable");
		//ArrayList<Integer> datesDisable = (ArrayList<Integer>) extraData.get("datesDisable");
		int type = (Integer) extraData.get("type");

		int topPadding = cellView.getPaddingTop();
		int leftPadding = cellView.getPaddingLeft();
		int bottomPadding = cellView.getPaddingBottom();
		int rightPadding = cellView.getPaddingRight();
		//cellView.setBackground(null);

		TextView tv1 = (TextView) cellView.findViewById(R.id.tv1);
		TextView tv2 = (TextView) cellView.findViewById(R.id.tv2);
		RelativeLayout layout = (RelativeLayout) cellView.findViewById(R.id.ly1);
		ImageView imageView = (ImageView) cellView.findViewById(R.id.iv1);
		tv1.setTextColor(Color.parseColor("#AAAAAA"));

		// Get dateTime of this cell
		DateTime dateTime = this.datetimeList.get(position);
		Resources resources = context.getResources();

		if(datesEnable.containsKey(dateTime)){
			if (type == TYPE_EXAM_ROOM) {
				layout.setBackgroundColor(context.getResources().getColor(R.color.common_blue));
			} else if (type == TYPE_EXAM_COACH) {
				imageView.setVisibility(View.VISIBLE);
			}
			tv1.setTextColor(Color.WHITE);
			tv2.setTextColor(Color.WHITE);
			tv2.setText("可预约");
		}

		/*for (int i = 0; i < datesEnable.size(); i++) {
//			System.out.println("date ::" + date.toString());
			System.out.println("datesEnable ::" + i + "   " +datesEnable.get(i).toString());

			if (datesEnable.get(i).toString().equals(date.toString())) {
				if (type == TYPE_EXAM_ROOM) {
					layout.setBackgroundColor(context.getResources().getColor(R.color.common_blue));
				} else if (type == TYPE_EXAM_COACH) {
					imageView.setVisibility(View.VISIBLE);
				}
				tv1.setTextColor(Color.WHITE);
				tv2.setTextColor(Color.WHITE);
				tv2.setText("可预约");
				break;
			}
		}*/

/*		if(datesEnable.contains(date)){
			System.out.println("111111");
			if(type == TYPE_EXAM_ROOM){
				layout.setBackgroundColor(context.getResources().getColor(R.color.common_blue));
			}else if(type == TYPE_EXAM_COACH){
				//layout.setBackgroundResource(R.drawable.shape_calendar_orange_bg);
				imageView.setVisibility(View.VISIBLE);
			}
			tv1.setTextColor(Color.WHITE);
			tv2.setTextColor(Color.WHITE);
			tv2.setText("可预约");
		}

		if (datesDisable.contains(dateTime.getDay())) {
			if (type == TYPE_EXAM_ROOM) {
				tv1.setTextColor(context.getResources().getColor(R.color.common_blue));
				tv2.setTextColor(context.getResources().getColor(R.color.common_blue));
			} else if (type == TYPE_EXAM_COACH) {
				tv1.setTextColor(context.getResources().getColor(R.color.common_orange));
				tv2.setTextColor(context.getResources().getColor(R.color.common_orange));
			}
			tv2.setText("已预约");
		}*/


		// Set color of the dates in previous / next month
		if (dateTime.getMonth() != month) {
			tv1.setTextColor(resources
					.getColor(R.color.caldroid_darker_gray));
//			tv1.setVisibility(View.INVISIBLE);
//			tv2.setVisibility(View.INVISIBLE);
			//cellView.setVisibility(View.INVISIBLE);
		}

		boolean shouldResetDiabledView = false;
		boolean shouldResetSelectedView = false;

		// Customize for disabled dates and date outside min/max dates
		/*if ((minDateTime != null && dateTime.lt(minDateTime))
				|| (maxDateTime != null && dateTime.gt(maxDateTime))
				|| (disableDates != null && disableDates.indexOf(dateTime) != -1)) {

			tv1.setTextColor(CaldroidFragment.disabledTextColor);
			if (CaldroidFragment.disabledBackgroundDrawable == -1) {
				cellView.setBackgroundResource(R.drawable.disable_cell);
			} else {
				cellView.setBackgroundResource(CaldroidFragment.disabledBackgroundDrawable);
			}

			if (dateTime.equals(getToday())) {
				cellView.setBackgroundResource(R.drawable.red_border_gray_bg);
			}

		} else {
			shouldResetDiabledView = true;
		}*/

		// Customize for selected dates
		/*if (selectedDates != null && selectedDates.indexOf(dateTime) != -1) {
			if (CaldroidFragment.selectedBackgroundDrawable != -1) {
				cellView.setBackgroundResource(CaldroidFragment.selectedBackgroundDrawable);
			} else {
				cellView.setBackgroundColor(resources
						.getColor(R.color.caldroid_sky_blue));
			}

			tv1.setTextColor(CaldroidFragment.selectedTextColor);

		} else {
			shouldResetSelectedView = true;
		}*/

		/*if (shouldResetDiabledView && shouldResetSelectedView) {
			// Customize for today
			if (dateTime.equals(getToday())) {
//				cellView.setBackgroundResource(R.drawable.red_border);
			} else {
//				cellView.setBackgroundResource(R.drawable.cell_bg);
			}
		}*/

		tv1.setText("" + dateTime.getDay());


		// Somehow after setBackgroundResource, the padding collapse.
		// This is to recover the padding
		cellView.setPadding(leftPadding, topPadding, rightPadding,
				bottomPadding);

		// Set custom color if required
		setCustomResources(dateTime, cellView, tv1);

		return cellView;
	}

}



/*
package com.jzkj.jianzhenkeji_road_car_person.caldroid;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.roomorama.caldroid.CaldroidGridAdapter;
import com.socks.library.KLog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import hirondelle.date4j.DateTime;

public class CaldroidSampleCustomAdapter extends CaldroidGridAdapter {

	public static final int TYPE_EXAM_ROOM = 0;
	public static final int TYPE_EXAM_COACH = 1;

	public CaldroidSampleCustomAdapter(Context context, int month, int year,
			HashMap<String, Object> caldroidData,
			HashMap<String, Object> extraData) {
		super(context, month, year, caldroidData, extraData);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View cellView = convertView;

		// For reuse
		if (convertView == null) {
			cellView = inflater.inflate(R.layout.custom_cell, null);
		}
		ArrayList<Integer> datesEnable = (ArrayList<Integer>) extraData.get("datesEnable");
		ArrayList<Integer> datesDisable = (ArrayList<Integer>) extraData.get("datesDisable");
		int type = (Integer)extraData.get("type");

		int topPadding = cellView.getPaddingTop();
		int leftPadding = cellView.getPaddingLeft();
		int bottomPadding = cellView.getPaddingBottom();
		int rightPadding = cellView.getPaddingRight();
		//cellView.setBackground(null);

		TextView tv1 = (TextView) cellView.findViewById(R.id.tv1);
		TextView tv2 = (TextView) cellView.findViewById(R.id.tv2);
		RelativeLayout layout = (RelativeLayout) cellView.findViewById(R.id.ly1);
		ImageView imageView = (ImageView) cellView.findViewById(R.id.iv1);
		tv1.setTextColor(Color.parseColor("#AAAAAA"));
		//layout.setBackgroundColor(context.getResources().getColor(R.color.common_blue));

		// Get dateTime of this cell
		DateTime dateTime = this.datetimeList.get(position);
		Resources resources = context.getResources();

		Calendar calendar = new GregorianCalendar();
		calendar.set(Calendar.MONTH , dateTime.getMonth());
		calendar.set(Calendar.DAY_OF_MONTH , dateTime.getDay());
		Date date = calendar.getTime();
		//KLog.e(Utils.TAG_DEBUG , "date...."+ date.toString());
*/
/*
		for (int i = 0; i < datesEnable.size(); i++) {
			MonthDayBean monthDayBean = datesEnable.get(i);
//			if(calendar1.get(Calendar.MONTH) == dateTime.getMonth() && calendar1.get(Calendar.DAY_OF_MONTH) == dateTime.getDay()){
			if(monthDayBean.getMonth() == dateTime.getMonth() && monthDayBean.getDay() == dateTime.getDay()){
				//layout = (RelativeLayout) cellView.findViewById(R.id.ly1);
				layout.setBackgroundColor(context.getResources().getColor(R.color.common_blue));
				KLog.e(Utils.TAG_DEBUG , "date...." + date.toString());

				if(type == TYPE_EXAM_ROOM){
					KLog.e(Utils.TAG_DEBUG , "1");
					layout.setBackgroundColor(context.getResources().getColor(R.color.common_blue));
					KLog.e(Utils.TAG_DEBUG , "1111");

				}else if(type == TYPE_EXAM_COACH){
					KLog.e(Utils.TAG_DEBUG , "2");

					//layout.setBackgroundResource(R.drawable.shape_calendar_orange_bg);
					imageView.setVisibility(View.VISIBLE);
				}
				tv1.setTextColor(Color.WHITE);
				tv2.setTextColor(Color.WHITE);
				tv2.setText("可预约");
				break;
			}
		}*//*


		if(datesEnable.contains(date)){
			KLog.e(Utils.TAG_DEBUG , "date...." + date.toString());
			if(type == TYPE_EXAM_ROOM){
				layout.setBackgroundColor(context.getResources().getColor(R.color.common_blue));
			}else if(type == TYPE_EXAM_COACH){
				//layout.setBackgroundResource(R.drawable.shape_calendar_orange_bg);
				imageView.setVisibility(View.VISIBLE);
			}
			tv1.setTextColor(Color.WHITE);
			tv2.setTextColor(Color.WHITE);
			tv2.setText("可预约");
		}

		if(datesDisable.contains(dateTime.getDay())){
			if(type == TYPE_EXAM_ROOM) {
				tv1.setTextColor(context.getResources().getColor(R.color.common_blue));
				tv2.setTextColor(context.getResources().getColor(R.color.common_blue));
			}else if (type == TYPE_EXAM_COACH){
				tv1.setTextColor(context.getResources().getColor(R.color.common_orange));
				tv2.setTextColor(context.getResources().getColor(R.color.common_orange));
			}
			tv2.setText("已预约");
		}

		// Set color of the dates in previous / next month
		if (dateTime.getMonth() != month) {
			tv1.setTextColor(resources
					.getColor(R.color.caldroid_darker_gray));
//			tv1.setVisibility(View.INVISIBLE);
//			tv2.setVisibility(View.INVISIBLE);
			//cellView.setVisibility(View.INVISIBLE);
		}

		boolean shouldResetDiabledView = false;
		boolean shouldResetSelectedView = false;

		// Customize for disabled dates and date outside min/max dates
		*/
/*if ((minDateTime != null && dateTime.lt(minDateTime))
				|| (maxDateTime != null && dateTime.gt(maxDateTime))
				|| (disableDates != null && disableDates.indexOf(dateTime) != -1)) {

			tv1.setTextColor(CaldroidFragment.disabledTextColor);
			if (CaldroidFragment.disabledBackgroundDrawable == -1) {
				cellView.setBackgroundResource(R.drawable.disable_cell);
			} else {
				cellView.setBackgroundResource(CaldroidFragment.disabledBackgroundDrawable);
			}

			if (dateTime.equals(getToday())) {
				cellView.setBackgroundResource(R.drawable.red_border_gray_bg);
			}

		} else {
			shouldResetDiabledView = true;
		}*//*


		// Customize for selected dates
		*/
/*if (selectedDates != null && selectedDates.indexOf(dateTime) != -1) {
			if (CaldroidFragment.selectedBackgroundDrawable != -1) {
				cellView.setBackgroundResource(CaldroidFragment.selectedBackgroundDrawable);
			} else {
				cellView.setBackgroundColor(resources
						.getColor(R.color.caldroid_sky_blue));
			}

			tv1.setTextColor(CaldroidFragment.selectedTextColor);

		} else {
			shouldResetSelectedView = true;
		}*//*


		if (shouldResetDiabledView && shouldResetSelectedView) {
			// Customize for today
			if (dateTime.equals(getToday())) {
//				cellView.setBackgroundResource(R.drawable.red_border);
			} else {
//				cellView.setBackgroundResource(R.drawable.cell_bg);
			}
		}

		tv1.setText("" + dateTime.getDay());


		// Somehow after setBackgroundResource, the padding collapse.
		// This is to recover the padding
		cellView.setPadding(leftPadding, topPadding, rightPadding,
				bottomPadding);

		// Set custom color if required
		setCustomResources(dateTime, cellView, tv1);

		return cellView;
	}

}
*/
