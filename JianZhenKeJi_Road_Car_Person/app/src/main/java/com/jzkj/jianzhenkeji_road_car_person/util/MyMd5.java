package com.jzkj.jianzhenkeji_road_car_person.util;

import java.security.MessageDigest;

public class MyMd5 {
	/**
	 * MD5加密
	 *
	 * @param str
	 * @return String
	 */
	public static String getMd5(String str) {
		MessageDigest digest = null;
		try {
			digest = MessageDigest.getInstance("MD5");
			digest.reset();
			digest.update(str.getBytes("UTF-8"));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		byte[] byteArray = digest.digest();
		StringBuffer md5stringBuffer = new StringBuffer();
		for (int i = 0; i < byteArray.length; i++) {
			if (Integer.toHexString(0xff & byteArray[i]).length() == 1) {
				md5stringBuffer.append("0").append(Integer.toHexString(0xff & byteArray[i]));
			} else {
				md5stringBuffer.append(Integer.toHexString(0xff & byteArray[i]));
			}
		}
		//16位加密，从第9位到第25位
//		return md5stringBuffer.substring(8, 24).toString().toUpperCase();
		//32位大写加密
		return md5stringBuffer.toString().toUpperCase();
	}
}
