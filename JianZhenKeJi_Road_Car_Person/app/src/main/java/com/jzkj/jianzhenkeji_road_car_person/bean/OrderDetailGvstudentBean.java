package com.jzkj.jianzhenkeji_road_car_person.bean;

/**
 * Created by Administrator on 2016/4/13.
 */
public class OrderDetailGvstudentBean {
	private String img;
	private String studentName;
	private String practiceId;
	private int IsPay;

	public int getIsPay() {
		return IsPay;
	}

	public void setIsPay(int isPay) {
		IsPay = isPay;
	}

	public String getPracticeId() {
		return practiceId;
	}

	public void setPracticeId(String practiceId) {
		this.practiceId = practiceId;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public OrderDetailGvstudentBean() {
	}
}
