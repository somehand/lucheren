package com.jzkj.jianzhenkeji_road_car_person.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.bean.MyDrivingSchoolBean;

import java.util.ArrayList;

public class SelectSchoolLVAdapter extends BaseAdapter{

	private Context context;
	private ArrayList<MyDrivingSchoolBean> list;
	private LayoutInflater inflater;

	public SelectSchoolLVAdapter(Context context, ArrayList<MyDrivingSchoolBean> list) {
		super();
		this.context = context;
		this.list = list;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list == null? 0:list.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		viewHolder holder = null;
		if (arg1 == null) {
			arg1 = inflater.inflate(R.layout.select_driving_school_lv_item, null);
		}
		holder = (viewHolder) arg1.getTag();
		if (holder == null) {
			holder = new viewHolder();
			holder.tvSchoolName = (TextView) arg1.findViewById(R.id.select_driving_school_text);
		}
		holder.clear();
		MyDrivingSchoolBean myDrivingSchoolBean= list.get(arg0);
		holder.tvSchoolName.setText(myDrivingSchoolBean.getSchoolName());
		return arg1;
	}
	class viewHolder{
		private TextView tvSchoolName;
		void clear(){
			tvSchoolName.setText("");
		}
	}
}
