package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.adapter.OrderNewsAdapter;
import com.jzkj.jianzhenkeji_road_car_person.bean.OrderNewsBean;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.kanak.emptylayout.EmptyLayout;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class GroupBuyActivity extends Activity {

	@InjectView(R.id.headframe_ib)
	ImageButton mHeadframeIb;
	@InjectView(R.id.headframe_title)
	TextView mHeadframeTitle;
	@InjectView(R.id.group_buy_lv)
	ListView mGroupBuyLv;

	PullToRefreshListView mPullToRefreshListView;


	OrderNewsAdapter adapter;
	private EmptyLayout mEmptyLayout;

	private ArrayList<OrderNewsBean> list;
	int nowPage = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_group_buy);
		ButterKnife.inject(this);
		mHeadframeTitle.setText("团购消息");
		mEmptyLayout = new EmptyLayout(this, mGroupBuyLv);
		//initData();
		setListener();
	}

	@Override
	protected void onResume() {
		super.onResume();
		initData();
	}

	private void setListener() {
		mHeadframeIb.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});
		mGroupBuyLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				Intent intent = new Intent(GroupBuyActivity.this, MyOrderNewsDetailsActivity.class);
				intent.putExtra("bean", list.get(i));
				startActivity(intent);
			}
		});
	}

	private void initData() {
		list = new ArrayList<OrderNewsBean>();

		getInfo();

	}

	private void getInfo() {
		RequestParams params = new RequestParams();
		params.put("pageindex", nowPage);
		params.put("pagesize", 10);
		params.put("userid", Utils.SPGetString(GroupBuyActivity.this, Utils.userId, ""));
		KLog.e("params", params.toString());
		MyHttp.getInstance(this).post(MyHttp.GET_GROUPBUY_INFO, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				/*try {
					Utils.ToastShort(GroupBuyActivity.this, response.getString("Reason"));
				} catch (JSONException e) {
					e.printStackTrace();
				}*/
				mEmptyLayout.setEmptyMessage("教练还没有对你发起团购消息~");
				mEmptyLayout.showEmpty();
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
				for (int i = 0; i < response.length(); i++) {
					try {
						JSONObject object = response.getJSONObject(i);
						OrderNewsBean bean = new OrderNewsBean();
						bean.setHeaderUrl(object.getString("UserLogo"));
//						bean.setName(object.getString("UserId"));
						bean.setContent(object.getString("MsgContent"));
						bean.setName(object.getString("UserName"));
//						bean.setNewsId(object.getInt("RC_MessageId"));
						bean.setTime(object.getString("PushDate"));
						bean.setNewsId(Integer.parseInt(object.getString("ExaminationId")));
						bean.setTypes(3);
						bean.setStatus(object.getInt("status"));
						/*if (TextUtils.isEmpty(object.getString("types"))) {
							bean.setTypes(0);
						}else{
							bean.setTypes(Integer.parseInt(object.getString("types")));
						}*/
						list.add(bean);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				adapter = new OrderNewsAdapter(GroupBuyActivity.this, R.layout.order_news_lv_item);
				mGroupBuyLv.setAdapter(adapter);
				adapter.addAll(list);
				if (list.size() == 0) {
					mEmptyLayout.setEmptyMessage("教练还没有对你发起团购消息~");
					mEmptyLayout.showEmpty();
				}
			}
		});
	}
}
