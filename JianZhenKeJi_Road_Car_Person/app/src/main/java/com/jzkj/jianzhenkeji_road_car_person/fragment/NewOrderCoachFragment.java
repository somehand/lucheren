package com.jzkj.jianzhenkeji_road_car_person.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.MyApplication;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.activity.ChooseCoachActivity;
import com.jzkj.jianzhenkeji_road_car_person.activity.HomeActivity;
import com.jzkj.jianzhenkeji_road_car_person.activity.MyCoachActivity;
import com.jzkj.jianzhenkeji_road_car_person.activity.OrderDetailActivity;
import com.jzkj.jianzhenkeji_road_car_person.adapter.MyGridAdapter;
import com.jzkj.jianzhenkeji_road_car_person.bean.OrderCoachBean;
import com.jzkj.jianzhenkeji_road_car_person.util.CenterShowHorizontalScrollView;
import com.jzkj.jianzhenkeji_road_car_person.util.MyGridView;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.kanak.emptylayout.EmptyLayout;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class NewOrderCoachFragment extends Fragment implements View.OnTouchListener, View.OnClickListener {
	public static String TAG = "NewOrderCoachFragment";

	@InjectView(R.id.gridview)
	MyGridView gridView;
	@InjectView(R.id.realName_tv)
	TextView realName;
	@InjectView(R.id.Coachtotal_tv)
	TextView coachtotal;
	@InjectView(R.id.head_img)
	ImageView head_img;
	@InjectView(R.id.scrollview1)
	CenterShowHorizontalScrollView scrollview;
	@InjectView(R.id.commitorder)
	Button comitBt;
	@InjectView(R.id.year)
	TextView year;
	@InjectView(R.id.plate_number)
	TextView plateNumber;
	@InjectView(R.id.linearLayout)
	LinearLayout llMyCoach;

	HomeActivity homeActivity;
	String currentDate;
	private ArrayList<OrderCoachBean> coachDatas;
	View view = null;
	EmptyLayout layout;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (view == null) {
			view = inflater.inflate(R.layout.new_fragment_order_coach, null);
			view.setOnTouchListener(this);
			ButterKnife.inject(this, view);
			vip = Utils.SPGetInt(getActivity(), Utils.IsVip, 2);
			homeActivity = (HomeActivity) getActivity();
			initview();
			initListener();
			layout = new EmptyLayout(getActivity(), gridView);
			layout.setEmptyMessage("对不起,当前日期没有可预约时段~");
		}
		return view;
	}

	AlertDialog.Builder dialog;

	@OnClick(R.id.commitorder)
	public void commit() {
		if (currentcoachbean == null)
			return;
		new AlertDialog.Builder(getActivity()).setMessage("是否确定预约？").setNegativeButton("确定", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {
				commitOrder();
				dialogInterface.dismiss();
			}
		}).setNeutralButton("取消", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {
				dialogInterface.dismiss();
			}
		}).create().show();

	}

	String CoachBookingsDateId;

	//提交预约
	public void commitOrder() {
		CoachBookingsDateId = currentcoachbean.getCoachBookingsDateId();
		RequestParams params = new RequestParams();
		params.put("UserId", Utils.SPGetString(getActivity(), Utils.userId, ""));
		params.put("CoachBookingsDateId", CoachBookingsDateId);
		params.put("Type", 1);
		KLog.e("params", params.toString());

		MyHttp.getInstance(getActivity()).post(MyHttp.COMMIT_ORDER, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e("Debug", "JSONObject:");
				KLog.json(response.toString());
				try {
					if (response.getString("Code").equals("2")) {
						new AlertDialog.Builder(getActivity()).setMessage("此时间段预约已满").create().show();
						return;
					}
					if (response.getString("Code").equals("0")) {
						new AlertDialog.Builder(getActivity()).setMessage(response.getString("Reason")).create().show();
						return;
					}
					Intent intent = new Intent(getActivity(), OrderDetailActivity.class);
					intent.putExtra("CoachBookingsDateId", CoachBookingsDateId);
					intent.putExtra("Studentbookingsid", response.getString("Result"));
					intent.putExtra("coachName", realName.getText().toString());
					intent.putExtra("flag", 3);
					startActivity(intent);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}


	/*
	 *	取得今天到15天后的日期并加载到界面
	 *	cid: 记录当前被选中的radiobutton的ID;
	 *	butpostion 记录第几个时间按钮
	 */
	int cid = 0;
	int butpostion = 1;
	MyGridAdapter myGridAdapter;
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	int data_range;

	public void initview() {
		myGridAdapter = new MyGridAdapter(getActivity(), R.layout.coach_griditem, this);
		gridView.setAdapter(myGridAdapter);
		if (vip == 1) {//是vip
			data_range = 7;
		} else {
			data_range = 5;
		}
		for (int i = 0; i < data_range; i++) {
			final TextView titleItem = new TextView(getActivity());
			scrollview.addItemView(titleItem, i);
			titleItem.setOnClickListener(this);
			titleItem.setTextColor(getResources().getColor(R.color.white));
			String time = df.format(new Date().getTime() + i * 24 * 60 * 60 * 1000);
			titleItem.setText(time.substring(5, time.length()).replaceAll("-", "月") + "日");
			titleItem.setTag(time);
			LinearLayout.LayoutParams titleParams = (LinearLayout.LayoutParams) titleItem.getLayoutParams();
			titleParams.leftMargin = 20;
			titleParams.rightMargin = 20;
			if (i == 0) {
				CurrentView = titleItem;
				titleItem.setTextSize(20);
				currentDate = titleItem.getTag().toString();
			}
		}
//		getMyCoach_1();
//		getMyCoach_2();
	}

	int vip;
	private View currentitem = null;
	private OrderCoachBean currentcoachbean = null;

	public void initListener() {
		gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view1, int i, long l) {
				View view = view1.findViewById(R.id.liner_item);
				if (currentitem == null) {
					view.setBackgroundResource(R.drawable.grid_bg4);
				} else {
					currentitem.setBackgroundResource(R.drawable.grid_bg3);
					view.setBackgroundResource(R.drawable.grid_bg4);
				}

				currentitem = view;
				currentcoachbean = coachDatas.get(i);
			}
		});
		llMyCoach.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(getActivity(), MyCoachActivity.class);
				intent.putExtra("plateNumber", platenumber);
				startActivity(intent);
			}
		});
	}

	public void clickBooking(String mCoachBookingsDateId) {
		Intent intent = new Intent(getActivity(), OrderDetailActivity.class);
		intent.putExtra("CoachBookingsDateId", mCoachBookingsDateId);
		intent.putExtra("Studentbookingsid", "0");
		intent.putExtra("coachName", realName.getText().toString());
		intent.putExtra("flag", 4);
		startActivity(intent);
	}

	public int getButpostion() {
		return butpostion;
	}

	int i = 0;
	String platenumber;

	//取教练的名字 头像
	public void getMyCoach_1() {
		String coachid = Utils.SPGetString(getActivity(), Utils.coachId, "");
		if (homeActivity.getItem() == 1) {
			i++;
			if (coachid.length() == 0) {
				new AlertDialog.Builder(getActivity()).setMessage("你还没有教练,是否绑定？").setNeutralButton("是", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						Intent intent = new Intent(getActivity(), ChooseCoachActivity.class);
						getActivity().startActivity(intent);
						dialogInterface.dismiss();
					}
				}).setNegativeButton("否", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						dialogInterface.dismiss();
					}
				}).show();
			}
		}
		RequestParams params = new RequestParams();
		params.put("coachid", coachid);
		params.put("type", 1);
		KLog.e("params", params.toString());
		MyHttp.getInstance(getActivity()).post(MyHttp.GET_MY_COACH, params, new JsonHttpResponseHandler() {

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
				Log.d("ssss", response.toString());
				try {
					JSONObject object = (JSONObject) response.get(0);
					if (TextUtils.isEmpty(object.getString("RealName"))) {
						realName.setText("");
					} else {
						realName.setText(object.getString("RealName"));
					}
					year.setText("教龄:" + object.getString("Driving") + "年");
					platenumber = object.getString("LicensePlate");
					plateNumber.setText("车牌号:" + platenumber);
					ImageLoader.getInstance().displayImage(object.getString("UserLogo"), head_img, MyApplication.options_user);
					Utils.SPutString(getActivity(), Utils.coachLogo, object.getString("UserLogo"));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

		});
	}

	//取得教练已经被预约的次数
	public void getMyCoach_2() {
		RequestParams params = new RequestParams();
		params.put("coachid", Utils.SPGetString(getActivity(), Utils.coachId, ""));
		params.put("type", 2);
		KLog.e("params", params.toString());
		MyHttp.getInstance(getActivity()).post(MyHttp.GET_MY_COACH, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
				try {
					JSONObject object = (JSONObject) response.get(0);
					coachtotal.setText("已被预约:" + object.getString("total") + "次");
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
			}
		});
	}

	ProgressDialog mDialog;

	//取得教练的预约列表
	public void CoachDate(String subjectNum, String date) {
		mDialog = new ProgressDialog(getActivity());
		mDialog.setMessage("正在加载中...");
		mDialog.setIndeterminate(true);
		mDialog.setCanceledOnTouchOutside(false);
		mDialog.show();
		if (date.equals("")) {
			date = currentDate;
		}
		currentcoachbean = null;
		coachDatas = new ArrayList<OrderCoachBean>();
		RequestParams params = new RequestParams();
		params.put("userid", Utils.SPGetString(getActivity(), Utils.userId, ""));
		params.put("CoachId", Utils.SPGetString(getActivity(), Utils.coachId, ""));
		params.put("Subjectnum", subjectNum);
		params.put("TeachType", Utils.SPGetString(getActivity(), Utils.TeachType, ""));
		params.put("Date", date);
		if (vip == 1) {//1vip  2非vip
			params.put("Isvip", 1);//0非vip  1vip
		} else {
			params.put("Isvip", 0);
		}

		KLog.e("params", params.toString());
		MyHttp.getInstance(getActivity()).post(MyHttp.GET_MY_COACH_DATE, params, new JsonHttpResponseHandler() {

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e("2016-06-02");
				KLog.json(response.toString());
				for (int i = 0; i < response.length(); i++) {
					try {
						JSONObject jsonObject = (JSONObject) response.get(i);
						OrderCoachBean orderCoachBean = new OrderCoachBean();
						orderCoachBean.setCoachBookingsDateId(jsonObject.getString("CoachBookingsDateId"));
						orderCoachBean.setStartStopDate(jsonObject.getString("StartStopDate"));
						orderCoachBean.setSubjectNum(jsonObject.getString("SubjectNum"));
						orderCoachBean.setYiyuenum(jsonObject.getInt("yiyuenum"));
						orderCoachBean.setTeachType(jsonObject.getString("TeachType"));
						orderCoachBean.setKeyuenum(jsonObject.getInt("keyuenum") < 0 ? 0 : jsonObject.getInt("keyuenum"));
						orderCoachBean.setIsBooking(jsonObject.getInt("isBooking"));
						orderCoachBean.setIsAppointemt(jsonObject.getInt("IsAppointment"));
						orderCoachBean.setIsyue(jsonObject.getString("isyue"));//isyue  string 1.可以预约，2.不可以预约
						orderCoachBean.setCarNum(jsonObject.getString("carNum"));//车牌号
						coachDatas.add(orderCoachBean);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				if (comitBt != null)
					comitBt.postDelayed(new Runnable() {
						@Override
						public void run() {
							if (comitBt != null)
								comitBt.setVisibility(View.VISIBLE);
						}
					}, 0);
				myGridAdapter.clear();
				myGridAdapter.addAll(coachDatas);
				myGridAdapter.notifyDataSetChanged();
				mDialog.cancel();
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e("debug", response.toString());
				mDialog.cancel();
				try {
					if ("0".equals(response.getString("Code")) && "0".equals(response.getString("Count"))) {
						comitBt.postDelayed(new Runnable() {
							@Override
							public void run() {
								comitBt.setVisibility(View.INVISIBLE);
							}
						}, 0);
						myGridAdapter.clear();
						layout.showEmpty();
					} else {
						layout.showError();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

		});
	}

	@Override
	public void onResume() {
		super.onResume();
		getMyCoach_1();
		getMyCoach_2();
		CoachDate(homeActivity.getSubjectnum(), currentDate);
	}


	@Override
	public void onDestroyView() {
		super.onDestroyView();
		ButterKnife.reset(this);
	}


	@Override
	public boolean onTouch(View view, MotionEvent motionEvent) {
		return true;
	}

	public TextView CurrentView = null;

	@Override
	public void onClick(View view) {
		TextView tv = (TextView) view;
		if (CurrentView != null) {
			CurrentView.setTextSize(15);
		}
		String time = df.format(new Date().getTime());
		if (time.equals(tv.getTag().toString()))
			butpostion = 1;
		else
			butpostion = 2;
		tv.setTextSize(20);
		CurrentView = tv;
		currentDate = tv.getTag().toString();
		scrollview.onClicked(view);
		CoachDate(homeActivity.getSubjectnum(), currentDate);
	}
}
