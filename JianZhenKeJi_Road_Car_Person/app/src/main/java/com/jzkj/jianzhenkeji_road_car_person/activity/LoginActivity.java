package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.jzkj.jianzhenkeji_road_car_person.BaseActivity;
import com.jzkj.jianzhenkeji_road_car_person.CircleImageView;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.network.HttpUtils;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.MyMd5;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Set;

import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;

public class LoginActivity extends BaseActivity implements OnClickListener {
	//返回码
	public static final int REQUEST_CODE = 1;
	public static final int RESULT_CODE = 101;

	private EditText etname, etpassword;
	private TextView tvRegist, tvForgetpw, tvTitle;
	private CircleImageView civ;
	private Button btnLogin;
	private ImageButton ibQq, ibWchat, ibBack;
	private Intent intent;
	private boolean flag = false;
	//	private ProgressDialog progressDialog;
	private String str;
	private String name;
	private String password;
	private ProgressDialog progressDialog;

	/**
	 * 使用Handler更新UI
	 */
	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
				case 1:
					showToast(LoginActivity.this, "登录成功", Toast.LENGTH_SHORT);
					break;
				case 2:
					showToast(LoginActivity.this, "登录失败", Toast.LENGTH_SHORT);
					break;
				case 3:
					Toast.makeText(LoginActivity.this, str, Toast.LENGTH_LONG).show();
					break;

				default:
					break;
			}
		}
	};
	private String userName;
	private String passWord1;
	private boolean login;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		init();
//        	etname.setText(Util.user.getUserMobile());
		bindListener();

	}

	/**
	 * 初始化绑定id
	 */
	private void init() {
		etname = (EditText) findViewById(R.id.login_et1);
		etpassword = (EditText) findViewById(R.id.login_et2);
		tvForgetpw = (TextView) findViewById(R.id.login_forgetpw);
		tvRegist = (TextView) findViewById(R.id.login_regist);
		btnLogin = (Button) findViewById(R.id.login_login);
		ibQq = (ImageButton) findViewById(R.id.login_qq);
		ibWchat = (ImageButton) findViewById(R.id.login_wchat);
		ibBack = (ImageButton) findViewById(R.id.headframe_ib);
		tvTitle = (TextView) findViewById(R.id.headframe_title);
		civ = (CircleImageView) findViewById(R.id.lgin_civ);

//		ImageLoader.getInstance().displayImage(Utils.SPGetString(LoginActivity.this));

	}

	@Override
	protected void onResume() {
		super.onResume();
		userName = getIntent().getStringExtra("username");
		passWord1 = getIntent().getStringExtra("password");
		login = getIntent().getBooleanExtra("isLogin", false);
		if (login) {
			etname.setText(userName);
			etpassword.setText(passWord1);
		}
	}

	private void bindListener() {
		btnLogin.setOnClickListener(new NoDoubleClickListenner() {
			@Override
			public void onNoDoubleClick(View v) {
				super.onNoDoubleClick(v);
				name = etname.getText().toString().trim();
				password = etpassword.getText().toString().trim();
				System.out.println(name + "----------->" + password);
				if (name.equals("") || name == null || password.equals("") || password == null) {
					showToast(LoginActivity.this, "手机号或密码不能为空", Toast.LENGTH_SHORT);
				} else {
					getLogin();
					if (HttpUtils.checkNetworkAvailable(LoginActivity.this) == false) {
						AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
						builder.setMessage("当前无可用网络，是否进行网络设置...");
						builder.setPositiveButton("是", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialogInterface, int i) {
								intent = new Intent(Settings.ACTION_SETTINGS);
								startActivity(intent);
							}
						});
						builder.setNegativeButton("否", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialogInterface, int i) {
								dialogInterface.dismiss();
								progressDialog.cancel();
							}
						});
						AlertDialog alertDialog = builder.create();
						alertDialog.show();
					}
				}
			}
		});
		tvForgetpw.setOnClickListener(this);
		tvRegist.setOnClickListener(this);
		ibQq.setOnClickListener(this);
		ibWchat.setOnClickListener(this);

	}

	/**
	 * 设置监听
	 *
	 * @param arg0
	 */
	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
			case R.id.login_regist://注册
				intent = new Intent(LoginActivity.this, RegistActivity.class);
				startActivity(intent);
//				finish();
				break;
			case R.id.login_forgetpw:
				Intent intentm = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
				startActivity(intentm);
				break;
			/*case R.id.login_login:
				if (HttpUtils.checkNetworkAvailable(LoginActivity.this) == false) {
					AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
					builder.setMessage("当前无可用网络，是否进行网络设置...");
					builder.setPositiveButton("是", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {//ACTION_WIRELESS_SETTINGS
							intent = new Intent(Settings.ACTION_SETTINGS);
							startActivity(intent);
						}
					});
					builder.setNegativeButton("否", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							dialogInterface.dismiss();
						}
					});
					AlertDialog alertDialog = builder.create();
					alertDialog.show();
				}
				getLogin();
				break;*/
			case R.id.login_qq:

				break;
			case R.id.login_wchat:

				break;
			/*case R.id.login_et1:
				if (flag) {
					*//*etname.setText(etname.getText().toString());
					Spannable spannable = etname.getText();
					Selection.selectAll(spannable);
					etname.setSelectAllOnFocus(true);*//*
					etname.setText("");
				}
				break;
			case R.id.login_et2:
				if (flag) {
					*//*etpassword.setText(etpassword.getText().toString());
					Spannable spannable1 = etpassword.getText();
					Selection.selectAll(spannable1);
					etpassword.setSelectAllOnFocus(true);*//*
					etpassword.setText("");
				}
				break;*/

			default:
				break;
		}

	}

	/**
	 * 获取登录数据
	 */
	private void getLogin() {
		progressDialog = new ProgressDialog(LoginActivity.this);
		progressDialog.setMessage("正在登陆中...");
		progressDialog.show();

		final String md5pw = MyMd5.getMd5(password);
		Log.e("zhangshuhua", password + "-------------");
		RequestParams params = new RequestParams();
		params.put("UserName", name);
		params.put("PassWord", password);
		Utils.SPutString(this, Utils.passWord, password);
		params.put("Type", 1);
		Log.e("UserName", name);
		Log.e("PassWord", md5pw);
		MyHttp.getInstance(LoginActivity.this).post(MyHttp.USER_LOGIN, params, new JsonHttpResponseHandler(
		) {

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonobject:");
				KLog.json(response.toString());
				try {
					str = response.getString("Reason");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							Thread.sleep(1500);
							progressDialog.cancel();
							handler.sendEmptyMessage(3);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}

				}).start();
				etname.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View view) {
						etname.setText("");
					}
				});
				etpassword.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View view) {
						etpassword.setText("");
					}
				});

			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				try {
					JSONObject jsonObject = response.getJSONObject(0);
					KLog.e(Utils.TAG_DEBUG, "jsonArray :");
					KLog.json(Utils.TAG_DEBUG, response.toString());

					Utils.SPPutBoolean(LoginActivity.this, "isLogin", true);
					Toast.makeText(LoginActivity.this, "登录成功(^_^)", Toast.LENGTH_LONG).show();
					Utils.SPutString(LoginActivity.this, Utils.userId, jsonObject.getString("UserId"));
					Utils.SPutString(LoginActivity.this, Utils.userName, jsonObject.getString("UserName"));

					Utils.SPutString(LoginActivity.this, Utils.realName, jsonObject.getString("RealName"));
					Utils.SPutString(LoginActivity.this, Utils.nickName, jsonObject.getString("NickName"));
					Utils.SPutString(LoginActivity.this, Utils.idCarNumber, jsonObject.getString("IDCardNumber"));
					Utils.SPutString(LoginActivity.this, Utils.userSex, jsonObject.getString("UserSex"));
					Utils.SPutString(LoginActivity.this, Utils.userAddress, jsonObject.getString("UserAddress"));
					Utils.SPutString(LoginActivity.this, Utils.userLogo, jsonObject.getString("UserLogo"));
					Utils.SPutString(LoginActivity.this, Utils.schoolName, jsonObject.getString("SchoolName"));

					Utils.SPPutInt(LoginActivity.this, Utils.IsVip, jsonObject.getInt("IsVip"));
					Utils.SPPutBoolean(LoginActivity.this, Utils.isLogin, true);
					Utils.SPutString(LoginActivity.this, Utils.schoolId, jsonObject.getString("DrivingSchoolId"));
					Utils.SPutString(LoginActivity.this, Utils.coachId, jsonObject.getString("CoachId"));
					Utils.SPutString(LoginActivity.this, Utils.CoachName, jsonObject.getString("CoachName"));
					Utils.SPutString(LoginActivity.this, Utils.SchoolUrl, jsonObject.getString("SchoolProfiles"));
					Utils.SPutString(LoginActivity.this, Utils.TeachType, jsonObject.getString("TeachType"));
					if (jsonObject.getInt("DrivingType") == 1) {
						Utils.SPutString(LoginActivity.this, Utils.DrivingType, "C1");
					} else {
						Utils.SPutString(LoginActivity.this, Utils.DrivingType, "C2");
					}
					setAlias(name);
					progressDialog.cancel();
					Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
					intent.putExtra("userpw", md5pw).addCategory("user_pw");
					intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
					startActivity(intent);

					finish();
				} catch (JSONException e) {
					e.printStackTrace();
				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				progressDialog.cancel();
			}
		});
//		}
	}


	public abstract class NoDoubleClickListenner implements OnClickListener {
		public static final int MIN_CLICK_DELAY_TIME = 5000;
		private long lastClickTime = 0;

		@Override
		public void onClick(View v) {
			long currentTime = Calendar.getInstance().getTimeInMillis();
			if (currentTime - lastClickTime > MIN_CLICK_DELAY_TIME) {
				lastClickTime = currentTime;
				onNoDoubleClick(v);
			}
		}

		public void onNoDoubleClick(View v) {

		}
	}


	private void setAlias(String name) {
		mHandler.sendMessage(mHandler.obtainMessage(12, name));

	}

	TagAliasCallback mAliasCallback = new TagAliasCallback() {
		@Override
		public void gotResult(int code, String alias, Set<String> tags) {
			switch (code) {
				case 0:
					KLog.e("alias", "别名设置成功");
					break;
				case 6002:
					KLog.e("alias", "别名设置失败， 60秒后重试");
					mHandler.sendMessageDelayed(mHandler.obtainMessage(12, alias), 1000 * 60);
					break;
				default:
					KLog.e("alias", "别名设置失败， errorCode = " + code);
					break;
			}
		}
	};

	Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
				case 12:
					JPushInterface.setAlias(getApplicationContext(), (String) msg.obj, mAliasCallback);
					break;
			}

		}
	};
}
