package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;

/**
 * Created by monster on 2016/3/27.
 */
public class TipActivity_2 extends Activity{
	ImageView mImageView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tip_1_layout);
		Utils.SPPutBoolean(this,Utils.tips_1_Showed ,true);
		mImageView = (ImageView) findViewById(R.id.tips_1_iv);
		mImageView.setImageResource(R.drawable.tips_2);
		Utils.SPPutBoolean(this,Utils.tips_2_Showed ,true);

		mImageView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(i == 0){
					mImageView.setImageResource(R.drawable.tips_3);
					Utils.SPPutBoolean(TipActivity_2.this,Utils.tips_3_Showed ,true);

					i++;
				}else{
					finish();
					overridePendingTransition(0 , 0);
				}
			}
		});

	}

	int i = 0;

}
