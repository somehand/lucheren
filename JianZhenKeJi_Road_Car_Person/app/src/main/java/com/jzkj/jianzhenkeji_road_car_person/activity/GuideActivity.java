package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.socks.library.KLog;
import com.viewpagerindicator.CirclePageIndicator;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class GuideActivity extends Activity implements ViewPager.OnPageChangeListener {

	@InjectView(R.id.guide_pager)
	ViewPager mGuidePager;
	@InjectView(R.id.guide_indicator)
	CirclePageIndicator mGuideIndicator;
	@InjectView(R.id.guide_btn)
	TextView mGuideBtn;

	private int[] images = new int[]{R.drawable.img_1, R.drawable.img_2, R.drawable.img_3};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		int witth = getWindowManager().getDefaultDisplay().getWidth();
		int height = getWindowManager().getDefaultDisplay().getHeight();
		KLog.e("debug" , + witth + "  height  :::: " + height);
		if(Utils.SPGetBoolean(this, "newUser" , true)){
			Utils.SPPutBoolean(this, "newUser" , false);
		}else if (Utils.SPGetBoolean(this, "isLogin" , false)){

			startActivity(new Intent(GuideActivity.this,HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
			finish();
		}else {

			startActivity(new Intent(this, LoginActivity.class));
			finish();
		}
		setContentView(R.layout.activity_main_activity_guide);
		ButterKnife.inject(this);
		mGuidePager.setAdapter(new MyViewPagerAdapter());
		mGuideIndicator.setViewPager(mGuidePager);
		mGuideIndicator.setCurrentItem(0);
		mGuideIndicator.setOnPageChangeListener(this);


	}

	@Override
	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

	}

	@Override
	public void onPageSelected(int position) {
		if (position == 2) {
			mGuideBtn.setVisibility(View.VISIBLE);
			mGuideBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					if (Utils.SPGetBoolean(GuideActivity.this, "isLogin" , false)){
						startActivity(new Intent(GuideActivity.this,HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
					}else {
						startActivity(new Intent(GuideActivity.this,LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
					}
					GuideActivity.this.finish();
				}
			});
		}else{
			mGuideBtn.setVisibility(View.GONE);
		}

	}

	@Override
	public void onPageScrollStateChanged(int state) {

	}


	class MyViewPagerAdapter extends PagerAdapter {


		@Override
		public int getCount() {
			return images == null ? 0 : images.length;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			ImageView imageView = new ImageView(GuideActivity.this);
			imageView.setImageResource(images[position]);
			imageView.setScaleType(ImageView.ScaleType.FIT_XY);
			container.addView(imageView);
			return imageView;

		}
	}

}
