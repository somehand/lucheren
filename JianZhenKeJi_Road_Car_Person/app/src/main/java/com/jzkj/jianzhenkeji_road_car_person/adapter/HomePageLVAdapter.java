package com.jzkj.jianzhenkeji_road_car_person.adapter;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.MyApplication;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.bean.UserInformationExchange;
import com.nostra13.universalimageloader.core.ImageLoader;

public class HomePageLVAdapter extends QuickAdapter<UserInformationExchange> {

	public HomePageLVAdapter(Context context, int layoutResId) {
		super(context, layoutResId);
	}


	@Override
	protected void convert(BaseAdapterHelper helper, UserInformationExchange bean) {
		LinearLayout ll = helper.getView(R.id.home_page_lv_item_imgll);
		TextView tvTitle = helper.getView(R.id.home_page_lv_item_tvtitle);
		TextView ReadNum = helper.getView(R.id.home_page_lv_item_tvnum);
		ImageView ivImgone = helper.getView(R.id.home_page_lv_item_imgone);
		ImageView ivImgtwo = helper.getView(R.id.home_page_lv_item_imgtwo);
		ImageView ivImgthree = helper.getView(R.id.home_page_lv_item_imgthree);
		tvTitle.setText(bean.getTitle());
		ReadNum.setText(bean.getReadnum());
		ImageLoader.getInstance().displayImage(bean.getImgOne(), ivImgone, MyApplication.options_other);
		ImageLoader.getInstance().displayImage(bean.getImgtwo(), ivImgtwo, MyApplication.options_other);
		ImageLoader.getInstance().displayImage(bean.getImgthree(), ivImgthree, MyApplication.options_other);
	}


}
