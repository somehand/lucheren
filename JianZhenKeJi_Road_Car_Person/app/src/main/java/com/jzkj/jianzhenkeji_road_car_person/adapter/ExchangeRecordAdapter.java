package com.jzkj.jianzhenkeji_road_car_person.adapter;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.MyApplication;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.bean.ExchangeRecordBean;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Administrator on 2016/4/6.
 */
public class ExchangeRecordAdapter extends QuickAdapter<ExchangeRecordBean> {
	public ExchangeRecordAdapter(Context context, int layoutResId) {
		super(context, layoutResId);
	}

	@Override
	protected void convert(BaseAdapterHelper helper, ExchangeRecordBean item) {
		ImageView image = helper.getView(R.id.exchange_record_lv_item_head_img);
		TextView nameTitle = helper.getView(R.id.exchange_record_lv_item_name_title);
		TextView scores = helper.getView(R.id.exchange_record_lv_item_scores_text);
		TextView time = helper.getView(R.id.exchange_record_lv_item_time);
		ImageLoader.getInstance().displayImage(item.getPic(), image, MyApplication.options_other);
		nameTitle.setText(item.getGoodname());
		scores.setText(item.getPrice());
		time.setText(item.getCreatedate());
	}
}
