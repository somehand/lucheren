package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.util.DateTimePickDialogUtil;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Administrator on 2016/5/11 0011.
 */
public class InsureDetailsActivity extends Activity implements View.OnClickListener {

	@InjectView(R.id.headframe_ib)
	ImageButton headframeIb;
	@InjectView(R.id.headframe_title)
	TextView headframeTitle;
	@InjectView(R.id.insure_details_rb1)
	RadioButton insureDetailsRb1;
	@InjectView(R.id.insure_details_rb2)
	RadioButton insureDetailsRb2;
	@InjectView(R.id.name)
	EditText name;
	@InjectView(R.id.text_applicant_papers)
	TextView textApplicantPapers;
	@InjectView(R.id.rl_insure_details_applicant)
	RelativeLayout rlInsureDetailsApplicant;
	@InjectView(R.id.iDCardNum)
	EditText iDCardNum;
	@InjectView(R.id.mail)
	EditText mail;
	@InjectView(R.id.phone)
	EditText phone;
	@InjectView(R.id.ll_insure_details_recognizee)
	LinearLayout llInsureDetailsRecognizee;
	@InjectView(R.id.text_data)
	TextView textData;
	@InjectView(R.id.rl_insure_details_data)
	RelativeLayout rlInsureDetailsData;
	@InjectView(R.id.text_expire)
	TextView textExpire;
	@InjectView(R.id.btn_insure_details_submit)
	Button btnInsureDetailsSubmit;
	@InjectView(R.id.name1)
	TextView name1;
	@InjectView(R.id.text_recognizee_papers)
	TextView textRecognizeePapers;
	@InjectView(R.id.iDCardNum1)
	EditText iDCardNum1;
	@InjectView(R.id.mail1)
	EditText mail1;
	@InjectView(R.id.phone1)
	EditText phone1;
	@InjectView(R.id.rl_insure_details_recognizee)
	RelativeLayout insuredetailsrecognizee;

	private PopupWindow popupWindow;
	private TextView tvPopText1;
	private TextView tvPopText2;
	private TextView tvPopTextLine;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_insure_details);
		ButterKnife.inject(this);
		initView();
		initListener();
		initData();
	}

	private void initData() {
		name.setText(Utils.SPGetString(this, Utils.realName, ""));
		iDCardNum.setText(Utils.SPGetString(this, Utils.idCarNumber, ""));
		phone.setText(Utils.SPGetString(this, Utils.userName, ""));
	}

	private void initListener() {
		headframeIb.setOnClickListener(this);
		insureDetailsRb1.setOnClickListener(this);
		insureDetailsRb2.setOnClickListener(this);
		rlInsureDetailsData.setOnClickListener(this);
		insuredetailsrecognizee.setOnClickListener(this);
		rlInsureDetailsApplicant.setOnClickListener(this);
		btnInsureDetailsSubmit.setOnClickListener(this);
	}

	private void initView() {
		headframeTitle.setText("保险基本信息");
		insureDetailsRb1.setBackgroundColor(getResources().getColor(R.color.common_blue));
		setPopupWindow();
		getcurrentDate();
	}

	DateTimePickDialogUtil dateTimePicKDialog;

	public void getcurrentDate() {
		dateTimePicKDialog = new DateTimePickDialogUtil(InsureDetailsActivity.this, null);
	}

	private int type = 1;
	private int typePop = 1;

	@Override
	public void onClick(View view) {
//		changeRbBackGround();
		switch (view.getId()) {
			case R.id.headframe_ib:
				finish();
				break;
			case R.id.insure_details_rb1:
				insureDetailsRb1.setBackgroundColor(getResources().getColor(R.color.common_blue));
				insureDetailsRb2.setBackgroundColor(getResources().getColor(R.color.white));
				llInsureDetailsRecognizee.setVisibility(View.GONE);
				type = 1;
				break;
			case R.id.insure_details_rb2:
				insureDetailsRb2.setBackgroundColor(getResources().getColor(R.color.common_blue));
				insureDetailsRb1.setBackgroundColor(getResources().getColor(R.color.white));
				llInsureDetailsRecognizee.setVisibility(View.VISIBLE);
				type = 2;
				break;
			case R.id.rl_insure_details_applicant:
				if (popupWindow.isShowing()) {
					popupWindow.dismiss();
				} else {
					popupWindow.showAsDropDown(rlInsureDetailsApplicant, 390, 0);
				}
				typePop = 1;
				break;
			case R.id.rl_insure_details_recognizee:
				if (popupWindow.isShowing()) {
					popupWindow.dismiss();
				} else {
					popupWindow.showAsDropDown(insuredetailsrecognizee, 390, 0);
				}
				typePop = 2;
				break;
			case R.id.rl_insure_details_data:
				dateTimePicKDialog.dateTimePicKDialog(textData, textExpire);
				break;
			case R.id.btn_insure_details_submit:
				payinsure();
				break;
			case R.id.simulation_exam_poptext1:
				if (typePop == 1) {
					textApplicantPapers.setText(tvPopText1.getText().toString());
				}
				if (typePop == 2) {
					textRecognizeePapers.setText(tvPopText1.getText().toString());
				}
				CersType = 1;
				popupWindow.dismiss();
				break;
			case R.id.simulation_exam_poptext2:
				if (typePop == 1) {
					textApplicantPapers.setText(tvPopText2.getText().toString());
				}
				if (typePop == 2) {
					textRecognizeePapers.setText(tvPopText2.getText().toString());
				}
				CersType = 2;
				popupWindow.dismiss();
				break;
			default:
				break;
		}
	}

	private int CersType = 1;
	String userName;
	String idCard;
	String telephone;
	String startime;
	String endtime;
	String usermail;
	String usermail1;

	String otherName;
	String otheridCard;
	String othertelephone;

	//确定投保
	public void payinsure() {
		userName = name.getText().toString().trim();
		idCard = iDCardNum.getText().toString().trim();
		telephone = phone.getText().toString().trim();
		startime = textData.getText().toString();
		endtime = textExpire.getText().toString();
		usermail = mail.getText().toString().trim();

		otherName = name1.getText().toString().trim();
		otheridCard = iDCardNum1.getText().toString().trim();
		othertelephone = phone1.getText().toString().trim();
		usermail1 = mail1.getText().toString().trim();

		if (TextUtils.isEmpty(userName)) {
			Utils.ToastShort(InsureDetailsActivity.this, "姓名不能为空!");
			return;
		}
		if (TextUtils.isEmpty(idCard) || idCard.length() != 18 && idCard.length() != 15) {
			Utils.ToastShort(InsureDetailsActivity.this, "身份证号格式不对");
			return;
		}
		if (TextUtils.isEmpty(telephone)) {
			Utils.ToastShort(InsureDetailsActivity.this, "手机号不能为空!");
			return;
		}
		if (!Utils.isPhoneNumber(telephone)) {
			Utils.ToastShort(InsureDetailsActivity.this, "手机号格式错误！");
			return;
		}
		if (TextUtils.isEmpty(startime)) {
			Utils.ToastShort(InsureDetailsActivity.this, "请选择保险起期!");
			return;
		}
		if (TextUtils.isEmpty(usermail)) {
			Utils.ToastShort(InsureDetailsActivity.this, "邮箱不能为空!");
			return;
		}
		if (!usermail.contains("@") || !usermail.contains(".com")) {
			Utils.ToastShort(InsureDetailsActivity.this, "邮箱格式错误!");
			return;
		}


		RequestParams params = new RequestParams();
		params.put("Name", userName);
		params.put("CersType", CersType);//证件类型
		params.put("CersNum", idCard);//证件号
		params.put("Iphone", telephone);
		params.put("BeginTime", startime);
		params.put("EndTime", endtime);
		params.put("UserId", Utils.SPGetString(InsureDetailsActivity.this, Utils.userId, ""));
		if (type == 1) {
			params.put("Name1", "");
			params.put("CersType1", "");
			params.put("CersNum1", "");
			params.put("Iphone1", "");
			params.put("BeginTime1", "");
			params.put("EndTime1", "");
		} else {
			if (TextUtils.isEmpty(otherName)) {
				Utils.ToastShort(InsureDetailsActivity.this, "被保人姓名不能为空!");
				return;
			}
			if (otherName.equals(userName)) {
				Utils.ToastShort(InsureDetailsActivity.this, "投保人与被保人重复!");
				return;
			}
			if (TextUtils.isEmpty(otheridCard)) {
				Utils.ToastShort(InsureDetailsActivity.this, "被保人身份证号不能为空!");
				return;
			}
			if (otheridCard.equals(idCard)) {
				Utils.ToastShort(InsureDetailsActivity.this, "投保人与被保人身份证号重复!");
				return;
			}
			if (TextUtils.isEmpty(otheridCard) || otheridCard.length() != 18 && otheridCard.length() != 15) {
				Utils.ToastShort(InsureDetailsActivity.this, "身份证号格式不对");
				return;
			}
			if (TextUtils.isEmpty(usermail1)) {
				Utils.ToastShort(InsureDetailsActivity.this, "被保人邮箱不能为空!");
				return;
			}
			if (usermail1.equals(usermail)) {
				Utils.ToastShort(InsureDetailsActivity.this, "投保人与被保人邮箱重复!");
				return;
			}
			if (!usermail1.contains("@") || !usermail1.contains(".com")) {
				Utils.ToastShort(InsureDetailsActivity.this, "邮箱格式错误!");
				return;
			}
			if (TextUtils.isEmpty(othertelephone)) {
				Utils.ToastShort(InsureDetailsActivity.this, "被保人电话不能为空!");
				return;
			}
			if (othertelephone.equals(telephone)) {
				Utils.ToastShort(InsureDetailsActivity.this, "投保人与被保人电话重复!");
				return;
			}
			if (!Utils.isPhoneNumber(othertelephone)) {
				Utils.ToastShort(InsureDetailsActivity.this, "手机号格式错误！");
				return;
			}
			params.put("Name1", otherName);
			params.put("CersType1", CersType);
			params.put("CersNum1", otheridCard);
			params.put("Iphon1e", othertelephone);
			params.put("BeginTime1", startime);
			params.put("EndTime1", endtime);
		}
		params.put("Type", type);
		KLog.e("params", params.toString());
		MyHttp.getInstance(InsureDetailsActivity.this).post(MyHttp.BaoXian, params, new JsonHttpResponseHandler(
		) {

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.json(response.toString());
				try {
					JSONArray array = response.getJSONArray("Result");
					Utils.SPutString(InsureDetailsActivity.this, Utils.payUrl, response.getString("Reason"));
					JSONObject object = array.getJSONObject(0);
					Utils.SPutString(InsureDetailsActivity.this, Utils.orderCode, object.getString("OrderCode"));
//					Utils.SPutString(InsureDetailsActivity.this, Utils.orderCodeId, object.getString("OrderId"));
					Utils.SPutString(InsureDetailsActivity.this, Utils.createtime, object.getString("createtime"));
					KLog.e("OderCode", Utils.SPGetString(InsureDetailsActivity.this, Utils.orderCode, ""));
					KLog.e("Money", object.getString("Money"));
					Intent intent = new Intent(InsureDetailsActivity.this, PayPageActivity.class);
					intent.putExtra("price", object.getString("Money"));
					intent.putExtra("orderName", "学车保险");
					intent.putExtra("tag", 2);
					intent.putExtra("type", 2);
					startActivity(intent);
//					MyAlipayManager.getInstance(InsureDetailsActivity.this, mMyPayCompleteListener).setPayType(0).pay("路车人", "支付", object.getString("Money"));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonArray :");
				KLog.json(Utils.TAG_DEBUG, response.toString());
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);

			}
		});
	}

	/*MyPayCompleteListener mMyPayCompleteListener = new MyPayCompleteListener() {
		@Override
		public void onPayComplete(int payTypeId) {
			Intent intent = new Intent(InsureDetailsActivity.this, PaySuccessActivity.class);
			intent.putExtra("PAY_TYPE", payTypeId);
			intent.putExtra("type", 2);//支付成功后只显示订单编号和支付方式
			startActivity(intent);
			finish();
		}
	};*/

	/**
	 * 设置PopupWindow的方法
	 */
	private void setPopupWindow() {
		LayoutInflater inflater = LayoutInflater.from(this);
		View view = inflater.inflate(R.layout.simulation_exam_popwindow, null);
		tvPopText1 = (TextView) view.findViewById(R.id.simulation_exam_poptext1);
		tvPopText2 = (TextView) view.findViewById(R.id.simulation_exam_poptext2);
		tvPopTextLine = (TextView) view.findViewById(R.id.simulation_exam_line);
		tvPopText1.setText("身份证");
		tvPopText2.setVisibility(View.GONE);
		tvPopTextLine.setVisibility(View.GONE);
		popupWindow = new PopupWindow(view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		popupWindow.setFocusable(true);
		popupWindow.setOutsideTouchable(true);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());

		//监听
		tvPopText1.setOnClickListener(this);
		tvPopText2.setOnClickListener(this);
	}

}
