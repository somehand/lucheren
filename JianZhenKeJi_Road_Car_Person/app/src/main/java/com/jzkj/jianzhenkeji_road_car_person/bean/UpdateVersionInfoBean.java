package com.jzkj.jianzhenkeji_road_car_person.bean;

/**
 * Created by Administrator on 2016/3/25.
 */
public class UpdateVersionInfoBean {
	private String version;
	private String url;
	private String description;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
