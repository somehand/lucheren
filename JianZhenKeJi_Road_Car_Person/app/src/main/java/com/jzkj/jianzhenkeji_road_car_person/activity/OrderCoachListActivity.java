package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.adapter.OrderRoomDetailsLVAdapter;
import com.jzkj.jianzhenkeji_road_car_person.bean.OrderRoomDetailsBean;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.apache.http.ParseException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class OrderCoachListActivity extends Activity {
	private ImageButton ibBack;
	private TextView tvTitle;
	private ListView lv;
	OrderRoomDetailsLVAdapter adapter;
	private ArrayList<OrderRoomDetailsBean> list;
	private ArrayList<Integer> yuyueIdList;
	String date;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_order_room_list);
		init();

		list = new ArrayList<OrderRoomDetailsBean>();
		yuyueIdList = new ArrayList<Integer>();
		/*for (int i = 0; i < 3; i++) {
			OrderRoomDetailsBean roomDetailsBean = new OrderRoomDetailsBean();
			roomDetailsBean.setTime("00:00 - 24:00");
			roomDetailsBean.setClass1("(科目一 科目二)");
			roomDetailsBean.setName("蔡依林");
			roomDetailsBean.setOrderNum(3);
			roomDetailsBean.setTotalNum(5);
			roomDetailsBean.setEnable(false);
			list.add(roomDetailsBean);
		}
		for (int i = 0; i < 5; i++) {
			OrderRoomDetailsBean roomDetailsBean = new OrderRoomDetailsBean();
			roomDetailsBean.setTime("00:00 - 24:00");
			roomDetailsBean.setClass1("(科目一 科目二)");
			roomDetailsBean.setName("蔡依林");
			roomDetailsBean.setOrderNum(3);
			roomDetailsBean.setTotalNum(5);
			roomDetailsBean.setEnable(true);
			list.add(roomDetailsBean);
		}*/


	}


	private void init() {
		date = getIntent().getStringExtra("date");
		ibBack = (ImageButton) findViewById(R.id.headframe_ib);
		tvTitle = (TextView) findViewById(R.id.headframe_title);
		lv = (ListView) findViewById(R.id.order_room_details_lv);
		tvTitle.setText(date);
		adapter = new OrderRoomDetailsLVAdapter(this, R.layout.order_room_details_lv_item);
		adapter.setAdapterType(OrderRoomDetailsLVAdapter.TYPE_COACH);
		lv.setAdapter(adapter);
//		adapter.addAll(list);
		getTimeIds();

		ibBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});
	}


	/**
	 * 获取可预约时段
	 */
	public void getAllTime() {
		RequestParams params = new RequestParams();
		params.put("CoachId", Utils.SPGetString(this, Utils.coachId, ""));
		params.put("Date", date);
		KLog.e(Utils.TAG_DEBUG, params.toString());
		//System.out.println("params : " + params.toString());
		MyHttp.getInstance(this).post(MyHttp.ORDER_COACH_LIST, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonObject :");
				KLog.json(Utils.TAG_DEBUG, response.toString());
				try {
					Utils.ToastShort(OrderCoachListActivity.this,response.getString("Reason"));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonArray :");
				KLog.json(Utils.TAG_DEBUG, response.toString());
				try {
					for (int i = 0; i < response.length(); i++) {
						JSONObject object = response.getJSONObject(i);
						OrderRoomDetailsBean bean = new OrderRoomDetailsBean();
						bean.setName(object.getString("RealName"));
						//bean.setStudentBookingId(object.getInt("StudentBookingsId"));
						bean.setTimeId(object.getInt("CoachBookingsDateId"));
						bean.setClass1(object.getString("SubjectNum"));
						bean.setTime(object.getString("StartStopDate"));
						bean.setOrderNum(object.getInt("yiyuenum"));
						bean.setTotalNum(object.getInt("BookingsNum"));
						if (yuyueIdList.contains(bean.getTimeId())) {
							bean.setEnable(false);
						}
						if (bean.getOrderNum() >= bean.getTotalNum()) {
							bean.setFull(true);
						}
						list.add(bean);
					}
					adapter.addAll(list);

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}

		});
	}

	/**
	 * 获取已预约时段Id
	 */
	public void getTimeIds() {
		RequestParams params = new RequestParams();
		params.put("UserId", Utils.SPGetString(this, Utils.userId, ""));
		params.put("Date", date);
		KLog.e(Utils.TAG_DEBUG, params.toString());
		//System.out.println("params : " + params.toString());
		MyHttp.getInstance(this).post(MyHttp.ORDER_COACH_LIST_TIME, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonObject :");
				KLog.json(Utils.TAG_DEBUG, response.toString());
				getAllTime();
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				KLog.e(Utils.TAG_DEBUG, "jsonArray :");
				KLog.json(Utils.TAG_DEBUG, response.toString());
				try {
					for (int i = 0; i < response.length(); i++) {
						JSONObject object = response.getJSONObject(i);
						yuyueIdList.add(object.getInt("CoachBookingsDateId"));
					}
					getAllTime();

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}

		});
	}
}
