package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class UserAgreementActivity extends Activity {

	@InjectView(R.id.headframe_ib)
	ImageButton mHeadframeIb;
	@InjectView(R.id.headframe_title)
	TextView mHeadframeTitle;
	@InjectView(R.id.is_webview)
	WebView mIsWebview;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_agreement_wb);
		ButterKnife.inject(this);
		mHeadframeTitle.setText("用户协议");
		WebSettings webSettings = mIsWebview.getSettings();
		//设置加载显示的URL
		mIsWebview.loadUrl("http://passport.baidu.com/protocal.html");
		//支持JavaScript
		webSettings.setJavaScriptEnabled(true);
		webSettings.setSupportZoom(true);
		webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
		webSettings.setBuiltInZoomControls(true);//support zoom
		webSettings.setUseWideViewPort(true);// 这个很关键
		webSettings.setLoadWithOverviewMode(true);
		mIsWebview.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
		//设置WebView的切换方式
		mIsWebview.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}
		});

		mHeadframeIb.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});
	}
}
