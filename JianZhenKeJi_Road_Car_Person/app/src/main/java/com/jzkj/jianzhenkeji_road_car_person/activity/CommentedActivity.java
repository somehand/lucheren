package com.jzkj.jianzhenkeji_road_car_person.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.CircleImageView;
import com.jzkj.jianzhenkeji_road_car_person.MyApplication;
import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.bean.MyCoachBean;
import com.jzkj.jianzhenkeji_road_car_person.util.KeyBoardUtils;
import com.jzkj.jianzhenkeji_road_car_person.util.MyHttp;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.socks.library.KLog;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class CommentedActivity extends Activity {
	@InjectView(R.id.commentedradio)
	RadioGroup commentGroup;
	@InjectView(R.id.good_but)
	RadioButton good_but;
	@InjectView(R.id.normal_but)
	RadioButton normal_but;
	@InjectView(R.id.bad_but)
	RadioButton bad_but;
	@InjectView(R.id.view1)
	View view1;
	@InjectView(R.id.view2)
	View view2;
	@InjectView(R.id.view3)
	View view3;
	@InjectView(R.id.view4)
	View view4;
	@InjectView(R.id.view5)
	View view5;
	@InjectView(R.id.headframe_title)
	TextView tvTitle;
	@InjectView(R.id.happy_ratingbar)
	RatingBar happyratingbar;
	@InjectView(R.id.service_ratingbar)
	RatingBar mServiceRatingbar;
	@InjectView(R.id.major_ratingbar)
	RatingBar mMajorRatingbar;
	@InjectView(R.id.commented_etcontent)
	EditText etContent;
	@InjectView(R.id.commented_etget_score)
	EditText etgiveScore;
	@InjectView(R.id.commented_btnsubmit)
	Button btnsubmit;
	@InjectView(R.id.commented_civ_head)
	CircleImageView civHead;
	@InjectView(R.id.headframe_ib)
	ImageButton mHeadframeIb;
	@InjectView(R.id.checkBox)
	CheckBox mCheckBox;
	@InjectView(R.id.allview)
	LinearLayout linearLayout;
	@InjectView(R.id.rl_bottom)
	RelativeLayout rlBottom;

	private int commentState = 0;//0:好评，1：中评，2：差评

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_commented);
		ButterKnife.inject(this);
		tvTitle.setText("发表评价");
		mHeadframeIb.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});
		initviews();
		btnsubmit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				getEvaluateInfo();
			}
		});
		if (getIntent().hasExtra("bean")) {
			tvTitle.setText("评价详情");
			rlBottom.setVisibility(View.GONE);
			MyCoachBean bean = (MyCoachBean) getIntent().getSerializableExtra("bean");
//			ImageLoader.getInstance().displayImage(bean.getStudentLogo(), civHead, MyApplication.options_other);
			etContent.setText(bean.getContents());
			etContent.setFocusable(false);
			happyratingbar.setRating(bean.getMYD());
			happyratingbar.setIsIndicator(true);
			mServiceRatingbar.setRating(bean.getServiceAttitude());
			mServiceRatingbar.setIsIndicator(true);
			mMajorRatingbar.setRating(bean.getMajor());
			mMajorRatingbar.setIsIndicator(true);
		}
	}

	private void getEvaluateInfo() {
		String giveScoreStr = etgiveScore.getText().toString().trim();
		int giveScore = 0;
		int Isanonymous = 0;//0匿名 1正常
		if (mCheckBox.isChecked()) {
			Isanonymous = 0;
		} else {
			Isanonymous = 1;
		}
		if (!TextUtils.isEmpty(giveScoreStr)) {
			giveScore = Integer.parseInt(giveScoreStr);
		}
		if (giveScore >= 0) {
			if (giveScore <= 1000) {
				RequestParams params = new RequestParams();
				params.put("UserId", Utils.SPGetString(CommentedActivity.this, Utils.userId, ""));
				params.put("CoachId", Utils.SPGetString(CommentedActivity.this, Utils.coachId, ""));
				params.put("StudentBookingsId", getIntent().getStringExtra("StudentBookingsId"));
//				params.put("Type", commentState);
				params.put("Contents", etContent.getText().toString().trim());
				params.put("Integral", giveScore);
				params.put("MYD", happyratingbar.getNumStars());//MYD  int  满意度的评分
				params.put("Fwtd", mServiceRatingbar.getNumStars());
				params.put("Zyzs", mMajorRatingbar.getNumStars());
				params.put("Isanonymous", Isanonymous);
				KLog.e("params", params.toString());
				System.out.println(params.toString());
				MyHttp.getInstance(this).post(MyHttp.GET_EVALUATE_INFO, params, new JsonHttpResponseHandler() {
					@Override
					public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
						super.onSuccess(statusCode, headers, response);
						KLog.e("Debug", "JSONObject:");
						KLog.json(response.toString());
						try {
							if (response.getInt("Code") == 1) {
								Utils.ToastShort(CommentedActivity.this, response.getString("Reason"));
//								CommentedActivity.this.finish();
								Intent intent = new Intent(CommentedActivity.this, HomeActivity.class);
//								EventBus.getDefault().post(new CommentedSecurity());
								intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
								startActivity(intent);
								finish();
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}

					}
				});
			} else {
				Utils.ToastShort(CommentedActivity.this, "最多可以赠送1000积分");
			}

		} else {
			Utils.ToastShort(CommentedActivity.this, "积分最少为0");
		}

	}

	ArrayList<View> views = new ArrayList<View>();

	public void initviews() {
		ImageLoader.getInstance().displayImage(Utils.SPGetString(this, Utils.coachLogo, ""), civHead, MyApplication.options_user);
		views.add(view1);
		views.add(view2);
		views.add(view3);
		linearLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				KeyBoardUtils.closeKeybord(etgiveScore, CommentedActivity.this);
			}
		});
		commentGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup radioGroup, int i) {
				if (i == good_but.getId()) {
					commentState = 0;
					setBGcolor(0, 1);
				} else if (i == normal_but.getId()) {
					commentState = 1;
					setBGcolor(1, 2);
				} else {
					commentState = 2;
					setBGcolor(2, 3);
				}
			}
		});
	}

	public void setBGcolor(int i, int k) {
		for (int j = 0; j < views.size(); j++) {
			if (j == i)
				views.get(j).setBackgroundResource(R.color.red_f15549);
			else
				views.get(j).setBackgroundColor(getResources().getColor(R.color.gray_d6));
		}
		if (k == 1) {
			view4.setBackgroundResource(R.color.red_f15549);
			view5.setBackgroundColor(getResources().getColor(R.color.gray_d6));
		} else {
			view4.setBackgroundColor(getResources().getColor(R.color.gray_d6));
			view5.setBackgroundResource(R.color.red_f15549);
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		ButterKnife.reset(this);
	}

	public class CommentedSecurity {

	}
}
