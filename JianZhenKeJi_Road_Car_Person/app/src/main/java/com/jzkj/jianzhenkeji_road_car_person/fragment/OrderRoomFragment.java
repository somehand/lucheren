package com.jzkj.jianzhenkeji_road_car_person.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.jzkj.jianzhenkeji_road_car_person.R;
import com.jzkj.jianzhenkeji_road_car_person.activity.OrderRoomInfoActivity;
import com.jzkj.jianzhenkeji_road_car_person.activity.TipActivity_2;
import com.jzkj.jianzhenkeji_road_car_person.caldroid.CaldroidSampleCustomAdapter;
import com.jzkj.jianzhenkeji_road_car_person.caldroid.CaldroidSampleCustomFragment;
import com.jzkj.jianzhenkeji_road_car_person.util.Utils;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;
import com.roomorama.caldroid.CalendarHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import butterknife.ButterKnife;
import hirondelle.date4j.DateTime;

public class OrderRoomFragment extends Fragment implements View.OnTouchListener{
	private CaldroidFragment caldroidFragment;
	int[] s = {12 , 17, 29};
	HashMap<String, Object> mMap = new HashMap<String, Object>();
	HashMap<DateTime, Integer> mEnableMap = new HashMap<DateTime, Integer>();

	ArrayList<Date> mDatesEnable = new ArrayList<Date>();
	ArrayList<Integer> mDatesDisable = new ArrayList<Integer>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		caldroidFragment = new CaldroidSampleCustomFragment();

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_order_room, null);
		ButterKnife.inject(this, view);
		initView();
		//添加額外的数据 比如哪天预约了
		caldroidFragment.setExtraData(mMap);
		initData();
		caldroidFragment.setCaldroidListener(caldroidListener);
		return view;
	}

	private void initData() {
		Date last;
		Calendar calendar = new GregorianCalendar();
		calendar.add(Calendar.MONTH , 1);
		//System.out.println("month :"+ calendar.get(Calendar.MONTH) + 1);
		int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		//System.out.println("max day :" + maxDay);
		calendar.set(Calendar.DAY_OF_MONTH , maxDay);
		last = calendar.getTime();
		System.out.println("date :"+ last.toString());
		Date date = new Date();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		mDatesEnable.add(calendar.getTime());
		while (calendar.getTime().before(last)){
			calendar.add(Calendar.DAY_OF_MONTH, 1);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			mDatesEnable.add(calendar.getTime());
			//System.out.println(calendar.getTime().toString());
		}
		mDatesEnable.remove(mDatesEnable.size() -1);
		for (Date date1 : mDatesEnable){
			mEnableMap.put(CalendarHelper.convertDateToDateTime(date1) , 1);
		}
		mMap.put("datesEnable" , mEnableMap);
		//mDatesDisable.add(24);
		mMap.put("datesDisable" , mDatesDisable);
		mMap.put("type" , CaldroidSampleCustomAdapter.TYPE_EXAM_COACH);

	}

	private void initView() {
		Bundle bundle = new Bundle();
		Calendar calendar = Calendar.getInstance();
		bundle.putInt(CaldroidFragment.MONTH , calendar.get(Calendar.MONTH) + 1);
		bundle.putInt(caldroidFragment.YEAR, calendar.get(Calendar.YEAR));
		bundle.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
		bundle.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, false);
		bundle.putBoolean(CaldroidFragment.SHOW_NAVIGATION_ARROWS, false);
		bundle.putBoolean(CaldroidFragment.ENABLE_CLICK_ON_DISABLED_DATES, false);
		caldroidFragment.setArguments(bundle);
		FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
		transaction.replace(R.id.order_room_ly , caldroidFragment);
		transaction.commit();
	}

	CaldroidListener caldroidListener = new CaldroidListener() {
		@Override
		public void onSelectDate(Date date, View view) {
			System.out.println("------------");
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(date);
			int day = calendar.get(Calendar.DAY_OF_MONTH);
			//如果属于可预约的日期

			for (int i = 0; i < mDatesEnable.size(); i++) {
				if(mDatesEnable.get(i).toString() .equals(date.toString())){
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//					Toast.makeText(getActivity() , format.format(date) , Toast.LENGTH_SHORT).show();

					if (Utils.SPGetInt(getActivity(),Utils.currentExamRoomId,0) != 0) {
						Intent intent = new Intent(getActivity(), OrderRoomInfoActivity.class);
						intent.putExtra("date" , format.format(date));
						getActivity().startActivity(intent);
					}else {
						Utils.ToastShort(getActivity(),"请先选择考场");
					}
					break;
				}
			}

		}
		@Override
		public void onCaldroidViewCreated() {
			super.onCaldroidViewCreated();
			TextView textView = caldroidFragment.getMonthTitleTextView();
//				RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) textView.getLayoutParams();
//				layoutParams.bottomMargin = -10;
//				textView.setPadding(0,0,0,0);
//				textView.setLayoutParams(layoutParams);
			textView.setTextSize(TypedValue.COMPLEX_UNIT_SP , 14);
			textView.setTextColor(getResources().getColor(R.color.caldroid_darker_gray));

			GridView weekGrid = caldroidFragment.getWeekdayGridView();
			weekGrid.setBackgroundColor(getResources().getColor(R.color.common_orange));

		}
	};

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		ButterKnife.reset(this);
	}

	public boolean onTouch(View v, MotionEvent event) {
		return true;
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if(!hidden){
			if(!Utils.SPGetBoolean(getActivity(),Utils.tips_2_Showed , false) || !Utils.SPGetBoolean(getActivity(),Utils.tips_3_Showed , false)){
				startActivity(new Intent(getActivity() , TipActivity_2.class).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY));
				getActivity().overridePendingTransition(0 , 0);
			}
		}
	}
}
